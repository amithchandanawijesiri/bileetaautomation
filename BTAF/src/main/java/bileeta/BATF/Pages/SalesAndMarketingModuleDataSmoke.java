package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class SalesAndMarketingModuleDataSmoke extends TestBase{

	/* Reading the element locators to variables */
	
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String setProcessDate;
	protected static String lnk_home;
	protected static String sales_Marketing;

	protected static String nav_btn;

	protected static String userNameData;
	protected static String passwordData;

	// stock Adjustment

	protected static String stockAdjustment;
	protected static String newStockAdjustment;
	protected static String descriptionTxtStockAdj;
	protected static String warehouseStockAdj;
	protected static String productLookUpBatchSpecific;
	protected static String productValTxt;
	protected static String selectedProduct;
	protected static String qtyTxtBatchSpecific;

	protected static String addNewBatchFifo;
	protected static String productLookUpBatchFifo;
	protected static String qtyTxtBatchFifo;
	protected static String addNewLot;
	protected static String productLookUpLot;
	protected static String qtyTxtLot;
	protected static String addNewSerialSpecific;
	protected static String productLookUpSerialSpecific;
	protected static String qtyTxtSerialSpecific;
	protected static String addNewSerialBatchSpec;
	protected static String productLookUpSerialBatchSpec;
	protected static String qtyTxtSerialBatchSpec;
	protected static String addNewSerialBthFifo;
	protected static String productLookUpSerialBatchFifo;
	protected static String qtyTxtSerialBatchFifo;
	protected static String addNewSeriFifo;
	protected static String productLookUpSerialFifo;
	protected static String qtyTxtSerialFifo;

	protected static String batchSpecificItem;
	protected static String serialSpecificItem;
	protected static String serialBatchSpecificItem;
	protected static String serialBatchFifoItem;
	protected static String serialFifoItem;
	protected static String lotItem;
	protected static String rangeSerialSpecific;
	protected static String serialNoSerialSpecific;
	protected static String lotNoSerialSpecific;
	protected static String expiryDateSerialSpecific;
	protected static String countText;
	protected static String batchFifoItem;
	protected static String batchNoTxtBatchSpecific;
	protected static String lotNoBatchSpecific;
	protected static String expiryDateBatchSpecific;
	protected static String expiryDateValBatchSpec;
	protected static String nextMonth;
	protected static String manufacDateSerialSpec;
	protected static String manufacDate;
	protected static String batchNoTxtSerialBatchSpecific;

	protected static String lotNoText;

	// Sales Order -> Outbound Shipment -> Sales Invoice -- 1
	protected static String searchBar;

	protected static String navMenuBtn;
	protected static String salesAndMarketing;
	protected static String salesOrder;
	protected static String salesOrderHeader;
	protected static String newSalesOrder;
	protected static String journey1;
	protected static String salesOrderToSalesInvoice;
	protected static String salesOrderToSalesInvoiceSer;
	protected static String customerAccountSearch;
	protected static String customerAccountText;
	protected static String selected_val;
	protected static String currencyDropdown;
	protected static String billingAddressSearch;
	protected static String billingAddressText;
	protected static String shippingAddressText;
	protected static String apply_btn;
	protected static String salesUnit;
	protected static String accountOwner;
	protected static String productLookup;
	protected static String productLookupSerialSpec;
	protected static String productLookupInv;
	protected static String productLookupBatchSpecific;
	protected static String productLookupBatchFIFO;
	protected static String productLookupSerialBatchSpecific;
	protected static String productLookupSerialBatchFifo;
	protected static String productLookupSerialFifo;
	protected static String productTextInv;
	protected static String productTextSer;
	protected static String productTextSerialSpec;
	protected static String searchInv;
	protected static String proSelectedVAl;
	protected static String invSelectedVal;
	protected static String InvenValueSalesOrder;
	protected static String outboundInvenValue;
	protected static String accountNameOutbound;
	protected static String searchButton;
	protected static String unitPriceTxt;
	protected static String unitPriceInve;
	protected static String unitPriceTxtService;
	protected static String unitPriceSerialSpec;
	protected static String unitPriceBatchSpecific;
	protected static String unitPriceBatchFifo;
	protected static String unitPriceSerialBatchSpec;
	protected static String unitPriceSerialBatchFifo;
	protected static String unitPriceSerialFifo;
	protected static String addNewRecord;
	protected static String addNewRecordSerialSpecific;
	protected static String addNewBatchSpecific;
	protected static String addNewSerialBatchFifo;
	protected static String addNewBatchFIFO;
	protected static String addNewSerialFifo;
	protected static String invenSearch;
	protected static String wareHouse;
	protected static String wareHouseBatchSpecific;
	protected static String wareHouseBatchFifo;
	protected static String wareHouseSerialSpecific;
	protected static String wareHouseSerialFifo;
	protected static String wareHouseSerialBatchFifo;
	protected static String wareHouseSerialBatchSpecific;
	protected static String row1;
	protected static String qtyText;
	protected static String qtyTextInve;
	protected static String qtyTextService;
	protected static String qtyTextSerialSpec;
	protected static String qtyTextBatchSpecific;
	protected static String qtyTextBatchFifo;
	protected static String qtyTxtSerialBatchSpecific;
	protected static String qtyTextSerialBatchFifo;
	protected static String qtyTextSerialFifo;
	protected static String bannerTotal1;
	protected static String bottomTotal;
	protected static String serialBatch;
	protected static String productListBtn;
	protected static String productListBtn2;
	protected static String range;
	protected static String count;
	protected static String serialText;
	protected static String updateBtn;
	protected static String serialbatchSpecificListBtn;
	protected static String batchNoTextSerialBatchSpecific;
	protected static String batchSpecificListBtn;
	protected static String backBtn;
	protected static String batchNoText;
	protected static String entutionLink;
	protected static String outboundLink;
	protected static String gotoPageWindow;
	protected static String invoiceLink;
	protected static String checkoutSalesOrder;
	protected static String draft;
	protected static String release;
	protected static String releaseInHeader;
	protected static String header_releasedSalesInvoice;
	protected static String documentVal;
	protected static String serviceProductValOutbound;
	protected static String totalSalesInvoice;
	protected static String checkoutOutbound;
	protected static String outboundDraft;
	protected static String outboundRelease;
	protected static String checkoutInvoice1;
	protected static String taskAndEvent;
	protected static String salesInvoiceBtn;
	protected static String newSalesInvoice;
	protected static String salesInvoiceToOutboundShipment;
	protected static String customerAccountSearchInv;
	protected static String menu;
	protected static String serialWise;
	protected static String serialTextVal;
	protected static String serialTextVal2;
	protected static String batchSpecificBatchNo;
	protected static String serialBatchSpec;
	protected static String serialMenuClose;
	protected static String menuBatchSpecific;
	protected static String menuSerialBatchSpec;

	protected static String serviceOut;
	protected static String serialSpecOut;
	protected static String batchSpeccOut;
	protected static String batchFifoOut;
	protected static String serialBatchSpecOut;
	protected static String serialbatchFifoOut;
	protected static String serialFifoOut;

	// Verify that user is able to create sales order (Sales order to Sales invoice
	// [Service]) ---- 2
	protected static String journey2;
	protected static String customerAccSearchService;
	protected static String customerSearchTxt;
	protected static String customerSelectedValue;
	protected static String currencyService;
	protected static String billingAddressService;
	protected static String billingAddressTxt;
	protected static String shippingAddressTxt;
	protected static String applyBtnService;
	protected static String salesUnitService;
	protected static String productLookupService;
	protected static String productTxtService;
	protected static String selectedValueSearch;
	protected static String unitPriceServicePro;
	protected static String checkoutService;
	protected static String draftService;
	protected static String releaseService;
	protected static String linkSalesInvoiceService;
	protected static String checkoutInvoiceService;
	protected static String serSalesInvoice;
	protected static String draftInvoiceService;
	protected static String releaseInvoiceService;

	// Sales Order -> Outbound Shipment -- 3
	protected static String journey3;
	protected static String customerAccountOutbound;
	protected static String customerAccountTxtOutbound;
	protected static String selectedValAcccountOutbound;
	protected static String currencyOutbound;
	protected static String billingAddressSearchOutbound;
	protected static String billingAddrTxtOutbound;
	protected static String shippingAddressSearchOutbound;
	protected static String shippingAddrTxtOutbound;
	protected static String applyAddress;
	protected static String salesUnitOutbound;
	protected static String productSearchOutboundSer;
	protected static String productSearchTxtOutboundSer;
	protected static String selectedRowSer;
	protected static String unitPriceSer;
	protected static String unitPriceInv;
	protected static String addNewProductOutboundInv;
	protected static String productSearchOutboundInv;
	protected static String productSearchTxtOutboundInv;
	protected static String warehouse;
	protected static String selectedRowInv;
	protected static String checkOutbound;
	protected static String draftOutbound;
	protected static String releaseOutbound;
	protected static String linkSalesInvoice;
	protected static String checkoutInvoice;
	protected static String draftInvoice;
	protected static String linkOutbound;
	// protected static String checkOutbound;
	protected static String draftutbound;
	// protected static String releaseOutbound;

	// Sales Order -> Sales Invoice [Drop Shipment] -- 4
	protected static String journey4;
	protected static String selectedValDropSer;
	protected static String selectedValDropInv;
	protected static String unitPriceInven;
	protected static String actionMenu;
	protected static String delete;
	protected static String documentFlow;
	protected static String convertToPurchaseOrder;
	protected static String vendorLookup;
	protected static String vendorText;
	protected static String vendorSelectedVal;
	protected static String salesOrderDropShipment;

	// Sales Invoice -> Outbound Shipment -- 5
	protected static String salesInvoice;
	protected static String newSalesInv;
	protected static String invoiceOutboundShipment;
	protected static String journeySalesInvoice1;
	protected static String cusAccount;
	protected static String productLookupInvoice;
	protected static String productLookupSerialSpecSI;
	protected static String productLookUpInven;
	protected static String productLookupBatchSpecificSI;
	protected static String productLookupBatchFIFOSI;
	protected static String productLookupSerialBatchSpecificSI;
	protected static String productLookupSerialBatchFifoSI;
	protected static String productLookupSerialFifoSI;
	protected static String serSelectedVAl;
	protected static String unitPriceTxtSISer;
	protected static String unitPriceSerialSpecSI;
	protected static String unitPriceBatchSpecificSI;
	protected static String unitPriceBatchFifoSI;
	protected static String qtyTextSISer;
	protected static String qtyTextSerialSpecSI;
	protected static String qtyTextBatchSpecificSI;
	protected static String qtyTextBatchFifoSI;
	protected static String wareHouseSalesInvoice;
	protected static String wareHouseSerialSpecificSI;
	protected static String wareHouseBatchSpecificSI;
	protected static String wareHouseBatchFifoSI;
	protected static String wareHouseSerialBatchSpecSI;
	protected static String wareHouseSerialBtchFifoSI;
	protected static String wareHouseSerialFifoSI;
	protected static String unitPriceSerialBatchSpecSI;
	protected static String unitPriceSerialBatchFifoSI;
	protected static String unitPriceSerialFifoSI;
	protected static String qtyTextSerialBtchSpecSI;
	protected static String qtyTextSerialBatchFifoSI;
	protected static String qtyTextSerialFifoSI;
	protected static String addNewRecordSerialSpecSI;
	protected static String addNewBatchSpecificSI;
	protected static String addNewBatchFIFOSI;
	protected static String addNewSerBatchSpecificSI;
	protected static String addNewSerialBatchFifoSI;
	protected static String addNewSerialFifoSI;
	protected static String menuSerial;
	protected static String menuClose;
	protected static String checkoutInvoiceOut;
	protected static String menuBatchSpecificSI;
	protected static String menuSerialBatchSpecSI;
	protected static String productListBtnSerialSpecific;
	protected static String productList;
	protected static String warehouseInv;

	// Sales Invoice -- Service -- 6
	protected static String salesInvoiceService;
	protected static String newSalesInvoiceBtn;
	protected static String salesInvoiceSerJourney;
	protected static String cusAccountSearch;
	protected static String productLookupBtn;
	protected static String unitPriceService;
	protected static String warehouseSer;
	protected static String checkoutSalesInvoice;
	protected static String serviceSelected;
	protected static String qtyServiceInvoice;
	protected static String addNewProduct;
	protected static String totalSalesInvoiceSer;
	protected static String productSearchOutbound;
	protected static String productSearchTxt;
	protected static String proSelectedVal;

	// Sales Quatation with a Lead -- 7

	protected static String salesQuatationBtn;
	protected static String newSalesQuatation;
	protected static String journey1Quatation;
	protected static String accountHead;
	protected static String customerAccSearchQuat;
	protected static String selectedValLead;
	protected static String billingAddrSearch;
	protected static String billingAddrTxtQuat;
	protected static String shippingAddrTxtQuat;
	protected static String productSearchQuatation;
	protected static String selectedValQuatInv;
	protected static String selectedValQuatSer;
	protected static String qtyQuatationSer;
	protected static String qtyQuatationInv;
	protected static String unitpriceQuatation;
	protected static String unitpriceQuatationInv;
	protected static String unitpriceBatchSpecQout;
	protected static String unitpriceBatchFifoQout;
	protected static String unitpriceSerialBatchSpecQout;
	protected static String unitpriceSerBatchFifoQout;
	protected static String unitpriceSerialFifoQout;
	protected static String qtyBatchSpecQuot;
	protected static String qtyBatchFifoQuot;
	protected static String qtySerialBatchSpecQuot;
	protected static String qtySerBatchFifoQuot;
	protected static String qtySerialFifoQuot;
	protected static String addNew;
	protected static String addNewBatchSpecificQuot;
	protected static String addNewBatchFifoQuot;
	protected static String addNewSerialBatchSpecQuot;
	protected static String addNewSerBatchFifoQuot;
	protected static String addNewSerialFifoQuot;
	protected static String searchBtnQuata;
	protected static String searchBatchSpecQuot;
	protected static String searchBatchFifoQuot;
	protected static String searchSerialBatchSpecQuot;
	protected static String searchSerBatchFifoQuot;
	protected static String searchSerialFifoQuot;
	protected static String checkoutQuatation;
	protected static String draftQuatation;
	protected static String releaseQuatation;
	protected static String versionBtn;
	protected static String versionTab;
	protected static String tickConfirmVersion;
	protected static String confirmVersion;
	protected static String confirmBtn;
	protected static String linkQuatation;
	protected static String warehouseSerialSpecificQuot;
	protected static String warehouseBatchSpecificQuot;
	protected static String warehouseBatchFifoQuot;
	protected static String warehouseSerBatchSpecQuot;
	protected static String warehouseSerialBatchFifoQuot;
	protected static String warehouseSerialFifoQuot;
	protected static String menuQuot;
	protected static String menuBatchSpec;
	protected static String menuSerialBatchSpecQuot;
	protected static String batchNo;
	protected static String serialBatchNo;
	protected static String checkoutSalesOrderQuata;
	protected static String draftSalesOrderQuata;
	protected static String releaseSalesOrderQuata;
	protected static String linkSalesOrder;
	protected static String checkoutOutboundquata;
	protected static String draftOutboundquata;
	protected static String serialOutboundQuata;
	protected static String sideMenuQuat;
	protected static String batchSpecificQuat;
	protected static String batchNoTxtBatchSpec;
	protected static String TextSerialBatchSpec;
	protected static String serialBatchSpecItem;
	protected static String batchNoTxtQuat;
	protected static String updateBtnQuat;
	protected static String backBtnQuat;
	protected static String serialTextQuat;

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Sales Invoice - Drop Shipment) -- 8
	protected static String journey2Quatation;
	protected static String actionBtn;
	protected static String convert;
	protected static String vendorSearch;
	protected static String vendorTextt;
	protected static String selectedValQuat2;

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Outbound Shipment) ---9

	protected static String journey3Quatation;
	protected static String warehouse3quat;
	protected static String menuOne;
	protected static String menuBatchSpecic;
	protected static String menuSerialBatchSpecQuot3;

	// Verify that user is able to create Sales Return Order (Sales Return Order to
	// Sales Return Invoice) -- 10

	protected static String salesReturnOrder;
	protected static String newSalesReturnOrder;
	protected static String salesReturnJourney1;
	protected static String customerAccSearchReturn;
	protected static String customerAccTxtReturn;
	protected static String selectedValCustomerReturn;
	protected static String currencyReturn;
	protected static String contactPersonReturn;
	protected static String billingAddrReturnSearch;
	protected static String billingAddrTxtReturn;
	protected static String shippingAddrTxtReturn;
	protected static String salesUnitReturn;
	protected static String documentListReturn;
	protected static String fromDateReturn;
	protected static String fromDateSelected;
	protected static String toDateReturn;
	protected static String toDateSelected;
	protected static String searchBtnReturn;
	protected static String tickSalesOrder;
	protected static String applyReturn;
	protected static String checkDocumentListRet2;
	protected static String checkDocumentListRet3;
	protected static String checkDocumentListRet4;
	protected static String checkDocumentListRet5;
	protected static String checkDocumentListRet6;
	protected static String checkDocumentListRet7;
	protected static String returnReason;
	protected static String dispositionCategory;
	protected static String checkoutReturn;
	protected static String draftReturn;
	protected static String releaseReturn;
	protected static String linkReturn;
	protected static String checkoutInboundshipment;
	protected static String draftInbound;
	protected static String serialInbound;

	protected static String listInbound1;
	protected static String listInbound2;
	protected static String listInbound3;
	protected static String listInbound4;
	protected static String listInbound5;
	protected static String listInbound6;
	protected static String checkBox;
	protected static String updateInbound;
	protected static String backInbound;
	protected static String releaseInbound;
	protected static String linkReturnInvoice;
	protected static String checkoutReturnInvoice;
	protected static String draftReturnInvoice;
	protected static String releaseReturnInvoice;

	// Verify that user is able to create Sales Return Order (Sales Return Order to
	// Sales Return Invoice- none deliveerd) --- 11
	protected static String journey2NonDeli;
	protected static String checkoutInboundshipment2;
	protected static String loadedProduct;

	// Verify that user is able to create Sales Return Order (Direct Sales Return
	// Order to Sales Return Invoice) -- 12

	protected static String salesReturnJourney3;
	protected static String productDirect;
	protected static String productText;
	protected static String productSelectedDirect;
	protected static String warehouseDirect;
	protected static String returnReasonDirect;
	protected static String expiryDateDirect;
	protected static String expiryDateDirectVAl;
	protected static String manufacDateDirect;
	protected static String serialNoTextDirect;
	protected static String LotNoTextDirect;
	protected static String expiryDate;
	protected static String expiryDateVal;

	// Verify that user cannot create new sales return invoice directly --- 13
	protected static String salesReturnInvoice;
	protected static String newSalesReturnInvoice;
	protected static String selectedValReturnInvoice;

	// Verify that user is able to create Consolidate Invoice --- 14
	protected static String consolidatedInvoice;
	protected static String newConsolidateInvoice;
	protected static String customerSearchConsolidate;
	protected static String textConsolidate;
	protected static String selectedValConsolidate;
	protected static String docList;
	protected static String checkboxConso;
	protected static String applyConso;
	protected static String searchInvoice;
	protected static String checkboxInvoice;
	protected static String applyInvoice;

	// Create New Account -- 15

	protected static String accountInformtion;
	protected static String newAccount;
	protected static String accountTypeAccInfor;
	protected static String accountNameTypeAccInfor;
	protected static String accountNameText;
	protected static String accountGroupAccInfor;
	protected static String currencyAccInfor;
	protected static String salesUnitAccInfor;
	protected static String accountOwnerAccInfor;
	protected static String contactTitleAccInfor;
	protected static String contactNameAccInfor;
	protected static String phoneAccInfor;
	protected static String mobileAccInfor;
	protected static String emailAccInfor;
	protected static String designationAccInfor;
	protected static String draftAccInfor;
	protected static String releaseAccInfor;
	protected static String editAccInfor;
	protected static String addedRow;
	protected static String addContactIcon;
	protected static String nameTitleAccInfor;
	protected static String nameAccInfor;
	protected static String designationAccInforr;
	protected static String phoneAccInforr;
	protected static String updateAccInfor;
	protected static String UpdateBarAccInfor;
	protected static String inquiryAccInfo;
	protected static String inquiryAboutAccInfor;
	protected static String categoryAccInfor;
	protected static String currentStageAccInfor;
	protected static String updateInquiryAccInfor;

	// Sales Inquiry -- 16
	protected static String salesInquiryBtn;
	protected static String newSalesInqBtn;
	protected static String leadSelectDropdown;
	protected static String leadAccSearchBtn;
	protected static String leadAccSearchText;
	protected static String leadTxtSearchBtn;
	protected static String leadSelectedVal;
	protected static String inquiryAboutTxt;
	protected static String leadSelect;
	protected static String inquiryCategory;
	protected static String inquiryOrigin;
	protected static String salesUnitInq;
	protected static String responsibleEmpSearch;
	protected static String responsibleEmpText;
	protected static String selectedValEmp;
	protected static String priorityDropdownInq;
	protected static String draftInq;
	protected static String addNewContactInq;
	protected static String nameTitleInq;
	protected static String nameTextInq;
	protected static String designationTextInq;
	protected static String phone1TextInq;
	protected static String phone2TextInq;
	protected static String releaseInq;

	// Lead Information -- 17

	protected static String leadInformation;
	protected static String newLead;
	protected static String nameTitleLead;
	protected static String nameLead;
	protected static String mobileLeadText;
	protected static String companyLead;
	protected static String NICLead;
	protected static String designationLead;
	protected static String accountOwnerLead;
	protected static String salesUnitLead;
	protected static String draftLead;
	protected static String releaseLead;
	protected static String updateTopBtnLead;
	protected static String addNewContactLead;
	protected static String nameTagLead;
	protected static String nameNewLead;
	protected static String updateBtnLead;
	protected static String addInquiryLeadBtn;
	protected static String inquiryAboutLead;
	protected static String caegoryLead;
	protected static String currentStageLead;
	protected static String updateLead;
	protected static String updateInquiry;

	// Contact Information -- 18
	protected static String contactInformationBtn;
	protected static String newContactBtn;
	protected static String nameTitleContact;
	protected static String nameTagContact;
	// protected static String nameContact;
	protected static String accountSearchContact;
	protected static String designationContact;
	protected static String salesUnitContact;
	protected static String accountOwnerContact;
	protected static String phoneContact;
	protected static String mobileContact;
	protected static String emailContact;
	protected static String IDContact;
	protected static String birthdateContact;
	protected static String billingContact;
	protected static String countryContact;
	protected static String updateContact;
	protected static String inquiryInformationTabContact;
	protected static String newInquiryBtnContact;
	protected static String inquiryAbout;

	// Verify that User is able to create new Pricing Rule with by one and get one
	// free items -- 19
	protected static String pricingRuleBtn;
	protected static String newPricingRule;
	protected static String newPricingruleookup;
	protected static String ruleCode;
	protected static String ruleDescription;
	protected static String draftPricingRule;
	protected static String choosePricingPolicyBtn;
	protected static String policyHeader;
	protected static String policyRowVal;
	protected static String editPolicy;
	protected static String discountTypePolicy;
	protected static String fromQty;
	protected static String toQty;
	protected static String advanceOpt;
	protected static String sameProductCheckBox;
	protected static String qtyTextBox;
	protected static String updateInEditValues;
	protected static String apply;
	protected static String advanceBtnInPolicy;
	protected static String activateScheduleBtn;
	protected static String daily;
	protected static String everyDay;
	protected static String fromTime;
	protected static String toTime;
	protected static String advanceRelationTab;
	protected static String applyBtnAdvanced;
	protected static String qtyInAdvance;
	protected static String updateRule;
	protected static String activateBtn;
	protected static String confirmationYes;
	protected static String validatorAlert;

	// Verify that User is able to create new Pricing Model with attaching the
	// Pricing rule --- 20

	protected static String pricingModel;
	protected static String newPricingModel;
	protected static String modelCode;
	protected static String description;
	protected static String rulesTab;
	protected static String addPricingRule;
	protected static String searchTxtRule;
	protected static String searchBtnRule;
	protected static String selectedRule;
	protected static String editPeriod;
	protected static String startDate;
	protected static String startDateValue;
	protected static String endDateValue;
	protected static String endDate;
	protected static String applyModel;
	protected static String draftModel;
	protected static String releaseModel;

	// Verify that user is able to created a Sales Order with added Price Model with
	// a Price Rule --- 21
	protected static String salesPriceModel;
	protected static String selectedQty;
	protected static String applyFreeIssues;
	protected static String updateFreeIssues;
	protected static String proSelectedFree;
	protected static String closeAlert;
	protected static String warehouseFree;
	protected static String qtyFree;
	protected static String discountFree;
	protected static String taxGroupFree;

	// Verify that User is able to created a new Sales Exchage order --- 22
	protected static String salesExchangeOrder;
	protected static String newSalesExchangeOrder;
	protected static String startNewJourney;
	protected static String journey1ExchangeToInvoice;
	protected static String customerSearchExchange;
	protected static String documentList;
	protected static String fromDate;
	protected static String dateSelected;
	protected static String toDate;
	protected static String searchDocumentList;
	protected static String checkDocumentList;
	protected static String applyExchange;
	protected static String addNewExchange;
	protected static String searchProOutput;
	protected static String searchProText;
	protected static String selectedOutput;
	protected static String warehouseOutput;
	protected static String product1List;
	protected static String checkFirst;
	protected static String productList2;
	protected static String proDetailsOutput;
	protected static String SerialWiseCost;
	protected static String firstSerial;

	// Free text invoice -- 23
	protected static String freeTextInvoice;
	protected static String newFreeTextInvoice;
	protected static String cusAccSearchFreeText;
	protected static String glAccSearch;
	protected static String glAccText;
	protected static String selectedRowGLAcc;
	protected static String GLAmount;
	protected static String checkoutGLAcc;
	protected static String checkAmount;
	protected static String draftGLAcc;
	protected static String releaseGLAcc;

	// Verify that user cannot proceed the Sales Order with adding full qty as
	// Separate product line in same Sales Order --- 24
	protected static String onHandQty;
	protected static String menuProduct;
	protected static String menuBtn;
	protected static String menuBtnFull;
	protected static String productAvailability;
	protected static String closeAvailabilityMenu;
	protected static String warehouse2;
	protected static String warehouseFullQty;
	protected static String qtyTextFull;
	protected static String unitPriceTxtFull;
	protected static String productListBtn1;
	protected static String closeAdvance;
	protected static String advanceMenu;
	protected static String productGroupConfiguration;
	protected static String newProductGroup;
	protected static String addProductGrp;
	protected static String plusMark;
	protected static String productGrpTxt;
	// protected static String activeCheckBox;
	protected static String updateProductGrp;
	protected static String selectProductGrp;
	protected static String detailsTabProductGrp;
	protected static String activateInventory;
	protected static String activatePurchase;
	protected static String activateSales;
	protected static String confirmYes;
	// protected static String

	// Vehicle Information -- 25
	protected static String vehicleInformation;
	protected static String vehicleInformationHeader;
	protected static String newVehicleInformationBtn;
	protected static String newVehicleInformationHeader;
	protected static String vehicleNoTxt;
	protected static String capacityTxt;
	protected static String capacityMeasure;
	protected static String weightTxt;
	protected static String weightMeasure;
	protected static String draftVehicleInf;
	protected static String vehicleSearchTop;
	protected static String sideMenuValVehicleInf;
	protected static String activeBtnVehicleInf;
	protected static String statusVehicleInf;
	protected static String alertYes;
	protected static String afterYes;

	// Create a Delivery Plan -- 26
	protected static String invenWarehouse;
	protected static String deliveryPlan;
	protected static String deliveryPlanInfo;
	protected static String deliveryPlanHeader;
	protected static String bookNo;
	protected static String vehicleSearch;
	protected static String vehicleSearchText;
	protected static String vehicleSelectedVal;
	protected static String fromDateVehicle;
	protected static String fromDateVal;
	protected static String toDateVehicle;
	protected static String toDateVal;
	protected static String draftVehicle;
	protected static String planningCount;
	protected static String loadingCount;
	protected static String deliveredCount;
	protected static String planningTab;
	protected static String valueVehicle;
	protected static String valueVehicleInLoading;
	protected static String releaseBtnVehicle;
	protected static String countLoading;
	protected static String loadingTab;
	protected static String releaseBtnInLoading;
	protected static String addDeliveryPlan;
	protected static String fromdateDeliveryPlan;
	protected static String fromdateyear;
	protected static String todateDeliveryPlan;
	protected static String searchBtn;
	protected static String selectedValDeliveryPlan;
	protected static String checkboxDeliveryplan;
	protected static String applyDeliveryPlan;
	protected static String salesInvoiceSearchTxt;
	protected static String deliveredBtn;
	protected static String countDelivered;
	protected static String verifyPlan;

	//
	protected static String reports;
	protected static String reportTemplate;
	protected static String productCodeDrop;
	protected static String productCodeText;
	protected static String quickPreview;
	protected static String timePeriod;
	protected static String today;
	protected static String binCardOutboundVal;

	// 5
	protected static String binCardInboundVal;

	// 6
	protected static String administration;
	protected static String journeyConfiguration;
	protected static String salesOrderToSalesInvoiceJourney;
	protected static String autogenerateOutboundShipment;
	protected static String autogenerateSalesInvoice;
	protected static String updateJourney;
	protected static String outboundDoc;
	protected static String salesInvoiceDoc;

	// 7
	protected static String entutionHeader;
	protected static String taskEvent;
	protected static String salesInvoiceTile;
	protected static String valueSalesOrder;

	protected static String updateAndNew;

	// Collection Unit
	protected static String collectionUnit;
	protected static String newCollectionUnit;
	protected static String collectionUnitCode;
	protected static String collectionUnitName;
	protected static String commissionRate;
	protected static String employeeSearch;
	protected static String employeeSearchText;
	protected static String selectedValEmployee;
	protected static String sharingRate;
	protected static String history;
	protected static String activities;
	protected static String historyReleased;
	
	// Sales Order
	protected static String POJourneyWindow;
	protected static String journeyWindowClose;
	protected static String reminders;
	protected static String createReminder;
	protected static String convertInboundCustomerMemo;
	
	//Sales Quotation
	protected static String unitPriceTextQuat;
	protected static String unitPriceAftTextQuat;
	protected static String activityHeader;
	protected static String activityMenuClose;
	protected static String convertToAccount;
	protected static String capacityPlanning;
	protected static String capacityPlanningHeader;
	protected static String capacityPlanningClose;
	protected static String linkMenuClose;
	protected static String duplicate;
	protected static String generateNewVersion;
	protected static String qoutationVersion;
	
	//Sales Return Invoice
	protected static String valReturnInvoice;
	protected static String hold;
	
	//Vehicle Information
	protected static String vehicleInfEditHeader;
	protected static String updateVehicleInf;
	protected static String inactiveBtnVehicleInf;
	protected static String inactivateAlertYes;
	
	//Consolidate Invoice
	protected static String invoiceVal1;
	protected static String invoiceVal2;
	
	//Warranty Profile
	protected static String warrantyProfile;
	protected static String newWarrantyProfile;
	protected static String warrantyProfileCode;
	protected static String warrantyProfileName;
	protected static String statusWarrantyProfile;
	
	//Pricing Rule
	protected static String pricingRule;
	protected static String updatePricingRule;
	protected static String resetPricing;
	
	//Sales Parameter
	protected static String salesParameter;
	protected static String addNewSalesParameter;
	protected static String salesParameterText;
	protected static String updateSales;
	protected static String closeAlertSales;
	
	//Contact Information
	protected static String editContact;
	protected static String contactInformation;
	protected static String contactInformationHeader;
	protected static String contactNameValText;
	protected static String createInHistory;
	protected static String inquiryInformation;
	protected static String newInquiry;
	protected static String salesInquiryHeader;
	protected static String inquiryFirstRow;
	protected static String stage;
	protected static String status;
	
	//Lead Information
	protected static String leadInformationHeader;
	protected static String deleteRule;
	protected static String salesUnitNavigation;
	
	protected static String newSalesUnit;
	protected static String salesUnitCode;
	protected static String unitLeaderSearch;
	protected static String unitLeaderText;
	protected static String salesUnitName;
	protected static String unitLeaderSelected;
	protected static String sharingRateSales;
	
	protected static String warehouseInformation;
	protected static String newWarehouse;
	protected static String warehouseCode;
	protected static String warehouseName;
	protected static String businessUnit;
	
	protected static String txt_ProductA;
	protected static String txt_ProductQtyA;
	protected static String btn_draft;
	protected static String header_releasePurchaseInvoice;
	protected static String header_releaseSalesInvoice;
	protected static String lnk_resultInfoOnLookupInforReplace;
	protected static String header_draftedSalesReturnOrder;
	protected static String header_draftedSalesOrder;

	/* Reading the Data to variables */

	// stock adjustment
	protected static String qtyVal;
	protected static String descrptionValStockAdj;
	protected static String countTextVal;

	// 1
	protected static String customerAccVal;
	protected static String billingAddrVal;
	protected static String shippingAddrVal;
	protected static String salesUnitVal;
	protected static String productTextValSer;
	protected static String productTextValInv;
	protected static String unitPriceVal;
	protected static String unitPriceValSerialSpec;
	protected static String unitPriceValBatchSpec;
	protected static String unitPriceValInv;
	protected static String quantityVal;
	protected static String quantityValInv;
	protected static String quantityValSerialSpec;
	protected static String serialCountVal;
	protected static String serialNo;
	protected static String warehouseVal1;

	protected static String batchNoValBatchSpecific;
	protected static String serialBatchSpecificVal;

	// 2
	protected static String customerAccServiceVal;
	protected static String billingAddrSerVal;
	protected static String shippingAddrSerVal;
	protected static String productSerVal;
	protected static String unitPriceValue;

	// 3
	protected static String customerAccOutboundVal;
	protected static String billingOutboundVal;
	protected static String shippingOutboundVal;
	protected static String serviceOutboundVal;
	protected static String invOutboundVal;
	protected static String warehouseVal3;
	protected static String unitpriceSerOutboundVal;
	protected static String unitpriceInvOutboundVal;

	// 4
	protected static String dropshipmentSerVal;
	protected static String dropshipmentInvVal;
	protected static String vendorVal;

	// 5
	protected static String serialNumVal;
	protected static String warehouseVal;

	// 6
	protected static String unitPriceSerVal;
	protected static String qtyValSalesInvoiceSer;

	// 7
	protected static String customerAccValQuat;
	protected static String unitPriceQuatVal;
	protected static String qtyValQout;
	protected static String qtyQuatationSerVal;
	protected static String qtyQuatationInvVal;
	protected static String billingAddrQuatVal;
	protected static String shippingAddrQuatVal;
	protected static String serialNoVal;
	protected static String warehouseValQuot;

	// 8
	protected static String serialNo2;
	protected static String vendorVal2;

	// 9
	protected static String serialNo3;

	// 10
	protected static String customerAccReturnVal;

	// 12
	protected static String productValDirect;
	protected static String warehouseDirectVal;
	protected static String returnReasonVal;

	// Account Information -- 15
	protected static String accountNameVal;
	protected static String contactNameVal;
	protected static String mobileNoVal;
	protected static String phoneNoVal;
	protected static String emailVal;
	protected static String designationVal;
	protected static String inquiryAboutVal;

	// Sales Inquiry -- 16
	protected static String leadAccSearchVal;
	protected static String salesInquiryAboutVal;
	protected static String responsibleEmpVal;
	protected static String nameTextSalesInqVal;
	protected static String salesInqDesigVal;
	protected static String salesInqPhone1Val;
	protected static String salesInqPhone2Val;

	// Lead Information -- 17
	protected static String nameTextLeadVal;
	protected static String mobileTexLeadtVal;
	protected static String companyLeadVal;
	protected static String NICLeadVAl;
	protected static String designationLeadVal;
	protected static String phoneLeadVal;
	protected static String companyUpdateVal;
	protected static String NameUpdateVal;
	protected static String inquiryUpdateAboutVal;

	// Contatc Information --- 18
	protected static String nameContactVal;
	protected static String designationContactVal;
	protected static String phoneContatcVal;
	protected static String mobileContactVal;
	protected static String emailContatctVal;
	protected static String inquiryAboutValu;

	// Pricing Rule -- 19
	protected static String ruleCodeVal;
	protected static String ruleDescriptionVal;
	protected static String fromVal;
	protected static String toVal;
	protected static String qtyEditVal;
	protected static String fromTimeVal;
	protected static String toTimeVal;

	// Pricing Model -- 20
	protected static String modelCodeVal;
	protected static String modelDescriptionVal;
	protected static String pricingRuleVal;
	
	//21
	protected static String selectedQtyVal;

	// 22
	protected static String outputProval;
	protected static String warehouseOutputPro;

	// --- 23
	protected static String glAccVal;
	protected static String GLAmountVal;

	// --24
	protected static String productValInv;
	protected static String productTextVal2;

	// Vehicle Information -- 25
	protected static String vehicleNoTxtVal;
	protected static String capacityTxtVal;
	protected static String weightTxtVal;

	// --26
	protected static String vehicleSearchVal;

	//
	protected static String accGrpVal;
	protected static String currencyVal;
	protected static String leadDaysVal;

	// s4
	protected static String ProductCodeVal;

	//
	protected static String listInbound;
	protected static String checkDocumentListRet;

	// Account Group Config
	protected static String AccountGroupConfiguration;
	protected static String newAccountGroupConfiguration;
	protected static String addNewAccGrp;
	protected static String addNewRecordAccGrp;
	protected static String accGrpText;
	protected static String updateAccGrp;
	protected static String selectAccGroup;
	protected static String activeCheckBox;
	protected static String currencyAccGroup;
	protected static String salesUnitAccGrp;
	protected static String detailsAccGrp;
	protected static String leadDays;
	protected static String draftNew;
	protected static String edit;
	protected static String update;

	// Account Information
	protected static String mobileAccInf;
	protected static String annualRevenue;
	protected static String detailsTab;
	protected static String annualRevenueVal;
	protected static String newMobileNo;

	// Collection Unit
	protected static String collectionNameVal;
	protected static String commissionRateVal;
	protected static String employeeVal;
	protected static String sharingRateVal;
	protected static String commisionRateEditedVal;
	protected static String sharingRateNewVal;
	
	//Sales Target
	
	//
	protected static String unitPriceQuatValEdit;
	protected static String weightTxtNewVal;
	
	//Warranty Profile
	//protected static String profileCodeVal;
	protected static String profileNameVal;
	protected static String profileNameNew;
	
	//Pricing Rule
	protected static String ruleDescriptionNewVal;
	
	//Contact Name
	protected static String contactNameNewVal;
	
	//Lead Information
	protected static String nameTextLeadNewVal;
	
	protected static String descriptionVal;
	
	protected static String unitLeaderVal;
	protected static String salesUnitNameVal;
	protected static String warehouseNameVal;
	protected static String businessUnitVal;

	// Calling the constructor
	public static void readElementlocators() throws Exception {

		siteLogo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "site logo");
		txt_username = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "user name");
		txt_password = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "password");
		btn_login = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "login button");
		setProcessDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "set Process Date");
		lnk_home = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "home link");
		nav_btn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "nav bar");
		sales_Marketing = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales and marketing");

		searchBar = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Bar");

		// Stock Adjustment

		stockAdjustment = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "stock Adjustment");
		newStockAdjustment = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Stock Adjustment");
		descriptionTxtStockAdj = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "description Txt Stock Adj");
		warehouseStockAdj = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Stock Adj");
		productLookUpBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product LookUp Batch Specific");
		productValTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Val Txt");
		selectedProduct = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Product");
		qtyTxtBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Batch Specific");

		addNewBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Batch Fifo");
		productLookUpBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product LookUp Batch Fifo");
		qtyTxtBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Batch Fifo");
		addNewLot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Lot");
		productLookUpLot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product LookUp Lot");
		qtyTxtLot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Lot");
		addNewSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Specific");
		productLookUpSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product LookUp Serial Specific");
		qtyTxtSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Serial Specific");
		addNewSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Batch Spec");
		productLookUpSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product LookUp Serial Batch Spec");
		qtyTxtSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Serial Batch Spec");
		addNewSerialBthFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Bth Fifo");
		productLookUpSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product LookUp Serial Batch Fifo");
		qtyTxtSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Serial Batch Fifo");
		addNewSeriFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Seri Fifo");
		productLookUpSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product LookUp Serial Fifo");
		qtyTxtSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Txt Serial Fifo");

		batchSpecificItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Specific Item");
		serialSpecificItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Specific Item");
		serialBatchSpecificItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch Specific Item");
		serialBatchFifoItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch Fifo Item");
		serialFifoItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Fifo Item");
		lotItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lot Item");
		rangeSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "range Serial Specific");
		serialNoSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial No Serial Specific");
		lotNoSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lotNo Serial Specific");
		expiryDateSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date Serial Specific");
		countText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "count Text");
		batchFifoItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Fifo Item");
		batchNoTxtBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch NoTxt Batch Specific");
		lotNoBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lotNo Batch Specific");
		expiryDateBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date Batch Specific");
		expiryDateValBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date Val Batch Spec");
		nextMonth = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "next Month");
		manufacDateSerialSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "manufac Date Serial Spec");
		manufacDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "manufac Date");
		batchNoTxtSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"batch No Txt Serial Batch Specific");

		lotNoText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lot No Text");

		navMenuBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "nav Menu Btn");
		salesAndMarketing = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales And Marketing");
		salesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Order Btn");
		salesOrderHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Order Header");
		newSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Order");
		journey1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey 1");
		salesOrderToSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Order To Sales Invoice");

		customerAccountSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Account Search");
		customerAccountText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Account Text");
		selected_val = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "cus Selected Val");
		currencyDropdown = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "currency Dropdown");
		billingAddressSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Address Search");
		billingAddressText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Address Text");
		shippingAddressText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "shipping Address Text");

		apply_btn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Btn");
		salesUnit = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Dropdown");
		accountOwner = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Owner Dropdown");

		productLookup = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup");
		productLookupSerialSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Serial Spec");
		productLookupInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Inv");

		productLookupBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Batch Specific");
		productLookupBatchFIFO = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Batch FIFO");
		productLookupSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Serial Batch Specific");
		productLookupSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Serial Batch Fifo");
		productLookupSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Serial Fifo");
		productTextSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Text Ser");
		productTextSerialSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Text Serial Spec");
		productTextInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Text Inv");

		searchInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inv Search");
		searchButton = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Button");
		proSelectedVAl = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pro Selected Val");
		invSelectedVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inv Selected Val");
		InvenValueSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Inven Value Sales Order");
		outboundInvenValue = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "outbound Inven Value");
		accountNameOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Name Outbound");
		addNewRecord = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Record");
		addNewBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Batch Specific");
		addNewRecordSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"add New Record Serial Specific");
		addNewSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Batch Fifo");
		addNewBatchFIFO = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "addNew Batch FIFO");
		addNewSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "addNew Serial Fifo");
		invenSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inv Search");

		unitPriceTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price");
		unitPriceInve = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Inve");
		unitPriceTxtService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Txt Service");
		unitPriceBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Batch Specific");
		unitPriceSerialSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Serial Spec");
		unitPriceBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Batch Fifo");
		unitPriceSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Serial Batch Spec");
		unitPriceSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Serial Batch Fifo");
		unitPriceSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Serial Fifo");
		wareHouse = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ware House");
		wareHouseBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Batch Specific");
		wareHouseBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse BatchFifo");
		wareHouseSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Serial Specific");
		wareHouseSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Serial Fifo");
		wareHouseSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Serial Batch Fifo");
		wareHouseSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"wareHouse Serial Batch Specific");
		qtyText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "quantity");
		qtyTextInve = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "quantity Inve");
		qtyTextService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Service");
		qtyTextSerialSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Spec");
		qtyTextBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Batch Specific");
		qtyTextBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Batch Fifo");
		qtyTxtSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"qty Txt Serial Batch Specific");
		qtyTextSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Batch Fifo");
		qtyTextSerialFifo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Fifo");
		bannerTotal1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "banner Total");
		bottomTotal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "bottom Total");
		serialBatch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch");
		productListBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product List Btn");
		productListBtn2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product List Btn 2");
		range = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "range Check");
		count = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Count");
		serialText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Text");
		backBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "back Btn");
		updateBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Btn");
		serialbatchSpecificListBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"serialbatch Specific List Btn");
		batchNoTextSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"batch No Text Serial Batch Specific");
		batchSpecificListBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Specific List Btn");

		checkoutSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Sales Order");
		draft = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft");
		release = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release");
		releaseInHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release In Header");
		header_releasedSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "header_releasedSalesInvoice");
		documentVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "document Val");
		serviceProductValOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "service Product Val Outbound");
		totalSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "total Sales Invoice");

		outboundLink = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "outboundshipment Link");
		gotoPageWindow = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "goto Page Window");
		checkoutOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Outbound");
		outboundDraft = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "outbound Draft");
		outboundRelease = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "outbound Release");
		checkoutInvoice1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Invoice");
		entutionLink = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "entution Link");
		batchNoText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch No Text");

		menu = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Display");
		serialWise = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Wise");
		serialTextVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Text Val");
		serialTextVal2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Text Val 2");
		batchSpecificBatchNo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Specific Batch No");
		serialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch Spec");
		serialMenuClose = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Menu Close");
		menuBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Batch Specific");
		menuSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Serial Batch Spec");

		serviceOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "service Out");
		serialSpecOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Spec Out");
		batchSpeccOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Specc Out");
		batchFifoOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Fifo Out");
		serialBatchSpecOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch Spec Out");
		serialbatchFifoOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial batch Fifo Out");
		serialFifoOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Fifo Out");

		// Verify that user is able to create sales order (Sales order to Sales invoice
		// [Service])--- 002

		journey2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey 2");
		customerAccSearchService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Acc Search Service");
		customerSearchTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Search Txt");
		customerSelectedValue = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Selected Value");
		currencyService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "currency Service");
		billingAddressService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Address Service");
		billingAddressTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Address Txt");
		shippingAddressTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "shipping Address Txt");
		applyBtnService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Btn Service");
		salesUnitService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Service");
		productLookupService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Service");
		productTxtService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Txt Service");
		selectedValueSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Value Search");
		unitPriceServicePro = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unitPrice Service Pro");
		checkoutService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Service");
		draftService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Service");
		releaseService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Service");
		linkSalesInvoiceService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "linkSales Invoice Service");
		checkoutInvoiceService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Invoice Service");
		serSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ser Sales Invoice");
		draftInvoiceService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Invoice Service");
		releaseInvoiceService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Invoice Service");

		// Verify that user is able to create sales order (Sales order to Outbound
		// shipment) --3

		journey3 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey 3");
		customerAccountOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Account Outbound");
		customerAccountTxtOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"customer Account TxtOutbound");
		selectedValAcccountOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"selected Val Acccount Outbound");
		currencyOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "currency Outbound");
		billingAddressSearchOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"billing Address Search Outbound");
		billingAddrTxtOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Addr Txt Outbound");
		shippingAddressSearchOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"shipping Address Search Outbound");
		shippingAddrTxtOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "shipping Addr Txt Outbound");
		applyAddress = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Address");
		salesUnitOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Outbound");
		productSearchOutboundSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Search Outbound Ser");
		productSearchTxtOutboundSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Search Txt Outbound Ser");
		selectedRowSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Row Ser");
		unitPriceSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Ser");
		unitPriceInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Inv");
		addNewProductOutboundInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Product Outbound Inv");
		productSearchOutboundInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Search Outbound Inv");
		productSearchTxtOutboundInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Search Txt Outbound Inv");
		warehouse = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse");
		selectedRowInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Row Inv");
		checkOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Outbound");
		draftOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Outbound");
		releaseOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Outbound");
		linkSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Sales Invoice");
		checkoutInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Invoice");
		draftInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Invoice");
		linkOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Outbound");
		// protected static String checkOutbound;
		// draftOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
		// "draftutbound");
		// protected static String releaseOutbound;

		// Verify that user is able to create sales order (Sales order to Sales Invoice
		// - Drop Shipment) ---4

		journey4 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey 4");
		selectedValDropSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Drop Ser");
		selectedValDropInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Drop Inv");
		unitPriceInven = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Inven");
		vendorLookup = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vendor Lookup");
		vendorText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vendor Text");
		vendorSelectedVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vendor Selected Val");
		// salesOrderDropShipment;

		actionMenu = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "action Menu");
		delete = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delete");
		documentFlow = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "document Flow");
		convertToPurchaseOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "convert To Purchase Order");

		// Verify that user is able to create sales Invoice (Sales invoice to Outbound
		// shipment) --- 5

		salesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Invoice");
		newSalesInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Inv");
		journeySalesInvoice1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "invoice OutboundShipment");
		cusAccount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "cus Account");
		productLookupInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Invoice");
		productLookupSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Serial Spec SI");
		productLookUpInven = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product LookUp Inven");
		productLookupBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Batch Specific SI");
		productLookupBatchFIFOSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Batch FIFO SI");
		productLookupSerialBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Serial Batch Specific SI");
		productLookupSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Serial Batch Fifo SI");
		productLookupSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product Lookup Serial Fifo SI");
		serSelectedVAl = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ser Selected Val");
		unitPriceTxtSISer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Txt SI Ser");
		unitPriceSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Serial Spec SI");
		unitPriceBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Batch Specific SI");
		unitPriceBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Batch Fifo SI");
		unitPriceSerialBatchSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"unit Price Serial Batch Spec SI");
		unitPriceSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"unit Price Serial Batch Fifo SI");
		unitPriceSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Serial Fifo SI");
		qtyTextSISer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text SI Ser");
		qtyTextSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Spec SI");
		qtyTextBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Batch Specific SI");
		qtyTextBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Batch Fifo SI");
		qtyTextSerialBtchSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Btch Spec SI");
		qtyTextSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Batch Fifo SI");
		qtyTextSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Serial Fifo SI");
		wareHouseSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Sales Invoice");
		wareHouseSerialSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Serial Specific SI");
		wareHouseBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Batch Specific SI");
		wareHouseBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Batch Fifo SI");
		wareHouseSerialBatchSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"wareHouse Serial Batch Spec SI");
		wareHouseSerialBtchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"wareHouse Serial Btch Fifo SI");
		wareHouseSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "wareHouse Serial Fifo SI");
		addNewRecordSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Record Serial Spec SI");
		addNewBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Batch Specific SI");
		addNewBatchFIFOSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Batch FIFO SI");
		addNewSerBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Ser Batch Specific SI");
		addNewSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Batch Fifo SI");
		addNewSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Fifo SI");
		menuSerial = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Serial");
		menuClose = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Close");
		checkoutInvoiceOut = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Invoice Out");
		menuBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Batch Specific SI");
		menuSerialBatchSpecSI = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Serial Batch Spec SI");
		productListBtnSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"product List Btn Serial Specific");
		productList = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product List");
		warehouseInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Inv");

		// Verify that user is able to create sales Invoice (Service) --6
		salesInvoiceService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Invoice Btn");
		newSalesInvoiceBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Invoicee");
		salesInvoiceSerJourney = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Invoice Service Journey");
		cusAccountSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Account Search Btn");
		productLookupBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Lookup Btn");
		unitPriceService = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Service");
		warehouseSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ware House Ser");
		checkoutSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Sales Invoice");
		serviceSelected = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "service Selected");
		qtyServiceInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Service Invoice");

		// Verify that user is able to create sales Quotation with a lead(Sales
		// Quotation to Sales Invoice) --- 7

		salesQuatationBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Quatation Btn");
		newSalesQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Quatation");
		journey1Quatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey1 Quatation");
		accountHead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Head");
		customerAccSearchQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Acc Search Quat");
		selectedValLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Lead");
		billingAddrSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Addr Search");
		billingAddrTxtQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Addr Txt Quat");
		shippingAddrTxtQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "shipping Addr Txt Quat");
		selectedValQuatInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Quat Inv");
		selectedValQuatSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Quat Ser");
		productSearchQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Search Quatation");
		qtyQuatationSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Quatation Ser");
		qtyQuatationInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Quatation Inv");
		unitpriceQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unitprice Quatation");
		unitpriceQuatationInv = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unitprice Quatation Inv");
		unitpriceBatchSpecQout = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unitprice Batch Spec Qout");
		unitpriceBatchFifoQout = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unitprice Batch Fifo Qout");
		unitpriceSerialBatchSpecQout = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"unitprice Serial Batch Spec Qout");
		unitpriceSerBatchFifoQout = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"unitprice Ser Batch Fifo Qout");
		unitpriceSerialFifoQout = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unitprice Serial Fifo Qout");
		qtyBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Batch Spec Quot");
		qtyBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Batch Fifo Quot");
		qtySerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Serial Batch Spec Quot");
		qtySerBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Ser Batch Fifo Quot");
		qtySerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Serial Fifo Quot");
		addNew = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New");
		addNewBatchSpecificQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Batch Specific Quot");
		addNewBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add NewBatch Fifo Quot");
		addNewSerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"add New Serial Batch Spec Quot");
		addNewSerBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Ser Batch Fifo Quot");
		addNewSerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Serial Fifo Quot");
		searchBtnQuata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Btn Quata");
		searchBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Batch Spec Quot");
		searchBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Batch Fifo Quot");
		searchSerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"search Serial Batch Spec Quot");
		searchSerBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Ser Batch Fifo Quot");
		searchSerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Serial Fifo Quot");
		checkoutQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Quatation");
		draftQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Quatation");
		releaseQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Quatation");
		versionBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "version Btn");
		versionTab = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "version Tab");
		tickConfirmVersion = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "tick Confirm Version");
		confirmVersion = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "confirm Version");
		confirmBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "confirm Btn");
		linkQuatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Quatation");
		warehouseSerialSpecificQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"warehouse Serial Specific Quot");
		warehouseBatchSpecificQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"warehouse Batch Specific Quot");
		warehouseBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Batch Fifo Quot");
		warehouseSerBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"warehouse Ser Batch Spec Quot");
		warehouseSerialBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"warehouse Serial Batch Fifo Quot");
		warehouseSerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Serial Fifo Quot");
		menuQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Quot");
		menuBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Batch Spec");
		menuSerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Serial Batch Spec Quot");
		batchNo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch No");
		serialBatchNo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch No");
		checkoutSalesOrderQuata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout SalesOrder Quata");
		draftSalesOrderQuata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft SalesOrder Quata");
		releaseSalesOrderQuata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release SalesOrder Quata");
		linkSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Sales Order");
		checkoutOutboundquata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Outbound Quata");
		draftOutboundquata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Outbound quata");
		serialOutboundQuata = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Outbound Quata");
		sideMenuQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "side Menu Quat");
		batchSpecificQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch Specific Quat");
		batchNoTxtBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch No Txt Batch Spec");
		TextSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Text Serial Batch Spec");
		serialBatchSpecItem = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Batch Spec Item");
		batchNoTxtQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "batch No Txt Quat");
		updateBtnQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Btn Quat");
		backBtnQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "back Btn Quat");
		serialTextQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Text Quat");

		// Verify that user is able to create sales Quotation with a lead
		// (Sales Quotation to Sales Invoice - Drop Shipment) --- 8

		journey2Quatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey2 Quatation");
		actionBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "action Btn");
		convert = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "convert");
		vendorSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vendor Search");
		vendorTextt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vendor Textt");
		selectedValQuat2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Quat2");

		// Verify that user is able to create sales Quotation with a lead(Sales
		// Quotation to Outbound Shipment) --9
		journey3Quatation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey3 Quatation");
		warehouse3quat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse3 quat");
		menuOne = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu One");
		menuBatchSpecic = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Batch Specic");
		menuSerialBatchSpecQuot3 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Serial Batch Spec Quot3");

		// Verify that user is able to create Sales Return Order (Sales Return Order to
		// Sales Return Invoice) --- 10

		salesReturnOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Return Order");
		newSalesReturnOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Return Order");
		salesReturnJourney1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Return Journey1");
		customerAccSearchReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Acc Search Return");
		customerAccTxtReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Acc Txt Return");
		selectedValCustomerReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Customer Return");
		currencyReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "currency Return");
		contactPersonReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Person Return");
		billingAddrReturnSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Addr Return Search");
		billingAddrTxtReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Addr Txt Return");
		shippingAddrTxtReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "shipping Addr Txt Return");
		salesUnitReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Return");
		documentListReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "document List Return");
		fromDateReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "from Date Return");
		fromDateSelected = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "from Date Selected");
		toDateReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "to Date Return");
		toDateSelected = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "to Date Selected");
		searchBtnReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Btn Return");
		tickSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "tick Sales Order");
		applyReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Return");
		checkDocumentListRet2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret2");
		checkDocumentListRet3 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret3");
		checkDocumentListRet4 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret4");
		checkDocumentListRet5 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret5");
		checkDocumentListRet6 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret6");
		checkDocumentListRet7 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret7");
		returnReason = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "return Reason");
		dispositionCategory = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "disposition Category");
		checkoutReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Return");
		draftReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Return");
		releaseReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Return");
		linkReturn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Return");
		checkoutInboundshipment = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Inboundshipment");
		draftInbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Inbound");
		serialInbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial Inbound");
		listInbound1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound1");
		listInbound2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound2");
		listInbound3 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound3");
		listInbound4 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound4");
		listInbound5 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound5");
		listInbound6 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound6");
		checkBox = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Box");
		updateInbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Inbound");
		backInbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "back Inbound");
		releaseInbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Inbound");
		linkReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Return Invoice");
		checkoutReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Return Invoice");
		draftReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Return Invoice");
		releaseReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Return Invoice");

		// Verify that user is able to create Sales Return Order (Sales Return Order to
		// Sales Return Invoice- none deliveerd) --- 11

		journey2NonDeli = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey2 Non Deli");
		checkoutInboundshipment2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout Inboundshipment 2");
		loadedProduct = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "loaded Product");

		// 12
		// salesReturnJourney3 = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
		// "sales Return Journey3");
		salesReturnJourney3 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Return Journey3");
		productDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Direct");
		productText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Text");
		productSelectedDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Selected Direct");
		warehouseDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Direct");
		returnReasonDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "return Reason Direct");
		expiryDateDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date Direct");
		expiryDateDirectVAl = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date Direct Val");
		manufacDateDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "manufac Date Direct");
		serialNoTextDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "serial No Text Direct");
		LotNoTextDirect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Lot No Text Direct");
		expiryDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date");
		expiryDateVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "expiry Date Val");

		// Verify that user cannot create new sales return invoice directly --- 13
		salesReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Return Invoice");
		newSalesReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Return Invoice");
		selectedValReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Return Invoice");

		// Verify that user is able to create Consolidate Invoice --- 14

		consolidatedInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "consolidated Invoice");
		newConsolidateInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Consolidate Invoice");
		customerSearchConsolidate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Search Consolidate");
		textConsolidate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "text Consolidate");
		selectedValConsolidate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Consolidate");
		docList = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "doc List");
		checkboxConso = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkbox Conso");
		applyConso = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Conso");
		searchInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Invoice");
		checkboxInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkbox Invoice");
		applyInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Invoice");

		// Verify that User is able to create new Account -- 15
		accountInformtion = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Information");
		newAccount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Account Acc Infor");
		accountTypeAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Type Acc Infor");
		accountNameTypeAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Name Type Acc Infor");
		accountNameText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Name Text");
		accountGroupAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Group Acc Infor");
		currencyAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "currency Acc Infor");
		salesUnitAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Acc Infor");
		accountOwnerAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Owner Acc Infor");
		contactTitleAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Title Acc Infor");
		contactNameAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Name Acc Infor");
		phoneAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "phone Acc Infor");
		mobileAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "mobile Acc Infor");
		emailAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "email Acc Infor");
		designationAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "designation Acc Infor");
		draftAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Acc Infor");
		releaseAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Acc Infor");
		editAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "edit Acc Infor");
		addedRow = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "added Row");
		addContactIcon = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add Contact Icon");
		nameTitleAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Title Acc Infor");
		nameAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Acc Infor");
		designationAccInforr = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "designation Acc Inforr");

		phoneAccInforr = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "phone Acc Inforr");

		updateAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Acc Infor");
		UpdateBarAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Update Bar Acc Infor");
		inquiryAccInfo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry Acc Infor");
		inquiryAboutAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry About Acc Infor");
		categoryAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "category Acc Infor");
		currentStageAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "current Stage Acc Infor");
		updateInquiryAccInfor = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Inquiry Acc Infor");

		// Verify that User is able to create new Sales Inquiry-- 16
		salesInquiryBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Inquiry");
		newSalesInqBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Inquiry");
		leadSelectDropdown = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lead Or Account");
		leadAccSearchBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lead Acc Search Inq");
		leadAccSearchText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lead Search Text");
		leadSelectedVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Inq");
		leadTxtSearchBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Btn Lead Text");
		inquiryAboutTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry About");
		leadSelect = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Inquiry Acc Infor");
		inquiryCategory = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "category Inq");
		inquiryOrigin = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry Origin");
		salesUnitInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unti Inq");
		responsibleEmpSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "responsible Employee Inq");
		responsibleEmpText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "responsible Employee Inq Text");
		selectedValEmp = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "responsible Person Selected Val");
		priorityDropdownInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "priority Inq");
		draftInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Inq");
		addNewContactInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Contact Inq");
		nameTitleInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Title inq");
		nameTextInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Text Inq");
		designationTextInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "desig Text Inq");
		phone1TextInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "phone1 Inq");
		phone2TextInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "phone2 Inq");
		releaseInq = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Inq");

		// Verify that User is able to create Lead Infromation -- 17
		leadInformation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lead Information");
		newLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Lead");
		nameTitleLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Title Lead");
		nameLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Lead Text");
		mobileLeadText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "mobile Lead Text");
		companyLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "company Text Lead");
		NICLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "NIC Text Lead");
		designationLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "designation Lead");
		salesUnitLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Lead");
		accountOwnerLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Owner Lead");
		draftLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Lead");
		releaseLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Lead");
		updateTopBtnLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Inq");
		addNewContactLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Contact Lead");
		nameTagLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Tag Lead");
		nameNewLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name New Lead");
		updateBtnLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Button Lead");
		addInquiryLeadBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add Inquiry Lead Btn");
		inquiryAboutLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry About Lead");
		caegoryLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "category Lead");
		currentStageLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "current Stage Lead");
		updateLead = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Lead");
		updateInquiry = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Inquiry");

		// Contact Information -- 18

		contactInformationBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Information Btn");
		newContactBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Contact Btn");
		nameTitleContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Title Contact");
		nameTagContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name Tag Contact");
		// nameContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "name
		// Contact");
		accountSearchContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Search Contact");
		designationContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "designation Contact");
		salesUnitContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Contact");
		accountOwnerContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "account Owner Contact");
		phoneContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "phone Contact");
		mobileContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "mobile Contact");
		emailContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "email Contact");
		IDContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ID Contact");
		birthdateContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "birthdate Contact");
		billingContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "billing Contact");
		countryContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "country Contact");
		updateContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Contact");
		inquiryInformationTabContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"inquiry Information Tab Contact");
		newInquiryBtnContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Inquiry Btn Contact");
		inquiryAbout = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry About");

		// Verify that User is able to create new Pricing Rule with by one and get one
		// free items --- 19

		pricingRuleBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pricing Rule Btn");
		newPricingRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Pricing Rule");
		newPricingruleookup = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Pricingrule Lookup");
		ruleCode = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "rule Code");
		ruleDescription = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "rule Description");
		draftPricingRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Pricing Rule");
		choosePricingPolicyBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "choose Pricing Policy Btn");
		policyHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "policy Header");
		policyRowVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "policy Row Val");
		editPolicy = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "edit Policy");
		discountTypePolicy = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "discount Type Policy");
		fromQty = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "from Qty");
		toQty = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "to Qty");
		advanceOpt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "advance Opt");
		sameProductCheckBox = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "same Product Check Box");
		qtyTextBox = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Box");
		updateInEditValues = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update In Edit Values");
		apply = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply");
		advanceBtnInPolicy = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "advance Btn In Policy");
		activateScheduleBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "activate Schedule Btn");
		daily = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "daily");
		everyDay = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "every Day");
		fromTime = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "from Time");
		toTime = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "to Time");
		advanceRelationTab = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "advance Relation Tab");
		applyBtnAdvanced = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Btn Advanced");
		qtyInAdvance = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty In Advance");
		updateRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Rule");
		activateBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "activate Btn");
		confirmationYes = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "confirmation Yes");
		validatorAlert = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "validator Alert");

		// Verify that User is able to create new Pricing Model with attaching the
		// Pricing rule --- 20

		pricingModel = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pricing Model");
		newPricingModel = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Pricing Model");
		modelCode = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "model Code");
		description = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "description");
		rulesTab = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "rules Tab");
		addPricingRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add Pricing Rule");
		searchTxtRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Txt Rule");
		searchBtnRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Btn Rule");
		selectedRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Rule");
		editPeriod = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "edit Period");
		startDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "start Date");
		startDateValue = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "start Date Value");
		endDateValue = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "end Date Value");
		endDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "end Date");
		applyModel = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Model");
		draftModel = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Model");
		releaseModel = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Model");

		// Verify that user is able to created a Sales Order with added Price Model with
		// a Price Rule --- 21
		salesPriceModel = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Price Model");
		selectedQty = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Qty");
		updateFreeIssues = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Free Issues");
		applyFreeIssues = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Free Issues");
		proSelectedFree = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pro Selected Free");
		closeAlert = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "close Alert");
		warehouseFree = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Free");
		qtyFree = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Free");
		discountFree = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "discount Free");
		taxGroupFree = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "tax Group Free");

		// Verify that User is able to created a new Sales Exchage order -- 22

		salesExchangeOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Exchange Order");
		newSalesExchangeOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Exchange Order");
		startNewJourney = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "start New Journey");
		journey1ExchangeToInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey1 Exchange To Invoice");
		customerSearchExchange = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "customer Search Exchange");
		documentList = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "document List");
		fromDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "from Date");
		dateSelected = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "date Selected");
		toDate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "to Date");
		searchDocumentList = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Document List");
		checkDocumentList = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List");
		applyExchange = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Exchange");
		addNewExchange = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Exchange");
		searchProOutput = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Pro Output");
		searchProText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Pro Text");
		selectedOutput = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Output");
		warehouseOutput = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Output");
		product1List = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product1 List");
		checkFirst = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check First");
		productList2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product List2");
		proDetailsOutput = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pro Details Output");
		SerialWiseCost = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Serial Wise Cost");
		firstSerial = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "first Serial");

		// Free text invoice --23
		freeTextInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "free Text Invoice");
		newFreeTextInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Free Text Invoice");
		cusAccSearchFreeText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "cus Acc Search Free Text");
		glAccSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "gl Account Search");
		glAccText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "gl Account Text");
		selectedRowGLAcc = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Row GL Acc");
		GLAmount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "GL Amount");
		checkoutGLAcc = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkout GL Acc");
		draftGLAcc = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft GL Acc");
		checkAmount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Amount");
		releaseGLAcc = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release GL Acc");

		// Verify that user cannot proceed the Sales Order with adding full qty -- 24
		onHandQty = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "on Hand Qty");
		menuProduct = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Product");
		menuBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Btn");
		menuBtnFull = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "menu Btn Full");
		productAvailability = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Availability");
		closeAvailabilityMenu = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "close Availability Menu");
		warehouse2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse2");
		warehouseFullQty = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Full Qty");
		qtyTextFull = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qty Text Full");
		unitPriceTxtFull = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Txt Full");
		productListBtn1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product List Btn1");
		closeAdvance = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "close Advance");
		advanceMenu = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "advance Menu");

		// Vehicle Information -- 25
		vehicleInformation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Information");
		vehicleInformationHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Information Header");
		newVehicleInformationBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Veh Infor");
		newVehicleInformationHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"new Vehicle Information Header");
		vehicleNoTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle No Text");
		capacityTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "capacity Text");
		capacityMeasure = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "capacity Measure");
		weightTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "weight Text");
		weightMeasure = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "weight Measure");
		draftVehicleInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Veh Infor");
		vehicleSearchTop = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Search Top");
		sideMenuValVehicleInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "side Menu Val");
		activeBtnVehicleInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "activate Btn Veh Infor");
		statusVehicleInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "status Vehicle Inf");
		alertYes = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "alert Yes");
		afterYes = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "after Yes");

		// Verify that user is able to create a delivery plan --- 26

		invenWarehouse = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inven Warehouse");
		deliveryPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delivery Plan");
		deliveryPlanInfo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delivery Plan Info");
		deliveryPlanHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delivery Plan Header");
		bookNo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "book No");
		vehicleSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Search");
		vehicleSearchText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Search Text");
		vehicleSelectedVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Selected Val");
		fromDateVehicle = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "fromDate Vehicle");
		fromDateVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "from Date Val");
		toDateVehicle = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "toDate Vehicle");
		toDateVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "to Date Val");
		draftVehicle = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft Vehicle");
		planningCount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "planning Count");
		loadingCount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "loading Count");
		deliveredCount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delivered Count");
		planningTab = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "planning Tab");
		valueVehicle = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "value Vehicle");
		valueVehicleInLoading = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "value Vehicle In Loading");
		releaseBtnVehicle = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Btn Vehicle");
		countLoading = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "count Loading");
		loadingTab = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "loading Tab");
		releaseBtnInLoading = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "release Btn In Loading");
		addDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add Delivery Plan");
		fromdateDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "fromdate Delivery Plan");
		fromdateyear= findElementInXLSheet(getParameterSalesAndMarketingSmoke, "fromdateyear");
		todateDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "todate Delivery Plan");
		searchBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "search Btn");
		selectedValDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Delivery Plan");
		checkboxDeliveryplan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "checkbox Deliveryplan");
		applyDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "apply Delivery Plan");
		salesInvoiceSearchTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Invoice Search Txt");
		deliveredBtn = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delivered Btn");
		countDelivered = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "count Delivered");
		verifyPlan = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "verify Plan");

		// 4
		reports = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "reports");
		reportTemplate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "report Template");
		productCodeDrop = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Code Drop");
		productCodeText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Code Text");
		quickPreview = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "quick Preview");
		timePeriod = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "time Period");
		today = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "today");
		binCardOutboundVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "bin Card Outbound Val");

		// 5
		binCardInboundVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "bin Card Inbound Val");

		// 6
		administration = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "administration");
		journeyConfiguration = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey Configuration");
		salesOrderToSalesInvoiceJourney = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"sales Order To Sales Invoice Journey");
		autogenerateOutboundShipment = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"autogenerate Outbound Shipment");
		autogenerateSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "autogenerate Sales Invoice");
		updateJourney = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Journey");
		outboundDoc = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "outbound Doc");
		salesInvoiceDoc = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Invoice Doc");
		totalSalesInvoiceSer = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "total Sales Invoice Ser");

		//
		entutionHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "entution Header");
		taskEvent = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "task Event");
		salesInvoiceTile = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Invoice Tile");
		valueSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "value Sales Order");

		// 8
		addNewProduct = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Product");
		productSearchOutbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Search Outbound");
		productSearchTxt = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "product Search Txt");
		proSelectedVal = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pro Selected Val");

		// smoke
		listInbound = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "list Inbound");
		checkDocumentListRet = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "check Document List Ret");

		// Account Group Config
		AccountGroupConfiguration = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "Account Group Configuration");
		newAccountGroupConfiguration = findElementInXLSheet(getParameterSalesAndMarketingSmoke,
				"new Account Group Configuration");
		addNewAccGrp = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Acc Grp");
		addNewRecordAccGrp = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Record Acc Grp");
		accGrpText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "acc Grp Text");
		updateAccGrp = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Acc Grp");
		selectAccGroup = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "select Acc Group");
		activeCheckBox = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "active Check Box");
		currencyAccGroup = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "currency Acc Group");
		salesUnitAccGrp = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Acc Grp");
		detailsAccGrp = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "details Acc Grp");
		leadDays = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lead Days");
		draftNew = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "draft New");
		edit = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "edit");
		update = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update");

		mobileAccInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "mobile Acc Inf");
		annualRevenue = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "annual Revenue");
		detailsTab = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "details Tab");
		updateAndNew = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update And New");

		// Collection Unit
		collectionUnit = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "collection Unit");
		newCollectionUnit = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Collection Unit");
		collectionUnitCode = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "collection Unit Code");
		collectionUnitName = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "collection Unit Name");
		commissionRate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "commission Rate");
		employeeSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "employee Search");
		employeeSearchText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "employee Search Text");
		selectedValEmployee = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "selected Val Employee");
		sharingRate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sharing Rate");
		history = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "history");
		activities = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "activities");
		historyReleased = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "history Released");
		
		// Sales Order
		POJourneyWindow = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "PO Journey Window");
		journeyWindowClose = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "journey Window Close");
		reminders = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "reminders");
		createReminder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "create Reminder");
		convertInboundCustomerMemo = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "convert Inbound Customer Memo");
		
		//Sales Quotation
		unitPriceTextQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Text Quat");
		unitPriceAftTextQuat = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Price Aft Text Quat");
		activityHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "activity Header");
		activityMenuClose = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "activity Menu Close");
		convertToAccount = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "convert To Account");
		capacityPlanning = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "capacity Planning");
		capacityPlanningHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "capacity Planning Header");
		capacityPlanningClose = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "capacity Planning Close");
		linkMenuClose = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "link Menu Close");
		duplicate = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "duplicate");
		generateNewVersion = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "generate New Version");
		qoutationVersion = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "qoutation Version");
		
		//
		valReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "val Return Invoice");
		hold = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "hold");
		
		//Vehicle Information
		vehicleInfEditHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "vehicle Inf Edit Header");
		updateVehicleInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Vehicle Inf");
		inactiveBtnVehicleInf = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inactive Btn Vehicle Inf");
		inactivateAlertYes = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inactivate Alert Yes");

		//Consolidate Invoice
		invoiceVal1 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "invoice Val 1");
		invoiceVal2 = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "invoice Val 2");
		
		//Warranty Profile
		warrantyProfile = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warranty Profile");
		newWarrantyProfile = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Warranty Profile");
		warrantyProfileCode = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warranty Profile Code");
		warrantyProfileName = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warranty Profile Name");
		statusWarrantyProfile = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "status Warranty Profile");
		
		//Pricing Rule
		pricingRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "pricing Rule");
		updatePricingRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Pricing Rule");
		resetPricing = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "reset Pricing");
		
		//sales Parameter
		salesParameter = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Parameter");
		addNewSalesParameter = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "add New Sales Parameter");
		salesParameterText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Parameter Text");
		updateSales = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "update Sales");
		closeAlertSales = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "close Alert Sales");
		
		//Contact Information
		editContact = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "edit Contact");
		contactInformation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Information");
		contactInformationHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Information Header");
		contactNameValText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "contact Name Val Text");
		createInHistory = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "create In History");
		inquiryInformation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry Information");
		newInquiry = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Inquiry");
		salesInquiryHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Inquiry Header");
		inquiryFirstRow = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "inquiry First Row");
		stage = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "stage");
		status = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "status");
		
		//Lead Information
		leadInformationHeader = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lead Information Header");
		deleteRule = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "delete Rule");
		salesUnitNavigation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Navigation");
		
		newSalesUnit = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Sales Unit");
		salesUnitCode = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Code");
		unitLeaderSearch = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Leader Search");
		unitLeaderText = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Leader Text");
		salesUnitName = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sales Unit Name");
		unitLeaderSelected = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "unit Leader Selected");
		sharingRateSales = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "sharing Rate Sales");
		
		warehouseInformation = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Information");
		newWarehouse = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "new Warehouse");
		warehouseCode = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Code");
		warehouseName = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "warehouse Name");
		
		txt_ProductA = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ProductA");
		txt_ProductQtyA = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "ProductQtyA");
		btn_draft = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "btn_draft");
		header_releasePurchaseInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "header_releasePurchaseInvoice");
		header_releaseSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "header_releaseSalesInvoice");
		lnk_resultInfoOnLookupInforReplace = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "lnk_resultInfoOnLookupInforReplace");
		header_draftedSalesReturnOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "header_draftedSalesReturnOrder");
		header_draftedSalesOrder = findElementInXLSheet(getParameterSalesAndMarketingSmoke, "header_draftedSalesOrder");
	
	}

	public static void readData() throws Exception {

		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU") || computerName.equals("MADHUSHAN-LAPscs")) {
			siteURL = findElementInXLSheet(getDataSalesAndMarketingSmoke, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataSalesAndMarketingSmoke, "site url");
		}
		userNameData = findElementInXLSheet(getDataSalesAndMarketingSmoke, "user name data");
		passwordData = findElementInXLSheet(getDataSalesAndMarketingSmoke, "password data");
		sales_Marketing = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales and marketing");

		// Sales Order --> Outbound Shipment --> Sales Invoice -- 1

		qtyVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "qty Val");
		descrptionValStockAdj = findElementInXLSheet(getDataSalesAndMarketingSmoke, "descrption Val Stock Adj");
		countTextVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "count Text Val");

		customerAccVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "customer Acc Val");
		billingAddrVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "billing Addr Val");
		shippingAddrVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "shipping Addr Val");
		productTextValSer = findElementInXLSheet(getDataSalesAndMarketingSmoke, "product Text Val Ser");
		productTextValInv = findElementInXLSheet(getDataSalesAndMarketingSmoke, "product Text Val Inv");
		unitPriceVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Val");
		unitPriceValSerialSpec = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Val Serial Spec");
		unitPriceValBatchSpec = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Val Batch Spec");
		unitPriceValInv = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Val Inv");
		quantityVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "quantity Val");
		quantityValInv = findElementInXLSheet(getDataSalesAndMarketingSmoke, "quantity Val Inv");
		quantityValSerialSpec = findElementInXLSheet(getDataSalesAndMarketingSmoke, "quantity Val Serial Spec");
		serialCountVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial Count Val");
		serialNo = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial No");
		warehouseVal1 = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Val1");
		salesUnitVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Unit Val");

		batchNoValBatchSpecific = findElementInXLSheet(getDataSalesAndMarketingSmoke, "batch NoVal Batch Specific");
		serialBatchSpecificVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial Batch Specific Val");

		// 2
		customerAccServiceVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "customer Acc Service Val");
		shippingAddrSerVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "shipping Addr Ser Val");
		billingAddrSerVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "billing Addr Ser Val");
		productSerVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "product Ser Val");
		unitPriceValue = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Value");

		// 3
		customerAccOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "customer Acc Outbound Val");
		billingOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "billing Outbound Val");
		shippingOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "shipping Outbound Val");
		serviceOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "service Outbound Val");
		invOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "inv Outbound Val");
		warehouseVal3 = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Val3");
		unitpriceSerOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unitprice SerOutbound Val");
		unitpriceInvOutboundVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unitprice InvOutbound Val");

		// 4
		dropshipmentSerVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "dropshipment Ser Val");
		dropshipmentInvVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "dropshipment Inv Val");
		vendorVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "vendor Val");

		// 5
		serialNumVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial Num Val");
		warehouseVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Val");

		// 6
		unitPriceSerVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Val Ser");
		qtyValSalesInvoiceSer = findElementInXLSheet(getDataSalesAndMarketingSmoke, "qty Val Sales Invoice Ser");

		// 7
		customerAccValQuat = findElementInXLSheet(getDataSalesAndMarketingSmoke, "customer Acc Val Quat");
		unitPriceQuatVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Price Val Inv");
		qtyValQout = findElementInXLSheet(getDataSalesAndMarketingSmoke, "qty Val Qout");
		qtyQuatationSerVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "qty Quatation Ser Val");
		qtyQuatationInvVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "qty Quatation Inv Val");
		billingAddrQuatVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "billing Addr Quat Val");
		shippingAddrQuatVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "shipping Addr Quat Val");
		serialNoVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial No Val");
		warehouseValQuot = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Val Quot");

		// 8
		serialNo2 = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial No2");
		vendorVal2 = findElementInXLSheet(getDataSalesAndMarketingSmoke, "vendor Val2");

		// 9
		serialNo3 = findElementInXLSheet(getDataSalesAndMarketingSmoke, "serial No3");

		// 10
		customerAccReturnVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "customer Acc Return Val");

		// 12
		productValDirect = findElementInXLSheet(getDataSalesAndMarketingSmoke, "product Val Direct");
		warehouseDirectVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Direct Val");
		returnReasonVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "return Reason Val");

		// Account Information
		accountNameVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "account Name Val");
		contactNameVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "contact Name Val");
		phoneNoVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "phone No Val");
		mobileNoVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "mobile No Val");
		emailVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "email Val");
		designationVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "designation Val");
		inquiryAboutVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "inquiry About Val");

		// Sales Inquiry -- 16
		leadAccSearchVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "lead Account Search Val");
		salesInquiryAboutVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Inquiry About Val");
		responsibleEmpVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "responsible Employee Text Val");
		nameTextSalesInqVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "name Text Sales Inq Val");
		salesInqDesigVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Inq Desig Text Val");
		salesInqPhone1Val = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Inq Phone1 Text Val");
		salesInqPhone2Val = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Inq Phone2 Text Val");

		// Lead Information -- 17
		nameTextLeadVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "name Lead Val");
		mobileTexLeadtVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "mobile Lead Text Val");
		companyLeadVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "company Lead Val");
		NICLeadVAl = findElementInXLSheet(getDataSalesAndMarketingSmoke, "NIC Lead Val");
		designationLeadVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "designation Lead Val");
		phoneLeadVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "phone Lead Val");
		companyUpdateVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "company Update Val");
		NameUpdateVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "name Update Val");
		inquiryUpdateAboutVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "inquiry Update About Val");

		// Create Contact Information -- 18
		nameContactVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "name Contact Val");
		designationContactVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "designation Contact Val");
		phoneContatcVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "phone Contatc Val");
		mobileContactVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "mobile Contact Val");
		emailContatctVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "email Contatct Val");
		inquiryAboutValu = findElementInXLSheet(getDataSalesAndMarketingSmoke, "inquiry About Valu");

		// Pricing Rule -- 19
		ruleCodeVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "rule Code Val");
		ruleDescriptionVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "rule Description Val");
		fromVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "from Val");
		toVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "to Val");
		qtyEditVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "qty Edit Val");
		fromTimeVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "from Time Val");
		toTimeVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "to Time Val");
		

		// Verify that User is able to create new Pricing Model with attaching the
		// Pricing rule -- 20
		modelCodeVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "model Code Val");
		modelDescriptionVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "model Description Val");
		pricingRuleVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "pricing Rule Val");
		
		//
		selectedQtyVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "selected Qty Val");

		// 22
		outputProval = findElementInXLSheet(getDataSalesAndMarketingSmoke, "output Proval");
		warehouseOutputPro = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Output Pro");

		// Verify that User is able to create Free Text Invoice --- 23
		glAccVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "gl Acc Val");
		GLAmountVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "GL Amount Val");

		// 24
		productValInv = findElementInXLSheet(getDataSalesAndMarketingSmoke, "product Val Inv");
		productTextVal2 = findElementInXLSheet(getDataSalesAndMarketingSmoke, "product Text Val2");

		// glaccounttext = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Unit
		// Val");

		// Verify that user cannot proceed the Sales Order with adding full qty as
		// Separate product ine in same Sales Order

		// Active Vehicle Information -- 25
		vehicleNoTxtVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "vehicle No Txt Val");
		capacityTxtVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "capacity Txt Val");
		weightTxtVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "weight Txt Val");

		// Verify that user is able to create a delivery plan --- 26
		vehicleSearchVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "vehicle Search Val");

		// s4
		ProductCodeVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "Product Code Val");

		// Account Group
		accGrpVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "acc Grp Val");
		currencyVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "currency Val");
		leadDaysVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "lead Days Val");

		//
		annualRevenueVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "annual Revenue Val");
		newMobileNo = findElementInXLSheet(getDataSalesAndMarketingSmoke, "new Mobile No");

		// Collection unit
		collectionNameVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "collection Name Val");
		commissionRateVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "commission Rate Val");
		employeeVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "employee Val");
		sharingRateVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sharing Rate Val");
		commisionRateEditedVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "commision Rate Edited Val");
		sharingRateNewVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sharing Rate New Val");
		
		//Sales Quotation
		unitPriceQuatValEdit = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unitPrice Quat Val Edit");
		
		//Vehicle Information
		weightTxtNewVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "weight Txt New Val");
		
		//Warranty Profile
		profileNameVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "profile Name Val");
		profileNameNew = findElementInXLSheet(getDataSalesAndMarketingSmoke, "profile Name New");
		
		//Pricing Rule
		ruleDescriptionNewVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "rule Description New Val");
		
		//Contact Name
		contactNameNewVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "contact Name New Val");
		
		//Lead Information
		nameTextLeadNewVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "name Text Lead New Val");
		
		descriptionVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "description Val");
		
		unitLeaderVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "unit Leader Val");
		salesUnitNameVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "sales Unit Name Val");
		warehouseNameVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "warehouse Name Val");
		businessUnitVal = findElementInXLSheet(getDataSalesAndMarketingSmoke, "business Unit Val");
		
	}

}
