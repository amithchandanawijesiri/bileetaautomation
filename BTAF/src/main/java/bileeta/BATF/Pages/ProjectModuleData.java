package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ProjectModuleData extends TestBase{
	
	/* Reading the element locators to variables */
	//Com_TC_001
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	//Com_TC_002
	protected static String btn_nav;
	protected static String sideMenu;
	protected static String subsideMenu;
	protected static String btn_projbutton;
	
	//Project_TC_001
	protected static String btn_projectbutton;
	protected static String projectpage;
	protected static String btn_newprojectbutton;
	protected static String newprojectpage;
	protected static String txt_ProjectCode;
	protected static String txt_Title;
	protected static String txt_ProjectGroup;
	protected static String btn_PricingProfilebutton;
	protected static String txt_PricingProfiletxt;
	protected static String sel_PricingProfilesel;
	protected static String btn_ResponsiblePersonbutton;
	protected static String txt_ResponsiblePersontxt;
	protected static String sel_ResponsiblePersonsel;
	protected static String txt_AssignedBusinessUnits;
	protected static String btn_CustomerAccountbutton;
	protected static String txt_CustomerAccounttxt;
	protected static String sel_CustomerAccountsel;
	protected static String btn_ProjectLocationbutton;
	protected static String txt_ProjectLocationtxt;
	protected static String sel_ProjectLocationsel;
	protected static String txt_RequestedStartDate;
	protected static String sel_RequestedStartDatesel;
	protected static String btn_draft;
	protected static String btn_release;
	protected static String txt_ActivitiesTabA;
	
	//Project_TC_002
	protected static String btn_edit;
	protected static String btn_WorkBreakdown;
	protected static String btn_AddTask;
	protected static String btn_InputProducts;
	protected static String txt_TaskName;
	protected static String txt_TaskType;
	protected static String btn_productbutton;
	protected static String txt_producttxt;
	protected static String sel_productsel;
	protected static String txt_EstimatedQty;
	protected static String btn_AddProPlusBtn;
	protected static String btn_Labour;
	protected static String btn_ServiceProductbutton;
	protected static String txt_ServiceProducttxt;
	protected static String sel_ServiceProductsel;
	protected static String txt_EstimatedWork;
	protected static String sel_EstimatedWorksel;
	protected static String btn_Resource;
	protected static String btn_Resourcebutton;
	protected static String txt_Resourcetxt;
	protected static String sel_Resourcesel;
	protected static String btn_ResourceServiceProductbutton;
	protected static String txt_ResourceEstimatedWork;
	protected static String sel_ResourceEstimatedWorksel;
	protected static String btn_Expense;
	protected static String txt_AnalysisCode;
	protected static String btn_ExpenseServiceProductbutton;
	protected static String txt_EstimatedCost;
	protected static String txt_EstimatedCostAfter;
	protected static String btn_TaskUpdate;
	protected static String btn_releaseTaskdropdown;
	protected static String btn_releaseTask;
	protected static String btn_update;
	protected static String txt_TaskReleaseTxt;

	
	//Project_TC_003
	protected static String btn_CostingSummary;
	protected static String btn_Material;
	protected static String txt_Materialvalue;
	protected static String btn_editTask;
	protected static String btn_productinfo;
	protected static String btn_ProductRelatedPrice;
	protected static String txt_LastPurchasePrice;
	protected static String btn_CloseProductRelatedPriceA;
	protected static String btn_ProductRelatedPriceHeaderclose;
	
	//Project_TC_004
	protected static String btn_Hours;
	protected static String txt_Hoursvalue;
	protected static String btn_Orgabutton;
	protected static String btn_EmployeeInformation;
	protected static String txt_EmployeeInformationtxt;
	protected static String sel_EmployeeInformationsel;
	protected static String txt_EmployeeInformationTemplate;
	protected static String txt_RateProfiletxt;
	protected static String txt_EstimatedWorkhour;
	protected static String btn_EstimatedWorkhourok;
	protected static String txt_projectserch;
	protected static String sel_projectserchsel;
	protected static String btn_RateProfile;
	protected static String txt_RateProfileserch;
	protected static String txt_ExternalHourlyRate;
	
	//Project_TC_005
	protected static String btn_ResourceCost;
	protected static String txt_ResourceCostvalue;
	protected static String btn_ResourceInformation;
	protected static String txt_ResourceInformationtxt;
	protected static String sel_ResourceInformationsel;
	
	//Project_TC_006
	protected static String btn_Expensearrow;
	protected static String txt_Expensevalue;

	//Project_TC_007to008
	protected static String txt_txtEmpNo;
	protected static String txt_EmployeeCode;
	protected static String txt_EmployeeFullName;
	protected static String btn_serbutton;
	protected static String btn_DailyWorkSheetbutton;
	protected static String txt_DailyWorkSheetpage;
	protected static String btn_newDailyWorkSheetbutton;
	protected static String txt_newDailyWorkSheetpage;
	protected static String btn_DailyWorkSheetProjectbutton;
	protected static String btn_AddNewRecord;
	protected static String txt_projectcode;
	protected static String txt_ResponsiblePersonText;
	protected static String btn_JobNobutton;
	protected static String txt_JobNotxt;
	protected static String sel_JobNosel;
	protected static String btn_Employeebutton;
	protected static String txt_Employeetxt;
	protected static String sel_Employeesel;
	protected static String txt_TimeIn;
	protected static String sel_TimeInsel;
	protected static String txt_TimeOut;
	protected static String sel_TimeOutsel;
	protected static String btn_AddNewJobupdate;
	protected static String btn_TimeInOK;
	protected static String btn_TimeOutOK;
	protected static String btn_HourlyRateBtn;
	protected static String txt_HourlyRateTxt;
	protected static String txt_HoursvalueActual;
	protected static String txt_WorkedHours;
	
	//Project_TC_008
	protected static String txt_projectcodeserch;
	protected static String sel_projectcodeserchsel;
	protected static String btn_Overview;
	protected static String btn_Labourtab;
	protected static String txt_OverviewTabLabourActualWork;
	protected static String txt_ActualWorktxt;
	protected static String txt_EstimatedWorktxt;

	//Project_TC_009to010
	protected static String btn_Action;
	protected static String btn_ActualUpdate;
	protected static String btn_ActionResource;
	protected static String btn_ActualWork;
	protected static String btn_ActualWorksel;
	protected static String btn_ActualWorkOK;
	protected static String btn_ActionActualUpdate;
	protected static String btn_ActionUpdate;
	protected static String btn_ResourcestabA;
	protected static String txt_ResourceCostActualValue;
	protected static String txt_OverviewTabResourceActualWork;
	
	//Project_TC_011
	protected static String btn_CreateInternalOrder;
	protected static String btn_Taskdropdown;
	protected static String btn_checkbox;
	protected static String btn_Apply;
	protected static String btn_Gotopage;
	protected static String btn_ActionUpdateclose;
	protected static String btn_IDOMasterinfoBtn;
	protected static String txt_IDOBatchNoPro1;
	protected static String txt_IDOSerialNoPro2;
	
	//Project_TC_012to013
	protected static String txt_ActualQty;
	protected static String btn_ActualUpdateSerialBatchNoSelBtn;
	protected static String sel_ActualUpdateSerialNoPopupSel;
	protected static String btn_ActualUpdateclose;
	protected static String btn_productinfoclose;
	protected static String btn_AddTaskclose;
	protected static String txt_ActualQtybox;
	protected static String btn_InputProductstab;
	protected static String txt_EstimatedQtybox;
	protected static String txt_MaterialActualvalue;
	protected static String txt_EstimatedQtyfiled;
	
	//Project_TC_014
	protected static String btn_InventoryWarehousing;
	protected static String btn_InternalReturnOrder;
	protected static String btn_newInternalReturnOrder;
	protected static String btn_WIPReturns;
	protected static String btn_documentMark;
	protected static String txt_InternalDispatchOrdercode;
	protected static String txt_ToWarehouse;
	protected static String txt_CommonSearch;
	protected static String btn_Search;
	protected static String btn_checkboxIRO;
	protected static String btn_ApplyIRO;
	protected static String btn_draftIR;
	protected static String btn_releaseIR;
	protected static String btn_SerialBatchIR;
	protected static String btn_SerialBatchbutton;
	protected static String btn_SerialBatchsel;
	protected static String txt_SerialBatchqtyfield;
	protected static String txt_SerialBatchqtyfield1;
	protected static String btn_SerialBatchrefresh;
	protected static String btn_SerialBatchback;
	protected static String btn_arrowdown;
	
	//Project_TC_015
	protected static String btn_UpdateTaskPOC;
	protected static String txt_cboxTaskPOC;
	protected static String txt_TskPOC;
	protected static String btn_POCApply;
	
	//Project_TC_016 19/11/20
	protected static String btn_CompleteA;
	protected static String btn_YesA;
	protected static String btn_PostDateA;
	protected static String sel_PostDateselA;
	protected static String btn_checkboxA;

	//Project_TC_017 19/11/20
	protected static String txt_ContractTypeA;
	protected static String btn_SubContractServicesA;
	protected static String btn_SubproductbuttonA;
	protected static String txt_SubEstimatedCostA;
	protected static String btn_BillableA;
	
	//Project_TC_017to019
	protected static String btn_SubContractA;
	protected static String txt_SubContractvalueA;
	protected static String btn_SubContractOrderA;
	protected static String txt_subTaskA;
	protected static String btn_subVendorbuttonA;
	protected static String txt_subVendortxtA;
	protected static String sel_subVendorselA;
	protected static String btn_subcheckboxA;
	protected static String btn_CheckoutA;
	protected static String txt_VendorReferenceNoA;
	protected static String PurchaseOrderpageA;
	protected static String PurchaseInvoicepageA;
	protected static String txt_SubContractActualcost;
	protected static String txt_SubContractvalueActualA;
	
	//Project_TC_021 19/11/24
	protected static String btn_SubContractServicestabA;
	protected static String txt_OrderDateA;
	protected static String txt_OrderNoA;
	protected static String txt_InvoiceDateA;
	protected static String txt_InvoiceNoA;
	protected static String txt_ProductA;
	protected static String txt_EstimatedAmountA;
	protected static String txt_ActualAmountA;
	
	//Project_TC_022 19/11/25
	protected static String btn_InterDepartmentA;
	protected static String btn_OwnerbuttonA;
	protected static String txt_OwnertxtA;
	protected static String sel_OwnerselA;
	protected static String txt_EstimatedCostA;
	protected static String txt_EstimatedDateA;
	protected static String sel_EstimatedDateselA;
	
	//Project_TC_023 19/11/25
	protected static String btn_ServiceCalendarA;
	protected static String txt_interdepartmentjobA;

	//Project_TC_024 19/11/25
	protected static String txt_ServiceJobOrderpageA;

	//Project_TC_025 19/11/25
	protected static String txt_ServiceGroupA;
	protected static String txt_PriorityA;
	protected static String btn_PricingProfilebuttonA;;
	protected static String txt_PricingProfiletxtA;
	protected static String sel_PricingProfileselA;
	protected static String btn_ServiceLocationbuttonA;
	protected static String txt_ServiceLocationtxtA;
	protected static String sel_ServiceLocationselA;
	protected static String btn_PerformerbuttonA;
	protected static String txt_PerformertxtA;
	protected static String sel_PerformerselA;

	protected static String txt_OrderDescriptionA;
	
	//Project_TC_026 19/11/25
	protected static String btn_InterDepartmenttabA;
	protected static String txt_InterDepartmentestimatedcostA;
	
	//Project_TC_027 19/11/25
	protected static String btn_TaskcloseA;
	protected static String btn_FinanceA;
	protected static String btn_OutboundPaymentAdviceA;
	protected static String txt_OutboundPaymentAdvicepageA;
	protected static String btn_NewOutboundPaymentAdviceA;
	protected static String btn_AccrualVoucherA;
	protected static String btn_AccrualVoucherVendorA;
	protected static String txt_AccrualVoucherVendortxtA;
	protected static String sel_AccrualVoucherVendorselA;
	protected static String txt_AnalysisCodeA;
	protected static String txt_AmountA;
	protected static String btn_costallocationA;
	protected static String txt_PercentageA;
	protected static String txt_CostAssignmentTypeA;
	protected static String btn_CostObjectbuttonA;
	protected static String txt_CostObjecttxtA;
	protected static String sel_CostObjectselA;
	protected static String btn_AddRecordA;
	protected static String btn_UpdateA;
	protected static String txt_AccrualVoucherVendorReferenceNoA;
	
	//Project_TC_028 19/11/26
	protected static String txt_InterDepartmentActualvalueA;
	
	//Project_TC_029 19/11/26
	protected static String btn_ServiceJobOrderA;
	protected static String txt_ServiceJobOrderserchA;
	protected static String sel_ServiceJobOrderselA;
	protected static String btn_AddProductTagNoA;
	protected static String btn_SpnSeriaA;
	protected static String btn_SerialNosA;
	protected static String btn_AddProductTagNoapplyA;
	protected static String btn_StopA;
	protected static String txt_POCA;
	protected static String txt_pageCompleted;
	
	
	//product7
	protected static String pro1A;
	protected static String plus1A;
	protected static String pro2A;
	protected static String plus2A;
	protected static String pro3A;
	protected static String plus3A;
	protected static String pro4A;
	protected static String plus4A;
	protected static String pro5A;
	protected static String plus5A;
	protected static String pro6A;
	protected static String plus6A;
	protected static String pro7A;
	protected static String plus7A;
	
	protected static String EstimatedQty2A;
	protected static String EstimatedQty3A;
	protected static String EstimatedQty4A;
	protected static String EstimatedQty5A;
	protected static String EstimatedQty6A;
	protected static String EstimatedQty7A;
	
	protected static String SerialBatchA;
	protected static String Cap4A;
	protected static String Cap5A;
	protected static String Cap2A;
	protected static String Cap3A;
	protected static String Cap7A;
	protected static String SerialRangeA;
	protected static String SerialFromNuA;
	protected static String updateSerialA;
	protected static String closeProductListA;
	protected static String createGridMasterInfo4A;
	protected static String createGridMasterInfo5A;
	protected static String ProductBatchSerialWiseCostA;
	protected static String ProductBatchSerialWiseCostSelA;
	protected static String headercloseA;
	
	protected static String openSerialNoSelection4A;
	protected static String openSerialNoSelection5A;
	protected static String captureQtyA;
	protected static String ActualSerialUpdate;
	protected static String ActualSerialYesA;

	protected static String createGridMasterInfo1A;
	protected static String openSerialNoSelection1A;
	protected static String ProductBatchSelA;
	protected static String Cap1A;
	protected static String BatchFromNuA;
	protected static String ProductBatchSerialWiseCostSel2A;
	
	protected static String openSerialNoSelection4_2A;
	protected static String openSerialNoSelection5_2A;
	protected static String openSerialNoSelection1_2A;
	
	protected static String captureQty2A;
	
	protected static String ActualQty1;
	protected static String ActualQty2;
	protected static String ActualQty3;
	protected static String ActualQty6;
	protected static String ActualQty7;

	protected static String pageDraft;
	protected static String pageRelease;
	protected static String pageDraftNew;
	protected static String pageReleaseNew;
	protected static String Status;
	protected static String Header;
	
	//Master
	protected static String btn_ProjectParametersA;
	protected static String btn_SaveA;
	protected static String txt_validatorA;
	
	//SetUp_1
	protected static String btn_ProjectGroupConfigurationA;
	protected static String btn_NewConfigurationA;
	protected static String txt_ConfigCodeA;
	protected static String txt_ProGroupA;
	protected static String btn_ProjectGroupConfigurationDraftA;
	protected static String btn_ProjectGroupConfigurationEditA;
	protected static String btn_ProjectGroupConfigurationUpdateA;
	protected static String txt_ProjectGroupConfigurationSerchA;
	protected static String btn_AddProGroupA;
	protected static String btn_AddNewProGroupA;
	protected static String txt_AddNewProGrouptxtA;
	protected static String btn_ProGroupUpdateA;
	
	//Transaction_1
	protected static String txt_CopyFromTextPCE;
	//Transaction_2
	protected static String txt_CopyFromTextPQ;
	
	//Transaction_3
	protected static String txt_ActualUpdatetxtA;
	protected static String txt_CreateInternalOrdertxtA;
	protected static String btn_ChangeRetentionA;
	protected static String txt_ChangeRetentiontxtA;
	protected static String btn_GenerateCustomerAdvanceA;
	protected static String txt_GenerateCustomerAdvancetxtA;
	protected static String btn_PendingApprovalA;
	protected static String txt_PendingApprovaltxtA;
	protected static String btn_ProductRegistrationA;
	protected static String txt_ProductRegistrationtxtA;
	protected static String txt_SubContractOrdertxtA;
	protected static String btn_TaskDetailAllocationA;
	protected static String txt_TaskDetailAllocationtxtA;
	protected static String btn_UpdateTaskDetailsA;
	protected static String txt_UpdateTaskDetailstxtA;
	protected static String txt_UpdateTaskPOCtxtA;
	protected static String btn_VendorAdvanceA;
	protected static String txt_VendorAdvancetxtA;
	protected static String txt_ReverseDateA;
	protected static String btn_ReverseA;
	protected static String btn_ReversebuttonA;
	protected static String btn_NoA;
	protected static String btn_DraftAndNewA;
	protected static String btn_UpdateAndNewA;
	protected static String btn_DuplicateA;
	protected static String btn_CopyFromA;
	protected static String txt_CopyFromTextA;
	protected static String btn_headercloseA;
	protected static String btn_DeleteA;
	protected static String txt_DeleteTextA;
	
	//SetUp_2
	protected static String txt_CopyFromTextPMA;
	


	/* Reading the Data to variables */
	//Com_TC_001
		protected static String userNameData;
		protected static String passwordData;
		
	//Project_TC_001
		protected static String ProjectCode;
		protected static String Title;
		protected static String PricingProfiletxt;
		protected static String ResponsiblePersontxt;
		protected static String CustomerAccounttxt;
		protected static String ProjectLocationtxt;

	//Project_TC_002
		protected static String producttxt;
		protected static String TaskName;
		protected static String EstimatedQty;
		protected static String ServiceProducttxt;
		protected static String Resourcetxt;
		protected static String EstimatedWork;
		protected static String EstimatedCost;
		
		//Project_TC_012
		protected static String ActualQty;
		
		//Project_TC_014
		protected static String SerialBatchqtyfield1;
		protected static String ToWarehouseA;
		
		//Project_TC_015
		protected static String TskPOC;
		
		//Project_TC_019 19/11/21
		protected static String subVendortxtA;
		protected static String VendorReferenceNotxtA;
		
		//Project_TC_022 19/11/25
		protected static String OwnertxtA;
		
		
		//Project_TC_025 19/11/25
		protected static String PricingProfiletxtA;
		protected static String PerformertxtA;
		protected static String OrderDescriptionA;
		
		//Project_TC_027 19/11/25
		protected static String AmountA;
		protected static String PercentageA;
		protected static String AccrualVoucherVendorReferenceNoA;
		protected static String ServiceGroupA;
		protected static String PricingProfileNewtxt;
/*==============================================================================================*/
		
		/* Reading the element locators to variables */
		// Verify the ability to do cost allocation for project tasks using accrual
		// voucher--030
		protected static String financeModule;
		protected static String searchBar;

		protected static String releaseInHeader;

		protected static String projectModule;
		protected static String newProject;
		protected static String finance;
		protected static String outboundPaymentAdvice;
		protected static String newOutboundPaymentAdvice;
		protected static String accuralVoucher;
		protected static String vendorSearch;
		protected static String vendorText;
		protected static String vendorSelectedVal;
		protected static String analysisCodeDrop;
		protected static String amountText;
		protected static String costAllocationBtn;
		protected static String precentageText;
		protected static String costAssignmentTypeNad;
		protected static String costObjectSearch;
		protected static String costObjectText;
		protected static String costObjectSelected;
		protected static String addRecord;
		protected static String updateCostAllocation;
		protected static String vendorRef;
		protected static String checkout;
		protected static String draftNad;
		protected static String releaseNad;
		protected static String documentVal;

		// Verify that expenses recorded from the accrual voucher shows in project
		// costing summary -- 31
		protected static String project;
		protected static String projectSearchText;
		protected static String projectSelectedVal;
		protected static String costingSummaryTab;
		protected static String expenseArrow;
		protected static String cleaningCharge;
		// protected static String actual;
		protected static String existingVal;
		protected static String OtherServicesexistingVal;

		// Verify that expenses recorded from the accrual voucher shows in project
		// overview -- 32
		protected static String accrualHeader;
		protected static String valPre;

		// Verify the ability to do cost allocation for project tasks using customer
		// refund--33
		protected static String downArrow;
		protected static String customerRefund;
		protected static String customerSearch;
		protected static String customerText;
		protected static String customerSelectedVal;

		// 34
		protected static String adjustment;
		protected static String cusRefundCostingSum;

		// Verify the ability to do cost allocation for project tasks using payment
		// voucher -- 36
		protected static String paymentVoucher;
		protected static String convertToOutboundPayment;
		protected static String payeeText;
		protected static String detailsTab;
		protected static String OutboundPaymentDocSel;
		protected static String paymentVoucherHeader;

		// Verify that expenses recorded from the payment voucher shows in project
		// costing summary -- 37
		protected static String projectInExpense;
		protected static String projectInExpenseVal;

		// 38
		protected static String val;

		// Verify the ability to do cost allocation for project tasks using customer
		// debit memo -- 39
		protected static String inboundPaymentAdvice;
		protected static String txt_InboundPaymentAdvicepageA;
		protected static String newInboundPaymentAdvice;
		protected static String customerDebitMemo;
		protected static String customerSearchDebit;
		protected static String amountDebitText;
		protected static String CustomerRef;
		protected static String costAllocation;

		// 40
		protected static String cusDebitMemoSum;

		// Verify that expenses recorded from the customer debit memo shows in project
		// overview -- 41
		protected static String customerDebitHeader;

		// Verify the ability to do cost allocation for project tasks using vendor
		// refund --- 42
		protected static String vendorRefund;
		protected static String btn_VRVendorSearchBtn;
		protected static String txt_VRVendorSearchTxt;
		protected static String sel_VRVendorSearchSel;
		protected static String precentageTextVendor;
		protected static String costAllocationBtnVendor;
		
		//43
		protected static String vendorRefundCosting;
		protected static String costsAdjustment;

		// Verify that expenses recorded from the vendor refund shows in project
		// overview -- 44
		protected static String costAdjustment;
		protected static String vendorRefundHeader;

		// Verify the ability to do cost allocation for project tasks using petty cash
		// --- 45
		protected static String pettyCash;
		protected static String newPettyCash;
		protected static String pettyCashAccount;
		protected static String descriptionPetty;
		protected static String paidtoSearch;
		protected static String employeeSearchText;
		protected static String empSelectedVal;
		protected static String type;
		protected static String IOUNoSearch;
		protected static String narration;
		protected static String amountTextPetty;
		protected static String costAllocationPetty;

		// Verify that expenses recorded from the petty cash shows in project overview
		// -- 47
		protected static String pettyCashHeader;
		protected static String valOverviewPetty;

		// Verify the ability to do cost allocation for project tasks using bank
		// adjustment -- 48
		protected static String bankAdjustment;
		protected static String newBankAdjustment;
		protected static String selectBank;
		protected static String bankAccountNo;
		protected static String analysisCode;
		protected static String GLAccountSearch;
		protected static String GLAccountText;
		protected static String GLAccountSelected;
		protected static String costAllocate;
		protected static String amountTextt;
		protected static String valOverviewBA;

		// Verify that expenses recorded from the bank adjustment shows in project
		// overview -- 50
		protected static String overviewNad;
		protected static String expenses;
		protected static String valueNad;
		protected static String amountCheck;

		// Verify the ability to do cost allocation for project tasks using purchase
		// invoice -- 51
		protected static String procument;
		protected static String purchaseInvoice;
		protected static String newPurchaseInvoice;
		protected static String purchaseInvoiceService;
		protected static String vendorSearchBtn;
		protected static String vendorTextt;
		protected static String productSearchBtn;
		protected static String costAllocationInv;
		protected static String removeFirstRow;
		protected static String bannerTotInvoice;
		protected static String Product;
		protected static String ProductQty;


		// 52
		protected static String expenseExisting;
		protected static String btn_ProductRegistrationBtn;
		protected static String txt_ProductRegistrationFilter;
		protected static String btn_WarrentyProfileBtn;
		protected static String txt_WarrentyProfileTxt;
		protected static String sel_WarrentyProfileSel;
		protected static String btn_ProductRegistrationApplyBtn;
		protected static String txt_ProductRegistrationValidation;
		// 53
		protected static String invoiceValue;

		// Verify the ability to draft and release invoice proposal, service invoice to
		// a project -- 54
		protected static String editNa;
		protected static String workBreakdown;
		protected static String addTask;
		protected static String taskName;
		protected static String taskType;
		protected static String inputProducts;
		protected static String searchProduct;
		protected static String productText;
		protected static String selectedVal;
		protected static String estimatedQty;
		protected static String billableCheck;
		protected static String labourNa;
		protected static String serviceProSearch;
		protected static String serviceProText;
		protected static String serviceSelected;
		protected static String EstimatedWorkNew;
		protected static String hoursNa;
		protected static String minutes;
		protected static String okBtn;
		protected static String billableResource;
		protected static String resourceNad;
		protected static String resourceSearch;
		protected static String resourceText;
		protected static String resourceSelected;
		protected static String serviceSearchResource;
		protected static String serviceTextResource;
		protected static String serviceSelectedResource;
		protected static String estimatedResouce;
		protected static String hoursResource;
		protected static String minutesResource;
		protected static String okResource;
		protected static String billableResour;
		protected static String updateNad;
		protected static String expandArrow;
		protected static String releaseTask;
		protected static String updateHeader;
		protected static String invoiceProposal;
		protected static String checkBoxNad;
		protected static String checkoutInvoice;
		protected static String applyNad;
		protected static String serviceDetails;
		protected static String close;
		protected static String link;
		protected static String txt_InvoiceProposalFilter;
		protected static String txt_InvoiceProposalTaskSelAmount;
		protected static String btn_InvoiceProposalProBtn;
		protected static String txt_InvoiceProposalProTxt;
		protected static String sel_InvoiceProposalProSel;
		protected static String btn_ServiceInvoice;
		protected static String txt_ServiceInvoiceActualVal;
		protected static String txt_ServiceInvoiceBaseVal;
		protected static String btn_InvoiceDetailsTab;
		protected static String txt_ProposalDate;
		protected static String txt_ProposalNo;
		protected static String txt_ProposalAmount;
		protected static String txt_InvoiceDate;
		protected static String txt_InvoiceNo;
		protected static String txt_InvoiceAmount;
		

		// Verify the ability of drafting and releasing a project quotation with
		// mandatory data -- 55
		protected static String projectQuatation;
		protected static String newProjectQuatation;
		protected static String searchCus;
		protected static String customerTextQuat;
		protected static String selectedValCus;
		protected static String salesUnit;
		protected static String productSearch;
		protected static String productSearchText;
		protected static String productSelectedVal;
		protected static String productQtyPQ;
		protected static String addNewRecord;
		protected static String productTypeDrop;
		protected static String referenceSearch;
		protected static String referenceText;
		protected static String referenceSelected;
		protected static String productSearchLabour;
		protected static String productTextLabour;
		protected static String serviceSelectedLabour;
		protected static String addNewRecord2;
		protected static String typeDrop;
		protected static String referenceSearchResource;
		protected static String referenceTextResource;
		protected static String referenceResourceSelected;
		protected static String resourceServiceSearch;
		protected static String resourceServiceText;
		protected static String resourceServiceSelected;
		protected static String quotationID;

		// Verify the confirm action for a released project quotation -- 56
		protected static String quotationSearchText;
		protected static String selectedValquot;
		protected static String version;
		protected static String colorSelect;
		protected static String tickVersion;
		protected static String confirm;
		protected static String txt_pageConfirmed;
		protected static String quotationHeaderID;

		// Verify that the confirmed project quotation can be converted into a project
		// -- 57
		protected static String actionMenu;
		protected static String convertToProject;

		protected static String ProjectCodeNew;
		protected static String titleText;
		protected static String projectGrp;
		protected static String searchResponsible;
		protected static String responsiblePersonText;
		protected static String responsiblePersonSelected;
		protected static String searchPricing;
		protected static String searchPricingText;
		protected static String pricingSelected;
		protected static String requestedStartDate;
		protected static String requestedSDate;
		protected static String requestedEndDate;
		protected static String requestedEDate;
		protected static String projectLocationSearch;
		protected static String projectLocationText;
		protected static String projectLocationSelected;
		protected static String inputWarehouse;
		protected static String wipWarehouse;
		
		// smoke 3
		protected static String taskStatus;
		protected static String releaseRelavantTask;
		protected static String draftCheck;
		protected static String accountOwner;

		// smoke 4
		protected static String projectCostEstimation;
		protected static String newProjectCostEstimation;
		protected static String descriptionCostEsti;
		protected static String pricingProfileSearch;
		protected static String pricingProText;
		protected static String pricingProSelected;

		protected static String estimationDetails;
		protected static String inputProductSearch;
		protected static String inputProductMenu;
		protected static String productRelatedPrice;
		protected static String lastPurchasePrice;
		protected static String closeWindow;
		protected static String billableInputProduct;
		protected static String inputProductText;
		protected static String addNewLabour;
		protected static String typeDropdownLabour;
		protected static String LabourProductSearch;
		protected static String billableLabour;
		protected static String addNewResource;
		protected static String resourceDropdown;
		protected static String resourceProductSearch;
		protected static String resourceBillable;
		protected static String addNewExpense;
		protected static String dropDownExpense;
		protected static String expenseProductSearch;
		protected static String expenseCostText;
		protected static String billableExpense;
		protected static String addNewService;
		protected static String serviceDropdown;
		protected static String serviceProductSearch;
		protected static String serviceCostText;
		protected static String billableService;
		protected static String summaryTab;
		protected static String inputProductEstimatedCost;
		protected static String expenseEstimatedCost;
		protected static String serviceProduct;
		
		// smoke 5
		protected static String projectModel;
		protected static String newProjectModel;
		protected static String proModelCode;
		protected static String proModelName;
		protected static String warehouseInput;
		protected static String warehouseWIP;
		
		//smoke 6
		protected static String btn_releaseTaskSubcontract;
		protected static String btn_releaseTaskInterDepartment;
		protected static String txt_POCA2;
		/* Reading the Data to variables */

		// Verify the ability to do cost allocation for project tasks using accrual
		// voucher--030
		protected static String vendorVal;
		protected static String amountVal;
		protected static String percentageVal;
		protected static String analysisCodeVal;
		protected static String costObjectVal;

		// Verify the ability to do cost allocation for project tasks using customer
		// refund--33
		protected static String customerVal;
		protected static String cusRefundAmountVal;
		protected static String customerRefundHeader;

		// Verify that expenses recorded from the customer refund shows in project
		// overview -- 35
		protected static String revenueAdjustment;
		protected static String valOverviewCusRefund;

		// Verify the ability to do cost allocation for project tasks using payment
		// voucher -- 36
		protected static String payeeVal;
		protected static String analysisCodePayVouVal;

		// Verify the ability to do cost allocation for project tasks using customer
		// debit memo--39
		protected static String amountDebitVal;

		// Verify that expenses recorded from the customer debit memo shows in project
		// overview -- 41
		protected static String valOverviewCusDebit;

		// Verify the ability to do cost allocation for project tasks using petty cash
		// --- 45
		protected static String desVal;
		protected static String employeeVal;
		protected static String narrationVal;
		protected static String amountPettyVal;
		protected static String analysisCodePettyCash;

		// Verify the ability to do cost allocation for project tasks using bank
		// adjustment -- 48
		protected static String GLAccountVal;
		public static String minusAmountVal;
		public static String analysisCodeBankAdj;
		protected static String valOverviewVendorRefund;
		

		// 51
		protected static String productSerVal;
		protected static String WarrentyProfileTxt;
		protected static String InvPro1;
		protected static String InvPro2;


		// Verify the ability to draft and release invoice proposal, service invoice to
		// a project -- 54
		protected static String taskNameVal;
		protected static String projectVal;
		protected static String productVal;
		protected static String serviceProTextVal;
		protected static String resourceVal;
		protected static String serviceTextResourceVal;

		// Verify the ability of drafting and releasing a project quotation with
		// mandatory data -- 55
		protected static String customerTextQuatVal;
		protected static String productSearchTextVal;
		protected static String referenceTextVal;
		protected static String productTextLabourVal;
		protected static String referenceTextResourceVal;
		protected static String resourceServiceTextVal;

		// 57
		protected static String titleVal;
		protected static String responsiblePersonVal;
		protected static String pricingVal;
		protected static String locationVal;
		
		//s3
		protected static String projectValTask;
		protected static String taskNameValue;
		
		//s4
		protected static String descriptionValCostEsti;
		protected static String expenseVal;
		protected static String serviceVal;

	//Calling the constructor


	public static void readElementlocators() throws Exception

	{
		//Com_TC_001
		siteLogo= findElementInXLSheet(getParameterProject,"site logo"); 
		txt_username= findElementInXLSheet(getParameterProject,"userName"); 
		txt_password= findElementInXLSheet(getParameterProject,"password"); 
		btn_login= findElementInXLSheet(getParameterProject,"login"); 
		lnk_home=findElementInXLSheet(getParameterProject,"headerlink");

		//Com_TC_002
		btn_nav = findElementInXLSheet(getParameterProject,"navbutton"); 
		sideMenu = findElementInXLSheet(getParameterProject,"sidemenu");
		btn_projbutton = findElementInXLSheet(getParameterProject,"projbutton");
		subsideMenu = findElementInXLSheet(getParameterProject,"subsidemenu");
		
		//Project_TC_001
		btn_projectbutton= findElementInXLSheet(getParameterProject,"projectbutton");
		projectpage= findElementInXLSheet(getParameterProject,"projectpage");
		btn_newprojectbutton= findElementInXLSheet(getParameterProject,"newprojectbutton");
		newprojectpage= findElementInXLSheet(getParameterProject,"newprojectpage");
		txt_ProjectCode= findElementInXLSheet(getParameterProject,"ProjectCode");
		txt_Title= findElementInXLSheet(getParameterProject,"Title");
		txt_ProjectGroup= findElementInXLSheet(getParameterProject,"ProjectGroup");
		btn_PricingProfilebutton= findElementInXLSheet(getParameterProject,"PricingProfilebutton");
		txt_PricingProfiletxt= findElementInXLSheet(getParameterProject,"PricingProfiletxt");
		sel_PricingProfilesel= findElementInXLSheet(getParameterProject,"PricingProfilesel");
		btn_ResponsiblePersonbutton= findElementInXLSheet(getParameterProject,"ResponsiblePersonbutton");
		txt_ResponsiblePersontxt= findElementInXLSheet(getParameterProject,"ResponsiblePersontxt");
		sel_ResponsiblePersonsel= findElementInXLSheet(getParameterProject,"ResponsiblePersonsel");
		txt_AssignedBusinessUnits= findElementInXLSheet(getParameterProject,"AssignedBusinessUnits");
		btn_CustomerAccountbutton= findElementInXLSheet(getParameterProject,"CustomerAccountbutton");
		txt_CustomerAccounttxt= findElementInXLSheet(getParameterProject,"CustomerAccounttxt");
		sel_CustomerAccountsel= findElementInXLSheet(getParameterProject,"CustomerAccountsel");
		btn_ProjectLocationbutton= findElementInXLSheet(getParameterProject,"ProjectLocationbutton");
		txt_ProjectLocationtxt= findElementInXLSheet(getParameterProject,"ProjectLocationtxt");
		sel_ProjectLocationsel= findElementInXLSheet(getParameterProject,"ProjectLocationsel");
		txt_RequestedStartDate= findElementInXLSheet(getParameterProject,"RequestedStartDate");
		sel_RequestedStartDatesel= findElementInXLSheet(getParameterProject,"RequestedStartDatesel");
		btn_draft= findElementInXLSheet(getParameterProject,"draft");
		btn_release= findElementInXLSheet(getParameterProject,"release");
		txt_ActivitiesTabA= findElementInXLSheet(getParameterProject,"ActivitiesTabA");
		
		//Project_TC_002
		btn_edit= findElementInXLSheet(getParameterProject,"edit");
		btn_WorkBreakdown= findElementInXLSheet(getParameterProject,"WorkBreakdown");
		btn_AddTask= findElementInXLSheet(getParameterProject,"AddTask");
		btn_InputProducts= findElementInXLSheet(getParameterProject,"InputProducts");
		txt_TaskName= findElementInXLSheet(getParameterProject,"TaskName");
		txt_TaskType= findElementInXLSheet(getParameterProject,"TaskType");
		btn_productbutton= findElementInXLSheet(getParameterProject,"productbutton");
		txt_producttxt= findElementInXLSheet(getParameterProject,"producttxt");
		sel_productsel= findElementInXLSheet(getParameterProject,"productsel");
		txt_EstimatedQty= findElementInXLSheet(getParameterProject,"EstimatedQty");
		btn_AddProPlusBtn = findElementInXLSheet(getParameterProject,"AddProPlusBtn");
		btn_Labour= findElementInXLSheet(getParameterProject,"Labour");
		btn_ServiceProductbutton= findElementInXLSheet(getParameterProject,"ServiceProductbutton");
		txt_ServiceProducttxt= findElementInXLSheet(getParameterProject,"ServiceProducttxt");
		sel_ServiceProductsel= findElementInXLSheet(getParameterProject,"ServiceProductsel");
		txt_EstimatedWork= findElementInXLSheet(getParameterProject,"EstimatedWork");
		sel_EstimatedWorksel= findElementInXLSheet(getParameterProject,"EstimatedWorksel");
		btn_Resource= findElementInXLSheet(getParameterProject,"Resource");
		btn_Resourcebutton= findElementInXLSheet(getParameterProject,"Resourcebutton");
		txt_Resourcetxt= findElementInXLSheet(getParameterProject,"Resourcetxt");
		sel_Resourcesel= findElementInXLSheet(getParameterProject,"Resourcesel");
		btn_ResourceServiceProductbutton= findElementInXLSheet(getParameterProject,"ResourceServiceProductbutton");
		txt_ResourceEstimatedWork= findElementInXLSheet(getParameterProject,"ResourceEstimatedWork");
		sel_ResourceEstimatedWorksel= findElementInXLSheet(getParameterProject,"ResourceEstimatedWorksel");
		btn_Expense= findElementInXLSheet(getParameterProject,"Expense");
		txt_AnalysisCode= findElementInXLSheet(getParameterProject,"AnalysisCode");
		btn_ExpenseServiceProductbutton= findElementInXLSheet(getParameterProject,"ExpenseServiceProductbutton");
		txt_EstimatedCost= findElementInXLSheet(getParameterProject,"EstimatedCost");
		txt_EstimatedCostAfter= findElementInXLSheet(getParameterProject,"EstimatedCostAfter");
		btn_TaskUpdate= findElementInXLSheet(getParameterProject,"TaskUpdate");
		btn_releaseTaskdropdown= findElementInXLSheet(getParameterProject,"releaseTaskdropdown");
		btn_releaseTask= findElementInXLSheet(getParameterProject,"releaseTask");
		btn_update= findElementInXLSheet(getParameterProject,"update");
		txt_TaskReleaseTxt= findElementInXLSheet(getParameterProject,"TaskReleaseTxt");
		
		//Project_TC_003
		btn_CostingSummary= findElementInXLSheet(getParameterProject,"CostingSummary");
		btn_Material= findElementInXLSheet(getParameterProject,"Material");
		txt_Materialvalue= findElementInXLSheet(getParameterProject,"Materialvalue");
		btn_editTask= findElementInXLSheet(getParameterProject,"editTask");
		btn_productinfo= findElementInXLSheet(getParameterProject,"productinfo");
		btn_ProductRelatedPrice= findElementInXLSheet(getParameterProject,"ProductRelatedPrice");
		txt_LastPurchasePrice= findElementInXLSheet(getParameterProject,"LastPurchasePrice");
		btn_CloseProductRelatedPriceA= findElementInXLSheet(getParameterProject,"CloseProductRelatedPriceA");
		btn_ProductRelatedPriceHeaderclose= findElementInXLSheet(getParameterProject,"ProductRelatedPriceHeaderclose");
		
		//Project_TC_004
		btn_Hours= findElementInXLSheet(getParameterProject,"Hours");
		txt_Hoursvalue= findElementInXLSheet(getParameterProject,"Hoursvalue");
		btn_Orgabutton= findElementInXLSheet(getParameterProject,"Orgabutton");
		btn_EmployeeInformation= findElementInXLSheet(getParameterProject,"EmployeeInformation");
		txt_EmployeeInformationtxt= findElementInXLSheet(getParameterProject,"EmployeeInformationtxt");
		sel_EmployeeInformationsel= findElementInXLSheet(getParameterProject,"EmployeeInformationsel");
		txt_EmployeeInformationTemplate= findElementInXLSheet(getParameterProject,"EmployeeInformationTemplate");
		txt_RateProfiletxt= findElementInXLSheet(getParameterProject,"RateProfiletxt");
		txt_EstimatedWorkhour= findElementInXLSheet(getParameterProject,"EstimatedWorkhour");
		btn_EstimatedWorkhourok= findElementInXLSheet(getParameterProject,"EstimatedWorkhourok");
		txt_projectserch= findElementInXLSheet(getParameterProject,"projectserch");
		sel_projectserchsel= findElementInXLSheet(getParameterProject,"projectserchsel");
		btn_RateProfile= findElementInXLSheet(getParameterProject,"RateProfile");
		txt_RateProfileserch= findElementInXLSheet(getParameterProject,"RateProfileserch");
		txt_ExternalHourlyRate= findElementInXLSheet(getParameterProject,"ExternalHourlyRate");
		
		//Project_TC_005
		btn_ResourceCost= findElementInXLSheet(getParameterProject,"ResourceCost");
		txt_ResourceCostvalue= findElementInXLSheet(getParameterProject,"ResourceCostvalue");
		btn_ResourceInformation= findElementInXLSheet(getParameterProject,"ResourceInformation");
		txt_ResourceInformationtxt= findElementInXLSheet(getParameterProject,"ResourceInformationtxt");
		sel_ResourceInformationsel= findElementInXLSheet(getParameterProject,"ResourceInformationsel");
		
		
		//Project_TC_006
		btn_Expensearrow= findElementInXLSheet(getParameterProject,"Expensearrow");
		txt_Expensevalue= findElementInXLSheet(getParameterProject,"Expensevalue");
		
		//Project_TC_007to008
		txt_txtEmpNo= findElementInXLSheet(getParameterProject,"txtEmpNo");
		txt_EmployeeCode= findElementInXLSheet(getParameterProject,"EmployeeCode");
		txt_EmployeeFullName= findElementInXLSheet(getParameterProject,"EmployeeFullName");
		btn_serbutton= findElementInXLSheet(getParameterProject,"serbutton");
		btn_DailyWorkSheetbutton= findElementInXLSheet(getParameterProject,"DailyWorkSheetbutton");
		txt_DailyWorkSheetpage= findElementInXLSheet(getParameterProject,"DailyWorkSheetpage");
		btn_newDailyWorkSheetbutton= findElementInXLSheet(getParameterProject,"newDailyWorkSheetbutton");
		txt_newDailyWorkSheetpage= findElementInXLSheet(getParameterProject,"newDailyWorkSheetpage");
		btn_DailyWorkSheetProjectbutton= findElementInXLSheet(getParameterProject,"DailyWorkSheet(Project)button");
		btn_AddNewRecord= findElementInXLSheet(getParameterProject,"AddNewRecord");
		txt_projectcode= findElementInXLSheet(getParameterProject,"projectcode");
		txt_ResponsiblePersonText= findElementInXLSheet(getParameterProject,"ResponsiblePersonText");
		btn_JobNobutton= findElementInXLSheet(getParameterProject,"JobNobutton");
		txt_JobNotxt= findElementInXLSheet(getParameterProject,"JobNotxt");
		sel_JobNosel= findElementInXLSheet(getParameterProject,"JobNosel");
		btn_Employeebutton= findElementInXLSheet(getParameterProject,"Employeebutton");
		txt_Employeetxt= findElementInXLSheet(getParameterProject,"Employeetxt");
		sel_Employeesel= findElementInXLSheet(getParameterProject,"Employeesel");
		txt_TimeIn= findElementInXLSheet(getParameterProject,"TimeIn");
		sel_TimeInsel= findElementInXLSheet(getParameterProject,"TimeInsel");
		txt_TimeOut= findElementInXLSheet(getParameterProject,"TimeOut");
		sel_TimeOutsel= findElementInXLSheet(getParameterProject,"TimeOutsel");
		btn_AddNewJobupdate= findElementInXLSheet(getParameterProject,"AddNewJobupdate");
		btn_TimeInOK= findElementInXLSheet(getParameterProject,"TimeInOK");
		btn_TimeOutOK= findElementInXLSheet(getParameterProject,"TimeOutOK");
		btn_HourlyRateBtn= findElementInXLSheet(getParameterProject,"HourlyRateBtn");
		txt_HourlyRateTxt= findElementInXLSheet(getParameterProject,"HourlyRateTxt");
		txt_HoursvalueActual= findElementInXLSheet(getParameterProject,"HoursvalueActual");
		txt_WorkedHours= findElementInXLSheet(getParameterProject,"WorkedHours");
		txt_projectcodeserch= findElementInXLSheet(getParameterProject,"projectcodeserch");
		sel_projectcodeserchsel= findElementInXLSheet(getParameterProject,"projectcodeserchsel");
		btn_Overview= findElementInXLSheet(getParameterProject,"Overview");
		btn_Labourtab= findElementInXLSheet(getParameterProject,"Labourtab");
		txt_OverviewTabLabourActualWork= findElementInXLSheet(getParameterProject,"OverviewTabLabourActualWork");
		txt_ActualWorktxt= findElementInXLSheet(getParameterProject,"ActualWorktxt");
		txt_EstimatedWorktxt= findElementInXLSheet(getParameterProject,"EstimatedWorktxt");
		
		//Project_TC_009to010
		btn_Action= findElementInXLSheet(getParameterProject,"Action");
		btn_ActualUpdate= findElementInXLSheet(getParameterProject,"ActualUpdate");
		btn_ActionResource= findElementInXLSheet(getParameterProject,"ActionResource");
		btn_ActualWork= findElementInXLSheet(getParameterProject,"ActualWork");
		btn_ActualWorksel= findElementInXLSheet(getParameterProject,"ActualWorksel");
		btn_ActualWorkOK= findElementInXLSheet(getParameterProject,"ActualWorkOK");
		btn_ActionActualUpdate= findElementInXLSheet(getParameterProject,"ActionActualUpdate");
		btn_ActionUpdate= findElementInXLSheet(getParameterProject,"ActionUpdate");
		btn_ResourcestabA= findElementInXLSheet(getParameterProject,"ResourcestabA");
		txt_ResourceCostActualValue= findElementInXLSheet(getParameterProject,"ResourceCostActualValue");
		txt_OverviewTabResourceActualWork= findElementInXLSheet(getParameterProject,"OverviewTabResourceActualWork");
		
		//Project_TC_011
		btn_CreateInternalOrder= findElementInXLSheet(getParameterProject,"CreateInternalOrder");
		btn_Taskdropdown= findElementInXLSheet(getParameterProject,"Taskdropdown");
		btn_checkbox= findElementInXLSheet(getParameterProject,"checkbox");
		btn_Apply= findElementInXLSheet(getParameterProject,"Apply");
		btn_Gotopage= findElementInXLSheet(getParameterProject,"Gotopage");
		btn_ActionUpdateclose= findElementInXLSheet(getParameterProject,"ActionUpdateclose");
		btn_IDOMasterinfoBtn= findElementInXLSheet(getParameterProject,"IDOMasterinfoBtn");
		txt_IDOBatchNoPro1= findElementInXLSheet(getParameterProject,"IDOBatchNoPro1");
		txt_IDOSerialNoPro2= findElementInXLSheet(getParameterProject,"IDOSerialNoPro2");
		
		//Project_TC_012to013
		txt_ActualQty= findElementInXLSheet(getParameterProject,"ActualQty");
		btn_ActualUpdateSerialBatchNoSelBtn= findElementInXLSheet(getParameterProject,"ActualUpdateSerialBatchNoSelBtn");
		sel_ActualUpdateSerialNoPopupSel= findElementInXLSheet(getParameterProject,"ActualUpdateSerialNoPopupSel");
		btn_ActualUpdateclose= findElementInXLSheet(getParameterProject,"ActualUpdateclose");
		btn_productinfoclose= findElementInXLSheet(getParameterProject,"productinfoclose");
		btn_AddTaskclose= findElementInXLSheet(getParameterProject,"AddTaskclose");
		txt_ActualQtybox= findElementInXLSheet(getParameterProject,"ActualQtybox");
		txt_EstimatedQtybox= findElementInXLSheet(getParameterProject,"EstimatedQtybox");
		btn_InputProductstab= findElementInXLSheet(getParameterProject,"InputProductstab");
		txt_MaterialActualvalue= findElementInXLSheet(getParameterProject,"MaterialActualvalue");
		txt_EstimatedQtyfiled= findElementInXLSheet(getParameterProject,"EstimatedQtyfiled");
		
		//Project_TC_014
		btn_InventoryWarehousing= findElementInXLSheet(getParameterProject,"Inventory&Warehousing");
		btn_InternalReturnOrder= findElementInXLSheet(getParameterProject,"InternalReturnOrder");
		btn_newInternalReturnOrder= findElementInXLSheet(getParameterProject,"newInternalReturnOrder");
		btn_WIPReturns= findElementInXLSheet(getParameterProject,"WIPReturns");
		btn_documentMark= findElementInXLSheet(getParameterProject,"documentMark");
		txt_InternalDispatchOrdercode= findElementInXLSheet(getParameterProject,"InternalDispatchOrdercode");
		txt_ToWarehouse= findElementInXLSheet(getParameterProject,"ToWarehouse");
		txt_CommonSearch= findElementInXLSheet(getParameterProject,"CommonSearch");
		btn_Search= findElementInXLSheet(getParameterProject,"Search");
		btn_checkboxIRO= findElementInXLSheet(getParameterProject,"checkboxIRO");
		btn_ApplyIRO= findElementInXLSheet(getParameterProject,"ApplyIRO");
		btn_draftIR= findElementInXLSheet(getParameterProject,"draftIR");
		btn_releaseIR= findElementInXLSheet(getParameterProject,"releaseIR");
		btn_SerialBatchIR= findElementInXLSheet(getParameterProject,"SerialBatchIR");
		btn_SerialBatchbutton= findElementInXLSheet(getParameterProject,"SerialBatchbutton");
		btn_SerialBatchsel= findElementInXLSheet(getParameterProject,"SerialBatchsel");
		txt_SerialBatchqtyfield= findElementInXLSheet(getParameterProject,"SerialBatchqtyfield");
		txt_SerialBatchqtyfield1= findElementInXLSheet(getParameterProject,"SerialBatchqtyfield1");
		btn_SerialBatchrefresh= findElementInXLSheet(getParameterProject,"SerialBatchrefresh");
		btn_SerialBatchback= findElementInXLSheet(getParameterProject,"SerialBatchback");
		btn_arrowdown= findElementInXLSheet(getParameterProject,"arrowdown");
		
		
		//Project_TC_015
		btn_UpdateTaskPOC= findElementInXLSheet(getParameterProject,"UpdateTaskPOC");
		txt_cboxTaskPOC= findElementInXLSheet(getParameterProject,"cboxTaskPOC");
		txt_TskPOC= findElementInXLSheet(getParameterProject,"TskPOC");
		btn_POCApply= findElementInXLSheet(getParameterProject,"POCApply");
		
		//Project_TC_016 19/11/20
		btn_CompleteA= findElementInXLSheet(getParameterProject,"CompleteA");
		btn_YesA= findElementInXLSheet(getParameterProject,"YesA");
		btn_PostDateA= findElementInXLSheet(getParameterProject,"PostDateA");
		sel_PostDateselA= findElementInXLSheet(getParameterProject,"PostDateselA");
		btn_checkboxA= findElementInXLSheet(getParameterProject,"checkboxA");
		
		//Project_TC_017 19/11/20
		txt_ContractTypeA= findElementInXLSheet(getParameterProject,"ContractTypeA");
		btn_SubContractServicesA= findElementInXLSheet(getParameterProject,"SubContractServicesA");
		btn_SubproductbuttonA= findElementInXLSheet(getParameterProject,"SubproductbuttonA");
		txt_SubEstimatedCostA= findElementInXLSheet(getParameterProject,"SubEstimatedCostA");
		btn_BillableA= findElementInXLSheet(getParameterProject,"BillableA");

		//Project_TC_018 19/11/20
		btn_SubContractA= findElementInXLSheet(getParameterProject,"SubContractA");
		txt_SubContractvalueA= findElementInXLSheet(getParameterProject,"SubContractvalueA");
		
		
		//Project_TC_017to019
		btn_SubContractOrderA= findElementInXLSheet(getParameterProject,"SubContractOrderA");
		txt_subTaskA= findElementInXLSheet(getParameterProject,"subTaskA");
		btn_subVendorbuttonA= findElementInXLSheet(getParameterProject,"subVendorbuttonA");
		txt_subVendortxtA= findElementInXLSheet(getParameterProject,"subVendortxtA");
		sel_subVendorselA= findElementInXLSheet(getParameterProject,"subVendorselA");
		btn_subcheckboxA= findElementInXLSheet(getParameterProject,"subcheckboxA");
		btn_CheckoutA= findElementInXLSheet(getParameterProject,"CheckoutA");
		txt_VendorReferenceNoA= findElementInXLSheet(getParameterProject,"VendorReferenceNoA");
		PurchaseOrderpageA= findElementInXLSheet(getParameterProject,"PurchaseOrderpageA");
		PurchaseInvoicepageA= findElementInXLSheet(getParameterProject,"PurchaseInvoicepageA");
		txt_SubContractActualcost= findElementInXLSheet(getParameterProject,"SubContractActualcost");
		txt_SubContractvalueActualA= findElementInXLSheet(getParameterProject,"SubContractvalueActualA");
		
		//Project_TC_021 19/11/24
		btn_SubContractServicestabA= findElementInXLSheet(getParameterProject,"SubContractServicestabA");
		txt_OrderDateA= findElementInXLSheet(getParameterProject,"OrderDateA");
		txt_OrderNoA= findElementInXLSheet(getParameterProject,"OrderNoA");
		txt_InvoiceDateA= findElementInXLSheet(getParameterProject,"InvoiceDateA");
		txt_InvoiceNoA= findElementInXLSheet(getParameterProject,"InvoiceNoA");
		txt_ProductA= findElementInXLSheet(getParameterProject,"ProductA");
		txt_EstimatedAmountA= findElementInXLSheet(getParameterProject,"EstimatedAmountA");
		txt_ActualAmountA= findElementInXLSheet(getParameterProject,"ActualAmountA");
		
		
		//Project_TC_022 19/11/25
		btn_InterDepartmentA= findElementInXLSheet(getParameterProject,"InterDepartmentA");
		btn_OwnerbuttonA= findElementInXLSheet(getParameterProject,"OwnerbuttonA");
		txt_OwnertxtA= findElementInXLSheet(getParameterProject,"OwnertxtA");
		sel_OwnerselA= findElementInXLSheet(getParameterProject,"OwnerselA");
		txt_EstimatedCostA= findElementInXLSheet(getParameterProject,"EstimatedCostA");
		txt_EstimatedDateA= findElementInXLSheet(getParameterProject,"EstimatedDateA");
		sel_EstimatedDateselA= findElementInXLSheet(getParameterProject,"EstimatedDateselA");
		
		//Project_TC_023 19/11/25
		btn_ServiceCalendarA= findElementInXLSheet(getParameterProject,"ServiceCalendarA");
		txt_interdepartmentjobA= findElementInXLSheet(getParameterProject,"interdepartmentjobA");
		
		//Project_TC_024 19/11/25
		txt_ServiceJobOrderpageA= findElementInXLSheet(getParameterProject,"ServiceJobOrderpageA");
		
		
		//Project_TC_025 19/11/25
		txt_ServiceGroupA= findElementInXLSheet(getParameterProject,"ServiceGroupA");
		txt_PriorityA= findElementInXLSheet(getParameterProject,"PriorityA");
		btn_PricingProfilebuttonA= findElementInXLSheet(getParameterProject,"PricingProfilebuttonA");
		txt_PricingProfiletxtA= findElementInXLSheet(getParameterProject,"PricingProfiletxtA");
		sel_PricingProfileselA= findElementInXLSheet(getParameterProject,"PricingProfileselA");
		btn_ServiceLocationbuttonA= findElementInXLSheet(getParameterProject,"ServiceLocationbuttonA");
		txt_ServiceLocationtxtA= findElementInXLSheet(getParameterProject,"ServiceLocationtxtA");
		sel_ServiceLocationselA= findElementInXLSheet(getParameterProject,"ServiceLocationselA");
		btn_PerformerbuttonA= findElementInXLSheet(getParameterProject,"PerformerbuttonA");
		txt_PerformertxtA= findElementInXLSheet(getParameterProject,"PerformertxtA");
		sel_PerformerselA= findElementInXLSheet(getParameterProject,"PerformerselA");
		txt_OrderDescriptionA= findElementInXLSheet(getParameterProject,"OrderDescriptionA");
		
		//Project_TC_026 19/11/25
		btn_InterDepartmenttabA= findElementInXLSheet(getParameterProject,"InterDepartmenttabA");
		txt_InterDepartmentestimatedcostA= findElementInXLSheet(getParameterProject,"InterDepartmentestimatedcostA");
		
		//Project_TC_027 19/11/25
		btn_TaskcloseA= findElementInXLSheet(getParameterProject,"TaskcloseA");
		btn_FinanceA= findElementInXLSheet(getParameterProject,"FinanceA");
		btn_OutboundPaymentAdviceA= findElementInXLSheet(getParameterProject,"OutboundPaymentAdviceA");
		txt_OutboundPaymentAdvicepageA= findElementInXLSheet(getParameterProject,"OutboundPaymentAdvicepageA");
		btn_NewOutboundPaymentAdviceA= findElementInXLSheet(getParameterProject,"NewOutboundPaymentAdviceA");
		btn_AccrualVoucherA= findElementInXLSheet(getParameterProject,"AccrualVoucherA");
		btn_AccrualVoucherVendorA= findElementInXLSheet(getParameterProject,"AccrualVoucherVendorA");
		txt_AccrualVoucherVendortxtA= findElementInXLSheet(getParameterProject,"AccrualVoucherVendortxtA");
		sel_AccrualVoucherVendorselA= findElementInXLSheet(getParameterProject,"AccrualVoucherVendorselA");
		txt_AnalysisCodeA= findElementInXLSheet(getParameterProject,"AnalysisCodeA");
		txt_AmountA= findElementInXLSheet(getParameterProject,"AmountA");
		btn_costallocationA= findElementInXLSheet(getParameterProject,"costallocationA");
		txt_PercentageA= findElementInXLSheet(getParameterProject,"PercentageA");
		txt_CostAssignmentTypeA= findElementInXLSheet(getParameterProject,"CostAssignmentTypeA");
		btn_CostObjectbuttonA= findElementInXLSheet(getParameterProject,"CostObjectbuttonA");
		txt_CostObjecttxtA= findElementInXLSheet(getParameterProject,"CostObjecttxtA");
		sel_CostObjectselA= findElementInXLSheet(getParameterProject,"CostObjectselA");
		btn_AddRecordA= findElementInXLSheet(getParameterProject,"AddRecordA");
		btn_UpdateA= findElementInXLSheet(getParameterProject,"UpdateA");
		txt_AccrualVoucherVendorReferenceNoA= findElementInXLSheet(getParameterProject,"AccrualVoucherVendorReferenceNoA");
		
		//Project_TC_028 19/11/26
		txt_InterDepartmentActualvalueA= findElementInXLSheet(getParameterProject,"InterDepartmentActualvalueA");
		
		//Project_TC_029 19/11/26
		btn_ServiceJobOrderA= findElementInXLSheet(getParameterProject,"ServiceJobOrderA");
		txt_ServiceJobOrderserchA= findElementInXLSheet(getParameterProject,"ServiceJobOrderserchA");
		sel_ServiceJobOrderselA= findElementInXLSheet(getParameterProject,"ServiceJobOrderselA");
		btn_AddProductTagNoA= findElementInXLSheet(getParameterProject,"AddProductTagNoA");
		btn_SpnSeriaA= findElementInXLSheet(getParameterProject,"SpnSeriaA");
		btn_SerialNosA= findElementInXLSheet(getParameterProject,"SerialNosA");
		btn_AddProductTagNoapplyA= findElementInXLSheet(getParameterProject,"AddProductTagNoapplyA");
		btn_StopA= findElementInXLSheet(getParameterProject,"StopA");
		txt_POCA= findElementInXLSheet(getParameterProject,"POCA");
		txt_pageCompleted= findElementInXLSheet(getParameterProject,"pageCompleted");

		//product7
		pro1A= findElementInXLSheet(getParameterProject,"pro1A");
		plus1A= findElementInXLSheet(getParameterProject,"plus1A");
		
		pro2A= findElementInXLSheet(getParameterProject,"pro2A");
		plus2A= findElementInXLSheet(getParameterProject,"plus2A");
		
		pro3A= findElementInXLSheet(getParameterProject,"pro3A");
		plus3A= findElementInXLSheet(getParameterProject,"plus3A");
		
		pro4A= findElementInXLSheet(getParameterProject,"pro4A");
		plus4A= findElementInXLSheet(getParameterProject,"plus4A");
		
		pro5A= findElementInXLSheet(getParameterProject,"pro5A");
		plus5A= findElementInXLSheet(getParameterProject,"plus5A");
		
		pro6A= findElementInXLSheet(getParameterProject,"pro6A");
		plus6A= findElementInXLSheet(getParameterProject,"plus6A");
		
		pro7A= findElementInXLSheet(getParameterProject,"pro7A");
		plus7A= findElementInXLSheet(getParameterProject,"plus7A");
		
		EstimatedQty2A= findElementInXLSheet(getParameterProject,"EstimatedQty2A");
		EstimatedQty3A= findElementInXLSheet(getParameterProject,"EstimatedQty3A");
		EstimatedQty4A= findElementInXLSheet(getParameterProject,"EstimatedQty4A");
		EstimatedQty5A= findElementInXLSheet(getParameterProject,"EstimatedQty5A");
		EstimatedQty6A= findElementInXLSheet(getParameterProject,"EstimatedQty6A");
		EstimatedQty7A= findElementInXLSheet(getParameterProject,"EstimatedQty7A");
		
		createGridMasterInfo4A= findElementInXLSheet(getParameterProject,"createGridMasterInfo4A");
		createGridMasterInfo5A= findElementInXLSheet(getParameterProject,"createGridMasterInfo5A");
		ProductBatchSerialWiseCostA= findElementInXLSheet(getParameterProject,"ProductBatchSerialWiseCostA");
		ProductBatchSerialWiseCostSelA= findElementInXLSheet(getParameterProject,"ProductBatchSerialWiseCostSelA");

		
		SerialBatchA= findElementInXLSheet(getParameterProject,"SerialBatchA");
		Cap4A= findElementInXLSheet(getParameterProject,"Cap4A");
		Cap5A= findElementInXLSheet(getParameterProject,"Cap5A");
		SerialRangeA= findElementInXLSheet(getParameterProject,"SerialRangeA");
		SerialFromNuA= findElementInXLSheet(getParameterProject,"SerialFromNuA");
		updateSerialA= findElementInXLSheet(getParameterProject,"updateSerialA");
		closeProductListA= findElementInXLSheet(getParameterProject,"closeProductListA");
		headercloseA= findElementInXLSheet(getParameterProject,"headercloseA");
		
		openSerialNoSelection4A= findElementInXLSheet(getParameterProject,"openSerialNoSelection4A");
		openSerialNoSelection5A= findElementInXLSheet(getParameterProject,"openSerialNoSelection5A");
		captureQtyA= findElementInXLSheet(getParameterProject,"captureQtyA");
		ActualSerialUpdate= findElementInXLSheet(getParameterProject,"ActualSerialUpdate");
		ActualSerialYesA= findElementInXLSheet(getParameterProject,"ActualSerialYesA");
		
		createGridMasterInfo1A= findElementInXLSheet(getParameterProject,"createGridMasterInfo1A");
		openSerialNoSelection1A= findElementInXLSheet(getParameterProject,"openSerialNoSelection1A");
		ProductBatchSelA= findElementInXLSheet(getParameterProject,"ProductBatchSelA");
		Cap1A= findElementInXLSheet(getParameterProject,"Cap1A");
		BatchFromNuA= findElementInXLSheet(getParameterProject,"BatchFromNuA");

		ProductBatchSerialWiseCostSel2A= findElementInXLSheet(getParameterProject,"ProductBatchSerialWiseCostSel2A");
		
		openSerialNoSelection4_2A= findElementInXLSheet(getParameterProject,"openSerialNoSelection4_2A");
		openSerialNoSelection5_2A= findElementInXLSheet(getParameterProject,"openSerialNoSelection5_2A");
		openSerialNoSelection1_2A= findElementInXLSheet(getParameterProject,"openSerialNoSelection1_2A");
		
		captureQty2A= findElementInXLSheet(getParameterProject,"captureQty2A");
		
		ActualQty1= findElementInXLSheet(getParameterProject,"ActualQty1");
		ActualQty2= findElementInXLSheet(getParameterProject,"ActualQty2");
		ActualQty3= findElementInXLSheet(getParameterProject,"ActualQty3");
		ActualQty6= findElementInXLSheet(getParameterProject,"ActualQty6");
		ActualQty7= findElementInXLSheet(getParameterProject,"ActualQty7");
		
		Cap2A= findElementInXLSheet(getParameterProject,"Cap2A");
		Cap3A= findElementInXLSheet(getParameterProject,"Cap3A");
		Cap7A= findElementInXLSheet(getParameterProject,"Cap7A");
		
		pageDraft= findElementInXLSheet(getParameterProject,"pageDraft");
		pageRelease= findElementInXLSheet(getParameterProject,"pageRelease");
		pageDraftNew= findElementInXLSheet(getParameterProject,"pageDraftNew");
		pageReleaseNew= findElementInXLSheet(getParameterProject,"pageReleaseNew");

		Status= findElementInXLSheet(getParameterProject,"Status");
		Header= findElementInXLSheet(getParameterProject,"Header");

		//Master
		btn_ProjectParametersA= findElementInXLSheet(getParameterProject,"ProjectParametersA");
		btn_SaveA= findElementInXLSheet(getParameterProject,"SaveA");
		txt_validatorA= findElementInXLSheet(getParameterProject,"validatorA");
		
		//SetUp_1
		btn_ProjectGroupConfigurationA= findElementInXLSheet(getParameterProject,"ProjectGroupConfigurationA");
		btn_NewConfigurationA= findElementInXLSheet(getParameterProject,"NewConfigurationA");
		txt_ConfigCodeA= findElementInXLSheet(getParameterProject,"ConfigCodeA");
		txt_ProGroupA= findElementInXLSheet(getParameterProject,"ProGroupA");
		btn_ProjectGroupConfigurationDraftA= findElementInXLSheet(getParameterProject,"ProjectGroupConfigurationDraftA");
		btn_ProjectGroupConfigurationEditA= findElementInXLSheet(getParameterProject,"ProjectGroupConfigurationEditA");
		btn_ProjectGroupConfigurationUpdateA= findElementInXLSheet(getParameterProject,"ProjectGroupConfigurationUpdateA");
		txt_ProjectGroupConfigurationSerchA= findElementInXLSheet(getParameterProject,"ProjectGroupConfigurationSerchA");
		btn_AddProGroupA= findElementInXLSheet(getParameterProject,"AddProGroupA");
		btn_AddNewProGroupA= findElementInXLSheet(getParameterProject,"AddNewProGroupA");
		txt_AddNewProGrouptxtA= findElementInXLSheet(getParameterProject,"AddNewProGrouptxtA");
		btn_ProGroupUpdateA= findElementInXLSheet(getParameterProject,"ProGroupUpdateA");
		
		//Transaction_1
		txt_CopyFromTextPCE= findElementInXLSheet(getParameterProject,"CopyFromTextPCE");
		//Transaction_2
		txt_CopyFromTextPQ= findElementInXLSheet(getParameterProject,"CopyFromTextPQ");
		
		//Transaction_3
		txt_ActualUpdatetxtA= findElementInXLSheet(getParameterProject,"ActualUpdatetxtA");
		txt_CreateInternalOrdertxtA= findElementInXLSheet(getParameterProject,"CreateInternalOrdertxtA");
		btn_ChangeRetentionA= findElementInXLSheet(getParameterProject,"ChangeRetentionA");
		txt_ChangeRetentiontxtA= findElementInXLSheet(getParameterProject,"ChangeRetentiontxtA");
		btn_GenerateCustomerAdvanceA= findElementInXLSheet(getParameterProject,"GenerateCustomerAdvanceA");
		txt_GenerateCustomerAdvancetxtA= findElementInXLSheet(getParameterProject,"GenerateCustomerAdvancetxtA");
		btn_PendingApprovalA= findElementInXLSheet(getParameterProject,"PendingApprovalA");
		txt_PendingApprovaltxtA= findElementInXLSheet(getParameterProject,"PendingApprovaltxtA");
		btn_ProductRegistrationA= findElementInXLSheet(getParameterProject,"ProductRegistrationA");
		txt_ProductRegistrationtxtA= findElementInXLSheet(getParameterProject,"ProductRegistrationtxtA");
		txt_SubContractOrdertxtA= findElementInXLSheet(getParameterProject,"SubContractOrdertxtA");
		btn_TaskDetailAllocationA= findElementInXLSheet(getParameterProject,"TaskDetailAllocationA");
		txt_TaskDetailAllocationtxtA= findElementInXLSheet(getParameterProject,"TaskDetailAllocationtxtA");
		btn_UpdateTaskDetailsA= findElementInXLSheet(getParameterProject,"UpdateTaskDetailsA");
		txt_UpdateTaskDetailstxtA= findElementInXLSheet(getParameterProject,"UpdateTaskDetailstxtA");
		txt_UpdateTaskPOCtxtA= findElementInXLSheet(getParameterProject,"UpdateTaskPOCtxtA");
		btn_VendorAdvanceA= findElementInXLSheet(getParameterProject,"VendorAdvanceA");
		txt_VendorAdvancetxtA= findElementInXLSheet(getParameterProject,"VendorAdvancetxtA");
		txt_ReverseDateA= findElementInXLSheet(getParameterProject,"ReverseDateA");
		btn_ReverseA= findElementInXLSheet(getParameterProject,"ReverseA");
		btn_ReversebuttonA= findElementInXLSheet(getParameterProject,"ReversebuttonA");
		btn_NoA= findElementInXLSheet(getParameterProject,"NoA");
		btn_DraftAndNewA= findElementInXLSheet(getParameterProject,"Draft&NewA");
		btn_UpdateAndNewA= findElementInXLSheet(getParameterProject,"Update&NewA");
		btn_DuplicateA= findElementInXLSheet(getParameterProject,"DuplicateA");
		btn_CopyFromA= findElementInXLSheet(getParameterProject,"CopyFromA");
		txt_CopyFromTextA= findElementInXLSheet(getParameterProject,"CopyFromTextA");
		btn_headercloseA= findElementInXLSheet(getParameterProject,"headercloseA");
		btn_DeleteA= findElementInXLSheet(getParameterProject,"DeleteA");
		txt_DeleteTextA= findElementInXLSheet(getParameterProject,"DeleteTextA");

		//SetUp_2
		txt_CopyFromTextPMA= findElementInXLSheet(getParameterProject,"CopyFromTextPMA");

		
		/*==========================================================================================*/
		financeModule = findElementInXLSheet(getParameterProject, "finance Module");
		searchBar = findElementInXLSheet(getParameterProject, "search Bar");
		
		releaseInHeader = findElementInXLSheet(getParameterProject, "release In Header");
		
		projectModule = findElementInXLSheet(getParameterProject, "project Module");
		newProject = findElementInXLSheet(getParameterProject, "new Project");

		// Verify the ability to do cost allocation for project tasks using accrual
				// voucher--030

				finance = findElementInXLSheet(getParameterProject, "finance");
				outboundPaymentAdvice = findElementInXLSheet(getParameterProject, "outbound Payment Advice");
				newOutboundPaymentAdvice = findElementInXLSheet(getParameterProject, "new Outbound Payment Advice");
				// journeyHeader = findElementInXLSheet(getParameterProject,"journey Header");
				accuralVoucher = findElementInXLSheet(getParameterProject, "accural Voucher");
				vendorSearch = findElementInXLSheet(getParameterProject, "vendor Search");
				vendorText = findElementInXLSheet(getParameterProject, "vendor Text");
				vendorSelectedVal = findElementInXLSheet(getParameterProject, "vendor Selected Val");
				analysisCodeDrop = findElementInXLSheet(getParameterProject, "analysis Code Drop");
				amountText = findElementInXLSheet(getParameterProject, "amount Text");
				costAllocationBtn = findElementInXLSheet(getParameterProject, "cost Allocation Btn");
				precentageText = findElementInXLSheet(getParameterProject, "percentage Text");
				costAssignmentTypeNad = findElementInXLSheet(getParameterProject, "cost Assignment Type");
				costObjectSearch = findElementInXLSheet(getParameterProject, "cost Object Search");
				costObjectText = findElementInXLSheet(getParameterProject, "cost Object Text");
				costObjectSelected = findElementInXLSheet(getParameterProject, "cost Object Selected");
				addRecord = findElementInXLSheet(getParameterProject, "add Record");
				updateCostAllocation = findElementInXLSheet(getParameterProject, "update Cost Allocation");
				vendorRef = findElementInXLSheet(getParameterProject, "vendor Ref");
				checkout = findElementInXLSheet(getParameterProject, "checkout");
				draftNad = findElementInXLSheet(getParameterProject, "draft Nad");
				releaseNad = findElementInXLSheet(getParameterProject, "release Nad");
				documentVal = findElementInXLSheet(getParameterProject, "document Val");

				// Verify that expenses recorded from the accrual voucher shows in project
				// costing summary -- 31
				project = findElementInXLSheet(getParameterProject, "project");
				projectSearchText = findElementInXLSheet(getParameterProject, "project Search Text");
				projectSelectedVal = findElementInXLSheet(getParameterProject, "project Selected Val");
				costingSummaryTab = findElementInXLSheet(getParameterProject, "costing Summary Tab");
				expenseArrow = findElementInXLSheet(getParameterProject, "expense Arrow");
				existingVal = findElementInXLSheet(getParameterProject, "existing Val");
				OtherServicesexistingVal = findElementInXLSheet(getParameterProject, "OtherServicesexistingVal");

				// Verify that expenses recorded from the accrual voucher shows in project
				// overview -- 32
				accrualHeader = findElementInXLSheet(getParameterProject, "accrual Header");
				valPre = findElementInXLSheet(getParameterProject, "val Pre");

				// Verify the ability to do cost allocation for project tasks using customer
				// refund--33
				downArrow = findElementInXLSheet(getParameterProject, "down Arrow");
				customerRefund = findElementInXLSheet(getParameterProject, "customer Refund");
				customerSearch = findElementInXLSheet(getParameterProject, "customer Search");
				customerText = findElementInXLSheet(getParameterProject, "customer Text");
				customerSelectedVal = findElementInXLSheet(getParameterProject, "customer Selected Val");
				customerRefundHeader = findElementInXLSheet(getParameterProject, "customer Refund Header");

				// Verify that expenses recorded from the customer refund shows in project
				// costing summary -- 34
				adjustment = findElementInXLSheet(getParameterProject, "adjustment");
				cusRefundCostingSum = findElementInXLSheet(getParameterProject, "cus Refund Costing Sum");

				// Verify that expenses recorded from the customer refund shows in project
				// overview -- 35
				revenueAdjustment = findElementInXLSheet(getParameterProject, "revenue Adjustment");
				valOverviewCusRefund = findElementInXLSheet(getParameterProject, "val Overview Cus Refund");

				// Verify the ability to do cost allocation for project tasks using payment
				// voucher -- 36

				paymentVoucher = findElementInXLSheet(getParameterProject, "payment Voucher");
				convertToOutboundPayment = findElementInXLSheet(getParameterProject, "convert To Outbound Payment");
				payeeText = findElementInXLSheet(getParameterProject, "payee Text");
				detailsTab = findElementInXLSheet(getParameterProject, "details Tab");
				OutboundPaymentDocSel = findElementInXLSheet(getParameterProject, "OutboundPaymentDocSel");
				paymentVoucherHeader = findElementInXLSheet(getParameterProject, "payment Voucher Header");

				// Verify that expenses recorded from the payment voucher shows in project
				// costing summary -- 37
				projectInExpense = findElementInXLSheet(getParameterProject, "project In Expense");
				projectInExpenseVal = findElementInXLSheet(getParameterProject, "project In Expense Val");

				// Verify that expenses recorded from the payment voucher shows in project
				// overview -- 38
				val = findElementInXLSheet(getParameterProject, "val");

				// Verify the ability to do cost allocation for project tasks using customer
				// debit memo--39
				inboundPaymentAdvice = findElementInXLSheet(getParameterProject, "inbound Payment Advice");
				txt_InboundPaymentAdvicepageA = findElementInXLSheet(getParameterProject, "InboundPaymentAdvicepageA");
				newInboundPaymentAdvice = findElementInXLSheet(getParameterProject, "new Inbound Payment Advice");
				customerDebitMemo = findElementInXLSheet(getParameterProject, "customer Debit Memo");
				customerSearchDebit = findElementInXLSheet(getParameterProject, "customer Search Debit");
				amountDebitText = findElementInXLSheet(getParameterProject, "amount Debit Text");
				CustomerRef = findElementInXLSheet(getParameterProject, "CustomerRef");
				costAllocation = findElementInXLSheet(getParameterProject, "cost Allocation");

				// 40
				cusDebitMemoSum = findElementInXLSheet(getParameterProject, "cus Debit Memo Sum");

				// Verify that expenses recorded from the customer debit memo shows in project
				// overview -- 41
				customerDebitHeader = findElementInXLSheet(getParameterProject, "customer Debit Header");
				valOverviewCusDebit = findElementInXLSheet(getParameterProject, "val Overview Cus Debit");

				// Verify the ability to do cost allocation for project tasks using vendor
				// refund --- 42
				vendorRefund = findElementInXLSheet(getParameterProject, "vendor Refund");
				btn_VRVendorSearchBtn = findElementInXLSheet(getParameterProject, "VRVendorSearchBtn");
				txt_VRVendorSearchTxt = findElementInXLSheet(getParameterProject, "VRVendorSearchTxt");
				sel_VRVendorSearchSel = findElementInXLSheet(getParameterProject, "VRVendorSearchSel");
				precentageTextVendor = findElementInXLSheet(getParameterProject, "precentage Text Vendor");
				costAllocationBtnVendor = findElementInXLSheet(getParameterProject, "cost Allocation Btn Vendor");
				
				//43
				//43
				vendorRefundCosting = findElementInXLSheet(getParameterProject, "vendor Refund Costing");
				costsAdjustment = findElementInXLSheet(getParameterProject, "costs Adjustment");

				//
				costAdjustment = findElementInXLSheet(getParameterProject, "cost Adjustment");
				vendorRefundHeader = findElementInXLSheet(getParameterProject, "vendor Refund Header");
				valOverviewVendorRefund = findElementInXLSheet(getParameterProject, "val Overview Vendor Refund");

				// Verify the ability to do cost allocation for project tasks using petty cash
				// --- 45

				pettyCash = findElementInXLSheet(getParameterProject, "petty Cash");
				newPettyCash = findElementInXLSheet(getParameterProject, "new Petty Cash");
				pettyCashAccount = findElementInXLSheet(getParameterProject, "petty Cash Account");
				descriptionPetty = findElementInXLSheet(getParameterProject, "description Petty");
				paidtoSearch = findElementInXLSheet(getParameterProject, "paidto Search");
				employeeSearchText = findElementInXLSheet(getParameterProject, "employee Search Text");
				empSelectedVal = findElementInXLSheet(getParameterProject, "emp Selected Val");
				type = findElementInXLSheet(getParameterProject, "type");
				IOUNoSearch = findElementInXLSheet(getParameterProject, "IOUNo Search");
				narration = findElementInXLSheet(getParameterProject, "narration");
				amountTextPetty = findElementInXLSheet(getParameterProject, "amount Text Petty");
				costAllocationPetty = findElementInXLSheet(getParameterProject, "cost Allocation Petty");

				// Verify that expenses recorded from the petty cash shows in project overview
				// -- 47
				pettyCashHeader = findElementInXLSheet(getParameterProject, "petty Cash Header");
				valOverviewPetty = findElementInXLSheet(getParameterProject, "val Overview Petty");

				// Verify the ability to do cost allocation for project tasks using bank
				// adjustment -- 48
				bankAdjustment = findElementInXLSheet(getParameterProject, "bank Adjustment");
				newBankAdjustment = findElementInXLSheet(getParameterProject, "new Bank Adjustment");
				selectBank = findElementInXLSheet(getParameterProject, "select Bank");
				bankAccountNo = findElementInXLSheet(getParameterProject, "bank Account No");
				analysisCode = findElementInXLSheet(getParameterProject, "analysis Code");
				GLAccountSearch = findElementInXLSheet(getParameterProject, "GLAccount Search");
				GLAccountText = findElementInXLSheet(getParameterProject, "GLAccount Text");
				GLAccountSelected = findElementInXLSheet(getParameterProject, "GLAccount Selected");
				costAllocate = findElementInXLSheet(getParameterProject, "cost Allocate");
				amountTextt = findElementInXLSheet(getParameterProject, "amount Textt");
				valOverviewBA = findElementInXLSheet(getParameterProject, "val Overview BA");

				// Verify that expenses recorded from the bank adjustment shows in project
				// overview -- 50
				overviewNad = findElementInXLSheet(getParameterProject, "overview Nad");
				expenses = findElementInXLSheet(getParameterProject, "expenses");
				valueNad = findElementInXLSheet(getParameterProject, "value Nad");
				amountCheck = findElementInXLSheet(getParameterProject, "amount Check");

				// Verify the ability to do cost allocation for project tasks using purchase
				// invoice -- 51

				procument = findElementInXLSheet(getParameterProject, "procument");
				purchaseInvoice = findElementInXLSheet(getParameterProject, "purchase Invoice");
				newPurchaseInvoice = findElementInXLSheet(getParameterProject, "new Purchase Invoice");
				purchaseInvoiceService = findElementInXLSheet(getParameterProject, "purchase Invoice Service");
				vendorSearchBtn = findElementInXLSheet(getParameterProject, "vendor Search Btn");
				vendorTextt = findElementInXLSheet(getParameterProject, "vendor Textt");
				productSearchBtn = findElementInXLSheet(getParameterProject, "product Search Btn");
				costAllocationInv = findElementInXLSheet(getParameterProject, "cost Allocation Inv");
				removeFirstRow = findElementInXLSheet(getParameterProject, "remove First Row");
				bannerTotInvoice = findElementInXLSheet(getParameterProject, "banner Tot Invoice");
				Product = findElementInXLSheet(getParameterProject, "Product");
				ProductQty = findElementInXLSheet(getParameterProject, "ProductQty");



				// 52
				expenseExisting = findElementInXLSheet(getParameterProject, "expense Existing");
				btn_ProductRegistrationBtn = findElementInXLSheet(getParameterProject, "ProductRegistrationBtn");
				txt_ProductRegistrationFilter = findElementInXLSheet(getParameterProject, "ProductRegistrationFilter");
				btn_WarrentyProfileBtn = findElementInXLSheet(getParameterProject, "WarrentyProfileBtn");
				txt_WarrentyProfileTxt = findElementInXLSheet(getParameterProject, "WarrentyProfileTxt");
				sel_WarrentyProfileSel = findElementInXLSheet(getParameterProject, "WarrentyProfileSel");
				btn_ProductRegistrationApplyBtn = findElementInXLSheet(getParameterProject, "ProductRegistrationApplyBtn");
				txt_ProductRegistrationValidation = findElementInXLSheet(getParameterProject, "ProductRegistrationValidation");
				

				// 53
				invoiceValue = findElementInXLSheet(getParameterProject, "invoice Value");

				// Verify the ability to draft and release invoice proposal, service invoice to
				// a project -- 54

				editNa = findElementInXLSheet(getParameterProject, "edit Na");
				workBreakdown = findElementInXLSheet(getParameterProject, "work Breakdown");
				addTask = findElementInXLSheet(getParameterProject, "add Task");
				taskName = findElementInXLSheet(getParameterProject, "task Name");
				taskType = findElementInXLSheet(getParameterProject, "task Type");
				inputProducts = findElementInXLSheet(getParameterProject, "input Products");
				searchProduct = findElementInXLSheet(getParameterProject, "search Product");
				productText = findElementInXLSheet(getParameterProject, "product Text");
				estimatedQty = findElementInXLSheet(getParameterProject, "estimated Qty");
				selectedVal = findElementInXLSheet(getParameterProject, "selected Val");
				billableCheck = findElementInXLSheet(getParameterProject, "billable Check");
				labourNa = findElementInXLSheet(getParameterProject, "labour");
				serviceProSearch = findElementInXLSheet(getParameterProject, "service Pro Search");
				serviceProText = findElementInXLSheet(getParameterProject, "service Pro Text");
				serviceSelected = findElementInXLSheet(getParameterProject, "service Selected");
				EstimatedWorkNew = findElementInXLSheet(getParameterProject, "Estimated Work New");
				hoursNa = findElementInXLSheet(getParameterProject, "hours Na");
				minutes = findElementInXLSheet(getParameterProject, "minutes");
				okBtn = findElementInXLSheet(getParameterProject, "ok Btn");
				billableResource = findElementInXLSheet(getParameterProject, "billable Resource");
				resourceNad = findElementInXLSheet(getParameterProject, "resource Nad");
				resourceSearch = findElementInXLSheet(getParameterProject, "resource Search");
				resourceText = findElementInXLSheet(getParameterProject, "resource Text");
				resourceSelected = findElementInXLSheet(getParameterProject, "resource Selected");
				serviceSearchResource = findElementInXLSheet(getParameterProject, "service Search Resource");
				serviceTextResource = findElementInXLSheet(getParameterProject, "service Text Resource");
				serviceSelectedResource = findElementInXLSheet(getParameterProject, "service Selected Resource");
				estimatedResouce = findElementInXLSheet(getParameterProject, "estimated Resouce");
				hoursResource = findElementInXLSheet(getParameterProject, "hours Resource");
				minutesResource = findElementInXLSheet(getParameterProject, "minutes Resource");
				okResource = findElementInXLSheet(getParameterProject, "ok Resource");
				billableResour = findElementInXLSheet(getParameterProject, "billable Resour");
				updateNad = findElementInXLSheet(getParameterProject, "update Nad");
				expandArrow = findElementInXLSheet(getParameterProject, "expand Arrow");
				releaseTask = findElementInXLSheet(getParameterProject, "release Task");
				updateHeader = findElementInXLSheet(getParameterProject, "update Header");
				invoiceProposal = findElementInXLSheet(getParameterProject, "invoice Proposal");
				checkBoxNad = findElementInXLSheet(getParameterProject, "checkBox Nad");
				checkoutInvoice = findElementInXLSheet(getParameterProject, "checkout Invoice");
				applyNad = findElementInXLSheet(getParameterProject, "apply");
				serviceDetails = findElementInXLSheet(getParameterProject, "service Details");
				close = findElementInXLSheet(getParameterProject, "close");
				link = findElementInXLSheet(getParameterProject, "link");
				txt_InvoiceProposalFilter = findElementInXLSheet(getParameterProject, "InvoiceProposalFilter");
				txt_InvoiceProposalTaskSelAmount = findElementInXLSheet(getParameterProject, "InvoiceProposalTaskSelAmount");
				btn_InvoiceProposalProBtn = findElementInXLSheet(getParameterProject, "InvoiceProposalProBtn");
				txt_InvoiceProposalProTxt = findElementInXLSheet(getParameterProject, "InvoiceProposalProTxt");
				sel_InvoiceProposalProSel = findElementInXLSheet(getParameterProject, "InvoiceProposalProSel");
				btn_ServiceInvoice = findElementInXLSheet(getParameterProject, "ServiceInvoice");
				txt_ServiceInvoiceActualVal = findElementInXLSheet(getParameterProject, "ServiceInvoiceActualVal");
				txt_ServiceInvoiceBaseVal = findElementInXLSheet(getParameterProject, "ServiceInvoiceBaseVal");
				btn_InvoiceDetailsTab = findElementInXLSheet(getParameterProject, "InvoiceDetailsTab");
				txt_ProposalDate = findElementInXLSheet(getParameterProject, "ProposalDate");
				txt_ProposalNo = findElementInXLSheet(getParameterProject, "ProposalNo");
				txt_ProposalAmount = findElementInXLSheet(getParameterProject, "ProposalAmount");
				txt_InvoiceDate = findElementInXLSheet(getParameterProject, "InvoiceDate");
				txt_InvoiceNo = findElementInXLSheet(getParameterProject, "InvoiceNo");
				txt_InvoiceAmount = findElementInXLSheet(getParameterProject, "InvoiceAmount");




				// Verify the ability of drafting and releasing a project quotation with
				// mandatory data -- 55
				projectQuatation = findElementInXLSheet(getParameterProject, "project Quatation");
				newProjectQuatation = findElementInXLSheet(getParameterProject, "new Project Quatation");
				searchCus = findElementInXLSheet(getParameterProject, "search Cus");
				customerTextQuat = findElementInXLSheet(getParameterProject, "customer Text Quat");
				selectedValCus = findElementInXLSheet(getParameterProject, "selected Val Cus");
				salesUnit = findElementInXLSheet(getParameterProject, "sales Unit");
				productSearch = findElementInXLSheet(getParameterProject, "product Search");
				productSearchText = findElementInXLSheet(getParameterProject, "product Search Text");
				productSelectedVal = findElementInXLSheet(getParameterProject, "product Selected Val");
				productQtyPQ = findElementInXLSheet(getParameterProject, "productQtyPQ");
				addNewRecord = findElementInXLSheet(getParameterProject, "add New Record");
				productTypeDrop = findElementInXLSheet(getParameterProject, "product Type Drop");
				referenceSearch = findElementInXLSheet(getParameterProject, "reference Search");
				referenceText = findElementInXLSheet(getParameterProject, "reference Text");
				referenceSelected = findElementInXLSheet(getParameterProject, "reference Selected");
				productSearchLabour = findElementInXLSheet(getParameterProject, "product Search Labour");
				productTextLabour = findElementInXLSheet(getParameterProject, "product Text Labour");
				serviceSelectedLabour = findElementInXLSheet(getParameterProject, "service Selected Labour");
				addNewRecord2 = findElementInXLSheet(getParameterProject, "add New Record2");
				typeDrop = findElementInXLSheet(getParameterProject, "type Drop");
				referenceSearchResource = findElementInXLSheet(getParameterProject, "reference Search Resource");
				referenceTextResource = findElementInXLSheet(getParameterProject, "reference Text Resource");
				referenceResourceSelected = findElementInXLSheet(getParameterProject, "reference Resource Selected");
				resourceServiceSearch = findElementInXLSheet(getParameterProject, "resource Service Search");
				resourceServiceText = findElementInXLSheet(getParameterProject, "resource Service Text");
				resourceServiceSelected = findElementInXLSheet(getParameterProject, "resource Service Selected");
				quotationID = findElementInXLSheet(getParameterProject, "quotation ID");

				// Verify the confirm action for a released project quotation -- 56

				quotationSearchText = findElementInXLSheet(getParameterProject, "quotation Search Text");
				selectedValquot = findElementInXLSheet(getParameterProject, "selected Val quot");
				version = findElementInXLSheet(getParameterProject, "version");
				colorSelect = findElementInXLSheet(getParameterProject, "color Select");
				tickVersion = findElementInXLSheet(getParameterProject, "tick Version");
				confirm = findElementInXLSheet(getParameterProject, "confirm");
				txt_pageConfirmed = findElementInXLSheet(getParameterProject, "pageConfirmed");
				quotationHeaderID = findElementInXLSheet(getParameterProject, "quotation Header ID");

				// Verify that the confirmed project quotation can be converted into a project
				// -- 57

				actionMenu = findElementInXLSheet(getParameterProject, "action Menu");
				convertToProject = findElementInXLSheet(getParameterProject, "convert To Project");

				ProjectCodeNew = findElementInXLSheet(getParameterProject, "Project Code");
				titleText = findElementInXLSheet(getParameterProject, "title Text");
				projectGrp = findElementInXLSheet(getParameterProject, "project Grp");
				searchResponsible = findElementInXLSheet(getParameterProject, "search Responsible");
				responsiblePersonText = findElementInXLSheet(getParameterProject, "responsible Person Text");
				responsiblePersonSelected = findElementInXLSheet(getParameterProject, "responsible Person Selected");
				searchPricing = findElementInXLSheet(getParameterProject, "search Pricing");
				searchPricingText = findElementInXLSheet(getParameterProject, "search Pricing Text");
				pricingSelected = findElementInXLSheet(getParameterProject, "pricing Selected");
				requestedStartDate = findElementInXLSheet(getParameterProject, "requested Start Date");
				requestedSDate = findElementInXLSheet(getParameterProject, "requested SDate");
				requestedEndDate = findElementInXLSheet(getParameterProject, "requested End Date");
				requestedEDate = findElementInXLSheet(getParameterProject, "requested EDate");
				projectLocationSearch = findElementInXLSheet(getParameterProject, "project Location Search");
				projectLocationText = findElementInXLSheet(getParameterProject, "project Location Text");
				projectLocationSelected = findElementInXLSheet(getParameterProject, "project Location Selected");

				inputWarehouse = findElementInXLSheet(getParameterProject, "input Warehouse");
				wipWarehouse = findElementInXLSheet(getParameterProject, "wip Warehouse");
				
				// s3
				taskStatus = findElementInXLSheet(getParameterProject, "task Status");
				releaseRelavantTask = findElementInXLSheet(getParameterProject, "release Relavant Task");
				draftCheck = findElementInXLSheet(getParameterProject, "draft Check");
				accountOwner = findElementInXLSheet(getParameterProject, "account Owner");

				// s4
				projectCostEstimation = findElementInXLSheet(getParameterProject, "project Cost Estimation");
				newProjectCostEstimation = findElementInXLSheet(getParameterProject, "new Project Cost Estimation");
				descriptionCostEsti = findElementInXLSheet(getParameterProject, "description Cost Esti");
				pricingProfileSearch = findElementInXLSheet(getParameterProject, "pricing Profile Search");
				pricingProText = findElementInXLSheet(getParameterProject, "pricing Pro Text");
				pricingProSelected = findElementInXLSheet(getParameterProject, "pricing Pro Selected");
				
				estimationDetails = findElementInXLSheet(getParameterProject, "estimation Details");
				inputProductSearch = findElementInXLSheet(getParameterProject, "input Product Search");
				inputProductMenu = findElementInXLSheet(getParameterProject, "input Product Menu");
				productRelatedPrice = findElementInXLSheet(getParameterProject, "product Related Price");
				lastPurchasePrice = findElementInXLSheet(getParameterProject, "last Purchase Price");
				closeWindow = findElementInXLSheet(getParameterProject, "close Window");
				billableInputProduct = findElementInXLSheet(getParameterProject, "billable Input Product");
				inputProductText = findElementInXLSheet(getParameterProject, "input Product Text");
				addNewLabour = findElementInXLSheet(getParameterProject, "add New Labour");
				typeDropdownLabour = findElementInXLSheet(getParameterProject, "type Dropdown Labour");
				LabourProductSearch = findElementInXLSheet(getParameterProject, "Labour Product Search");
				billableLabour = findElementInXLSheet(getParameterProject, "billable Labour");
				
				addNewResource = findElementInXLSheet(getParameterProject, "add New Resource");
				resourceDropdown = findElementInXLSheet(getParameterProject, "resource Dropdown");
				resourceProductSearch = findElementInXLSheet(getParameterProject, "resource Product Search");
				resourceBillable = findElementInXLSheet(getParameterProject, "resource Billable");
				
				addNewExpense = findElementInXLSheet(getParameterProject, "add New Expense");
				dropDownExpense = findElementInXLSheet(getParameterProject, "drop Down Expense");
				expenseProductSearch = findElementInXLSheet(getParameterProject, "expense Product Search");
				expenseCostText = findElementInXLSheet(getParameterProject, "expense Cost Text");
				billableExpense = findElementInXLSheet(getParameterProject, "billable Expense");
				
				addNewService = findElementInXLSheet(getParameterProject, "add New Service");
				serviceDropdown = findElementInXLSheet(getParameterProject, "service Dropdown");
				serviceProductSearch = findElementInXLSheet(getParameterProject, "service Product Search");
				serviceCostText = findElementInXLSheet(getParameterProject, "service Cost Text");
				billableService = findElementInXLSheet(getParameterProject, "billable Service");
				summaryTab = findElementInXLSheet(getParameterProject, "summary Tab");
				
				inputProductEstimatedCost = findElementInXLSheet(getParameterProject, "input Product Estimated Cost");
				expenseEstimatedCost = findElementInXLSheet(getParameterProject, "expense Estimated Cost");
				serviceProduct = findElementInXLSheet(getParameterProject, "service Product");
				

				// s5
				projectModel = findElementInXLSheet(getParameterProject, "project Model");
				newProjectModel = findElementInXLSheet(getParameterProject, "new Project Model");
				proModelCode = findElementInXLSheet(getParameterProject, "pro Model Code");
				proModelName = findElementInXLSheet(getParameterProject, "pro Model Name");
				warehouseInput = findElementInXLSheet(getParameterProject, "warehouse Input");
				warehouseWIP = findElementInXLSheet(getParameterProject, "warehouse WIP");
				
				//smoke 6
				
				btn_releaseTaskSubcontract = findElementInXLSheet(getParameterProject, "btn_release Task Subcontract");
				btn_releaseTaskInterDepartment = findElementInXLSheet(getParameterProject, "btn_release Task Inter Department");
				txt_POCA2 = findElementInXLSheet(getParameterProject, "POCA2");

	}
	
	public static void readData() throws Exception
	{
		//Com_TC_001
		siteURL = findElementInXLSheet(getDataProject,"site url");
		userNameData=findElementInXLSheet(getDataProject,"user name data");
		passwordData=findElementInXLSheet(getDataProject,"password data");
		
		//Project_TC_001
		ProjectCode= findElementInXLSheet(getDataProject,"ProjectCode");
		Title= findElementInXLSheet(getDataProject,"Title");
		PricingProfiletxt= findElementInXLSheet(getDataProject,"PricingProfiletxt");
		ResponsiblePersontxt= findElementInXLSheet(getDataProject,"ResponsiblePersontxt");
		CustomerAccounttxt= findElementInXLSheet(getDataProject,"CustomerAccounttxt");
		ProjectLocationtxt= findElementInXLSheet(getDataProject,"ProjectLocationtxt");
		
		//Project_TC_002
		producttxt= findElementInXLSheet(getDataProject,"producttxt");
		TaskName= findElementInXLSheet(getDataProject,"TaskName");
		EstimatedQty = findElementInXLSheet(getDataProject,"EstimatedQty");
		ServiceProducttxt= findElementInXLSheet(getDataProject,"ServiceProducttxt");
		Resourcetxt= findElementInXLSheet(getDataProject,"Resourcetxt");
		EstimatedWork= findElementInXLSheet(getDataProject,"EstimatedWork");
		EstimatedCost= findElementInXLSheet(getDataProject,"EstimatedCost");
		
		//Project_TC_012
		ActualQty= findElementInXLSheet(getDataProject,"ActualQty");
		
		//Project_TC_014
		SerialBatchqtyfield1= findElementInXLSheet(getDataProject,"SerialBatchqtyfield1");
		ToWarehouseA= findElementInXLSheet(getDataProject,"ToWarehouseA");
		
		//Project_TC_015
		TskPOC= findElementInXLSheet(getDataProject,"TskPOC");
		
		//Project_TC_019 19/11/21
		subVendortxtA= findElementInXLSheet(getDataProject,"subVendortxtA");
		VendorReferenceNotxtA= findElementInXLSheet(getDataProject,"VendorReferenceNotxtA");
		
		//Project_TC_022 19/11/25
		OwnertxtA= findElementInXLSheet(getDataProject,"OwnertxtA");
		
		//Project_TC_025 19/11/25
		PricingProfiletxtA= findElementInXLSheet(getDataProject,"PricingProfiletxtA");
		PerformertxtA= findElementInXLSheet(getDataProject,"PerformertxtA");
		OrderDescriptionA= findElementInXLSheet(getDataProject,"OrderDescriptionA");
		
		//Project_TC_027 19/11/25
		AmountA= findElementInXLSheet(getDataProject,"AmountA");
		PercentageA= findElementInXLSheet(getDataProject,"PercentageA");
		AccrualVoucherVendorReferenceNoA= findElementInXLSheet(getDataProject,"AccrualVoucherVendorReferenceNoA");
		ServiceGroupA= findElementInXLSheet(getDataProject,"ServiceGroupA");
		PricingProfileNewtxt= findElementInXLSheet(getDataProject,"PricingProfileNewtxt");
		
/*==========================================================================================*/
		
		vendorVal = findElementInXLSheet(getDataProject, "vendor Val");
		amountVal = findElementInXLSheet(getDataProject, "amount Val");
		percentageVal = findElementInXLSheet(getDataProject, "percentage Val");
		analysisCodeVal = findElementInXLSheet(getDataProject, "analysis Code Val");
		costObjectVal = findElementInXLSheet(getDataProject, "cost Object Val");
		// vendorRefVal = findElementInXLSheet(getDataProject, "vendor Ref Val");

		// Verify the ability to do cost allocation for project tasks using customer
		// refund--33
		customerVal = findElementInXLSheet(getDataProject, "customer Val");
		cusRefundAmountVal = findElementInXLSheet(getDataProject, "cus Refund Amount Val");

		// Verify the ability to do cost allocation for project tasks using payment
		// voucher -- 36

		payeeVal = findElementInXLSheet(getDataProject, "payee Val");
		analysisCodePayVouVal = findElementInXLSheet(getDataProject, "analysis Code Pay Vou Val");

		// Verify the ability to do cost allocation for project tasks using customer
		// debit memo--39
		amountDebitVal = findElementInXLSheet(getDataProject, "amount Debit Val");

		// Verify the ability to do cost allocation for project tasks using vendor
		// refund --- 42

		// Verify the ability to do cost allocation for project tasks using petty cash
		// --- 45
		desVal = findElementInXLSheet(getDataProject, "des Val");
		employeeVal = findElementInXLSheet(getDataProject, "employee Val");
		narrationVal = findElementInXLSheet(getDataProject, "narration Val");
		amountPettyVal = findElementInXLSheet(getDataProject, "amount Petty Val");
		analysisCodePettyCash = findElementInXLSheet(getDataProject, "analysis Code Petty Cash");

		// Verify the ability to do cost allocation for project tasks using bank
		// adjustment -- 48
		GLAccountVal = findElementInXLSheet(getDataProject, "GLAccount Val");
		minusAmountVal = findElementInXLSheet(getDataProject, "minus Amount Val");
		analysisCodeBankAdj = findElementInXLSheet(getDataProject, "analysis Code Bank Adj");

		// 51
		productSerVal = findElementInXLSheet(getDataProject, "product Ser Val");
		WarrentyProfileTxt = findElementInXLSheet(getDataProject, "WarrentyProfileTxt");
		InvPro1 = findElementInXLSheet(getDataProject, "InvPro1");
		InvPro2 = findElementInXLSheet(getDataProject, "InvPro2");

		// Verify the ability to draft and release invoice proposal, service invoice to
		// a project -- 54
		taskNameVal = findElementInXLSheet(getDataProject, "task Name Val");
		projectVal = findElementInXLSheet(getDataProject, "project Val");
		productVal = findElementInXLSheet(getDataProject, "product Val");
		serviceProTextVal = findElementInXLSheet(getDataProject, "service Pro Text Val");
		resourceVal = findElementInXLSheet(getDataProject, "resource Val");
		serviceTextResourceVal = findElementInXLSheet(getDataProject, "service Text Resource Val");

		// Verify the ability of drafting and releasing a project quotation with
		// mandatory data -- 55
		customerTextQuatVal = findElementInXLSheet(getDataProject, "customer Text Quat Val");
		productSearchTextVal = findElementInXLSheet(getDataProject, "product Search Text Val");
		referenceTextVal = findElementInXLSheet(getDataProject, "reference Text Val");
		productTextLabourVal = findElementInXLSheet(getDataProject, "product Text Labour Val");
		referenceTextResourceVal = findElementInXLSheet(getDataProject, "reference Text Resource Val");
		resourceServiceTextVal = findElementInXLSheet(getDataProject, "resource Service Text Val");

		// 57
		titleVal = findElementInXLSheet(getDataProject, "title Val");
		responsiblePersonVal = findElementInXLSheet(getDataProject, "responsible Person Val");
		pricingVal = findElementInXLSheet(getDataProject, "pricing Val");
		locationVal = findElementInXLSheet(getDataProject, "location Val");
		
		//s3
		projectValTask = findElementInXLSheet(getDataProject, "project Val Task");
		taskNameValue = findElementInXLSheet(getDataProject, "task Name Value");

		//s4
		descriptionValCostEsti = findElementInXLSheet(getDataProject, "description Val Cost Esti");
		expenseVal = findElementInXLSheet(getDataProject, "expense Val");
		serviceVal = findElementInXLSheet(getDataProject, "service Val"); 

	}

}
