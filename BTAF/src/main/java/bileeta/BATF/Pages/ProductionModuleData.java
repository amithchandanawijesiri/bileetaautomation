package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ProductionModuleData extends TestBase {

	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;

	/* Navigate to side menu */
	protected static String navigateMenu;
	protected static String sideNavBar;
	protected static String btn_Production;
	protected static String logo_Production;
	

	/* Reading the Data to variables */

	protected static String UserNameData;
	protected static String PasswordData;
	protected static String automationUserEmail;
	protected static String automationUserPassword;
	
	/*Multi Platform*/
	protected static String siteUrlMultiPlatformm;
	protected static String usernameMultiPlatformm;
	protected static String passwordMultiPlatformm;

	/*Navigate to inventory module*/
	protected static String btn_inventory;
	protected static String txt_inventory;
	protected static String btn_billOfMaterial;
	protected static String label_billOfMaterial;
	protected static String btn_newBOM;
	protected static String txt_newBOM;
	protected static String txt_desc;
	protected static String btn_productGrp;
	protected static String btn_outputProductSearch;
	protected static String txt_outputProductSearch;
	protected static String doubleClickoutputProduct;
	protected static String txt_BOMQuantity;
	protected static String btn_expiryDate;
	protected static String clickExpiryDate;
	protected static String btn_rawMaterial;
	protected static String txt_rawMaterial;
	protected static String doubleClickrawMaterial;
	protected static String txt_rawQuantity;
	protected static String btn_draft;
	protected static String btn_release;
	
	protected static String btn_resources;
	protected static String btn_resourceSearch;
	protected static String txt_additionalResources;
	protected static String doubleCLickAdditinalResources;
	protected static String txt_lotNo;
	protected static String bulkno;
	
	/*Navigate to Production module*/
	protected static String btn_production;
	protected static String txt_production;
	protected static String btn_BOO;
	protected static String txt_BOO;
	protected static String btn_newBOO;
	protected static String txt_newBOO;
	protected static String txt_BOOCode;
	protected static String btn_descriptionBOO;
	protected static String txt_descrBOO;
	protected static String btn_BOOApply;
	protected static String txt_BOOLogo;
	protected static String btn_plusBOO;
	protected static String txt_ElementCategory;
	protected static String txt_ElementDescription;
	protected static String btn_applyOperation;
	protected static String label_operationActivity;
	
	protected static String txt_ElementCategoryOperation;
	protected static String txt_ElementDescriptionOperation;
	protected static String GroupIdData;
	protected static String txt_activityType;
	protected static String txt_FinalInvestigation;
	protected static String btn_plusBOOSupply;
	protected static String btn_plusBOOOperation;
	protected static String btn_plusBOOQCBillOfOperation;
	
	protected static String btn_supplyBillOfOperationPlusMark;
	protected static String btn_productionModel;
	protected static String txt_productionModel; 
	protected static String btn_newProductionModel;
	protected static String txt_modelCode;
	protected static String txt_productGroup;
	protected static String btn_yes;
	protected static String btn_ProductionUnit;
	
	protected static String txt_ProductionUnit;
	protected static String txt_ProductionUnitLookup;
	protected static String doubleClickProductionUnit;
	protected static String btn_MRPEnabled;
	protected static String btn_Infinite;
	protected static String txt_batchQty;
	protected static String txt_barcodeBook;
	protected static String txt_CostingPriority;
	protected static String txt_standardCost;
	protected static String btn_pricingProfile;
	protected static String txt_PricingProfile;
	protected static String doubleClickPricingProfile;
	protected static String txt_newproductionModel;
	
	protected static String btn_BOONoSearch;
	protected static String txt_BOOOperation;
	protected static String doubleClickBOOOperation;
	protected static String btn_informationOK;
	
	protected static String btn_BOOTab;
	protected static String btn_Expand;
	protected static String btn_supply;
	protected static String btn_inputProducts;
	protected static String btn_checkBox;
	protected static String btn_apply;
	
	
	protected static String btn_product;
	protected static String txt_product;
	protected static String doubleClickproduct;
	protected static String txt_processTime;
	protected static String btn_operation;
	protected static String btn_QC;
	
	protected static String btn_productionOrder;
	protected static String btn_newProductionOrder;
	protected static String txt_productionOrder;
	protected static String btn_mainUpdate;
	protected static String btn_internalReceiptControl;
	
	protected static String btn_outputProduct;
	protected static String txt_plannedqty;
	
	protected static String txt_productionOrderGroup;
	protected static String txt_productionOrderDesc;
	protected static String btn_productionOrderModel;
	protected static String txt_productionOrderModel;
	protected static String doubleClickOrderModel;
	protected static String txt_productionOrderNo;
	
	protected static String btn_ProductionControl;
	protected static String txt_searchProductionOrder;
	protected static String btn_searchProductionOrder;
	protected static String btn_checkBoxProductionOrderNo;
	protected static String btn_action;
	protected static String btn_RunMRP;
	protected static String txt_MRPNo;
	protected static String btn_run;
	protected static String btn_releaseProductionOrder;
	protected static String btn_close;
	
	protected static String btn_entutionLogo;
	protected static String btn_taskEvent;
	protected static String btn_InternalDispatchOrder;
	protected static String btn_arrow;
	protected static String txt_shippingAddress;
	
	protected static String txt_plannedQty;
	protected static String btn_serialCapture;
	protected static String btn_productList;
	protected static String txt_BatchNo;
	protected static String btn_back;
	
	
	protected static String btn_productionOrderNo;
	protected static String btn_shopFloorUpdate;
	protected static String btn_start;
	protected static String btn_startProcess;
	protected static String btn_actual;
	protected static String txt_producedQty;
	protected static String btn_update;
	protected static String btn_inputProduct;
	protected static String txt_actualQty;
	protected static String btn_complete;
	protected static String txt_prodOrderNo;
	protected static String btn_searchProdOrder;
	protected static String doubleClickprodNo;
	
	protected static String btn_labelPrint;
	protected static String btn_view;
	protected static String txt_batchNo;
	protected static String txt_machineNo;
	protected static String txt_color;
	protected static String btn_draftNPrint;
	
	protected static String btn_labelPrintHistory;
	protected static String btn_productOrder;
	protected static String btn_QcComplete;
	
	protected static String txt_productonOrder;
	protected static String btn_costingRefresh;
	protected static String btn_tick;
	protected static String btn_updateCosting;
	protected static String btn_inputProductsCostingTab;
	
	protected static String btn_organizationManagement;
	protected static String txt_organizationManagement;
	
	protected static String btn_locationInfo;
	protected static String txt_productionUnit;
	protected static String doubleClickProductUnit;
	protected static String txt_locationInformation;
	protected static String btn_productionUnit;
	
	protected static String btn_newLocation;
	protected static String txt_locationCode;
	protected static String txt_description;
	protected static String txt_businessUnit;
	protected static String txt_inputWare;
	protected static String txt_outWare;
	protected static String txt_WIPWare;
	protected static String txt_site;
	protected static String txt_newLocation;
	
	protected static String txt_BOONo;
	
	protected static String txt_overheadInformation;
	protected static String btn_searchNewLocation;
	protected static String txt_overhead;
	protected static String doubleClickOverhead;
	
	protected static String btn_draftLocation;
	protected static String txt_activate;
	
	protected static String btn_PricingProfile;
	protected static String btn_newPricingProfile;
	protected static String txt_pricingProfileCode;
	protected static String txt_descPricingProfile;
	protected static String txt_pricingGroup;
	
	protected static String txt_material;
	protected static String txt_labour;
	protected static String txt_Overhead;
	protected static String txt_service;
	protected static String txt_expense;
	protected static String btn_estimate;
	
	protected static String btn_actualCalculation;
	protected static String txt_materialAC;
	protected static String txt_labourAC;
	protected static String txt_OverheadAC;
	protected static String txt_serviceAC;
	protected static String txt_expenseAC;
	
	protected static String btn_closeShop; 
	protected static String btn_completeTick;
	protected static String txt_complete;
	
	protected static String btn_active;
	protected static String btn_delete;
	protected static String txt_rate;
	protected static String txt_Unit;
	
	protected static String txt_overheadInfo;
	protected static String btn_new;
	protected static String txt_overheadCode;
	protected static String txt_descOverhead;
	protected static String txt_overheadGroup;
	protected static String txt_rateType;
	protected static String btn_draftOverhead;
	protected static String txt_newOverheadCode;
	
	protected static String txt_BOM;
	protected static String btn_started;
	protected static String btn_open;
	
	protected static String btn_bulkProductionOrder;
	protected static String new_bulkProductionOrder;
	protected static String txt_descrProductionOrderGroup;
	protected static String txt_referenceType;
	
	protected static String txt_BulkProduction;
	protected static String txt_BulkProductionForm;
	
	protected static String btn_productionUnitBulk;
	protected static String txt_productionUnitBulk;
	protected static String doubleClickProductionUnitBulk;
	
	protected static String btn_productTab;
	protected static String btn_productSearch;
	protected static String txt_productSearch;
	protected static String doubleClickProductSearch;
	protected static String btn_productModel;
	protected static String txt_productModel;
	protected static String doubleClickProductModel;
	protected static String btn_checkOut;
	
	protected static String btn_createInternalOrder;
	protected static String btn_selectAll;
	
	protected static String txt_title;
	protected static String btn_requester;
	protected static String txt_requester;
	protected static String doubleClickRequester;
	
	protected static String btn_goTopage;
	protected static String txt_plannedQtyBulk;
	protected static String btn_draftProductionOrder;
	protected static String btn_releaseBulkProductionOrder;
	protected static String btn_actionBulk;
	protected static String btn_applyInternal;
	protected static String txt_shipping; 
	
	protected static String btn_productionCosting;
	protected static String btn_date;
	protected static String btn_input;
	protected static String btn_internalReceipt;
	protected static String btn_smallcheckBox;
	protected static String btn_generate;
	
	protected static String txt_bulkOrder;
	protected static String bulkOrder;
	
	protected static String txt_standardCost1;
	
	protected static String btn_inputProductsTab;
	protected static String btn_searchProduct;
	protected static String txt_searchProduct;
	protected static String doubleClickSearchProduct;
	protected static String txt_plannedqty1;
	
	protected static String btn_closeTab;
	protected static String btn_actualDate;
	
	protected static String txt_actualQty1;
	protected static String btn_releaseTab;
	protected static String txt_internalReceipt;
	
	protected static String txt_internalDispatchRelease;
	protected static String txt_internalOrderRelease;
	protected static String txt_BulkOrderRelease;
	protected static String txt_productionModelRelease;
	protected static String txt_POReleased;
	protected static String txt_internalDispatchOrderReleased;
	protected static String txt_Pricing;
	protected static String txt_activateStatus;
	protected static String txt_openStatus;
	
	protected static String btn_Capture;
	protected static String btn_productListClick;
	protected static String txt_batchNoTXT;
	protected static String btn_refresh;
	
	protected static String link_internalReceipt;
	protected static String btn_journalEntry;
	protected static String link_gernalEntryDetails;
	protected static String btn_closeCosting;
	
	protected static String btn_actionCosting;
	protected static String btn_generateReceipt;
	protected static String txt_journalEntryReleased;
	protected static String btn_actionJournal;
	
	protected static String txt_internalReceiptReleased;
	
	//creating new product
	protected static String btn_productInfo;
	protected static String btn_newProduct;
	protected static String txt_productCode;
	protected static String txt_productDescription;
	protected static String txt_prodGrop;
	protected static String txt_basePrice;
	protected static String btn_manufacturer;
	protected static String txt_manufacturer;
	protected static String doubleCLickManufacturer;
	protected static String txt_UOMGroup;
	protected static String txt_UOM;
	protected static String txt_length;
	protected static String txt_lengthVal;
	protected static String txt_width;
	protected static String txt_widthVal;
	protected static String txt_height;
	protected static String txt_heightVal;
	protected static String txt_volume;
	protected static String txt_VolumeVal;
	protected static String btn_tabDetails;
	protected static String btn_allowInventoryWithoutCosting;
	protected static String txt_manufacturingType;
	protected static String txt_batchBook;
	protected static String txt_productName;
	
	

	/* Reading the Data to variables */
	protected static String rawQuantityData;
	protected static String descData;
	protected static String productGrpData;
	protected static String BOMQuantityData;
	protected static String rawMaterialData;
	protected static String outputProductData;
	
	protected static String BOOCodeData;
	protected static String BOODescrData;
	protected static String ElementCategoryData;
	protected static String ElementDescData;
	
	protected static String ElementCategoryDataOperation;
	protected static String ElementDescDataOperation;
	protected static String txt_GroupId;
	protected static String GroupIdData2;
	
	protected static String GroupIdData1;
	protected static String GroupIdData4;
	protected static String GroupIdDataQC;
	protected static String modelCodeData;
	protected static String productGroupData;
	protected static String ProductionUnitData;
	
	protected static String batchQtyData;
	protected static String standardCostData;
	protected static String PricingProfileData;
	
	protected static String BOOOperationData;
	protected static String productData;
	protected static String plannedqtyData;
	
	protected static String productionOrderDescData;
	protected static String productionOrderModelData;
	
	protected static String shippingAddressData;
	
	protected static String plannedQtyData;
	protected static String BatchNoData;
	
	protected static String producedData;
	protected static String actualQtyData;
	
	protected static String productionUnitData;
	
	protected static String locationCodeData;
	protected static String descriptionData;
	protected static String inputwareData;
	protected static String outputwareData;
	protected static String WIPwareData;
	
	
	protected static String overheadData;
	
	protected static String pricingProfileCodeData;
	protected static String descPricingProfileData;
	protected static String rateData;
	
	protected static String overheadCodeData;
	protected static String overheadDescData;
	
	protected static String productionOrderGroupData;
	
	protected static String productionUnitBulkData;
	protected static String productSearchData;
	protected static String productModel;
	
	protected static String titleData;
	protected static String requesterData;
	
	protected static String additionalResourcesData;
	protected static String plannedqtyBulkData;
	protected static String shippingData;
	protected static String lotNo;
	
	protected static String bulkOrderData;
	
	protected static String standardCostData1;
	
	protected static String searchProductData;
	protected static String plannedqtyData1;
	
	protected static String actualQtyData1;
	protected static String modelData;
	
	protected static String batchData;
	protected static String GroupIdDatafinal;
	
	
	//creating new product
	protected static String productCodeData;
	protected static String basePriceData;
	protected static String manufacturerData;
	protected static String lengthData;
	protected static String widthData;
	protected static String heightData;
	protected static String volumeData;
	
	
	/*New Regression Test Cases*/
	protected static String	btn_productionorder;
	protected static String btn_edit;
	protected static String btn_Update;
	protected static String txt_searchProductionOrder1;
	protected static String btn_duplicate;
	protected static String txt_outputProduct1;
	protected static String txt_outputProduct2;
	protected static String txt_costingPriority1;
	protected static String txt_costingPriority2;
	protected static String txt_prodOrderGroup1;
	protected static String txt_prodOrderGroup2;
	protected static String txt_description1;
	protected static String txt_description2;
	protected static String txt_prodModel1;
	protected static String txt_prodModel2;

	
	
	
	/* QA Form level action verification */
	protected static String txt_draftBOM;
	protected static String txt_editBOM;
	protected static String txt_updateBOM;
	protected static String txt_duplicateBOM;
	protected static String btn_draftNNew;
	protected static String txt_draftNNewBOM;
	protected static String btn_copyFrom;
	protected static String txt_BOm;
	protected static String txt_previousBOM;
	protected static String doublePreviousBOM;
	protected static String txt_num;
	protected static String btn_hold;
	protected static String txt_changeStatus;
	protected static String txt_reason;
	protected static String btn_ok;
	protected static String btn_unHold;
	protected static String btn_newVersion;
	protected static String txt_validMsg;
	protected static String btn_reminder;
	protected static String txt_createReminder; 
	protected static String btn_history;
	protected static String txt_history;
	protected static String btn_reverse;
	protected static String txt_reverse;
	protected static String btn_updateReminder;
	protected static String txt_delete;
	protected static String btn_Delete;
	protected static String btn_updateNNew;
	protected static String txt_updateNNewBOM;
	protected static String txt_previousBOO;
	protected static String doublePreviousBOO;
	protected static String txt_duplicateBOO;
	protected static String txt_BOOAction;
	protected static String btn_activities;
	protected static String txt_activity;
	protected static String txt_subject;
	protected static String btn_assignTo;
	protected static String txt_AssignTo;
	protected static String doubleAssignTo;
	protected static String btn_UpdateTask;
	protected static String txt_Boo;
	protected static String txt_productionModelCode;
	protected static String txt_previousmodel;
	protected static String doublePreviousmodel;
	protected static String txt_productionModelTab;
	protected static String txt_BillOfMaterial;
	protected static String tab_summary;
	protected static String txt_plannedQty1;
	protected static String txt_priority;
	protected static String txt_previousproductionOrder;
	protected static String doublePreviousProductionOrder;
	protected static String txt_productionOrderTab;
	protected static String btn_okHold;
	protected static String btn_jobTechnicalDetail;
	protected static String btn_dockFlow;
	protected static String txt_dockFlow;
	protected static String btn_generatePackinOrder;
	protected static String txt_generatePackingOrder;
	protected static String btn_costEstimation;
	protected static String txt_costEstimation;
	protected static String txt_prodControl;
	protected static String txt_internalDispatchOrderdrafted;
	protected static String btn_editInternal;
	protected static String txt_release;
	protected static String txt_update;
	protected static String btn_datenew;
	protected static String btn_reverseClick;
	protected static String txt_date;
	protected static String txt_draftProductionOrder;
	protected static String txt_updateNNew;
	protected static String btn_products;
	protected static String txt_bulkproductionorderTab;
	protected static String txt_previousBulkOrder;
	protected static String doublePreviousbulkOrder;
	protected static String btn_InternalOrder;
	protected static String txt_InternalOrder;
	protected static String txt_title1;
	protected static String btn_datenew1;
	protected static String btn_report;
	protected static String btn_viewReport;
	protected static String txt_viewReport;
	protected static String txt_active;
	protected static String btn_inactive;
	protected static String txt_inactive;
	protected static String btn_yes1;
	protected static String btn_yes2;
	protected static String txt_new;
	protected static String txt_deleteAc;
	protected static String btn_deleteAc;
	protected static String btn_yes4;
	protected static String txt_updateOrganization;
	protected static String txt_edit;
	protected static String txt_search;
	protected static String btn_reset;
	protected static String txt_reset;
	protected static String txt_internalReceiptLabel;
	protected static String btn_productionParameter;
	protected static String btn_inactivePP;
	protected static String txt_inactivePP;
	protected static String btn_UPDATE;
	protected static String txt_UPDATE;
	protected static String btn_productcostEstimation;
	protected static String btn_newCostEstimation;
	protected static String txt_costEstDescription;
	protected static String btn_costProduct;
	protected static String txt_costProduct;
	protected static String costProductDoubleClick;
	protected static String txt_costEstQuantity;
	protected static String btn_pricingProfileCost;
	protected static String txt_pricingProfileCost;
	protected static String pricingProfileCostDoubleClick;
	protected static String btn_checkout;
	protected static String div_loginVerification;
	
	/* Regression */
	/*Comm_001_002*/
	protected static String lbl_entutionLogoLoginPage;
	protected static String lbl_entutionLogoHomePage;
	protected static String img_navigationPane;
	protected static String btn_productionModule;
	
	/*PROD_PM_004_006to016*/
	protected static String header_productionModelByPage;
	protected static String drop_templateOnByPages;
	protected static String drop_docStatus;
	protected static String th_modelNoByPagePM;
	protected static String th_productCodeByPagePM;
	protected static String th_description1ByPagePM;
	protected static String th_description2ByPagePM;
	protected static String txt_serchProductionModelByPage;
	protected static String btn_serchIconPMByPage;
	protected static String btn_advanceFilterPMByPage;
	protected static String btn_refreshPMByPage;
	protected static String btn_alpabaticalSearchPMByPage;
	protected static String span_pageDetailsByPage;
	protected static String txt_currentPagePMByPage;
	protected static String i_doubleBackMidleFooterControllersByPage;
	protected static String i_backMidleFooterControllersByPage;
	protected static String i_doubleNextMidleFooterControllersByPage;
	protected static String i_nextMidleFooterControllersByPage;
	protected static String span_leftFooterDeatilsOnByPage;
	protected static String header_productionModelByPageWithClass;
	protected static String btn_newButtonProductionModel;
	protected static String lnk_modelNoPMByPageWithHref;
	
	/* PROD_PM_017to */
	protected static String header_newProductionModel;
	protected static String tab_summaryProductionModel;
	protected static String tab_billOfOperationProductionModel;
	protected static String txt_modelCodeProductionModel;
	protected static String drop_productionGroupProductionModel;
	protected static String btn_productLookupProductionModel;
	protected static String txt_descriptionProductionModel;
	protected static String btn_searchProductionUnit;
	protected static String drop_inputWarehousePM;
	protected static String drop_outputWarehousePM;
	protected static String drop_wipWarehousePM;
	protected static String drop_sitePM;
	protected static String btn_yesClearConfirmation;
	protected static String txt_searchProductCommon;
	protected static String tr_reultOnLookupInfoReplace;
	protected static String header_productLookup;
	protected static String txt_productOnFrontPagePM;
	protected static String btn_searchProductProductionModel;
	protected static String header_productionUnitLookupPM;
	protected static String txt_searchProductionUnit;
	protected static String btn_searchProductionUnitOnLookup;
	protected static String lnk_resultProductionUnitOnLookup;
	protected static String txt_productionUnitOnFrontPage;
	
	/* PROD_PM_025 */
	protected static String txt_validFromDatePM;
	protected static String txt_validToDatePM;
	protected static String txt_minimumLotSizePM;
	protected static String txt_maximumLotSizePM;
	protected static String txt_batchQuantityOtherInformationPM;
	protected static String drop_barcodeBookOtherInformationPM;
	protected static String txt_productionScrapOtherInformationPM;
	protected static String drop_costingPriorityOtherInformationPM;
	protected static String txt_statndardCostOtherInformationPM;
	protected static String txt_pricingProfileOtherInformationPM;
	protected static String chk_planningEnabledOtherInformationPM;
	protected static String chk_mrpEnabledOtherInformationPM;
	protected static String chk_infinitrOtherInformationPM;
	
	/* PROD_PM_028 */
	protected static String btn_lookupPricingProfilePM;
	protected static String header_pricingProfileLookupPM;
	protected static String txt_searchPricingProfile;
	protected static String btn_searchPricingProfilePM;
	protected static String td_resultPricingProfilePM;
	
	/* PROD_PM_030 */
	protected static String drop_bomPM;
	
	/* PROD_PM_034 */
	protected static String btn_lookupBOOPM;
	protected static String txt_descriptionBOOPM;
	protected static String chk_supplyToProductionBOOPM;
	
	/* PROD_PM_035 */
	protected static String header_billOfOperatioPM;
	protected static String txt_billOfOperationPMFrontPage;
	protected static String txt_searchBillOfOperationPM;
	protected static String btn_searchBOOInLookup;
	protected static String td_resultBOOPM;
	protected static String btn_okClearConfirmationBOOPM;
	
	/*PROD_PM_038*/
	protected static String tab_billOfOperationAfterSelected;
	
	/* PROD_PM_039 */
	protected static String th_columnHeaderEleentIDPM;
	protected static String th_columnHeaderEleentCategoryPM;
	protected static String th_columnHeaderEleentDescriptionPM;
	protected static String th_columnHeaderMainResoursePM;
	protected static String th_columnHeaderFixedDueationPM;
	protected static String th_columnHeaderVariableDurationPM;
	protected static String btn_expandPM;
	
	/* PROD_PM_040 */
	protected static String tr_booRow1PM;
	protected static String tr_booRow2PM;
	protected static String tr_booRow3PM;
	protected static String tr_booRow4PM;
	
	/* PROD_PM_041 */
	protected static String header_draftedProductionModel;
	protected static String btn_editBillOfOperationPMRowReplace;
	protected static String chk_inputProductsRowReplaceBOOPM;
	protected static String drop_processTimeTypeBOOPM;
	protected static String btn_inputProductsTabBOOPM;
	protected static String btn_applyBOOPM;
	
	/*PROD_PM_042*/
	protected static String tab_productsPM;
	
	/* PROD_PM_043 */
	protected static String tab_inputProductsProductsTabPM;
	protected static String tab_outputProductsProductsTabPM;
	
	/* PROD_PM_044 */
	protected static String div_productOnOutputProductsTabPM;
	protected static String div_warehouseOnOutputProductsTabPM;
	protected static String div_plannedQtyOnOutputProductsTabPM;
	
	/* PROD_PM_045 */
	protected static String th_columHeaderProductOutputProductsTabPM;
	protected static String th_columHeaderWarehouseOutputProductsTabPM;
	protected static String th_columHeaderPlanedQtyOutputProductsTabPM;
	
	/* PROD_PM_051 */
	protected static String li_inputProductSelected;
	protected static String td_selectedInputProductBOO1GridPM;
	protected static String td_selectedInputProductBOO2GridPM;

	/* PROD_PM_052 */
	protected static String btn_editOperation01PM;
	protected static String header_operationActivityPopup;
	protected static String btn_byProductTabPM;
	protected static String btn_byProductTabSelectedPM;
	protected static String btn_byProductsLookupByProductsTabPM;
	protected static String td_resultByProductPM;
	protected static String header_productLookupBox;
	protected static String btn_productSearchInLookup;
	protected static String div_byProductOnByProductGridPM;
	protected static String txt_byProductsQuantityPM;
	protected static String txt_byProductsLeadDaysPM;
	protected static String btn_applyOperationActivity;
	protected static String td_appliedByProductBOOGridPM;
	
	/* PROD_PM_053_054 */
	protected static String btn_serviceProductsTabPM;
	protected static String btn_serviceProductTabSelectedPM;
	protected static String btn_serviceProductsLookupByProductsTabPM;
	protected static String td_resultServiceProductPM;
	protected static String td_resultBatchProductServiceTabPM;
	protected static String div_serviceProductOnByProductGridPM;
	protected static String txt_serviceProductsAmountsPM;
	protected static String txt_serviceProductsLeadDaysPM;
	
	/* PROD_PM_55 */
	protected static String btn_additionalResourseTabPM;
	protected static String btn_additionalResourseTabSelectedPM;
	protected static String btn_resourseLookupAdditionalResourseTabPM;
	protected static String lookup_resource;
	protected static String txt_searchResource;
	protected static String btn_resourceSearchInLookup;
	protected static String td_resultResourceAdditionalResourceTabPM;
	protected static String div_resourceOnAdditonalResourceGridPM;
	protected static String btn_selectHoursAddtionalResourceTabPM;
	protected static String header_resourceHoursAdditionalResourseTabPM;
	protected static String btn_oneHourAdditionalResourceHoursPM;
	protected static String btn_okDurationAdditionalResourceHoursPM;
	protected static String btn_selectMinutesAddtionalResourceTabPM;
	
	/* PROD_PM_56 */
	protected static String btn_overheadTabPM;
	protected static String btn_overheadTabSelectedPM;
	protected static String div_overheadOnOverheadGridPM;
	
	/* PROD_PM_57 */
	protected static String btn_stepsTabPM;
	protected static String btn_stepsTabSelectedPM;
	protected static String header_descriptionStepsTabPM;
	protected static String txt_descriptionStepsTabPM;
	
	/* PROD_PM_59 */
	protected static String header_releasedProductionModel;
	protected static String div_productionModelCode;
	
	/* PROD_PM_60_61 */
	protected static String btn_productionModelByPageLikn;
	protected static String txt_searchProductionModel;
	protected static String btn_searchProductionModel;
	protected static String lnk_resultProductionModelOnByPage;
	protected static String header_releasedProductionModelWithDocReplace;
	protected static String tab_productsProductionModel;
	protected static String div_productOneUnderInputProductsTabAfterReleasedPM;
	protected static String div_productTwoUnderInputProductsTabAfterReleasedPM;
	protected static String header_draftedProductionModelWithDocReplace;
	
	/* PROD_PM_65 */
	protected static String p_mainValidation;
	protected static String txt_batchQtyWithValidationPO;
	
	/* PROD_PM_66 */
	protected static String lnk_productionModelOnDraftedStatusPM;
	protected static String td_resulProductionModelOnByPageDocReplace;
	
	/* PROD_PM_67 */
	protected static String a_validFromDatePM;
	protected static String a_validToDatePM;
	
	/* PROD_PM_071_072_073_074_076 */
	protected static String div_plannedQtyOnOutputProductsTabInfinitePM;
	
	/* PROD_PM_096_097 */
	protected static String th_columHeaderProductInputProductsTabPM;
	protected static String th_columHeadeWarehouseInputProductsTabPM;
	protected static String th_columHeadePlanedQtyInputProductsTabPM;
	protected static String th_columHeadeElementDescriptionInputProductsTabPM;
	protected static String div_productOneOnInputProductsTabPM;
	protected static String div_productTwoOnInputProductsTabPM;
	protected static String div_warehouseOneOnInputProductsTabPM;
	protected static String div_warehouseTwoOnInputProductsTabPM;
	protected static String div_planedQtyOneOnInputProductsTabPM;
	protected static String div_planedQtyTwoOnInputProductsTabPM;
	protected static String div_elementDescriptionOneOnInputProductsTabPM;
	protected static String div_elementDescriptionTwoOnInputProductsTabPM;
	
	/* Production Order */
	/* PROD_PO_002 */
	protected static String header_productionOrderDocNoColumnOnByPage;
	protected static String lnk_countProductionOrdersOnByPAge;
	protected static String header_productionOrderByPage;
	
	/* PROD_PO_003 */
	protected static String header_newProductionOrder;
	
	/* PROD_PO_004 */
	protected static String li_orderStructureTabPO;
	protected static String li_costingSummaryTabPO;
	
	/* PROD_PO_005 */
	protected static String drop_productOrderGroupSummarySectionSummaryTabPO;
	protected static String txt_descriptionSummarySectionSummaryTabPO;
	protected static String txt_productionModelSummarySectionSummaryTabPO;
	protected static String txt_costEstimationSummarySectionSummaryTabPO;
	protected static String drop_barcodeBookSummarySectionSummaryTabPO;
	protected static String div_batchNoSummarySectionSummaryTabPO;
	protected static String txt_productionLotNoSummarySectionSummaryTabPO;
	protected static String drop_prioritySummarySectionSummaryTabPO;
	protected static String txt_productionUnitSummarySectionSummaryTabPO;
	protected static String drop_inputWarehouseSummarySectionSummaryTabPO;
	protected static String drop_outputWarehouseSummarySectionSummaryTabPO;
	protected static String drop_WIPWarehouseSummarySectionSummaryTabPO;
	protected static String drop_siteSummarySectionSummaryTabPO;
	
	/* PROD_PO_006 */
	protected static String txt_outputProductProductInformationSectionSummaryTabPO;
	protected static String txt_planedQtyProductInformationSectionSummaryTabPO;
	protected static String txt_producedQtyProductInformationSectionSummaryTabPO;
	protected static String txt_balanceQtyProductInformationSectionSummaryTabPO;
	protected static String txt_minimumLotSizeProductInformationSectionSummaryTabPO;
	protected static String txt_maximumLotSizeProductInformationSectionSummaryTabPO;
	protected static String txt_batchQtyProductInformationSectionSummaryTabPO;
	protected static String txt_productionScrapProductInformationSectionSummaryTabPO;
	protected static String drop_costingPriorityProductInformationSectionSummaryTabPO;
	protected static String txt_standardCostProductInformationSectionSummaryTabPO;
	protected static String chk_planningEnabledtProductInformationSectionSummaryTabPO;
	protected static String chk_MRPEnabledtProductInformationSectionSummaryTabPO;
	protected static String chk_infiniteProductInformationSectionSummaryTabPO;
	
	/* PROD_PO_007 */
	protected static String drop_boMBoMDetailsSectionSummaryTabPO;
	protected static String div_boMVarientBoMDetailsSectionSummaryTabPO;
	protected static String div_varientDescriptionBoMDetailsSectionSummaryTabPO;
	
	/* PROD_PO_008 */
	protected static String div_boOBoODetailsSectionSummaryTabPO;
	protected static String div_boODescriptionDetailsSectionSummaryTabPO;
	protected static String chk_supplyToOperationDetailsSectionSummaryTabPO;
	
	/* PROD_PO_009 */
	protected static String div_currentStatusStatusSectionSummaryTabPO;
	protected static String div_shedulingStatusStatusSectionSummaryTabPO;
	
	/* PROD_PO_010 */
	protected static String txt_requestStartDateSheduleSectionSummaryTabPO;
	protected static String txt_requestEndDateSheduleSectionSummaryTabPO;
	protected static String div_earliestStartDateSheduleSectionSummaryTabPO;
	protected static String div_earliestEndDateSheduleSectionSummaryTabPO;
	protected static String div_latestStartDateSheduleSectionSummaryTabPO;
	protected static String div_latestEndDateSheduleSectionSummaryTabPO;
	
	/* PROD_PO_011 */
	protected static String span_productLookupPO;
	protected static String lookup_product;
	protected static String txt_searchProductProductLookup;
	protected static String btn_searchProductProductLookup;
	protected static String td_resultProductRegressionPO;
	
	/* PROD_PO_012 */
	protected static String btn_productionModelLookupPO;
	protected static String lookup_productionModel;
	protected static String td_resultProductionModelDocReplace;
	
	/* Pre-Requisites PO Regression */
	protected static String txt_productCodeProductInformation;
	protected static String header_releasedProduct;
	
	/* PROD_PO_013 */
	protected static String count_resultRowsProductionModelLookup;
	
	/* PROD_PO_017 */
	protected static String chk_mrpProductionOrderRegression;
	
	/* PROD_PO_018 */
	protected static String chk_infinitePO;
	protected static String chk_supplyToOperationPO;
	
	/* PROD_PO_019 */
	protected static String txt_plabedQtyPO;
	
	/* PROD_PO_024 */
	protected static String header_draftedProductionOrder;
	
	/* PROD_PO_025 */
	protected static String chk_supplyToProductionPO;
	protected static String a_requestStartDatePOWhenDuplicating;
	protected static String a_requestEndDatePOWhenDuplicating;
	
	/* PROD_PO_027 */
	protected static String header_deletedProductionOrder;
	protected static String btn_delete2;
	
	/* PROD_PO_028 */
	protected static String header_releasedProductionOrder;
	
	/* PROD_PO_045_046_047 */
	protected static String tab_summaryPO;
	protected static String tab_orderStructurePO;
	protected static String tab_productsPO;
	protected static String tab_resoursePO;
	protected static String tab_costingSummaryPO;
	
	/* PROD_PO_048_049 */
	protected static String btn_plusIconPO;
	
	/*PROD_PO_050*/
	protected static String lbl_docNo;
	protected static String header_loockupProductionOrder;
	protected static String lnk_inforOnLookupInformationReplace;
	protected static String txt_searchProductionOrder2;
	
	/*PROD_PO_052*/
	protected static String btn_draftAndNew;
	protected static String lbl_recentlyDraftedProductionOrder;
	protected static String div_descriptionAfterDraftPO;
	protected static String lnk_releventDraftedProductionOrderDocReplace;
	
	/* PROD_PO_053 */
	protected static String td_updatedTimeHistoryDetailsOnlyOneAction;
	protected static String td_actionDraftDoneByUseHistoryDetailsOnlyOneAction;
	protected static String td_userHistoryDetailsOnlyOneAction;
	protected static String td_descriptionHistoryDetailsUpdated;
	protected static String td_updatedTimeHistoryDetailsUpdate;
	protected static String td_actionDraftDoneByUseHistoryDetailsUpdate;
	protected static String td_userHistoryDetailsUpdate;
	protected static String td_descriptionHistoryDetailsUpdatedUpdate;
	
	/* Production Control */
	/* PROD_PC_001 */
	protected static String btn_productionControl;
	protected static String header_productionControlByPage;
	
	/* PROD_PC_002 */
	protected static String div_columnProductionOrderStatusPC;
	protected static String div_columnPostBusinessUnitPC;
	protected static String div_columnDocNoPC;
	protected static String div_columnPriorityPC;
	protected static String div_columnProductionOrderNoPC;
	protected static String div_columnOutputProductPC;
	protected static String div_columnOpenQtyPC;
	protected static String div_columnRequestedStartDatePC;
	
	/* PROD_PC_003 */
	protected static String a_postBusinessUnitHeaderDeatailsPO;
	protected static String div_postBusinessUnitProductControlByPage;
	
	/* PROD_PC_005 */
	protected static String div_priorityProductControlByPage;
	
	/* PROD_PC_006 */
	protected static String div_productionOrderNoProductControlByPage;
	
	/* PROD_PC_007 */
	protected static String div_outputProductProductControlByPage;
	
	/* PROD_PC_008 */
	protected static String div_openQtyProductControlByPage;
	
	/* PROD_PC_009 */
	protected static String div_requestedStartDateProductControlByPage;
	
	/* PROD_PC_010 */
	protected static String btn_runMRP;
	protected static String div_productionOrderSelectValidationPC;
	
	/* PROD_PC_011_012 */
	protected static String txt_searchProductionControlOnByPage;
	protected static String btn_searchProductionControlByPage;
	protected static String div_resultCountPCByPage;
	protected static String div_chkSelectDocReplacePC;
	protected static String div_chkSelectedDocReplacePC;
	protected static String lookup_productionControl;
	
	/* Production Multy Platform Scenarios */
	/* PROD_E2E_001 */
	protected static String btn_billOfMaterial2;
	protected static String btn_newBillOfMaterial;
	protected static String txt_descriptionBillOfMaterials;
	protected static String drop_productGroupBOM;
	protected static String btn_productInformation;
	protected static String btn_newProduct2;
	protected static String drop_productGroupProductInfromation;
	protected static String chk_allowDecimalProductInformation;
	protected static String btn_lookupMenufcturerProductInformation;
	protected static String txt_searchMenfacture;
	protected static String td_firstResultMenufacturerLookup;
	protected static String drop_defouldUOMGroupProductInfromation;
	protected static String drop_defouldUOMProductInfromation;
	protected static String tab_detailsProductInformation;
	protected static String header_warehouseInfromationDetailsTabProductInfromation;
	protected static String drop_outboundCostingMethodProductInfromation;
	protected static String drop_menufacTypeProductionProductInformation;
	protected static String chk_isBatchProduct;
	protected static String btn_lookupOutputProduct;
	protected static String txt_productFrontPageBOM;
	protected static String txt_descriptionProductInformation;
	protected static String txt_quantityBOM;
	protected static String tab_classificationProductInformation;
	protected static String btn_lokkupVarienGroupProductInformation;
	protected static String txt_productVarientSearch;
	protected static String td_firstResultVarirnLookupPI;
	protected static String drop_varienSelectClassificationTab;
	protected static String div_varienOnVarientSectionProductReplace;
	protected static String btn_varientUpdate;
	protected static String p_varientUpdatedValidation;
	protected static String btn_lookupOnLineBOMRowReplace;
	protected static String txt_qtyBOMInputProductLinesRowReplace;
	protected static String btn_addNewRecordBoM;
	protected static String header_comlumQtyBoMMaterialInfoTable;
	protected static String header_comlumFixedQtyBoMMaterialInfoTable;
	protected static String header_scrapFixedQtyBoMMaterialInfoTable;
	protected static String chk_fixedQtyRowReplaceBOM;
	protected static String txt_scrapRowReplaceBOM;
	protected static String header_draftedBOM;
	protected static String header_releasedBillOfMaterial;
	protected static String btn_overheadInformation;
	protected static String btn_newOverheadInformation;
	protected static String txt_overheadCode2;
	protected static String txt_overheadDescription;
	protected static String drop_overheadGroup;
	protected static String drop_rateTypeOverhead;
	protected static String btn_draftOverhead2;
	protected static String lookup_newOverheadInformation;
	protected static String div_selectOverheadReplaceOverhead;
	protected static String a_acitveOverhead;
	protected static String btn_yesConfirmationOverheadActivation;
	protected static String b_activatedStatusOverhead;
	protected static String div_selectedOverheadReplaceOverhead;
	protected static String btn_organizationMgt;
	protected static String btn_resourseInformation;
	protected static String btn_newResourse;
	protected static String txt_resourseCodeRI;
	protected static String txt_resourseDescriptionRI;
	protected static String drop_resourseGroupRI;
	protected static String btn_lookupEmployeeRI;
	protected static String txt_searchEmployeeRI;
	protected static String td_resultEmplyeeLookupEmployeeReplace;
	protected static String txt_hourlyRateResourse;
	protected static String chk_productionProjectResourse;
	protected static String btn_overheadLookupResourseInformation;
	protected static String txt_searchOverhead;
	protected static String txt_overheadeRateAttachedToRsourseOne;
	protected static String btn_addNewRowOverheadRI;
	protected static String btn_overheadSecondRowLookupResourseInformation;
	protected static String txt_overheadeSecondRowRateAttachedToRsourseOne;
	protected static String header_releasedReourse;
	protected static String btn_plusAddNewResourse;
	protected static String td_resultOveheadInformationOverheadReplace;
	protected static String btn_billOfOperation;
	protected static String header_BillOfOperationByPage;
	protected static String btn_newBillOfOperation;
	protected static String header_newBillOfOperation;
	protected static String txt_booCodeBillOfOperation;
	protected static String btn_addBillOfOperationRowReplace;
	protected static String drop_elementCategoryBOO;
	protected static String drop_activityTypeBOO;
	protected static String txt_elementDescriptionBOO;
	protected static String btn_applyOperationOneBOO;
	protected static String txt_groupIdBOO;
	protected static String drop_batchMethodBOO;
	protected static String chk_finalInvestigationBOO;
	protected static String header_draftedBOO;
	protected static String header_releasedBOO;
	protected static String btn_mainResourseLookuoOperationOne;
	protected static String td_resultResourseOperationOne;
	protected static String txt_mainResourseOnLookupOperationOne;
	protected static String txt_fixedDurationOpeartionOne;
	protected static String a_oneHourFixedDurationOpeationOne;
	protected static String div_fixedDurationOpeationOne;
	protected static String btn_okFixedDurationOpeationOne;
	protected static String txt_fixedDurationMinOpeartionOne;
	protected static String lbl_docNoBoo;
	protected static String txt_locationCOde;
	protected static String txt_descriptionLocationInformation;
	protected static String drop_businessUnitLI;
	protected static String drop_inputWarehouseLI;
	protected static String drop_outputWarehouseLI;
	protected static String drop_wipWarehouseLI;
	protected static String drop_addressLI;
	protected static String drop_siteLI;
	protected static String btn_draftLI;
	protected static String div_selectLocationReplaceLocation;
	protected static String a_acitveLocation;
	protected static String btn_yesConfirmationLocationActivation;
	protected static String b_activatedStatusLocation;
	protected static String div_selectedLocationReplaceLocation;
	protected static String window_newLocation;
	protected static String btn_pricingProfile2;
	protected static String btn_newPricingProfile2;
	protected static String txt_pricingProfileCode2;
	protected static String txt_pricingProfileDescription;
	protected static String tab_estimationPricingProfile;
	protected static String drop_materialEstimationTabPP;
	protected static String drop_labourResorseEstimationTabPP;
	protected static String drop_overheadEstimationTabPP;
	protected static String drop_serviceEstimationTabPP;
	protected static String drop_expenceEstimationTabPP;
	protected static String drop_materialActualTabPP;
	protected static String drop_labourResorseActualTabPP;
	protected static String drop_overheadActualTabPP;
	protected static String drop_serviceActualTabPP;
	protected static String drop_expenceActualTabPP;
	protected static String btn_draftPP;
	protected static String div_selectPricingProfilenReplacePricingProfile;
	protected static String a_acitvePP;
	protected static String btn_yesConfirmationPPActivation;
	protected static String b_activatedStatusPP;
	protected static String div_selectedPricingProfileReplacePricingProfile;
	protected static String window_newPP;
	protected static String btn_actualCalclationTabPP;
	protected static String drop_pricingGroupPP;

	/* Production Model */
	protected static String lnk_resultProductionUnitOnLookupUnitReplace;
	protected static String td_resultPricingProfileReplacePP;
	protected static String div_resourceOnAdditonalResourceGridPMResorceReplace;
	protected static String td_resultBOOPMBooReplace;
	protected static String td_resultResourceAdditionalResourceTabPMResourseReplace;
	protected static String chk_mrpProductionModel;
	
	/* Production Order */
	protected static String td_resultProductRegressionPOProductReplace;
	
	/* MRP Run */
	protected static String chk_mrpPODoRepalce;
	protected static String btn_actionMRP;
	protected static String header_productionControlWindow;
	protected static String btn_runMRPWindow;
	protected static String para_MRPReleaseValidation;
	protected static String btn_logTabProductionControlWindow;
	protected static String lbl_genaratedInternalOrderMRP;
	protected static String btn_cloaseMRPWindow;
	protected static String btn_homeLink;
	protected static String btn_taskEvent2;
	protected static String btn_internalDispatchOrderTaskAndEvent;
	protected static String btn_pendingIDODocRepalce;
	protected static String header_newIDO;
	protected static String txt_shippingAddressIDO;
	protected static String header_draftedIDO;
	protected static String lbl_docNumber;
	protected static String header_releasedIDO;
	protected static String btn_releaseMRP;
	protected static String btn_firstRownErrorOnGrid;
	protected static String div_quantityNotAvaialableValidation;
	
	/* Allocated - Costing Summary */
	protected static String btn_expandCostingSummary;
	protected static String column_headerAllocatedCostingSummary;
	protected static String td_mainResourseHeaderCostCostingSummary;
	protected static String td_resourceCostAllocatedCostingSummary;
	protected static String td_resurce1CostingSummary;
	protected static String td_resurce2CostingSummary;
	protected static String td_mainMaterialsHeaderCostCostingSummary;
	protected static String td_material1CostingSummary;
	protected static String td_material2CostingSummary;
	protected static String td_material3CostingSummary;
	protected static String td_mainOverheadHeaderCostCostingSummary;
	protected static String td_overhead1CostingSummary;
	protected static String td_overhead2CostingSummary;
	protected static String td_overhead3CostingSummary;
	protected static String td_unitCostHeaderResourseCostingSummary;
	protected static String td_unitCostHeaderMaterialCostingSummary;
	protected static String td_unitCostHeaderOverheadCostingSummary;
	protected static String td_totalCostingSummary;
	protected static String td_unitCostHeaderCostingSummary;
	protected static String td_resourceCost1AllocatedCostingSummary;
	protected static String td_resourceCost2AllocatedCostingSummary;
	protected static String td_materialCostAllocatedCostingSummary;
	protected static String td_material1CostAllocatedCostingSummary;
	protected static String td_material2CostAllocatedCostingSummary;
	protected static String td_material3CostAllocatedCostingSummary;
	protected static String td_overheadCostAllocatedCostingSummary;
	protected static String td_overhead1CostAllocatedCostingSummary;
	protected static String td_overhead2CostAllocatedCostingSummary;
	protected static String td_overhead3CostAllocatedCostingSummary;
	protected static String td_totalCostAllocatedCostingSummary;
	protected static String td_unitCostAllocatedCostingSummary;
	protected static String td_unitCostResourseAllocatedCostingSummary;
	protected static String td_unitCostMateraialAllocatedCostingSummary;
	protected static String td_unitCostOverheadAllocatedCostingSummary;
	
	/* Purchase Order */
	protected static String btn_prcumentModule;
	protected static String btn_purchaseOrder;
	protected static String btn_newPurchaseOrder;
	protected static String btn_purchaseOrderInboundShipmentToPurchaseInvoiceJourney;
	protected static String btn_vendorLookup;
	protected static String txt_vendorInformation;
	protected static String td_resultVendorFirstResult;
	protected static String btn_billingAddressLookup;
	protected static String txt_billingAddresePurchaseOrder;
	protected static String btn_applyAddressPurchaseOrder;
	protected static String btn_productionLookupPurchaseOrderGrid;
	protected static String td_resultProcuctProductionLookupProductReplace;
	protected static String txt_qtyPurchaseOrderRowReplace;
	protected static String txt_unitPricePurchaseOrderRowReplace;
	protected static String btn_goToPage;
	protected static String drop_selectWarehouseInboundShipmentRowReplace;
	protected static String btn_searilBatch;
	protected static String btn_captureProductOne;
	protected static String btn_captureProductTwo;
	protected static String btn_captureProductThree;
	protected static String txt_batchNoSerialBatchCaptureWindow;
	protected static String txt_lotNoSerialBatchCaptureWindow;
	protected static String txt_expiryDateSerialBatchCaptureWindow;
	protected static String btn_nextArrowSerialBatchCaptureWindow;
	protected static String btn_defoultLastDateCalender;
	protected static String txt_menufactureDateSerialBatchCaptureWindow;
	protected static String btn_defoultFirstDateCalender;
	protected static String btn_updateSearielBatchCaptureWindow;
	protected static String btn_backSearielBatchCaptureWindow;
	protected static String header_releasedPurchaseInvoice;
	protected static String btn_addNewRecordToGrid;
	protected static String btn_captureProductOneGreen;
	protected static String btn_captureProductTwoGreen;
	protected static String btn_captureProductThreeGreen;
	
	/* Shop Floor Update */
	protected static String btn_action2;
	protected static String btn_shopFloorUpdate2;
	protected static String header_shopFloorUpdateWindow;
	protected static String btn_startFirstOperationSFU;
	protected static String btn_startSecondLeveleSFU;
	protected static String btn_actualUpdateSFU;
	protected static String txt_outoutQuantityACtualUpdateSFU;
	protected static String btn_upateActualSFU;
	protected static String para_successfullValidationOutProductActualUpdate;
	protected static String btn_tapInputProductsShopFloorUpdate;
	protected static String txt_actualUpdaInputProductQuantityFirstOperation;
	protected static String btn_closeFisrstOperationActualUpdateWindow;
	protected static String btn_completeFirstOperation;
	protected static String btn_completeSecondLeveleSFU;
	protected static String lbl_completedShopFloorUpdateBarFirstOperation;
	protected static String btn_closeSecondOperationActualUpdateWindow;
	protected static String btn_completeSecondOperation;
	protected static String lbl_completedShopFloorUpdateBarSecondOperation;
	protected static String para_successfullValidationActualUpdate;
	protected static String txt_actualUpdaInputProductQuantitySecondOperation;
	protected static String txt_actualUpdaInputProductQuantity2FirstOperation;
	protected static String tab_resourseShopFloorUpdate;
	protected static String div_actualHoursResourseTabRow01;
	protected static String div_actualMinsResourseTabRow01;
	protected static String div_actualHoursResourseTabRow02;
	protected static String div_actualMinsResourseTabRow02;
	protected static String para_successfullValidationActualUpdateResourse;

	/* QC */
	protected static String btn_productionQCAndSorting;
	protected static String drop_documentTypeProductionQC;
	protected static String txt_docNumberQC;
	protected static String div_selectedQcDocProduction;
	protected static String chk_qcProductionAccordingToProductReplaceProduct;
	protected static String txt_passQuantityProductionQCOneRow;
	protected static String txt_failQuantityProductionQCOneRow;
	protected static String btn_releseProductionQC;
	protected static String para_successfullValidationProductionQC;
	protected static String column_headerTotalQtyQC;
	protected static String txt_totalQtyQC;
	protected static String tab_historyQC;
	protected static String column_headerFailedQtyHistoryTabQC;
	protected static String txt_failedQtyQC;
	protected static String tab_historySelectedQC;
	protected static String btn_reverseQC;
	protected static String tab_pendingQC;
	protected static String tab_pendingSelectedQC;
	protected static String div_selectedQcDocProductionHistoryTab;
	protected static String div_qcDocNo;
	protected static String column_headerPassedQtyHistoryTabQC;
	protected static String txt_passedQtyQC;
	
	/* Production Costing */
	protected static String btn_productionCostingActions;
	protected static String header_productionCostingWindow;
	protected static String btn_actualUpdateProductionCosting;
	protected static String tab_inputProductsProductionCosting;
	protected static String txt_actualUpdateProductionCosting;
	protected static String btn_updateProductionCosting;
	protected static String para_successfullValidationActualUpdateProductionCosting;
	protected static String btn_closeSecondWindowCostingUpdate;
	protected static String chk_pendingProductionCostingSelected;
	protected static String btn_updateProductionCostingMainWindow;
	protected static String para_batchCostingUpdateValidation;
	protected static String btn_closeMainCostingUpdateWindowProductionOrder;
	protected static String header_completedPO;
	protected static String chk_pendingProductionCosting;
	
	/* Costing Summary - Actual */
	protected static String column_headerActualCostingSummary;
	protected static String td_resourceCostActualCostingSummary;
	protected static String td_resourceCost1ActualCostingSummary;
	protected static String td_resourceCost2ActualCostingSummary;
	protected static String td_materialCostActualCostingSummary;
	protected static String td_material1CostActualCostingSummary;
	protected static String td_material2CostActualCostingSummary;
	protected static String td_material3CostActualCostingSummary;
	protected static String td_overheadCostActualCostingSummary;
	protected static String td_overhead1CostActualCostingSummary;
	protected static String td_overhead2CostActualCostingSummary;
	protected static String td_overhead3CostActualCostingSummary;
	protected static String td_totalCostActualCostingSummary;
	protected static String td_unitCostActualCostingSummary;
	protected static String td_unitCostResourseActualCostingSummary;
	protected static String td_unitCostMateraialActualCostingSummary;
	protected static String td_unitCostOverheadActualCostingSummary;
	protected static String btn_closeMainValidation;
	
	/* Cost Estimation - Base Amount */
	protected static String column_headerBaseAmountCostingSummary;
	protected static String td_resourceCostBaseAmountCostingSummary;
	protected static String td_resourceCost1BaseAmountCostingSummary;
	protected static String td_resourceCost2BaseAmountCostingSummary;
	protected static String td_materialCostBaseAmountCostingSummary;
	protected static String td_material1CostBaseAmountCostingSummary;
	protected static String td_material2CostBaseAmountCostingSummary;
	protected static String td_material3CostBaseAmountCostingSummary;
	protected static String td_overheadCostBaseAmountCostingSummary;
	protected static String td_overhead1CostBaseAmountCostingSummary;
	protected static String td_overhead2CostBaseAmountCostingSummary;
	protected static String td_overhead3CostBaseAmountCostingSummary;
	protected static String td_totalCostBaseAmountCostingSummary;
	protected static String td_unitCostBaseAmountCostingSummary;
	protected static String td_unitCostResourseBaseAmountCostingSummary;
	protected static String td_unitCostMateraialBaseAmountCostingSummary;
	protected static String td_unitCostOverheadBaseAmountCostingSummary;
	
	/* Genarate Internal Receipt / Journal Entry */
	protected static String btn_convertToInternalReceipt;
	protected static String header_genarateInternalReciptSmoke;
	protected static String chk_seletcOroductForInternalRecipt;
	protected static String btn_genarateInternalRecipt;
	protected static String para_validationInternalREciptGenarateSuccessfull;
	protected static String lnk_genaratedInternalREciptSmoke;
	protected static String header_releasedInternalRecipt;
	protected static String btn_journal;
	protected static String header_journalEntryPopup;
	protected static String lbl_debitValueJournelEntry;
	protected static String lbl_creditValueJournelEntry;
	protected static String lbl_debitTotalJournelEntrySingleEntry;
	protected static String lbl_creditTotalJournelEntrySingleEntry;
	protected static String txt_reciptDateGenarateInternalREceiptWindow;
	protected static String a_documentDate;
	protected static String a_postDate;
	protected static String a_dueDate;
	protected static String div_warehouseInternalRecipt;
	protected static String div_planedQtyInternalRecipt;
	protected static String div_overheadMainList;
	protected static String txt_searchResources;
	protected static String td_resultResourceCodeReplace;
	protected static String div_productionUnitReplaceCode;
	protected static String div_pricingProfileReplaceCode;
	protected static String lbl_debitValueJournelEntryStaging;
	protected static String lbl_creditValueJournelEntryStaging;
	protected static String span_firstErrorOnGridInputProductActualUpdate;
	protected static String div_notEnoughQuantityAvaialableValidation;
	
	/* PROD_E2E_002 */
	protected static String btn_inventoryModule;
	protected static String btn_internalOrderInventoryModule;
	protected static String btn_newInternalOrder;
	protected static String txt_tiltleInternelOrder;
	protected static String btn_wipRequestJourneyIO;
	protected static String drop_toWarehouseIO;
	protected static String btn_requesterIO;
	protected static String txt_requestedIO;
	protected static String td_firstResultOfRequestedLookup;
	protected static String btn_productLookupFirstRowIO;
	protected static String btn_productLookupSecondRowIO;
	protected static String btn_productLookupThirdRowIO;
	protected static String td_resultProductReplaceInfo;
	protected static String btn_productionOrderLookupIOFirstRow;
	protected static String btn_productionOrderLookupIOSecondRow;
	protected static String btn_productionOrderLookupIOThirdRow;
	protected static String txt_searchProductionOrderIO;
	protected static String td_resultProductionOrderIOReplace;
	protected static String drop_operationFirstRow;
	protected static String drop_operationSecondRow;
	protected static String drop_operationThirdRow;
	protected static String txt_qtyFirstRow;
	protected static String txt_qtySecondRow;
	protected static String txt_qtyThirdRow;
	protected static String btn_addNewRowToGridIO;
	protected static String header_draftedIO;
	protected static String header_releasedIO;
	protected static String drop_warehouseIDOFirstRow;
	protected static String drop_warehouseIDOSecondRow;
	protected static String drop_warehouseIDOThirdRow;
	protected static String txt_actualDuarationResourse01ActualUpdate;
	protected static String btn_oneHouserActualDurationResourse01;
	protected static String txt_actualDuarationResourse02ActualUpdate;
	protected static String btn_oneHouserActualDurationResourse02;
	protected static String btn_okActualDurationResourse02;
	protected static String drop_batchNoShopFloor;
	protected static String btn_addNewBatchShopFloor;
	protected static String btn_genarateBatchShopFloor;
	protected static String btn_completeSecondBatchShopFloor;
	protected static String dic_relevantHistoryDocCount;
	protected static String dic_relevantHistoryDocument;
	protected static String div_batchNoReleventToHistoryDocument;
	
	/* Costing Summary Second Batch */
	protected static String td_resourceCostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_resourceCost1ActualCostingSummaryInfiniteSecondBatch;
	protected static String td_resourceCost2ActualCostingSummaryInfiniteSecondBatch;
	protected static String td_materialCostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_material1CostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_material2CostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_material3CostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_overheadCostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_overhead1CostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_overhead2CostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_overhead3CostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_totalCostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostActualCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostResourseActualCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostMateraialActualCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostOverheadActualCostingSummaryInfiniteSecondBatch;
	
	/* Costing Summary - Base Amount - Second Batch */
	protected static String td_resourceCostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_resourceCost1BaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_resourceCost2BaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_materialCostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_material1CostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_material2CostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_material3CostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_overheadCostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_overhead1CostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_overhead2CostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_overhead3CostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_totalCostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostResourseBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostMateraialBaseAmountCostingSummaryInfiniteSecondBatch;
	protected static String td_unitCostOverheadBaseAmountCostingSummaryInfiniteSecondBatch;
	
	/* Generate Internal Receipt */
	protected static String chk_seletcOroductForInternalReciptSecondBatch;
	protected static String lnk_genaratedInternalREciptSmokeSecondBatch;
	
	/* Third Batch */
	protected static String btn_completeThirdBatchShopFloor;
	
	/* Costing Summary - Third Batch - Actual */
	protected static String td_resourceCostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_resourceCost1ActualCostingSummaryInfiniteThirdBatch;
	protected static String td_resourceCost2ActualCostingSummaryInfiniteThirdBatch;
	protected static String td_materialCostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_material1CostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_material2CostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_material3CostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_overheadCostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_overhead1CostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_overhead2CostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_overhead3CostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_totalCostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostActualCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostResourseActualCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostMateraialActualCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostOverheadActualCostingSummaryInfiniteThirdBatch;
	
	/* Costing Summary - Third Batch - Base Amount */
	protected static String td_resourceCostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_resourceCost1BaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_resourceCost2BaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_materialCostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_material1CostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_material2CostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_material3CostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_overheadCostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_overhead1CostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_overhead2CostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_overhead3CostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_totalCostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostResourseBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostMateraialBaseAmountCostingSummaryInfiniteThirdBatch;
	protected static String td_unitCostOverheadBaseAmountCostingSummaryInfiniteThirdBatch;
	
	/*Generate Internal Receipt - Third Batch*/
	protected static String chk_seletcOroductForInternalReciptThirdBatch;
	protected static String lnk_genaratedInternalREciptSmokeThirdBatch;
	
	/* PROD_E2E_003 */
	protected static String chk_allowInventoryWithoutCostingProductInformation;
	protected static String lbl_creditValueJournelEntryStagingStandardCost;
	protected static String td_materialCostAllocatedCostingSummaryBeforePurchase;
	protected static String td_material1CostAllocatedCostingSummaryBeforePurchase;
	protected static String td_material2CostAllocatedCostingSummaryBeforePurchase;
	protected static String td_material3CostAllocatedCostingSummaryBeforePurchase;
	protected static String td_totalCostAllocatedCostingSummaryBeforePurchase;
	protected static String td_unitCostAllocatedCostingSummaryBeforePurchase;
	protected static String td_unitCostMateraialAllocatedCostingSummaryBeforePurchase;
	protected static String btn_okActualDurationResourse01;
	protected static String lbl_creditValueJournelEntryQAStandardCost;

	/*Data*/
	protected static String subjectData;
	protected static String AssignToData;
	protected static String previousBOMData;
	protected static String reasonData;
	protected static String reasonData1;
	protected static String BOOCodeData1;
	protected static String modelCodeData1;
	protected static String modelCode;
	protected static String plannedqtyDataver;
	protected static String productionOrderData;
	protected static String prodNo;
	protected static String previousbulkOrderData;
	protected static String costEstDescription;
	protected static String costProductData;
	protected static String costEstQuantityData;
	protected static String pricingProfileCostData;
	
	/*PROD_PM_017to024*/
	protected static String commonDescription;
	protected static String productGroupRegression;
	protected static String productionTypeProduct;
	protected static String productionTypeProductWithDescription;
	protected static String productionUnit;
	protected static String inputWarehouseAutoSelectedPM;
	protected static String outputWarehouseAutoSelectedPM;
	protected static String wipWarehouseAutoSelectedPM;
	
	/* PROD_PM_026 */
	protected static String barcodeBookPM;
	
	/* PROD_PM_027 */
	protected static String costicPriorityNormalPM;
	
	/* PROD_PM_028 */
	protected static String pricingProfilePM;
	
	/* PROD_PM_030 */
	protected static String bomPM;
	
	/* PROD_PM_035 */
	protected static String booPM;
	
	/* PROD_PM_036 */
	protected static String descriptionBOOPM;
	protected static String batchQuantityPM;
	
	/*PROD_PM_041*/
	protected static String processTimeTypePM;
	
	/* PROD_PM_052 */
	protected static String byProductPM;
	protected static String byProductsQtyPM;
	protected static String byProductsLeadDaysPM;
	
	/* PROD_PM_053_054 */
	protected static String serviceProductBOOPM;
	protected static String batchSpecificProductBOOPM;
	protected static String estimatedAmountServiceProductServiceProductTabBOOPM;
	protected static String leadDaysServiceProductServiceProductTabBOOPM;
	
	/* PROD_PM_55 */
	protected static String resourseRegressionPM;
	
	/* PROD_PM_57 */
	protected static String descriptionStepsTabPM;
	
	/* PROD_PM_067 */
	protected static String minimumLotSizePMRegression;
	protected static String maximumLotSizePMRegression;
	protected static String productionScrapPMRegression;
	protected static String duplicatedProductOnProductionModelRegression;
	
	/* PROD_PM_069 */
	protected static String editedMinimumLotSizePMRegression;
	protected static String editedMaximumLotSizePMRegression;
	protected static String editedBatchQuantityPMRegression;
	protected static String editedDescriptionPMRegression;
	
	/* Production Order */
	/* PROD_PO_011 */
	protected static String selectedProductFrontPageRegressionPO;
	
	/* Pre-Requisites Regression PO */
	protected static String urlReleasedPORegression;
	
	/* PROD_PO_014 */
	protected static String prductionUnitAaccordigToPMRegression;
	protected static String inputWarehouseAaccordigToPMRegression;
	protected static String outputWarehouseAaccordigToPMRegression;
	protected static String wipWarehouseAaccordigToPMRegression;
	protected static String siteAaccordigToPMRegression;
	protected static String costingPriorityAaccordigToPMRegression;
	protected static String bomVarientAaccordigToPMRegression;
	protected static String varientDescriptionAaccordigToPMRegression;
	protected static String booNoAaccordigToPMRegression;
	protected static String booDescriptionAaccordigToPMRegression;
	
	/* PROD_PO_015 */
	protected static String productionOrderPriorityRegression;
	
	/* PROD_PO_016 */
	protected static String planedQtyPORegression;
	
	/* PROD_PO_024 */
	protected static String productionOrderGroupRegression;
	protected static String productionOrderDescriptionRegression;
	
	/* PROD_PO_025 */
	protected static String barcodeBookPORegression;
	protected static String productionUnitRegressionPODupliacted;
	protected static String inputWarehouseRegressionPODupliacted;
	protected static String outputWarehouseRegressionPODupliacted;
	protected static String wipWarehouseRegressionPODupliacted;
	protected static String siteRegressionPODupliacted;
	protected static String costingPriorityRegressionPODupliacted;
	protected static String booRegressionPODupliacted;
	protected static String descriptionDuplicatedPORegression;
	protected static String priorityDuplicatedPORegression;
	
	/* PROD_PO_026 */
	protected static String descriptionEditedPORegression;
	protected static String priorityEditedPORegression;
	protected static String planedQtyEditedPORegression;
	
	/* PROD_PO_050 */
	protected static String productionUnitRegressionPOCopyFrom;
	protected static String inputWarehouseRegressionPOCopyFrom;
	protected static String outputWarehouseRegressionPOCopyFrom;
	protected static String wipWarehouseRegressionPOCopyFrom;
	protected static String siteRegressionPOCopyFrom;
	protected static String costingPriorityRegressionPOCopyFrom;
	protected static String descriptionCopyFromPORegression;
	protected static String priorityCopyFromPORegression;
	
	/* Production Multi Platform Scenarios */
	/* PROD_E2E_001 */
	protected static String descriptionBOM;
	protected static String productionGroupE2E;
	protected static String menfacTypeProduction;
	protected static String productDesriptionProductInfo;
	protected static String hundred;
	protected static String rowMaterialQtyOne;
	protected static String rowMaterialQtyTwo;
	protected static String rowMaterialQtyThree;
	protected static String scrapRowMaterilThree;
	protected static String overheadDescription;
	protected static String rateTypeOverhead;
	protected static String resourseDescription;
	protected static String employeeResourseCreation;
	protected static String resourseOneHourlyRate;
	protected static String hourlyRateOfOverheadeAttachedToResourseOne;
	protected static String hourlyRateOfOverheadeAttachedToResourseTwo;
	protected static String hourlyRateOfOverheadeTwoAttachedToResourseTwo;
	protected static String elementCategorySupply;
	protected static String activityTypeMoveStock;
	protected static String elementCategoryOperation;
	protected static String activityTypeProduce;
	protected static String produceElementDescription1;
	protected static String groupIdBOOOne;
	protected static String batchMethodTransferBOO;
	protected static String produceElementDescription2;
	protected static String groupIdBOOTwo;
	protected static String activityTypeQualityCheck;
	protected static String qcElementDescription;
	protected static String groupIdBOOThree;
	protected static String moveStockElementDescription;
	protected static String inputWarehouseLI;
	protected static String outputWarehouseLI;
	protected static String wipWarehouseLI;
	protected static String locationDescription;
	protected static String locationAddress;
	protected static String materialEstimationTabPP;
	protected static String labourResorceEstimatioTabPP;
	protected static String overheadEstimatioTabPP;
	protected static String serviceEstimatioTabPP;
	protected static String expenceEstimatioTabPP;
	protected static String materialActualTabPP;
	protected static String labourResourseActualTabPP;
	protected static String overheadActualTabPP;
	protected static String serviceActualTabPP;
	protected static String expenceActualTabPP;
	protected static String descriptionPP;
	protected static String pricingProfileGroup;

	/*Production Model*/
	protected static String batchQuantityE2E;
	
	/*Production Order*/
	protected static String planedQtyE2E;
	
	/*MRP Run*/
	protected static String shippingAddress;
	
	/* Purchase Order */
	protected static String qtyProductPurchaseOrder;
	protected static String unitPriceProduct1PurchaseOrder;
	protected static String unitPriceProduct2PurchaseOrder;
	protected static String unitPriceProduct3PurchaseOrder;
	protected static String billingAddressPO;
	
	/* Shop Floor Update */
	protected static String actualQuantityOutputProductFirstOperation;
	protected static String actualQuantityInputProductFirstOperation;
	protected static String actualQuantityOutputProductSecondOperation;
	protected static String actualQuantityInputProductSecondOperation;
	protected static String actualQuantity2InputProductFirstOperation;
	
	/* QC */
	protected static String qcTypeProductionOrder;
	protected static String firstFailQty;
	protected static String secondFailQty;
	protected static String thirdFailQty;
	protected static String thirdPassQty;
	
	/* Production Costing */
	protected static String actualUpdateQuantityProductionQuntity;
	
	/* PROD_E2E_002 */
	protected static String titleInternelOrder;
	protected static String stepOnePO;
	protected static String stepTwoPO;
	protected static String internalOrderQtyWIPRequest;
	
	/*PROD_E2E_003*/
	protected static String standardCostPM;

	// Calling the constructor
	public static void readElementlocators() throws Exception {
		siteLogo = findElementInXLSheet(getParameterProduction, "site logo");
		txt_username = findElementInXLSheet(getParameterProduction, "user name");
		txt_password = findElementInXLSheet(getParameterProduction, "password");
		btn_login = findElementInXLSheet(getParameterProduction, "login button");
		lnk_home = findElementInXLSheet(getParameterProduction, "home_lnk");

		/* Navigate to side menu */
		navigateMenu = findElementInXLSheet(getParameterProduction, "nav_btn");
		sideNavBar = findElementInXLSheet(getParameterProduction, "nav_view");
		btn_Production = findElementInXLSheet(getParameterProduction, "production_btn");
		logo_Production = findElementInXLSheet(getParameterProduction, "production_logo");
		
		btn_inventory = findElementInXLSheet(getParameterProduction, "inventory_btn");
		txt_inventory = findElementInXLSheet(getParameterProduction, "inventory_txt");
		btn_billOfMaterial = findElementInXLSheet(getParameterProduction, "billOfMaterial_btn");
		label_billOfMaterial = findElementInXLSheet(getParameterProduction, "billOfMaterial_label");
		btn_newBOM = findElementInXLSheet(getParameterProduction, "newBOM_btn");
		txt_newBOM = findElementInXLSheet(getParameterProduction, "newBOM_txt");
		txt_desc = findElementInXLSheet(getParameterProduction, "txt_desc");
		btn_productGrp = findElementInXLSheet(getParameterProduction, "productGrp_btn");
		txt_BOMQuantity = findElementInXLSheet(getParameterProduction, "BOMQuantity_txt");
		btn_expiryDate = findElementInXLSheet(getParameterProduction, "expiryDate_btn");
		clickExpiryDate = findElementInXLSheet(getParameterProduction, "click_ExpiryDate");
		btn_rawMaterial = findElementInXLSheet(getParameterProduction, "rawMaterial_btn");
		txt_rawMaterial = findElementInXLSheet(getParameterProduction, "rawMaterial_txt");
		doubleClickrawMaterial = findElementInXLSheet(getParameterProduction, "rawMaterialDoubleClick");
		txt_rawQuantity = findElementInXLSheet(getParameterProduction, "rawQuantity_txt");
		btn_draft = findElementInXLSheet(getParameterProduction, "draft_btn");
		btn_release = findElementInXLSheet(getParameterProduction, "release_btn");
		btn_outputProductSearch = findElementInXLSheet(getParameterProduction, "outputProductSearch_btn");
		txt_outputProductSearch = findElementInXLSheet(getParameterProduction, "outputProductSearch_txt");
		doubleClickoutputProduct = findElementInXLSheet(getParameterProduction, "outputputProductDoubleclick");
		
		
		btn_production = findElementInXLSheet(getParameterProduction, "production_btn");
		txt_production = findElementInXLSheet(getParameterProduction, "production_txt");
		btn_BOO = findElementInXLSheet(getParameterProduction, "BOO_btn");
		txt_BOO = findElementInXLSheet(getParameterProduction, "BOO_txt");
		btn_newBOO = findElementInXLSheet(getParameterProduction, "newBOO_btn");
		txt_newBOO = findElementInXLSheet(getParameterProduction, "newBOO_txt");
		txt_BOOCode = findElementInXLSheet(getParameterProduction, "BOOCode_txt");
		btn_descriptionBOO = findElementInXLSheet(getParameterProduction, "descriptionBOO_btn");
		txt_descrBOO = findElementInXLSheet(getParameterProduction, "descrBOO_txt");
		btn_BOOApply = findElementInXLSheet(getParameterProduction, "BOOApply_btn");
		txt_BOOLogo = findElementInXLSheet(getParameterProduction, "BOOLogo_txt");
		btn_plusBOO = findElementInXLSheet(getParameterProduction, "plusBOO_btn");
		txt_ElementCategory = findElementInXLSheet(getParameterProduction, "ElementCategory_txt");
		txt_ElementDescription = findElementInXLSheet(getParameterProduction, "ElementDescription_txt");
		btn_applyOperation = findElementInXLSheet(getParameterProduction, "applyOperation_btn");
		label_operationActivity = findElementInXLSheet(getParameterProduction, "operationActivity_label");
		
		txt_ElementCategoryOperation = findElementInXLSheet(getParameterProduction, "ElementCategoryOperation_txt");
		txt_ElementDescriptionOperation = findElementInXLSheet(getParameterProduction, "ElementDescriptionOperation_txt");
		txt_GroupId = findElementInXLSheet(getParameterProduction, "GroupId_txt");
		txt_activityType = findElementInXLSheet(getParameterProduction, "activityType_txt");
		txt_FinalInvestigation = findElementInXLSheet(getParameterProduction, "finalInvestigation_txt");
		btn_plusBOOSupply = findElementInXLSheet(getParameterProduction, "plusBOOSupply_btn");
		btn_plusBOOOperation = findElementInXLSheet(getParameterProduction, "plusBOOOperation_btn");
		btn_plusBOOQCBillOfOperation = findElementInXLSheet(getParameterProduction, "plusBOOQCBillOfOperation_btn");
		
		
		btn_supplyBillOfOperationPlusMark = findElementInXLSheet(getParameterProduction, "supplyBillOfOperationPlusMark_btn");
		btn_productionModel = findElementInXLSheet(getParameterProduction, "productionModel_btn");
		txt_productionModel = findElementInXLSheet(getParameterProduction, "productionModel_txt");
		btn_newProductionModel = findElementInXLSheet(getParameterProduction, "newProductionModel_btn");
		txt_modelCode = findElementInXLSheet(getParameterProduction, "modelCode_txt");
		txt_productGroup = findElementInXLSheet(getParameterProduction, "productGroup_txt");
		btn_yes = findElementInXLSheet(getParameterProduction, "yes_btn");
		btn_ProductionUnit = findElementInXLSheet(getParameterProduction, "ProductionUnit_btn");
		txt_Unit = findElementInXLSheet(getParameterProduction, "Unit_txt");
		doubleClickProductionUnit = findElementInXLSheet(getParameterProduction, "ProductionUnitDoubleclick");
		
		btn_MRPEnabled = findElementInXLSheet(getParameterProduction, "MRPEnabled_btn");
		btn_Infinite = findElementInXLSheet(getParameterProduction, "Infinite_btn");
		txt_batchQty = findElementInXLSheet(getParameterProduction, "batchQty_txt");
		txt_barcodeBook = findElementInXLSheet(getParameterProduction, "barcodeBook_txt");
		txt_CostingPriority = findElementInXLSheet(getParameterProduction, "CostingPriority_txt");
		txt_standardCost = findElementInXLSheet(getParameterProduction, "standardCost_txt");
		btn_pricingProfile = findElementInXLSheet(getParameterProduction, "pricingProfile_btn");
		txt_PricingProfile = findElementInXLSheet(getParameterProduction, "PricingProfile_txt");
		doubleClickPricingProfile = findElementInXLSheet(getParameterProduction, "PricingProfileDoubleclick");
		txt_newproductionModel = findElementInXLSheet(getParameterProduction, "newProductionModel_txt");
		
		btn_BOONoSearch = findElementInXLSheet(getParameterProduction, "BOONoSearch_btn");
		txt_BOOOperation = findElementInXLSheet(getParameterProduction, "BOOOperation_txt");
		doubleClickBOOOperation = findElementInXLSheet(getParameterProduction, "doubleclick_BOOOperation");
		btn_informationOK = findElementInXLSheet(getParameterProduction, "informationOK_btn");
		
		btn_BOOTab = findElementInXLSheet(getParameterProduction, "BOOTab_btn");
		btn_Expand = findElementInXLSheet(getParameterProduction, "Expand_btn");
		btn_supply = findElementInXLSheet(getParameterProduction, "supply_btn");
		btn_inputProducts = findElementInXLSheet(getParameterProduction, "inputProducts_btn");
		btn_checkBox = findElementInXLSheet(getParameterProduction, "checkBox_btn");
		btn_apply = findElementInXLSheet(getParameterProduction, "apply_btn");
		
		btn_product = findElementInXLSheet(getParameterProduction, "product_btn");
		txt_product = findElementInXLSheet(getParameterProduction, "product_txt");
		btn_operation = findElementInXLSheet(getParameterProduction, "operation_btn");
		doubleClickproduct = findElementInXLSheet(getParameterProduction, "doubleClickProduct data");
		txt_processTime = findElementInXLSheet(getParameterProduction, "processTime_txt");
		btn_QC = findElementInXLSheet(getParameterProduction, "QC_btn");
		
		btn_productionOrder = findElementInXLSheet(getParameterProduction, "productionOrder_btn");
		btn_newProductionOrder = findElementInXLSheet(getParameterProduction, "newProductionOrder_btn");
		txt_productionOrder = findElementInXLSheet(getParameterProduction, "productionOrder_txt");
		btn_outputProduct = findElementInXLSheet(getParameterProduction, "outputProduct_btn");
		txt_plannedqty = findElementInXLSheet(getParameterProduction, "plannedqty_txt");
		
		txt_productionOrderGroup = findElementInXLSheet(getParameterProduction, "productionOrderGroup_txt");
		txt_productionOrderDesc = findElementInXLSheet(getParameterProduction, "productionOrderDesc_txt");
		btn_productionOrderModel = findElementInXLSheet(getParameterProduction, "productionOrderModel_btn");
		txt_productionOrderModel = findElementInXLSheet(getParameterProduction, "productionOrderModel_txt");
		doubleClickOrderModel = findElementInXLSheet(getParameterProduction, "doubleClick OrderModel");
		txt_productionOrderNo = findElementInXLSheet(getParameterProduction, "productionOrderNo_txt");
		
		btn_ProductionControl = findElementInXLSheet(getParameterProduction, "ProductionControl_btn");
		txt_searchProductionOrder = findElementInXLSheet(getParameterProduction, "searchProductionOrder_txt");
		btn_searchProductionOrder = findElementInXLSheet(getParameterProduction, "searchProductionOrder_btn");
		btn_checkBoxProductionOrderNo = findElementInXLSheet(getParameterProduction, "checkBoxProductionOrderNo_btn");
		btn_action = findElementInXLSheet(getParameterProduction, "action_btn");
		btn_RunMRP = findElementInXLSheet(getParameterProduction, "RunMRP_btn");
		txt_MRPNo = findElementInXLSheet(getParameterProduction, "MRPNo_txt");
		btn_run = findElementInXLSheet(getParameterProduction, "run_btn");
		btn_releaseProductionOrder = findElementInXLSheet(getParameterProduction, "releaseProductionOrder_btn");
		btn_close = findElementInXLSheet(getParameterProduction, "close_btn");
		
		btn_entutionLogo = findElementInXLSheet(getParameterProduction, "entutuionLogo_btn");
		btn_taskEvent = findElementInXLSheet(getParameterProduction, "taskEvent_btn");
		btn_InternalDispatchOrder = findElementInXLSheet(getParameterProduction, "InternalDispatchOrder_btn");
		btn_arrow = findElementInXLSheet(getParameterProduction, "arrow_btn");
		txt_shippingAddress = findElementInXLSheet(getParameterProduction, "shippingAddress_txt");
		
		txt_plannedQty = findElementInXLSheet(getParameterProduction, "plannedQty_txt");
		btn_serialCapture = findElementInXLSheet(getParameterProduction, "serialCapture_btn");
		btn_productList = findElementInXLSheet(getParameterProduction, "productList_btn");
		txt_BatchNo = findElementInXLSheet(getParameterProduction, "BatchNo_txt");
		btn_back = findElementInXLSheet(getParameterProduction, "back_btn");
		
		btn_productionOrderNo = findElementInXLSheet(getParameterProduction, "productionOrderNo_btn");
		btn_shopFloorUpdate = findElementInXLSheet(getParameterProduction, "shopFloorUpdate_btn");
		btn_start = findElementInXLSheet(getParameterProduction, "start_btn");
		btn_startProcess = findElementInXLSheet(getParameterProduction, "startProcess_btn");
		btn_actual = findElementInXLSheet(getParameterProduction, "actual_btn");
		txt_producedQty = findElementInXLSheet(getParameterProduction, "producedQty_txt");
		btn_update = findElementInXLSheet(getParameterProduction, "update_btn");
		btn_inputProduct = findElementInXLSheet(getParameterProduction, "inputProduct_btn");
		txt_actualQty = findElementInXLSheet(getParameterProduction, "actualQty_txt");
		btn_complete = findElementInXLSheet(getParameterProduction, "complete_btn");
		txt_prodOrderNo = findElementInXLSheet(getParameterProduction, "prodOrderNo_txt");
		btn_searchProdOrder = findElementInXLSheet(getParameterProduction, "searchProdOrder_btn");
		doubleClickprodNo = findElementInXLSheet(getParameterProduction, "doubleclick prodNo");
		
		
		
		btn_labelPrint = findElementInXLSheet(getParameterProduction, "labelPrint_btn");
		btn_view = findElementInXLSheet(getParameterProduction, "view_btn");
		txt_batchNo = findElementInXLSheet(getParameterProduction, "batchNo_txt");
		txt_machineNo = findElementInXLSheet(getParameterProduction, "machineNo_txt");
		txt_color = findElementInXLSheet(getParameterProduction, "color_txt");
		btn_draftNPrint = findElementInXLSheet(getParameterProduction, "draftNPrint_btn");
		
		btn_labelPrintHistory = findElementInXLSheet(getParameterProduction, "labelPrintHistory_btn");
		btn_productOrder = findElementInXLSheet(getParameterProduction, "productOrder_btn");
		btn_QcComplete = findElementInXLSheet(getParameterProduction, "QcComplete_btn");
		
		txt_productonOrder = findElementInXLSheet(getParameterProduction, "productionOrder_txt");
		btn_costingRefresh = findElementInXLSheet(getParameterProduction, "costingRefresh_btn");
		btn_tick = findElementInXLSheet(getParameterProduction, "tick_btn");
		btn_updateCosting = findElementInXLSheet(getParameterProduction, "updateCosting_btn");
		btn_inputProductsCostingTab = findElementInXLSheet(getParameterProduction, "inputProductsCostingTab_btn");
		btn_closeCosting = findElementInXLSheet(getParameterProduction, "closeCosting_btn");
		btn_mainUpdate= findElementInXLSheet(getParameterProduction, "mainUpdate_btn");
		
		btn_organizationManagement = findElementInXLSheet(getParameterProduction, "organizationManagement_btn");
		txt_organizationManagement = findElementInXLSheet(getParameterProduction, "organizationManagement_txt");
		
		
		btn_locationInfo = findElementInXLSheet(getParameterProduction, "locationInfo_btn");
		txt_productionUnit = findElementInXLSheet(getParameterProduction, "productionUnit_txt");
		txt_ProductionUnitLookup = findElementInXLSheet(getParameterProduction, "productionUnitLookup_txt");
		doubleClickProductUnit = findElementInXLSheet(getParameterProduction, "doubleClick ProductUnit");
		txt_locationInformation = findElementInXLSheet(getParameterProduction, "locationInformation_txt");
		
		
		btn_newLocation = findElementInXLSheet(getParameterProduction, "newLocation_btn");
		txt_locationCode = findElementInXLSheet(getParameterProduction, "locationCode_txt");
		txt_description = findElementInXLSheet(getParameterProduction, "description_txt");
		txt_businessUnit = findElementInXLSheet(getParameterProduction, "businessUnit_txt");
		txt_inputWare = findElementInXLSheet(getParameterProduction, "inputWare_txt");
		txt_outWare = findElementInXLSheet(getParameterProduction, "outWare_txt");
		txt_WIPWare = findElementInXLSheet(getParameterProduction, "WIPWare_txt");
		txt_site = findElementInXLSheet(getParameterProduction, "site_txt");
		txt_newLocation = findElementInXLSheet(getParameterProduction, "newLocation_txt");
		btn_productionUnit = findElementInXLSheet(getParameterProduction, "productionUnit_btn");
		
		txt_overheadInformation = findElementInXLSheet(getParameterProduction, "overheadInformation_txt");
		btn_searchNewLocation = findElementInXLSheet(getParameterProduction, "searchNewLocation_txt");
		txt_overhead = findElementInXLSheet(getParameterProduction, "overhead_txt");
		doubleClickOverhead = findElementInXLSheet(getParameterProduction, "doubleClick overhead");
		
		btn_draftLocation = findElementInXLSheet(getParameterProduction, "draftLocation_btn");
		txt_activate = findElementInXLSheet(getParameterProduction, "activate_txt");
		
		btn_PricingProfile = findElementInXLSheet(getParameterProduction, "PricingProfile_btn");
		btn_newPricingProfile = findElementInXLSheet(getParameterProduction, "newPricingProfile_btn");
		txt_pricingProfileCode = findElementInXLSheet(getParameterProduction, "pricingProfileCode_btn");
		txt_descPricingProfile = findElementInXLSheet(getParameterProduction, "descPricingProfile_txt");
		txt_pricingGroup = findElementInXLSheet(getParameterProduction, "pricingGroup_txt");
		
		txt_material = findElementInXLSheet(getParameterProduction, "material_txt");
		txt_labour = findElementInXLSheet(getParameterProduction, "labour_txt");
		txt_Overhead = findElementInXLSheet(getParameterProduction, "Overhead_txt");
		txt_service = findElementInXLSheet(getParameterProduction, "service_txt");
		txt_expense = findElementInXLSheet(getParameterProduction, "expense_txt");
		btn_estimate = findElementInXLSheet(getParameterProduction, "estimate_btn");
		
		btn_actualCalculation = findElementInXLSheet(getParameterProduction, "actualCalculation_btn");
		txt_materialAC = findElementInXLSheet(getParameterProduction, "materialAC_txt");
		txt_labourAC = findElementInXLSheet(getParameterProduction, "labourAC_txt");
		txt_OverheadAC = findElementInXLSheet(getParameterProduction, "OverheadAC_txt");
		txt_serviceAC = findElementInXLSheet(getParameterProduction, "serviceAC_txt");
		txt_expenseAC = findElementInXLSheet(getParameterProduction, "expenseAC_txt");
		
		btn_closeShop = findElementInXLSheet(getParameterProduction, "closeShop_btn");
		btn_completeTick = findElementInXLSheet(getParameterProduction, "completeTick_btn");
		txt_complete = findElementInXLSheet(getParameterProduction, "complete_txt");
		
		btn_active = findElementInXLSheet(getParameterProduction, "active_btn");
		btn_delete = findElementInXLSheet(getParameterProduction, "delete_btn");
		
		txt_rate = findElementInXLSheet(getParameterProduction, "rate_btn");
		
		txt_overheadInfo = findElementInXLSheet(getParameterProduction, "overheadInfo_btn");
		btn_new = findElementInXLSheet(getParameterProduction, "new_btn");
		txt_overheadCode = findElementInXLSheet(getParameterProduction, "overheadCode_btn");
		txt_descOverhead = findElementInXLSheet(getParameterProduction, "decOverhead_btn");
		txt_overheadGroup = findElementInXLSheet(getParameterProduction, "overheadGroup_btn");
		txt_rateType = findElementInXLSheet(getParameterProduction, "rateType_btn");
		btn_draftOverhead = findElementInXLSheet(getParameterProduction, "draftOverhead_btn");
		txt_newOverheadCode = findElementInXLSheet(getParameterProduction, "newOverheadCode_txt");
		
		txt_BOM = findElementInXLSheet(getParameterProduction, "BOM_txt");
		btn_started = findElementInXLSheet(getParameterProduction, "started_btn");
		btn_open = findElementInXLSheet(getParameterProduction, "open_btn");
		
		btn_bulkProductionOrder = findElementInXLSheet(getParameterProduction, "bulkProductionOrder_btn");
		new_bulkProductionOrder = findElementInXLSheet(getParameterProduction, "bulkProductionOrdernew_btn");
		txt_descrProductionOrderGroup = findElementInXLSheet(getParameterProduction, "descrProductionOrderGroup_txt");
		txt_referenceType = findElementInXLSheet(getParameterProduction, "referenceType_txt");
		
		txt_BulkProduction = findElementInXLSheet(getParameterProduction, "BulkProduction_txt");
		txt_BulkProductionForm = findElementInXLSheet(getParameterProduction, "BulkProductionForm_txt");
		
		btn_productionUnitBulk = findElementInXLSheet(getParameterProduction, "productionUnitBulk_btn");
		txt_productionUnitBulk = findElementInXLSheet(getParameterProduction, "productionUnitBulk_txt");
		doubleClickProductionUnitBulk = findElementInXLSheet(getParameterProduction, "doubleClick ProductionUnitBulk");
		
		btn_productTab = findElementInXLSheet(getParameterProduction, "productTab_btn");
		btn_productSearch = findElementInXLSheet(getParameterProduction, "productSearch_btn");
		txt_productSearch = findElementInXLSheet(getParameterProduction, "productSearch_txt");
		doubleClickProductSearch = findElementInXLSheet(getParameterProduction, "doubleClick ProductSearch");
		btn_productModel = findElementInXLSheet(getParameterProduction, "productModel_btn");
		txt_productModel = findElementInXLSheet(getParameterProduction, "productModel_txt");
		doubleClickProductModel = findElementInXLSheet(getParameterProduction, "doubleClick ProductModel");
		btn_checkOut = findElementInXLSheet(getParameterProduction, "checkOut_btn");
		
		btn_createInternalOrder = findElementInXLSheet(getParameterProduction, "createInternalOrder_btn");
		btn_selectAll = findElementInXLSheet(getParameterProduction, "selectAll_btn");
		
		txt_title = findElementInXLSheet(getParameterProduction, "title_txt");
		btn_requester = findElementInXLSheet(getParameterProduction, "requester_btn");
		txt_requester = findElementInXLSheet(getParameterProduction, "requester_txt");
		doubleClickRequester = findElementInXLSheet(getParameterProduction, "doubleClick Requester");
		
		
		btn_goTopage = findElementInXLSheet(getParameterProduction, "goToPage_btn");
		
		btn_resources = findElementInXLSheet(getParameterProduction, "resources_btn");
		btn_resourceSearch = findElementInXLSheet(getParameterProduction, "resourceSearch_btn");
		txt_additionalResources = findElementInXLSheet(getParameterProduction, "additionalResources_btn");
		doubleCLickAdditinalResources = findElementInXLSheet(getParameterProduction, "doubleCLickAdditinal Resources");
		
		txt_plannedQtyBulk = findElementInXLSheet(getParameterProduction, "plannedQtyBulk_txt");
		btn_draftProductionOrder = findElementInXLSheet(getParameterProduction, "draftProductionOrder_btn");
		btn_releaseBulkProductionOrder = findElementInXLSheet(getParameterProduction, "releaseBulkProductionOrder_btn");
		btn_actionBulk = findElementInXLSheet(getParameterProduction, "actionBulk_btn");
		
		btn_applyInternal = findElementInXLSheet(getParameterProduction, "applyInternal_btn");
		txt_shipping = findElementInXLSheet(getParameterProduction, "shipping_txt");
		btn_productionCosting = findElementInXLSheet(getParameterProduction, "productionCosting_btn");
		btn_date = findElementInXLSheet(getParameterProduction, "date_btn");
		btn_input = findElementInXLSheet(getParameterProduction, "input_btn");
		btn_internalReceipt = findElementInXLSheet(getParameterProduction, "internalReceipt_btn");
		btn_smallcheckBox = findElementInXLSheet(getParameterProduction, "smallcheckBox_btn");
		btn_generate = findElementInXLSheet(getParameterProduction, "generate_btn");
		txt_lotNo = findElementInXLSheet(getParameterProduction, "lotNo_txt");
		
		txt_bulkOrder = findElementInXLSheet(getParameterProduction, "bulkOrder_txt");
		bulkOrder = findElementInXLSheet(getParameterProduction, "bulk Order");
		bulkno = findElementInXLSheet(getParameterProduction, "bulk no");
		
		txt_standardCost1 = findElementInXLSheet(getParameterProduction, "standardCost1_txt");
		
		btn_inputProductsTab = findElementInXLSheet(getParameterProduction, "inputProductsTab_btn");
		btn_searchProduct = findElementInXLSheet(getParameterProduction, "searchProduct_btn");
		txt_searchProduct = findElementInXLSheet(getParameterProduction, "searchProduct_txt");
		doubleClickSearchProduct = findElementInXLSheet(getParameterProduction, "doubleClickSearch Product");
		txt_plannedqty1 = findElementInXLSheet(getParameterProduction, "plannedqty1_txt");
		
		btn_closeTab = findElementInXLSheet(getParameterProduction, "closeTab_btn");
		btn_actualDate = findElementInXLSheet(getParameterProduction, "actualDate_btn");
		
		txt_actualQty1 = findElementInXLSheet(getParameterProduction, "actualQty1_txt");
		btn_releaseTab = findElementInXLSheet(getParameterProduction, "releaseTab_btn");
		txt_internalReceipt = findElementInXLSheet(getParameterProduction, "internalReceipt_txt");
		txt_internalDispatchRelease = findElementInXLSheet(getParameterProduction, "internalDispatchRelease_txt");
		txt_internalOrderRelease = findElementInXLSheet(getParameterProduction, "internalOrderRelease_txt");
		txt_BulkOrderRelease = findElementInXLSheet(getParameterProduction, "BulkOrderRelease_txt");
		
		txt_productionModelRelease = findElementInXLSheet(getParameterProduction, "productionModelRelease_txt");
		txt_POReleased = findElementInXLSheet(getParameterProduction, "POReleased_txt");
		txt_internalDispatchOrderReleased = findElementInXLSheet(getParameterProduction, "internalDispatchOrderReleased_txt");
		txt_Pricing = findElementInXLSheet(getParameterProduction, "Pricing_txt");
		txt_activateStatus = findElementInXLSheet(getParameterProduction, "activateStatus_txt");
		txt_openStatus = findElementInXLSheet(getParameterProduction, "openStatus_txt");
		
		btn_Capture = findElementInXLSheet(getParameterProduction, "capture_btn");
		btn_productListClick = findElementInXLSheet(getParameterProduction, "productListClick_btn");
		txt_batchNoTXT = findElementInXLSheet(getParameterProduction, "batchNoTXT_txt");
		btn_refresh = findElementInXLSheet(getParameterProduction, "refresh_btn");
		
		btn_internalReceiptControl = findElementInXLSheet(getParameterProduction, "internalReceiptControl_btn");
		
		link_internalReceipt  = findElementInXLSheet(getParameterProduction, "internalReceipt_link");
		btn_journalEntry  = findElementInXLSheet(getParameterProduction, "journalEntry_btn");
		link_gernalEntryDetails = findElementInXLSheet(getParameterProduction, "gernalEntryDetails_link");
		btn_actionCosting =findElementInXLSheet(getParameterProduction, "actionCosting_btn");
		btn_generateReceipt = findElementInXLSheet(getParameterProduction, "generateReceipt_btn"); 
		txt_journalEntryReleased = findElementInXLSheet(getParameterProduction, "journalEntryReleased_txt");
		btn_actionJournal = findElementInXLSheet(getParameterProduction, "actionJournal_btn");
		
		txt_BOONo = findElementInXLSheet(getParameterProduction, "BOONO_txt");
		txt_internalReceiptReleased = findElementInXLSheet(getParameterProduction, "internalReceiptReleased_txt");
		
		
		
		btn_productInfo = findElementInXLSheet(getParameterProduction, "productInfo_btn");
		btn_newProduct = findElementInXLSheet(getParameterProduction, "newProduct_btn");
		txt_productCode = findElementInXLSheet(getParameterProduction, "productCode_txt");
		txt_productDescription = findElementInXLSheet(getParameterProduction, "productDescription_txt");
		txt_prodGrop = findElementInXLSheet(getParameterProduction, "prodGrop_txt");
		txt_basePrice = findElementInXLSheet(getParameterProduction, "basePrice_txt");
		btn_manufacturer = findElementInXLSheet(getParameterProduction, "manufacturer_btn");
		txt_manufacturer = findElementInXLSheet(getParameterProduction, "manufacturer_txt");
		doubleCLickManufacturer = findElementInXLSheet(getParameterProduction, "doubleCLick Manufacturer");
		txt_UOMGroup = findElementInXLSheet(getParameterProduction, "UOMGroup_txt");
		txt_UOM = findElementInXLSheet(getParameterProduction, "UOM_txt");
		txt_length = findElementInXLSheet(getParameterProduction, "length_txt");
		txt_lengthVal = findElementInXLSheet(getParameterProduction, "lengthVal_txt");
		txt_width = findElementInXLSheet(getParameterProduction, "width_txt");
		txt_widthVal = findElementInXLSheet(getParameterProduction, "widthVal_txt");
		txt_height = findElementInXLSheet(getParameterProduction, "height_txt");
		txt_heightVal = findElementInXLSheet(getParameterProduction, "heightVal_txt");
		txt_volume = findElementInXLSheet(getParameterProduction, "volume_txt");
		txt_VolumeVal = findElementInXLSheet(getParameterProduction, "volumeVal_txt");
		btn_tabDetails = findElementInXLSheet(getParameterProduction, "tabDetails_btn");
		btn_allowInventoryWithoutCosting = findElementInXLSheet(getParameterProduction, "allowInventoryWithoutCosting_btn");
		txt_manufacturingType = findElementInXLSheet(getParameterProduction, "manufacturingType_txt");
		txt_batchBook = findElementInXLSheet(getParameterProduction, "batchBook_txt");
		txt_productName = findElementInXLSheet(getParameterProduction, "productName_txt");
		txt_draftProductionOrder = findElementInXLSheet(getParameterProduction, "txt_draftProductionOrder");
		
		
		/*New Regression Test Cases*/
		btn_productionorder = findElementInXLSheet(getParameterProduction, "productionorder_btn");
		btn_edit = findElementInXLSheet(getParameterProduction, "edit_btn");
		btn_Update = findElementInXLSheet(getParameterProduction, "Update_btn");
		txt_searchProductionOrder1 = findElementInXLSheet(getParameterProduction, "searchProductionOrder1_txt");
		btn_duplicate = findElementInXLSheet(getParameterProduction, "duplicate_btn");
		txt_outputProduct1 = findElementInXLSheet(getParameterProduction, "outputProduct1_txt");
		txt_outputProduct2 = findElementInXLSheet(getParameterProduction, "outputProduct2_txt");
		txt_costingPriority1 = findElementInXLSheet(getParameterProduction, "costingPriority1_txt");
		txt_costingPriority2 = findElementInXLSheet(getParameterProduction, "costingPriority2_txt");
		txt_prodOrderGroup1 = findElementInXLSheet(getParameterProduction, "prodOrderGroup1_txt");
		txt_prodOrderGroup2 = findElementInXLSheet(getParameterProduction, "prodOrderGroup2_txt");
		txt_description1 = findElementInXLSheet(getParameterProduction, "description1_txt");
		txt_description2 = findElementInXLSheet(getParameterProduction, "description2_txt");
		txt_prodModel1 = findElementInXLSheet(getParameterProduction, "prodModel1_txt");
		txt_prodModel2 = findElementInXLSheet(getParameterProduction, "prodModel2_txt");
		
		
		
		/* QA Form level action verification */
		txt_draftBOM = findElementInXLSheet(getParameterProduction, "draftBOM_txt");
		txt_editBOM = findElementInXLSheet(getParameterProduction, "editBOM_txt");
		txt_updateBOM = findElementInXLSheet(getParameterProduction, "updateBOM_txt");
		txt_duplicateBOM = findElementInXLSheet(getParameterProduction, "duplicateBOM_txt");
		btn_draftNNew = findElementInXLSheet(getParameterProduction, "draftNNew_btn");
		txt_draftNNewBOM = findElementInXLSheet(getParameterProduction, "draftNNewBOM_txt");
		btn_copyFrom = findElementInXLSheet(getParameterProduction, "copyFrom_btn");
		txt_BOm = findElementInXLSheet(getParameterProduction, "Bom_txt");
		txt_previousBOM = findElementInXLSheet(getParameterProduction, "previousBOM_txt");
		doublePreviousBOM = findElementInXLSheet(getParameterProduction, "doublePrevious BOM");
		txt_num = findElementInXLSheet(getParameterProduction, "num_txt");
		btn_hold = findElementInXLSheet(getParameterProduction, "hold_btn");
		txt_changeStatus = findElementInXLSheet(getParameterProduction, "changeStatus_txt");
		txt_reason = findElementInXLSheet(getParameterProduction, "reason_txt");
		btn_ok = findElementInXLSheet(getParameterProduction, "ok_btn");
		btn_unHold = findElementInXLSheet(getParameterProduction, "unHold_btn");
		btn_newVersion = findElementInXLSheet(getParameterProduction, "newVersion_btn");
		txt_validMsg = findElementInXLSheet(getParameterProduction, "validMsg_txt");
		btn_reminder = findElementInXLSheet(getParameterProduction, "btn_reminder");
		txt_createReminder = findElementInXLSheet(getParameterProduction, "txt_createReminder");
		btn_history = findElementInXLSheet(getParameterProduction, "btn_history");
		txt_history = findElementInXLSheet(getParameterProduction, "txt_history");
		btn_reverse = findElementInXLSheet(getParameterProduction, "btn_reverse");
		txt_reverse = findElementInXLSheet(getParameterProduction, "txt_reverse");
		btn_updateReminder = findElementInXLSheet(getParameterProduction, "updateReminder_btn");
		txt_delete = findElementInXLSheet(getParameterProduction, "txt_delete");
		btn_Delete = findElementInXLSheet(getParameterProduction, "Delete_btn");
		btn_updateNNew = findElementInXLSheet(getParameterProduction, "updateNnew_btn");
		txt_updateNNewBOM = findElementInXLSheet(getParameterProduction, "updateNNew_txt"); 
		txt_previousBOO = findElementInXLSheet(getParameterProduction, "previousBOO_txt");
		doublePreviousBOO = findElementInXLSheet(getParameterProduction, "doublePrevious BOO");
		txt_duplicateBOO = findElementInXLSheet(getParameterProduction, "duplicateBOO_txt");
		txt_BOOAction = findElementInXLSheet(getParameterProduction, "BOOAction_txt");
		btn_activities = findElementInXLSheet(getParameterProduction, "btn_activities");
		txt_activity = findElementInXLSheet(getParameterProduction, "txt_activity");
		txt_subject = findElementInXLSheet(getParameterProduction, "txt_subject");
		btn_assignTo = findElementInXLSheet(getParameterProduction, "btn_assignTo");
		txt_AssignTo = findElementInXLSheet(getParameterProduction, "txt_AssignTo");
		doubleAssignTo = findElementInXLSheet(getParameterProduction, "doubleAssignTo");
		btn_UpdateTask = findElementInXLSheet(getParameterProduction, "btn_UpdateTask");
		txt_Boo = findElementInXLSheet(getParameterProduction, "txt_Boo");
		txt_productionModelCode = findElementInXLSheet(getParameterProduction, "productionModelCode_txt");
		txt_previousmodel = findElementInXLSheet(getParameterProduction, "txt_previousmodel");
		doublePreviousmodel = findElementInXLSheet(getParameterProduction, "doublePreviousmodel");
		txt_productionModelTab = findElementInXLSheet(getParameterProduction, "txt_productionModelTab");
		txt_BillOfMaterial = findElementInXLSheet(getParameterProduction, "txt_BillOfMaterial");
		tab_summary = findElementInXLSheet(getParameterProduction, "tab_summary");
		txt_plannedQty1 = findElementInXLSheet(getParameterProduction, "txt_plannedQty1");
		txt_priority = findElementInXLSheet(getParameterProduction, "txt_priority");
		txt_previousproductionOrder = findElementInXLSheet(getParameterProduction, "txt_previousproductionOrder");
		doublePreviousProductionOrder = findElementInXLSheet(getParameterProduction, "doublePreviousProductionOrder");
		txt_productionOrderTab = findElementInXLSheet(getParameterProduction, "txt_productionOrderTab");
		btn_okHold = findElementInXLSheet(getParameterProduction, "btn_okHold"); 
		btn_jobTechnicalDetail = findElementInXLSheet(getParameterProduction, "btn_jobTechnicalDetail");
		btn_dockFlow = findElementInXLSheet(getParameterProduction, "btn_dockFlow");
		txt_dockFlow = findElementInXLSheet(getParameterProduction, "txt_dockFlow");
		btn_generatePackinOrder = findElementInXLSheet(getParameterProduction, "btn_generatePackinOrder");
		txt_generatePackingOrder = findElementInXLSheet(getParameterProduction, "txt_generatePackingOrder");
		btn_costEstimation = findElementInXLSheet(getParameterProduction, "btn_costEstimation");
		txt_costEstimation = findElementInXLSheet(getParameterProduction, "txt_costEstimation");
		txt_prodControl = findElementInXLSheet(getParameterProduction, "txt_prodControl");
		txt_internalDispatchOrderdrafted = findElementInXLSheet(getParameterProduction, "txt_internalDispatchOrderdrafted");
		btn_editInternal = findElementInXLSheet(getParameterProduction, "btn_editInternal");
		txt_release = findElementInXLSheet(getParameterProduction, "txt_release");
		txt_update = findElementInXLSheet(getParameterProduction, "txt_update");
		btn_datenew = findElementInXLSheet(getParameterProduction, "btn_datenew");
		btn_reverseClick = findElementInXLSheet(getParameterProduction, "btn_reverseClick");
		txt_date = findElementInXLSheet(getParameterProduction, "txt_date");
		txt_updateNNew = findElementInXLSheet(getParameterProduction, "txt_updateNNew");
		btn_products = findElementInXLSheet(getParameterProduction, "btn_products");
		txt_previousBulkOrder = findElementInXLSheet(getParameterProduction, "txt_previousBulkOrder");
		doublePreviousbulkOrder = findElementInXLSheet(getParameterProduction, "doublePreviousbulkOrder");
		txt_bulkproductionorderTab = findElementInXLSheet(getParameterProduction, "txt_bulkproductionorderTab");
		btn_InternalOrder = findElementInXLSheet(getParameterProduction, "btn_InternalOrder");
		txt_InternalOrder = findElementInXLSheet(getParameterProduction, "txt_InternalOrder");
		txt_title1 = findElementInXLSheet(getParameterProduction, "txt_title1");
		btn_datenew1 = findElementInXLSheet(getParameterProduction, "btn_datenew1");
		btn_report = findElementInXLSheet(getParameterProduction, "btn_report");
		btn_viewReport = findElementInXLSheet(getParameterProduction, "btn_viewReport"); 
		txt_viewReport = findElementInXLSheet(getParameterProduction, "txt_viewReport");
		txt_active = findElementInXLSheet(getParameterProduction, "txt_active");
		btn_inactive = findElementInXLSheet(getParameterProduction, "btn_inactive");
		txt_inactive = findElementInXLSheet(getParameterProduction, "txt_inactive");
		btn_yes1 = findElementInXLSheet(getParameterProduction, "btn_yes1");
		btn_yes2 = findElementInXLSheet(getParameterProduction, "btn_yes2");
		txt_new = findElementInXLSheet(getParameterProduction, "txt_new");
		txt_deleteAc = findElementInXLSheet(getParameterProduction, "txt_deleteAc");
		btn_deleteAc = findElementInXLSheet(getParameterProduction, "btn_deleteAc");
		btn_yes4 = findElementInXLSheet(getParameterProduction, "btn_yes4");
		txt_updateOrganization = findElementInXLSheet(getParameterProduction, "txt_updateOrganization");
		txt_edit = findElementInXLSheet(getParameterProduction, "txt_edit");
		txt_search = findElementInXLSheet(getParameterProduction, "txt_search");
		txt_reset = findElementInXLSheet(getParameterProduction, "txt_reset");
		btn_reset = findElementInXLSheet(getParameterProduction, "btn_reset");
		txt_internalReceiptLabel = findElementInXLSheet(getParameterProduction, "txt_internalReceiptLabel");
		btn_productionParameter = findElementInXLSheet(getParameterProduction, "btn_productionParameter");
		btn_inactivePP = findElementInXLSheet(getParameterProduction, "btn_inactivePP");
		btn_UPDATE = findElementInXLSheet(getParameterProduction, "btn_UPDATE");
		txt_UPDATE = findElementInXLSheet(getParameterProduction, "txt_UPDATE");
		btn_productcostEstimation = findElementInXLSheet(getParameterProduction, "btn_productcostEstimation");
		btn_newCostEstimation = findElementInXLSheet(getParameterProduction, "btn_newCostEstimation");
		txt_costEstDescription = findElementInXLSheet(getParameterProduction, "txt_costEstDescription");
		btn_costProduct = findElementInXLSheet(getParameterProduction, "btn_costProduct");
		txt_costProduct = findElementInXLSheet(getParameterProduction, "txt_costProduct");
		costProductDoubleClick = findElementInXLSheet(getParameterProduction, "costProductDoubleClick");
		txt_costEstQuantity = findElementInXLSheet(getParameterProduction, "txt_costEstQuantity");
		btn_pricingProfileCost = findElementInXLSheet(getParameterProduction, "btn_pricingProfileCost");
		txt_pricingProfileCost = findElementInXLSheet(getParameterProduction, "txt_pricingProfileCost");
		pricingProfileCostDoubleClick = findElementInXLSheet(getParameterProduction, "pricingProfileCostDoubleClick");
		btn_checkout = findElementInXLSheet(getParameterProduction, "btn_checkout");
		div_loginVerification = findElementInXLSheet(getParameterProduction, "div_loginVerification");
		
		/* Regression */
		/* Comm_001_002 */
		lbl_entutionLogoLoginPage = findElementInXLSheet(getParameterProduction, "lbl_entutionLogoLoginPage");
		lbl_entutionLogoHomePage = findElementInXLSheet(getParameterProduction, "lbl_entutionLogoHomePage");
		img_navigationPane = findElementInXLSheet(getParameterProduction, "img_navigationPane");
		btn_productionModule = findElementInXLSheet(getParameterProduction, "btn_productionModule");
		
		/* Prod_TC_004_006to016 */
		header_productionModelByPage = findElementInXLSheet(getParameterProduction, "header_productionModelByPage");
		drop_templateOnByPages = findElementInXLSheet(getParameterProduction, "drop_templateOnByPages");
		drop_docStatus = findElementInXLSheet(getParameterProduction, "drop_docStatus");
		th_modelNoByPagePM = findElementInXLSheet(getParameterProduction, "th_modelNoByPagePM");
		th_productCodeByPagePM = findElementInXLSheet(getParameterProduction, "th_productCodeByPagePM");
		th_description1ByPagePM = findElementInXLSheet(getParameterProduction, "th_description1ByPagePM");
		th_description2ByPagePM = findElementInXLSheet(getParameterProduction, "th_description2ByPagePM");
		txt_serchProductionModelByPage = findElementInXLSheet(getParameterProduction, "txt_serchProductionModelByPage");
		btn_serchIconPMByPage = findElementInXLSheet(getParameterProduction, "btn_serchIconPMByPage");
		btn_advanceFilterPMByPage = findElementInXLSheet(getParameterProduction, "btn_advanceFilterPMByPage");
		btn_refreshPMByPage = findElementInXLSheet(getParameterProduction, "btn_refreshPMByPage");
		btn_alpabaticalSearchPMByPage = findElementInXLSheet(getParameterProduction, "btn_alpabaticalSearchPMByPage");
		span_pageDetailsByPage = findElementInXLSheet(getParameterProduction, "span_pageDetailsByPage");
		txt_currentPagePMByPage = findElementInXLSheet(getParameterProduction, "txt_currentPagePMByPage");
		i_doubleBackMidleFooterControllersByPage = findElementInXLSheet(getParameterProduction, "i_doubleBackMidleFooterControllersByPage");
		i_backMidleFooterControllersByPage = findElementInXLSheet(getParameterProduction, "i_backMidleFooterControllersByPage");
		i_doubleNextMidleFooterControllersByPage = findElementInXLSheet(getParameterProduction, "i_doubleNextMidleFooterControllersByPage");
		i_nextMidleFooterControllersByPage = findElementInXLSheet(getParameterProduction, "i_nextMidleFooterControllersByPage");
		span_leftFooterDeatilsOnByPage = findElementInXLSheet(getParameterProduction, "span_leftFooterDeatilsOnByPage");
		header_productionModelByPageWithClass = findElementInXLSheet(getParameterProduction, "header_productionModelByPageWithClass");
		btn_newButtonProductionModel = findElementInXLSheet(getParameterProduction, "btn_newButtonProductionModel");
		lnk_modelNoPMByPageWithHref = findElementInXLSheet(getParameterProduction, "lnk_modelNoPMByPageWithHref");

		/* Prod_TC_017to024 */
		header_newProductionModel = findElementInXLSheet(getParameterProduction, "header_newProductionModel");
		tab_summaryProductionModel = findElementInXLSheet(getParameterProduction, "tab_summaryProductionModel");
		tab_billOfOperationProductionModel = findElementInXLSheet(getParameterProduction, "tab_billOfOperationProductionModel");
		txt_modelCodeProductionModel = findElementInXLSheet(getParameterProduction, "txt_modelCodeProductionModel");
		drop_productionGroupProductionModel = findElementInXLSheet(getParameterProduction, "drop_productionGroupProductionModel");
		btn_productLookupProductionModel = findElementInXLSheet(getParameterProduction, "btn_productLookupProductionModel");
		txt_descriptionProductionModel = findElementInXLSheet(getParameterProduction, "txt_descriptionProductionModel");
		btn_searchProductionUnit = findElementInXLSheet(getParameterProduction, "btn_searchProductionUnit");
		drop_inputWarehousePM = findElementInXLSheet(getParameterProduction, "drop_inputWarehousePM");
		drop_outputWarehousePM = findElementInXLSheet(getParameterProduction, "drop_outputWarehousePM");
		drop_wipWarehousePM = findElementInXLSheet(getParameterProduction, "drop_wipWarehousePM");
		drop_sitePM = findElementInXLSheet(getParameterProduction, "drop_sitePM");
		btn_yesClearConfirmation = findElementInXLSheet(getParameterProduction, "btn_yesClearConfirmation");
		txt_searchProductCommon = findElementInXLSheet(getParameterProduction, "txt_searchProductCommon");
		tr_reultOnLookupInfoReplace = findElementInXLSheet(getParameterProduction, "tr_reultOnLookupInfoReplace");
		header_productLookup = findElementInXLSheet(getParameterProduction, "header_productLookup");
		txt_productOnFrontPagePM = findElementInXLSheet(getParameterProduction, "txt_productOnFrontPagePM");
		btn_searchProductProductionModel = findElementInXLSheet(getParameterProduction, "btn_searchProductProductionModel");
		header_productionUnitLookupPM = findElementInXLSheet(getParameterProduction, "header_productionUnitLookupPM");
		txt_searchProductionUnit = findElementInXLSheet(getParameterProduction, "txt_searchProductionUnit");
		btn_searchProductionUnitOnLookup = findElementInXLSheet(getParameterProduction, "btn_searchProductionUnitOnLookup");
		lnk_resultProductionUnitOnLookup = findElementInXLSheet(getParameterProduction, "lnk_resultProductionUnitOnLookup");
		txt_productionUnitOnFrontPage = findElementInXLSheet(getParameterProduction, "txt_productionUnitOnFrontPage");
		
		/* PROD_PM_025 */
		txt_validFromDatePM = findElementInXLSheet(getParameterProduction, "txt_validFromDatePM");
		txt_validToDatePM = findElementInXLSheet(getParameterProduction, "txt_validToDatePM");
		txt_minimumLotSizePM = findElementInXLSheet(getParameterProduction, "txt_minimumLotSizePM");
		txt_maximumLotSizePM = findElementInXLSheet(getParameterProduction, "txt_maximumLotSizePM");
		txt_batchQuantityOtherInformationPM = findElementInXLSheet(getParameterProduction, "txt_batchQuantityOtherInformationPM");
		drop_barcodeBookOtherInformationPM = findElementInXLSheet(getParameterProduction, "drop_barcodeBookOtherInformationPM");
		txt_productionScrapOtherInformationPM = findElementInXLSheet(getParameterProduction, "txt_productionScrapOtherInformationPM");
		drop_costingPriorityOtherInformationPM = findElementInXLSheet(getParameterProduction, "drop_costingPriorityOtherInformationPM");
		txt_statndardCostOtherInformationPM = findElementInXLSheet(getParameterProduction, "txt_statndardCostOtherInformationPM");
		txt_pricingProfileOtherInformationPM = findElementInXLSheet(getParameterProduction, "txt_pricingProfileOtherInformationPM");
		chk_planningEnabledOtherInformationPM = findElementInXLSheet(getParameterProduction, "chk_planningEnabledOtherInformationPM");
		chk_mrpEnabledOtherInformationPM = findElementInXLSheet(getParameterProduction, "chk_mrpEnabledOtherInformationPM");
		chk_infinitrOtherInformationPM = findElementInXLSheet(getParameterProduction, "chk_infinitrOtherInformationPM");
		
		/* PROD_PM_028 */
		btn_lookupPricingProfilePM = findElementInXLSheet(getParameterProduction, "btn_lookupPricingProfilePM");
		header_pricingProfileLookupPM = findElementInXLSheet(getParameterProduction, "header_pricingProfileLookupPM");
		txt_searchPricingProfile = findElementInXLSheet(getParameterProduction, "txt_searchPricingProfile");
		btn_searchPricingProfilePM = findElementInXLSheet(getParameterProduction, "btn_searchPricingProfilePM");
		td_resultPricingProfilePM = findElementInXLSheet(getParameterProduction, "td_resultPricingProfilePM");
		
		/* PROD_PM_030 */
		drop_bomPM = findElementInXLSheet(getParameterProduction, "drop_bomPM");
		
		/* PROD_PM_034 */
		btn_lookupBOOPM = findElementInXLSheet(getParameterProduction, "btn_lookupBOOPM");
		txt_descriptionBOOPM = findElementInXLSheet(getParameterProduction, "txt_descriptionBOOPM");
		chk_supplyToProductionBOOPM = findElementInXLSheet(getParameterProduction, "chk_supplyToProductionBOOPM");
		
		/* PROD_PM_035 */
		header_billOfOperatioPM = findElementInXLSheet(getParameterProduction, "header_billOfOperatioPM");
		txt_billOfOperationPMFrontPage = findElementInXLSheet(getParameterProduction, "txt_billOfOperationPMFrontPage");
		txt_searchBillOfOperationPM = findElementInXLSheet(getParameterProduction, "txt_searchBillOfOperationPM");
		btn_searchBOOInLookup = findElementInXLSheet(getParameterProduction, "btn_searchBOOInLookup");
		td_resultBOOPM = findElementInXLSheet(getParameterProduction, "td_resultBOOPM");
		btn_okClearConfirmationBOOPM = findElementInXLSheet(getParameterProduction, "btn_okClearConfirmationBOOPM");
		
		/* PROD_PM_038 */
		tab_billOfOperationAfterSelected = findElementInXLSheet(getParameterProduction, "tab_billOfOperationAfterSelected");
	
		/* PROD_PM_039 */
		th_columnHeaderEleentIDPM = findElementInXLSheet(getParameterProduction, "th_columnHeaderEleentIDPM");
		th_columnHeaderEleentCategoryPM = findElementInXLSheet(getParameterProduction, "th_columnHeaderEleentCategoryPM");
		th_columnHeaderEleentDescriptionPM = findElementInXLSheet(getParameterProduction, "th_columnHeaderEleentDescriptionPM");
		th_columnHeaderMainResoursePM = findElementInXLSheet(getParameterProduction, "th_columnHeaderMainResoursePM");
		th_columnHeaderFixedDueationPM = findElementInXLSheet(getParameterProduction, "th_columnHeaderFixedDueationPM");
		th_columnHeaderVariableDurationPM = findElementInXLSheet(getParameterProduction, "th_columnHeaderVariableDurationPM");
		btn_expandPM = findElementInXLSheet(getParameterProduction, "btn_expandPM");
		
		/* PROD_PM_040 */
		tr_booRow1PM = findElementInXLSheet(getParameterProduction, "tr_booRow1PM");
		tr_booRow2PM = findElementInXLSheet(getParameterProduction, "tr_booRow2PM");
		tr_booRow3PM = findElementInXLSheet(getParameterProduction, "tr_booRow3PM");
		tr_booRow4PM = findElementInXLSheet(getParameterProduction, "tr_booRow4PM");
		
		/* PROD_PM_041 */
		header_draftedProductionModel = findElementInXLSheet(getParameterProduction, "header_draftedProductionModel");
		btn_editBillOfOperationPMRowReplace = findElementInXLSheet(getParameterProduction, "btn_editBillOfOperationPMRowReplace");
		chk_inputProductsRowReplaceBOOPM = findElementInXLSheet(getParameterProduction, "chk_inputProductsRowReplaceBOOPM");
		drop_processTimeTypeBOOPM = findElementInXLSheet(getParameterProduction, "drop_processTimeTypeBOOPM");
		btn_inputProductsTabBOOPM = findElementInXLSheet(getParameterProduction, "btn_inputProductsTabBOOPM");
		btn_applyBOOPM = findElementInXLSheet(getParameterProduction, "btn_applyBOOPM");
		
		/* PROD_PM_042 */
		tab_productsPM = findElementInXLSheet(getParameterProduction, "tab_productsPM");
		
		/* PROD_PM_043 */
		tab_inputProductsProductsTabPM = findElementInXLSheet(getParameterProduction, "tab_inputProductsProductsTabPM");
		tab_outputProductsProductsTabPM = findElementInXLSheet(getParameterProduction, "tab_outputProductsProductsTabPM");

		/* PROD_PM_044 */
		div_productOnOutputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_productOnOutputProductsTabPM");
		div_warehouseOnOutputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_warehouseOnOutputProductsTabPM");
		div_plannedQtyOnOutputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_plannedQtyOnOutputProductsTabPM");

		/* PROD_PM_045 */
		th_columHeaderProductOutputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeaderProductOutputProductsTabPM");
		th_columHeaderWarehouseOutputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeaderWarehouseOutputProductsTabPM");
		th_columHeaderPlanedQtyOutputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeaderPlanedQtyOutputProductsTabPM");

		/* PROD_PM_051 */
		li_inputProductSelected = findElementInXLSheet(getParameterProduction, "li_inputProductSelected");
		td_selectedInputProductBOO1GridPM = findElementInXLSheet(getParameterProduction, "td_selectedInputProductBOO1GridPM");
		td_selectedInputProductBOO2GridPM = findElementInXLSheet(getParameterProduction, "td_selectedInputProductBOO2GridPM");

		/* PROD_PM_052 */
		btn_editOperation01PM = findElementInXLSheet(getParameterProduction, "btn_editOperation01PM");
		header_operationActivityPopup = findElementInXLSheet(getParameterProduction, "header_operationActivityPopup");
		btn_byProductTabPM = findElementInXLSheet(getParameterProduction, "btn_byProductTabPM");
		btn_byProductTabSelectedPM = findElementInXLSheet(getParameterProduction, "btn_byProductTabSelectedPM");
		btn_byProductsLookupByProductsTabPM = findElementInXLSheet(getParameterProduction, "btn_byProductsLookupByProductsTabPM");
		td_resultByProductPM = findElementInXLSheet(getParameterProduction, "td_resultByProductPM");
		header_productLookupBox = findElementInXLSheet(getParameterProduction, "header_productLookupBox");
		btn_productSearchInLookup = findElementInXLSheet(getParameterProduction, "btn_productSearchInLookup");
		btn_productSearch = findElementInXLSheet(getParameterProduction, "btn_productSearch");
		div_byProductOnByProductGridPM = findElementInXLSheet(getParameterProduction, "div_byProductOnByProductGridPM");
		txt_byProductsQuantityPM = findElementInXLSheet(getParameterProduction, "txt_byProductsQuantityPM");
		txt_byProductsLeadDaysPM = findElementInXLSheet(getParameterProduction, "txt_byProductsLeadDaysPM");
		btn_applyOperationActivity = findElementInXLSheet(getParameterProduction, "btn_applyOperationActivity");
		td_appliedByProductBOOGridPM = findElementInXLSheet(getParameterProduction, "td_appliedByProductBOOGridPM");
		
		/* PROD_PM_053_054 */
		btn_serviceProductsTabPM = findElementInXLSheet(getParameterProduction, "btn_serviceProductsTabPM");
		btn_serviceProductTabSelectedPM = findElementInXLSheet(getParameterProduction, "btn_serviceProductTabSelectedPM");
		btn_serviceProductsLookupByProductsTabPM = findElementInXLSheet(getParameterProduction, "btn_serviceProductsLookupByProductsTabPM");
		td_resultServiceProductPM = findElementInXLSheet(getParameterProduction, "td_resultServiceProductPM");
		td_resultBatchProductServiceTabPM = findElementInXLSheet(getParameterProduction, "td_resultBatchProductServiceTabPM");
		div_serviceProductOnByProductGridPM = findElementInXLSheet(getParameterProduction, "div_serviceProductOnByProductGridPM");
		txt_serviceProductsAmountsPM = findElementInXLSheet(getParameterProduction, "txt_serviceProductsAmountsPM");
		txt_serviceProductsLeadDaysPM = findElementInXLSheet(getParameterProduction, "txt_serviceProductsLeadDaysPM");
		
		/* PROD_PM_55 */
		btn_additionalResourseTabPM = findElementInXLSheet(getParameterProduction, "btn_additionalResourseTabPM");
		btn_additionalResourseTabSelectedPM = findElementInXLSheet(getParameterProduction, "btn_additionalResourseTabSelectedPM");
		btn_resourseLookupAdditionalResourseTabPM = findElementInXLSheet(getParameterProduction, "btn_resourseLookupAdditionalResourseTabPM");
		lookup_resource = findElementInXLSheet(getParameterProduction, "lookup_resource");
		txt_searchResource = findElementInXLSheet(getParameterProduction, "txt_searchResource");
		btn_resourceSearchInLookup = findElementInXLSheet(getParameterProduction, "btn_resourceSearchInLookup");
		td_resultResourceAdditionalResourceTabPM = findElementInXLSheet(getParameterProduction, "td_resultResourceAdditionalResourceTabPM");
		div_resourceOnAdditonalResourceGridPM = findElementInXLSheet(getParameterProduction, "div_resourceOnAdditonalResourceGridPM");
		btn_selectHoursAddtionalResourceTabPM = findElementInXLSheet(getParameterProduction, "btn_selectHoursAddtionalResourceTabPM");
		header_resourceHoursAdditionalResourseTabPM = findElementInXLSheet(getParameterProduction, "header_resourceHoursAdditionalResourseTabPM");
		btn_oneHourAdditionalResourceHoursPM = findElementInXLSheet(getParameterProduction, "btn_oneHourAdditionalResourceHoursPM");
		btn_okDurationAdditionalResourceHoursPM = findElementInXLSheet(getParameterProduction, "btn_okDurationAdditionalResourceHoursPM");
		btn_selectMinutesAddtionalResourceTabPM = findElementInXLSheet(getParameterProduction, "btn_selectMinutesAddtionalResourceTabPM");

		/* PROD_PM_56 */
		btn_overheadTabPM = findElementInXLSheet(getParameterProduction, "btn_overheadTabPM");
		btn_overheadTabSelectedPM = findElementInXLSheet(getParameterProduction, "btn_overheadTabSelectedPM");
		div_overheadOnOverheadGridPM = findElementInXLSheet(getParameterProduction, "div_overheadOnOverheadGridPM");
		
		/* PROD_PM_57 */
		btn_stepsTabPM = findElementInXLSheet(getParameterProduction, "btn_stepsTabPM");
		btn_stepsTabSelectedPM = findElementInXLSheet(getParameterProduction, "btn_stepsTabSelectedPM");
		header_descriptionStepsTabPM = findElementInXLSheet(getParameterProduction, "header_descriptionStepsTabPM");
		txt_descriptionStepsTabPM = findElementInXLSheet(getParameterProduction, "txt_descriptionStepsTabPM");
		
		/* PROD_PM_59 */
		header_releasedProductionModel = findElementInXLSheet(getParameterProduction, "header_releasedProductionModel");
		div_productionModelCode = findElementInXLSheet(getParameterProduction, "div_productionModelCode");
		
		/* PROD_PM_60_61 */
		btn_productionModelByPageLikn = findElementInXLSheet(getParameterProduction, "btn_productionModelByPageLikn");
		txt_searchProductionModel = findElementInXLSheet(getParameterProduction, "txt_searchProductionModel");
		btn_searchProductionModel = findElementInXLSheet(getParameterProduction, "btn_searchProductionModel");
		lnk_resultProductionModelOnByPage = findElementInXLSheet(getParameterProduction, "lnk_resultProductionModelOnByPage");
		header_releasedProductionModelWithDocReplace = findElementInXLSheet(getParameterProduction, "header_releasedProductionModelWithDocReplace");
		tab_productsProductionModel = findElementInXLSheet(getParameterProduction, "tab_productsProductionModel");
		div_productOneUnderInputProductsTabAfterReleasedPM = findElementInXLSheet(getParameterProduction, "div_productOneUnderInputProductsTabAfterReleasedPM");
		div_productTwoUnderInputProductsTabAfterReleasedPM = findElementInXLSheet(getParameterProduction, "div_productTwoUnderInputProductsTabAfterReleasedPM");
		header_draftedProductionModelWithDocReplace = findElementInXLSheet(getParameterProduction, "header_draftedProductionModelWithDocReplace");
	
		/* PROD_PM_65 */
		p_mainValidation = findElementInXLSheet(getParameterProduction, "p_mainValidation");
		txt_batchQtyWithValidationPO = findElementInXLSheet(getParameterProduction, "txt_batchQtyWithValidationPO");
		
		/* PROD_PM_66 */
		lnk_productionModelOnDraftedStatusPM = findElementInXLSheet(getParameterProduction, "lnk_productionModelOnDraftedStatusPM");
		td_resulProductionModelOnByPageDocReplace = findElementInXLSheet(getParameterProduction, "td_resulProductionModelOnByPageDocReplace");
		
		/* PROD_PM_67 */
		a_validFromDatePM = findElementInXLSheet(getParameterProduction, "a_validFromDatePM");
		a_validToDatePM = findElementInXLSheet(getParameterProduction, "a_validToDatePM");
		
		/* PROD_PM_071_072_073_074_076 */
		div_plannedQtyOnOutputProductsTabInfinitePM = findElementInXLSheet(getParameterProduction, "div_plannedQtyOnOutputProductsTabInfinitePM");
		
		/* PROD_PM_096_097 */
		th_columHeaderProductInputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeaderProductInputProductsTabPM");
		th_columHeadeWarehouseInputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeadeWarehouseInputProductsTabPM");
		th_columHeadePlanedQtyInputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeadePlanedQtyInputProductsTabPM");
		th_columHeadeElementDescriptionInputProductsTabPM = findElementInXLSheet(getParameterProduction, "th_columHeadeElementDescriptionInputProductsTabPM");
		div_productOneOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_productOneOnInputProductsTabPM");
		div_productTwoOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_productTwoOnInputProductsTabPM");
		div_warehouseOneOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_warehouseOneOnInputProductsTabPM");
		div_warehouseTwoOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_warehouseTwoOnInputProductsTabPM");
		div_planedQtyOneOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_planedQtyOneOnInputProductsTabPM");
		div_planedQtyTwoOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_planedQtyTwoOnInputProductsTabPM");
		div_elementDescriptionOneOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_elementDescriptionOneOnInputProductsTabPM");
		div_elementDescriptionTwoOnInputProductsTabPM = findElementInXLSheet(getParameterProduction, "div_elementDescriptionTwoOnInputProductsTabPM");
		
		/* Production Order */
		/* PROD_PO_002 */
		header_productionOrderDocNoColumnOnByPage = findElementInXLSheet(getParameterProduction, "header_productionOrderDocNoColumnOnByPage");
		lnk_countProductionOrdersOnByPAge = findElementInXLSheet(getParameterProduction, "lnk_countProductionOrdersOnByPAge");
		header_productionOrderByPage = findElementInXLSheet(getParameterProduction, "header_productionOrderByPage");
		
		/* PROD_PO_003 */
		header_newProductionOrder = findElementInXLSheet(getParameterProduction, "header_newProductionOrder");
		
		/* PROD_PO_004 */
		li_orderStructureTabPO = findElementInXLSheet(getParameterProduction, "li_orderStructureTabPO");
		li_costingSummaryTabPO = findElementInXLSheet(getParameterProduction, "li_costingSummaryTabPO");
		
		/* PROD_PO_005 */
		drop_productOrderGroupSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_productOrderGroupSummarySectionSummaryTabPO");
		txt_descriptionSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_descriptionSummarySectionSummaryTabPO");
		txt_productionModelSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_productionModelSummarySectionSummaryTabPO");
		txt_costEstimationSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_costEstimationSummarySectionSummaryTabPO");
		drop_barcodeBookSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_barcodeBookSummarySectionSummaryTabPO");
		div_batchNoSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_batchNoSummarySectionSummaryTabPO");
		txt_productionLotNoSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_productionLotNoSummarySectionSummaryTabPO");
		drop_prioritySummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_prioritySummarySectionSummaryTabPO");
		txt_productionUnitSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_productionUnitSummarySectionSummaryTabPO");
		drop_inputWarehouseSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_inputWarehouseSummarySectionSummaryTabPO");
		drop_outputWarehouseSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_outputWarehouseSummarySectionSummaryTabPO");
		drop_WIPWarehouseSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_WIPWarehouseSummarySectionSummaryTabPO");
		drop_siteSummarySectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_siteSummarySectionSummaryTabPO");
		
		/* PROD_PO_006 */
		txt_outputProductProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_outputProductProductInformationSectionSummaryTabPO");
		txt_planedQtyProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_planedQtyProductInformationSectionSummaryTabPO");
		txt_producedQtyProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_producedQtyProductInformationSectionSummaryTabPO");
		txt_balanceQtyProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_balanceQtyProductInformationSectionSummaryTabPO");
		txt_minimumLotSizeProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_minimumLotSizeProductInformationSectionSummaryTabPO");
		txt_maximumLotSizeProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_maximumLotSizeProductInformationSectionSummaryTabPO");
		txt_batchQtyProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_batchQtyProductInformationSectionSummaryTabPO");
		txt_productionScrapProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_productionScrapProductInformationSectionSummaryTabPO");
		drop_costingPriorityProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_costingPriorityProductInformationSectionSummaryTabPO");
		txt_standardCostProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_standardCostProductInformationSectionSummaryTabPO");
		chk_planningEnabledtProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "chk_planningEnabledtProductInformationSectionSummaryTabPO");
		chk_MRPEnabledtProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "chk_MRPEnabledtProductInformationSectionSummaryTabPO");
		chk_infiniteProductInformationSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "chk_infiniteProductInformationSectionSummaryTabPO");

		/* PROD_PO_007 */
		drop_boMBoMDetailsSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "drop_boMBoMDetailsSectionSummaryTabPO");
		div_boMVarientBoMDetailsSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_boMVarientBoMDetailsSectionSummaryTabPO");
		div_varientDescriptionBoMDetailsSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_varientDescriptionBoMDetailsSectionSummaryTabPO");

		/* PROD_PO_008 */
		div_boOBoODetailsSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_boOBoODetailsSectionSummaryTabPO");
		div_boODescriptionDetailsSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_boODescriptionDetailsSectionSummaryTabPO");
		chk_supplyToOperationDetailsSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "chk_supplyToOperationDetailsSectionSummaryTabPO");

		/* PROD_PO_009 */
		div_currentStatusStatusSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_currentStatusStatusSectionSummaryTabPO");
		div_shedulingStatusStatusSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_shedulingStatusStatusSectionSummaryTabPO");

		/* PROD_PO_010 */
		txt_requestStartDateSheduleSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_requestStartDateSheduleSectionSummaryTabPO");
		txt_requestEndDateSheduleSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "txt_requestEndDateSheduleSectionSummaryTabPO");
		div_earliestStartDateSheduleSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_earliestStartDateSheduleSectionSummaryTabPO");
		div_earliestEndDateSheduleSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_earliestEndDateSheduleSectionSummaryTabPO");
		div_latestStartDateSheduleSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_latestStartDateSheduleSectionSummaryTabPO");
		div_latestEndDateSheduleSectionSummaryTabPO = findElementInXLSheet(getParameterProduction, "div_latestEndDateSheduleSectionSummaryTabPO");
	
		/* PROD_PO_011 */
		span_productLookupPO = findElementInXLSheet(getParameterProduction, "span_productLookupPO");
		lookup_product = findElementInXLSheet(getParameterProduction, "lookup_product");
		txt_searchProductProductLookup = findElementInXLSheet(getParameterProduction, "txt_searchProductProductLookup");
		btn_searchProductProductLookup = findElementInXLSheet(getParameterProduction, "btn_searchProductProductLookup");
		td_resultProductRegressionPO = findElementInXLSheet(getParameterProduction, "td_resultProductRegressionPO");
	
		/* PROD_PO_012 */
		btn_productionModelLookupPO = findElementInXLSheet(getParameterProduction, "btn_productionModelLookupPO");
		lookup_productionModel = findElementInXLSheet(getParameterProduction, "lookup_productionModel");
		td_resultProductionModelDocReplace = findElementInXLSheet(getParameterProduction, "td_resultProductionModelDocReplace");

		/* Pre-Requisites PO Regression */
		txt_productCodeProductInformation = findElementInXLSheet(getParameterProduction, "txt_productCodeProductInformation");
		header_releasedProduct = findElementInXLSheet(getParameterProduction, "header_releasedProduct");
		
		/* PROD_PO_013 */
		count_resultRowsProductionModelLookup = findElementInXLSheet(getParameterProduction, "count_resultRowsProductionModelLookup");
		
		/* PROD_PO_017 */
		chk_mrpProductionOrderRegression = findElementInXLSheet(getParameterProduction, "chk_mrpProductionOrderRegression");
		
		/* PROD_PO_018 */
		chk_infinitePO = findElementInXLSheet(getParameterProduction, "chk_infinitePO");
		chk_supplyToOperationPO = findElementInXLSheet(getParameterProduction, "chk_supplyToOperationPO");
		
		/* PROD_PO_019 */
		txt_plabedQtyPO = findElementInXLSheet(getParameterProduction, "txt_plabedQtyPO");
		
		/* PROD_PO_024 */
		header_draftedProductionOrder = findElementInXLSheet(getParameterProduction, "header_draftedProductionOrder");
		
		/* PROD_PO_025 */
		chk_supplyToProductionPO = findElementInXLSheet(getParameterProduction, "chk_supplyToProductionPO");
		a_requestStartDatePOWhenDuplicating = findElementInXLSheet(getParameterProduction, "a_requestStartDatePOWhenDuplicating");
		a_requestEndDatePOWhenDuplicating = findElementInXLSheet(getParameterProduction, "a_requestEndDatePOWhenDuplicating");
		
		/* PROD_PO_027 */
		header_deletedProductionOrder = findElementInXLSheet(getParameterProduction, "header_deletedProductionOrder");
		btn_delete2 = findElementInXLSheet(getParameterProduction, "btn_delete2");

		/* PROD_PO_027 */
		header_releasedProductionOrder = findElementInXLSheet(getParameterProduction, "header_releasedProductionOrder");
		
		/* PROD_PO_045_046_047 */
		tab_summaryPO = findElementInXLSheet(getParameterProduction, "tab_summaryPO");
		tab_orderStructurePO = findElementInXLSheet(getParameterProduction, "tab_orderStructurePO");
		tab_productsPO = findElementInXLSheet(getParameterProduction, "tab_productsPO");
		tab_resoursePO = findElementInXLSheet(getParameterProduction, "tab_resoursePO");
		tab_costingSummaryPO = findElementInXLSheet(getParameterProduction, "tab_costingSummaryPO");

		/* PROD_PO_048_049 */
		btn_plusIconPO = findElementInXLSheet(getParameterProduction, "btn_plusIconPO");
		
		/* PROD_PO_050 */
		lbl_docNo = findElementInXLSheet(getParameterProduction, "lbl_docNo");
		header_loockupProductionOrder = findElementInXLSheet(getParameterProduction, "header_loockupProductionOrder");
		lnk_inforOnLookupInformationReplace = findElementInXLSheet(getParameterProduction, "lnk_inforOnLookupInformationReplace");
		txt_searchProductionOrder2 = findElementInXLSheet(getParameterProduction, "txt_searchProductionOrder2");
		
		/* PROD_PO_052 */
		btn_draftAndNew = findElementInXLSheet(getParameterProduction, "btn_draftAndNew");
		lbl_recentlyDraftedProductionOrder = findElementInXLSheet(getParameterProduction, "lbl_recentlyDraftedProductionOrder");
		div_descriptionAfterDraftPO = findElementInXLSheet(getParameterProduction, "div_descriptionAfterDraftPO");
		lnk_releventDraftedProductionOrderDocReplace = findElementInXLSheet(getParameterProduction, "lnk_releventDraftedProductionOrderDocReplace");
		
		/* PROD_PO_053 */
		td_updatedTimeHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterProduction, "td_updatedTimeHistoryDetailsOnlyOneAction");
		td_actionDraftDoneByUseHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterProduction, "td_actionDraftDoneByUseHistoryDetailsOnlyOneAction");
		td_userHistoryDetailsOnlyOneAction = findElementInXLSheet(getParameterProduction, "td_userHistoryDetailsOnlyOneAction");
		td_descriptionHistoryDetailsUpdated = findElementInXLSheet(getParameterProduction, "td_descriptionHistoryDetailsUpdated");
		td_updatedTimeHistoryDetailsUpdate = findElementInXLSheet(getParameterProduction, "td_updatedTimeHistoryDetailsUpdate");
		td_actionDraftDoneByUseHistoryDetailsUpdate = findElementInXLSheet(getParameterProduction, "td_actionDraftDoneByUseHistoryDetailsUpdate");
		td_userHistoryDetailsUpdate = findElementInXLSheet(getParameterProduction, "td_userHistoryDetailsUpdate");
		td_descriptionHistoryDetailsUpdatedUpdate = findElementInXLSheet(getParameterProduction, "td_descriptionHistoryDetailsUpdatedUpdate");
	
		/* Production Control */
		/* PROD_PC_001 */
		btn_productionControl = findElementInXLSheet(getParameterProduction, "btn_productionControl");
		header_productionControlByPage = findElementInXLSheet(getParameterProduction, "header_productionControlByPage");
		
		/* PROD_PC_002 */
		div_columnProductionOrderStatusPC = findElementInXLSheet(getParameterProduction, "div_columnProductionOrderStatusPC");
		div_columnPostBusinessUnitPC = findElementInXLSheet(getParameterProduction, "div_columnPostBusinessUnitPC");
		div_columnDocNoPC = findElementInXLSheet(getParameterProduction, "div_columnDocNoPC");
		div_columnPriorityPC = findElementInXLSheet(getParameterProduction, "div_columnPriorityPC");
		div_columnProductionOrderNoPC = findElementInXLSheet(getParameterProduction, "div_columnProductionOrderNoPC");
		div_columnOutputProductPC = findElementInXLSheet(getParameterProduction, "div_columnOutputProductPC");
		div_columnOpenQtyPC = findElementInXLSheet(getParameterProduction, "div_columnOpenQtyPC");
		div_columnRequestedStartDatePC = findElementInXLSheet(getParameterProduction, "div_columnRequestedStartDatePC");
		
		/* PROD_PC_003 */
		a_postBusinessUnitHeaderDeatailsPO = findElementInXLSheet(getParameterProduction, "a_postBusinessUnitHeaderDeatailsPO");
		div_postBusinessUnitProductControlByPage = findElementInXLSheet(getParameterProduction, "div_postBusinessUnitProductControlByPage");

		/* PROD_PC_005 */
		div_priorityProductControlByPage = findElementInXLSheet(getParameterProduction, "div_priorityProductControlByPage");

		/* PROD_PC_006 */
		div_productionOrderNoProductControlByPage = findElementInXLSheet(getParameterProduction, "div_productionOrderNoProductControlByPage");
		
		/* PROD_PC_007 */
		div_outputProductProductControlByPage = findElementInXLSheet(getParameterProduction, "div_outputProductProductControlByPage");
		
		/* PROD_PC_008 */
		div_openQtyProductControlByPage = findElementInXLSheet(getParameterProduction, "div_openQtyProductControlByPage");
		
		/* PROD_PC_009 */
		div_requestedStartDateProductControlByPage = findElementInXLSheet(getParameterProduction, "div_requestedStartDateProductControlByPage");
		
		/* PROD_PC_010 */
		btn_runMRP = findElementInXLSheet(getParameterProduction, "btn_runMRP");
		div_productionOrderSelectValidationPC = findElementInXLSheet(getParameterProduction, "div_productionOrderSelectValidationPC");

		/* PROD_PC_011_012 */
		txt_searchProductionControlOnByPage = findElementInXLSheet(getParameterProduction, "txt_searchProductionControlOnByPage");
		btn_searchProductionControlByPage = findElementInXLSheet(getParameterProduction, "btn_searchProductionControlByPage");
		div_resultCountPCByPage = findElementInXLSheet(getParameterProduction, "div_resultCountPCByPage");
		div_chkSelectDocReplacePC = findElementInXLSheet(getParameterProduction, "div_chkSelectDocReplacePC");
		div_chkSelectedDocReplacePC = findElementInXLSheet(getParameterProduction, "div_chkSelectedDocReplacePC");
		lookup_productionControl = findElementInXLSheet(getParameterProduction, "lookup_productionControl");
		
		/* Production Multi Platform Scenarios */
		/* PROD_E2E_001 */
		btn_billOfMaterial2 = findElementInXLSheet(getParameterProduction, "btn_billOfMaterial2");
		btn_newBillOfMaterial = findElementInXLSheet(getParameterProduction, "btn_newBillOfMaterial");
		txt_descriptionBillOfMaterials = findElementInXLSheet(getParameterProduction, "txt_descriptionBillOfMaterials");
		drop_productGroupBOM = findElementInXLSheet(getParameterProduction, "drop_productGroupBOM");
		btn_productInformation = findElementInXLSheet(getParameterProduction, "btn_productInformation");
		btn_newProduct2 = findElementInXLSheet(getParameterProduction, "btn_newProduct2");
		drop_productGroupProductInfromation = findElementInXLSheet(getParameterProduction, "drop_productGroupProductInfromation");
		chk_allowDecimalProductInformation = findElementInXLSheet(getParameterProduction, "chk_allowDecimalProductInformation");
		btn_lookupMenufcturerProductInformation = findElementInXLSheet(getParameterProduction, "btn_lookupMenufcturerProductInformation");
		txt_searchMenfacture = findElementInXLSheet(getParameterProduction, "txt_searchMenfacture");
		td_firstResultMenufacturerLookup = findElementInXLSheet(getParameterProduction, "td_firstResultMenufacturerLookup");
		drop_defouldUOMGroupProductInfromation = findElementInXLSheet(getParameterProduction, "drop_defouldUOMGroupProductInfromation");
		drop_defouldUOMProductInfromation = findElementInXLSheet(getParameterProduction, "drop_defouldUOMProductInfromation");
		tab_detailsProductInformation = findElementInXLSheet(getParameterProduction, "tab_detailsProductInformation");
		header_warehouseInfromationDetailsTabProductInfromation = findElementInXLSheet(getParameterProduction, "header_warehouseInfromationDetailsTabProductInfromation");
		drop_outboundCostingMethodProductInfromation = findElementInXLSheet(getParameterProduction, "drop_outboundCostingMethodProductInfromation");
		drop_menufacTypeProductionProductInformation = findElementInXLSheet(getParameterProduction, "drop_menufacTypeProductionProductInformation");
		chk_isBatchProduct = findElementInXLSheet(getParameterProduction, "chk_isBatchProduct");
		btn_lookupOutputProduct = findElementInXLSheet(getParameterProduction, "btn_lookupOutputProduct");
		txt_productFrontPageBOM = findElementInXLSheet(getParameterProduction, "txt_productFrontPageBOM");
		txt_descriptionProductInformation = findElementInXLSheet(getParameterProduction, "txt_descriptionProductInformation");
		txt_quantityBOM = findElementInXLSheet(getParameterProduction, "txt_quantityBOM");
		tab_classificationProductInformation = findElementInXLSheet(getParameterProduction, "tab_classificationProductInformation");
		btn_lokkupVarienGroupProductInformation = findElementInXLSheet(getParameterProduction, "btn_lokkupVarienGroupProductInformation");
		txt_productVarientSearch = findElementInXLSheet(getParameterProduction, "txt_productVarientSearch");
		td_firstResultVarirnLookupPI = findElementInXLSheet(getParameterProduction, "td_firstResultVarirnLookupPI");
		drop_varienSelectClassificationTab = findElementInXLSheet(getParameterProduction, "drop_varienSelectClassificationTab");
		div_varienOnVarientSectionProductReplace = findElementInXLSheet(getParameterProduction, "div_varienOnVarientSectionProductReplace");
		btn_varientUpdate = findElementInXLSheet(getParameterProduction, "btn_varientUpdate");
		p_varientUpdatedValidation = findElementInXLSheet(getParameterProduction, "p_varientUpdatedValidation");
		btn_lookupOnLineBOMRowReplace = findElementInXLSheet(getParameterProduction, "btn_lookupOnLineBOMRowReplace");
		txt_qtyBOMInputProductLinesRowReplace = findElementInXLSheet(getParameterProduction, "txt_qtyBOMInputProductLinesRowReplace");
		btn_addNewRecordBoM = findElementInXLSheet(getParameterProduction, "btn_addNewRecordBoM");
		header_comlumQtyBoMMaterialInfoTable = findElementInXLSheet(getParameterProduction, "header_comlumQtyBoMMaterialInfoTable");
		header_comlumFixedQtyBoMMaterialInfoTable = findElementInXLSheet(getParameterProduction, "header_comlumFixedQtyBoMMaterialInfoTable");
		header_scrapFixedQtyBoMMaterialInfoTable = findElementInXLSheet(getParameterProduction, "header_scrapFixedQtyBoMMaterialInfoTable");
		chk_fixedQtyRowReplaceBOM = findElementInXLSheet(getParameterProduction, "chk_fixedQtyRowReplaceBOM");
		txt_scrapRowReplaceBOM = findElementInXLSheet(getParameterProduction, "txt_scrapRowReplaceBOM");
		header_draftedBOM = findElementInXLSheet(getParameterProduction, "header_draftedBOM");
		header_releasedBillOfMaterial = findElementInXLSheet(getParameterProduction, "header_releasedBillOfMaterial");
		btn_overheadInformation = findElementInXLSheet(getParameterProduction, "btn_overheadInformation");
		btn_newOverheadInformation = findElementInXLSheet(getParameterProduction, "btn_newOverheadInformation");
		txt_overheadCode2 = findElementInXLSheet(getParameterProduction, "txt_overheadCode2");
		txt_overheadDescription = findElementInXLSheet(getParameterProduction, "txt_overheadDescription");
		drop_overheadGroup = findElementInXLSheet(getParameterProduction, "drop_overheadGroup");
		drop_rateTypeOverhead = findElementInXLSheet(getParameterProduction, "drop_rateTypeOverhead");
		btn_draftOverhead2 = findElementInXLSheet(getParameterProduction, "btn_draftOverhead2");
		lookup_newOverheadInformation = findElementInXLSheet(getParameterProduction, "lookup_newOverheadInformation");
		div_selectOverheadReplaceOverhead = findElementInXLSheet(getParameterProduction, "div_selectOverheadReplaceOverhead");
		a_acitveOverhead = findElementInXLSheet(getParameterProduction, "a_acitveOverhead");
		btn_yesConfirmationOverheadActivation = findElementInXLSheet(getParameterProduction, "btn_yesConfirmationOverheadActivation");
		b_activatedStatusOverhead = findElementInXLSheet(getParameterProduction, "b_activatedStatusOverhead");
		div_selectedOverheadReplaceOverhead = findElementInXLSheet(getParameterProduction, "div_selectedOverheadReplaceOverhead");
		btn_organizationMgt = findElementInXLSheet(getParameterProduction, "btn_organizationMgt");
		btn_resourseInformation = findElementInXLSheet(getParameterProduction, "btn_resourseInformation");
		btn_newResourse = findElementInXLSheet(getParameterProduction, "btn_newResourse");
		txt_resourseCodeRI = findElementInXLSheet(getParameterProduction, "txt_resourseCodeRI");
		txt_resourseDescriptionRI = findElementInXLSheet(getParameterProduction, "txt_resourseDescriptionRI");
		drop_resourseGroupRI = findElementInXLSheet(getParameterProduction, "drop_resourseGroupRI");
		btn_lookupEmployeeRI = findElementInXLSheet(getParameterProduction, "btn_lookupEmployeeRI");
		txt_searchEmployeeRI = findElementInXLSheet(getParameterProduction, "txt_searchEmployeeRI");
		td_resultEmplyeeLookupEmployeeReplace = findElementInXLSheet(getParameterProduction, "td_resultEmplyeeLookupEmployeeReplace");
		txt_hourlyRateResourse = findElementInXLSheet(getParameterProduction, "txt_hourlyRateResourse");
		chk_productionProjectResourse = findElementInXLSheet(getParameterProduction, "chk_productionProjectResourse");
		btn_overheadLookupResourseInformation = findElementInXLSheet(getParameterProduction, "btn_overheadLookupResourseInformation");
		txt_searchOverhead = findElementInXLSheet(getParameterProduction, "txt_searchOverhead");
		txt_overheadeRateAttachedToRsourseOne = findElementInXLSheet(getParameterProduction, "txt_overheadeRateAttachedToRsourseOne");
		btn_addNewRowOverheadRI = findElementInXLSheet(getParameterProduction, "btn_addNewRowOverheadRI");
		btn_overheadSecondRowLookupResourseInformation = findElementInXLSheet(getParameterProduction, "btn_overheadSecondRowLookupResourseInformation");
		txt_overheadeSecondRowRateAttachedToRsourseOne = findElementInXLSheet(getParameterProduction, "txt_overheadeSecondRowRateAttachedToRsourseOne");
		header_releasedReourse = findElementInXLSheet(getParameterProduction, "header_releasedReourse");
		btn_plusAddNewResourse = findElementInXLSheet(getParameterProduction, "btn_plusAddNewResourse");
		td_resultOveheadInformationOverheadReplace = findElementInXLSheet(getParameterProduction, "td_resultOveheadInformationOverheadReplace");
		btn_billOfOperation = findElementInXLSheet(getParameterProduction, "btn_billOfOperation");
		header_BillOfOperationByPage = findElementInXLSheet(getParameterProduction, "header_BillOfOperationByPage");
		btn_newBillOfOperation = findElementInXLSheet(getParameterProduction, "btn_newBillOfOperation");
		header_newBillOfOperation = findElementInXLSheet(getParameterProduction, "header_newBillOfOperation");
		txt_booCodeBillOfOperation = findElementInXLSheet(getParameterProduction, "txt_booCodeBillOfOperation");
		btn_addBillOfOperationRowReplace = findElementInXLSheet(getParameterProduction, "btn_addBillOfOperationRowReplace");
		drop_elementCategoryBOO = findElementInXLSheet(getParameterProduction, "drop_elementCategoryBOO");
		drop_activityTypeBOO = findElementInXLSheet(getParameterProduction, "drop_activityTypeBOO");
		txt_elementDescriptionBOO = findElementInXLSheet(getParameterProduction, "txt_elementDescriptionBOO");
		btn_applyOperationOneBOO = findElementInXLSheet(getParameterProduction, "btn_applyOperationOneBOO");
		txt_groupIdBOO = findElementInXLSheet(getParameterProduction, "txt_groupIdBOO");
		drop_batchMethodBOO = findElementInXLSheet(getParameterProduction, "drop_batchMethodBOO");
		chk_finalInvestigationBOO = findElementInXLSheet(getParameterProduction, "chk_finalInvestigationBOO");
		header_draftedBOO = findElementInXLSheet(getParameterProduction, "header_draftedBOO");
		header_releasedBOO = findElementInXLSheet(getParameterProduction, "header_releasedBOO");
		btn_mainResourseLookuoOperationOne = findElementInXLSheet(getParameterProduction, "btn_mainResourseLookuoOperationOne");
		td_resultResourseOperationOne = findElementInXLSheet(getParameterProduction, "td_resultResourseOperationOne");
		txt_mainResourseOnLookupOperationOne = findElementInXLSheet(getParameterProduction, "txt_mainResourseOnLookupOperationOne");
		txt_fixedDurationOpeartionOne = findElementInXLSheet(getParameterProduction, "txt_fixedDurationOpeartionOne");
		a_oneHourFixedDurationOpeationOne = findElementInXLSheet(getParameterProduction, "a_oneHourFixedDurationOpeationOne");
		div_fixedDurationOpeationOne = findElementInXLSheet(getParameterProduction, "div_fixedDurationOpeationOne");
		btn_okFixedDurationOpeationOne = findElementInXLSheet(getParameterProduction, "btn_okFixedDurationOpeationOne");
		txt_fixedDurationMinOpeartionOne = findElementInXLSheet(getParameterProduction, "txt_fixedDurationMinOpeartionOne");
		lbl_docNoBoo = findElementInXLSheet(getParameterProduction, "lbl_docNoBoo");
		txt_locationCOde = findElementInXLSheet(getParameterProduction, "txt_locationCOde");
		txt_descriptionLocationInformation = findElementInXLSheet(getParameterProduction, "txt_descriptionLocationInformation");
		drop_businessUnitLI = findElementInXLSheet(getParameterProduction, "drop_businessUnitLI");
		drop_inputWarehouseLI = findElementInXLSheet(getParameterProduction, "drop_inputWarehouseLI");
		drop_outputWarehouseLI = findElementInXLSheet(getParameterProduction, "drop_outputWarehouseLI");
		drop_wipWarehouseLI = findElementInXLSheet(getParameterProduction, "drop_wipWarehouseLI");
		drop_addressLI = findElementInXLSheet(getParameterProduction, "drop_addressLI");
		drop_siteLI = findElementInXLSheet(getParameterProduction, "drop_siteLI");
		btn_draftLI = findElementInXLSheet(getParameterProduction, "btn_draftLI");
		div_selectLocationReplaceLocation = findElementInXLSheet(getParameterProduction, "div_selectLocationReplaceLocation");
		a_acitveLocation = findElementInXLSheet(getParameterProduction, "a_acitveLocation");
		btn_yesConfirmationLocationActivation = findElementInXLSheet(getParameterProduction, "btn_yesConfirmationLocationActivation");
		b_activatedStatusLocation = findElementInXLSheet(getParameterProduction, "b_activatedStatusLocation");
		div_selectedLocationReplaceLocation = findElementInXLSheet(getParameterProduction, "div_selectedLocationReplaceLocation");
		window_newLocation = findElementInXLSheet(getParameterProduction, "window_newLocation");
		btn_pricingProfile2 = findElementInXLSheet(getParameterProduction, "btn_pricingProfile2");
		btn_newPricingProfile2 = findElementInXLSheet(getParameterProduction, "btn_newPricingProfile2");
		txt_pricingProfileCode2 = findElementInXLSheet(getParameterProduction, "txt_pricingProfileCode2");
		txt_pricingProfileDescription = findElementInXLSheet(getParameterProduction, "txt_pricingProfileDescription");
		tab_estimationPricingProfile = findElementInXLSheet(getParameterProduction, "tab_estimationPricingProfile");
		drop_materialEstimationTabPP = findElementInXLSheet(getParameterProduction, "drop_materialEstimationTabPP");
		drop_labourResorseEstimationTabPP = findElementInXLSheet(getParameterProduction, "drop_labourResorseEstimationTabPP");
		drop_overheadEstimationTabPP = findElementInXLSheet(getParameterProduction, "drop_overheadEstimationTabPP");
		drop_serviceEstimationTabPP = findElementInXLSheet(getParameterProduction, "drop_serviceEstimationTabPP");
		drop_expenceEstimationTabPP = findElementInXLSheet(getParameterProduction, "drop_expenceEstimationTabPP");
		drop_materialActualTabPP = findElementInXLSheet(getParameterProduction, "drop_materialActualTabPP");
		drop_labourResorseActualTabPP = findElementInXLSheet(getParameterProduction, "drop_labourResorseActualTabPP");
		drop_overheadActualTabPP = findElementInXLSheet(getParameterProduction, "drop_overheadActualTabPP");
		drop_serviceActualTabPP = findElementInXLSheet(getParameterProduction, "drop_serviceActualTabPP");
		drop_expenceActualTabPP = findElementInXLSheet(getParameterProduction, "drop_expenceActualTabPP");
		btn_draftPP = findElementInXLSheet(getParameterProduction, "btn_draftPP");
		div_selectPricingProfilenReplacePricingProfile = findElementInXLSheet(getParameterProduction, "div_selectPricingProfilenReplacePricingProfile");
		a_acitvePP = findElementInXLSheet(getParameterProduction, "a_acitvePP");
		btn_yesConfirmationPPActivation = findElementInXLSheet(getParameterProduction, "btn_yesConfirmationPPActivation");
		b_activatedStatusPP = findElementInXLSheet(getParameterProduction, "b_activatedStatusPP");
		div_selectedPricingProfileReplacePricingProfile = findElementInXLSheet(getParameterProduction, "div_selectedPricingProfileReplacePricingProfile");
		window_newPP = findElementInXLSheet(getParameterProduction, "window_newPP");
		btn_actualCalclationTabPP = findElementInXLSheet(getParameterProduction, "btn_actualCalclationTabPP");
		drop_pricingGroupPP = findElementInXLSheet(getParameterProduction, "drop_pricingGroupPP");
		
		/* Production Model */
		lnk_resultProductionUnitOnLookupUnitReplace = findElementInXLSheet(getParameterProduction, "lnk_resultProductionUnitOnLookupUnitReplace");
		td_resultPricingProfileReplacePP = findElementInXLSheet(getParameterProduction, "td_resultPricingProfileReplacePP");
		div_resourceOnAdditonalResourceGridPMResorceReplace = findElementInXLSheet(getParameterProduction, "div_resourceOnAdditonalResourceGridPMResorceReplace");
		td_resultBOOPMBooReplace = findElementInXLSheet(getParameterProduction, "td_resultBOOPMBooReplace");
		td_resultResourceAdditionalResourceTabPMResourseReplace = findElementInXLSheet(getParameterProduction, "td_resultResourceAdditionalResourceTabPMResourseReplace");
		chk_mrpProductionModel = findElementInXLSheet(getParameterProduction, "chk_mrpProductionModel");
		
		/* Production Order */
		td_resultProductRegressionPOProductReplace = findElementInXLSheet(getParameterProduction, "td_resultProductRegressionPOProductReplace");
	
		/* MRP Run */
		chk_mrpPODoRepalce = findElementInXLSheet(getParameterProduction, "chk_mrpPODoRepalce");
		btn_actionMRP = findElementInXLSheet(getParameterProduction, "btn_actionMRP");
		header_productionControlWindow = findElementInXLSheet(getParameterProduction, "header_productionControlWindow");
		btn_runMRPWindow = findElementInXLSheet(getParameterProduction, "btn_runMRPWindow");
		para_MRPReleaseValidation = findElementInXLSheet(getParameterProduction, "para_MRPReleaseValidation");
		btn_logTabProductionControlWindow = findElementInXLSheet(getParameterProduction, "btn_logTabProductionControlWindow");
		lbl_genaratedInternalOrderMRP = findElementInXLSheet(getParameterProduction, "lbl_genaratedInternalOrderMRP");
		btn_cloaseMRPWindow = findElementInXLSheet(getParameterProduction, "btn_cloaseMRPWindow");
		btn_homeLink = findElementInXLSheet(getParameterProduction, "btn_homeLink");
		btn_taskEvent2 = findElementInXLSheet(getParameterProduction, "btn_taskEvent2");
		btn_internalDispatchOrderTaskAndEvent = findElementInXLSheet(getParameterProduction, "btn_internalDispatchOrderTaskAndEvent");
		btn_pendingIDODocRepalce = findElementInXLSheet(getParameterProduction, "btn_pendingIDODocRepalce");
		header_newIDO = findElementInXLSheet(getParameterProduction, "header_newIDO");
		txt_shippingAddressIDO = findElementInXLSheet(getParameterProduction, "txt_shippingAddressIDO");
		header_draftedIDO = findElementInXLSheet(getParameterProduction, "header_draftedIDO");
		lbl_docNumber = findElementInXLSheet(getParameterProduction, "lbl_docNumber");
		header_releasedIDO = findElementInXLSheet(getParameterProduction, "header_releasedIDO");
		btn_releaseMRP = findElementInXLSheet(getParameterProduction, "btn_releaseMRP");
		btn_firstRownErrorOnGrid = findElementInXLSheet(getParameterProduction, "btn_firstRownErrorOnGrid");
		div_quantityNotAvaialableValidation = findElementInXLSheet(getParameterProduction, "div_quantityNotAvaialableValidation");
		
		/* Allocated - Costing Summary */
		btn_expandCostingSummary = findElementInXLSheet(getParameterProduction, "btn_expandCostingSummary");
		column_headerAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "column_headerAllocatedCostingSummary");
		td_mainResourseHeaderCostCostingSummary = findElementInXLSheet(getParameterProduction, "td_mainResourseHeaderCostCostingSummary");
		td_resourceCostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCostAllocatedCostingSummary");
		td_resurce1CostingSummary = findElementInXLSheet(getParameterProduction, "td_resurce1CostingSummary");
		td_resurce2CostingSummary = findElementInXLSheet(getParameterProduction, "td_resurce2CostingSummary");
		td_mainMaterialsHeaderCostCostingSummary = findElementInXLSheet(getParameterProduction, "td_mainMaterialsHeaderCostCostingSummary");
		td_material1CostingSummary = findElementInXLSheet(getParameterProduction, "td_material1CostingSummary");
		td_material2CostingSummary = findElementInXLSheet(getParameterProduction, "td_material2CostingSummary");
		td_material3CostingSummary = findElementInXLSheet(getParameterProduction, "td_material3CostingSummary");
		td_mainOverheadHeaderCostCostingSummary = findElementInXLSheet(getParameterProduction, "td_mainOverheadHeaderCostCostingSummary");
		td_overhead1CostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead1CostingSummary");
		td_overhead2CostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead2CostingSummary");
		td_overhead3CostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead3CostingSummary");
		td_unitCostHeaderResourseCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostHeaderResourseCostingSummary");
		td_unitCostHeaderMaterialCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostHeaderMaterialCostingSummary");
		td_unitCostHeaderOverheadCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostHeaderOverheadCostingSummary");
		td_totalCostingSummary = findElementInXLSheet(getParameterProduction, "td_totalCostingSummary");
		td_unitCostHeaderCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostHeaderCostingSummary");
		td_resourceCost1AllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCost1AllocatedCostingSummary");
		td_resourceCost2AllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCost2AllocatedCostingSummary");
		td_materialCostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_materialCostAllocatedCostingSummary");
		td_material1CostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_material1CostAllocatedCostingSummary");
		td_material2CostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_material2CostAllocatedCostingSummary");
		td_material3CostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_material3CostAllocatedCostingSummary");
		td_overheadCostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_overheadCostAllocatedCostingSummary");
		td_overhead1CostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead1CostAllocatedCostingSummary");
		td_overhead2CostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead2CostAllocatedCostingSummary");
		td_overhead3CostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead3CostAllocatedCostingSummary");
		td_totalCostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_totalCostAllocatedCostingSummary");
		td_unitCostAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostAllocatedCostingSummary");
		td_unitCostResourseAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostResourseAllocatedCostingSummary");
		td_unitCostMateraialAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialAllocatedCostingSummary");
		td_unitCostOverheadAllocatedCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadAllocatedCostingSummary");
		
		/* Purchase Order */
		btn_prcumentModule = findElementInXLSheet(getParameterProduction, "btn_prcumentModule");
		btn_purchaseOrder = findElementInXLSheet(getParameterProduction, "btn_purchaseOrder");
		btn_newPurchaseOrder = findElementInXLSheet(getParameterProduction, "btn_newPurchaseOrder");
		btn_purchaseOrderInboundShipmentToPurchaseInvoiceJourney = findElementInXLSheet(getParameterProduction, "btn_purchaseOrderInboundShipmentToPurchaseInvoiceJourney");
		btn_vendorLookup = findElementInXLSheet(getParameterProduction, "btn_vendorLookup");
		txt_vendorInformation = findElementInXLSheet(getParameterProduction, "txt_vendorInformation");
		td_resultVendorFirstResult = findElementInXLSheet(getParameterProduction, "td_resultVendorFirstResult");
		btn_billingAddressLookup = findElementInXLSheet(getParameterProduction, "btn_billingAddressLookup");
		txt_billingAddresePurchaseOrder = findElementInXLSheet(getParameterProduction, "txt_billingAddresePurchaseOrder");
		btn_applyAddressPurchaseOrder = findElementInXLSheet(getParameterProduction, "btn_applyAddressPurchaseOrder");
		btn_productionLookupPurchaseOrderGrid = findElementInXLSheet(getParameterProduction, "btn_productionLookupPurchaseOrderGrid");
		td_resultProcuctProductionLookupProductReplace = findElementInXLSheet(getParameterProduction, "td_resultProcuctProductionLookupProductReplace");
		txt_qtyPurchaseOrderRowReplace = findElementInXLSheet(getParameterProduction, "txt_qtyPurchaseOrderRowReplace");
		txt_unitPricePurchaseOrderRowReplace = findElementInXLSheet(getParameterProduction, "txt_unitPricePurchaseOrderRowReplace");
		btn_goToPage = findElementInXLSheet(getParameterProduction, "btn_goToPage");
		drop_selectWarehouseInboundShipmentRowReplace = findElementInXLSheet(getParameterProduction, "drop_selectWarehouseInboundShipmentRowReplace");
		btn_searilBatch = findElementInXLSheet(getParameterProduction, "btn_searilBatch");
		btn_captureProductOne = findElementInXLSheet(getParameterProduction, "btn_captureProductOne");
		btn_captureProductTwo = findElementInXLSheet(getParameterProduction, "btn_captureProductTwo");
		btn_captureProductThree = findElementInXLSheet(getParameterProduction, "btn_captureProductThree");
		txt_batchNoSerialBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "txt_batchNoSerialBatchCaptureWindow");
		txt_lotNoSerialBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "txt_lotNoSerialBatchCaptureWindow");
		txt_expiryDateSerialBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "txt_expiryDateSerialBatchCaptureWindow");
		btn_nextArrowSerialBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "btn_nextArrowSerialBatchCaptureWindow");
		btn_defoultLastDateCalender = findElementInXLSheet(getParameterProduction, "btn_defoultLastDateCalender");
		txt_menufactureDateSerialBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "txt_menufactureDateSerialBatchCaptureWindow");
		btn_defoultFirstDateCalender = findElementInXLSheet(getParameterProduction, "btn_defoultFirstDateCalender");
		btn_updateSearielBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "btn_updateSearielBatchCaptureWindow");
		btn_backSearielBatchCaptureWindow = findElementInXLSheet(getParameterProduction, "btn_backSearielBatchCaptureWindow");
		header_releasedPurchaseInvoice = findElementInXLSheet(getParameterProduction, "header_releasedPurchaseInvoice");
		btn_addNewRecordToGrid = findElementInXLSheet(getParameterProduction, "btn_addNewRecordToGrid");
		btn_captureProductOneGreen = findElementInXLSheet(getParameterProduction, "btn_captureProductOneGreen");
		btn_captureProductTwoGreen = findElementInXLSheet(getParameterProduction, "btn_captureProductTwoGreen");
		btn_captureProductThreeGreen = findElementInXLSheet(getParameterProduction, "btn_captureProductThreeGreen");

		/* Shop Floor Update */
		btn_action2 = findElementInXLSheet(getParameterProduction, "btn_action2");
		btn_shopFloorUpdate2 = findElementInXLSheet(getParameterProduction, "btn_shopFloorUpdate2");
		header_shopFloorUpdateWindow = findElementInXLSheet(getParameterProduction, "header_shopFloorUpdateWindow");
		btn_startFirstOperationSFU = findElementInXLSheet(getParameterProduction, "btn_startFirstOperationSFU");
		btn_startSecondLeveleSFU = findElementInXLSheet(getParameterProduction, "btn_startSecondLeveleSFU");
		btn_actualUpdateSFU = findElementInXLSheet(getParameterProduction, "btn_actualUpdateSFU");
		txt_outoutQuantityACtualUpdateSFU = findElementInXLSheet(getParameterProduction, "txt_outoutQuantityACtualUpdateSFU");
		btn_upateActualSFU = findElementInXLSheet(getParameterProduction, "btn_upateActualSFU");
		para_successfullValidationOutProductActualUpdate = findElementInXLSheet(getParameterProduction, "para_successfullValidationOutProductActualUpdate");
		btn_tapInputProductsShopFloorUpdate = findElementInXLSheet(getParameterProduction, "btn_tapInputProductsShopFloorUpdate");
		txt_actualUpdaInputProductQuantityFirstOperation = findElementInXLSheet(getParameterProduction, "txt_actualUpdaInputProductQuantityFirstOperation");
		btn_closeFisrstOperationActualUpdateWindow = findElementInXLSheet(getParameterProduction, "btn_closeFisrstOperationActualUpdateWindow");
		btn_completeFirstOperation = findElementInXLSheet(getParameterProduction, "btn_completeFirstOperation");
		btn_completeSecondLeveleSFU = findElementInXLSheet(getParameterProduction, "btn_completeSecondLeveleSFU");
		lbl_completedShopFloorUpdateBarFirstOperation = findElementInXLSheet(getParameterProduction, "lbl_completedShopFloorUpdateBarFirstOperation");
		btn_closeSecondOperationActualUpdateWindow = findElementInXLSheet(getParameterProduction, "btn_closeSecondOperationActualUpdateWindow");
		btn_completeSecondOperation = findElementInXLSheet(getParameterProduction, "btn_completeSecondOperation");
		lbl_completedShopFloorUpdateBarSecondOperation = findElementInXLSheet(getParameterProduction, "lbl_completedShopFloorUpdateBarSecondOperation");
		para_successfullValidationActualUpdate = findElementInXLSheet(getParameterProduction, "para_successfullValidationActualUpdate");
		txt_actualUpdaInputProductQuantitySecondOperation = findElementInXLSheet(getParameterProduction, "txt_actualUpdaInputProductQuantitySecondOperation");
		txt_actualUpdaInputProductQuantity2FirstOperation = findElementInXLSheet(getParameterProduction, "txt_actualUpdaInputProductQuantity2FirstOperation");
		tab_resourseShopFloorUpdate = findElementInXLSheet(getParameterProduction, "tab_resourseShopFloorUpdate");
		div_actualHoursResourseTabRow01 = findElementInXLSheet(getParameterProduction, "div_actualHoursResourseTabRow01");
		div_actualMinsResourseTabRow01 = findElementInXLSheet(getParameterProduction, "div_actualMinsResourseTabRow01");
		div_actualHoursResourseTabRow02 = findElementInXLSheet(getParameterProduction, "div_actualHoursResourseTabRow02");
		div_actualMinsResourseTabRow02 = findElementInXLSheet(getParameterProduction, "div_actualMinsResourseTabRow02");
		para_successfullValidationActualUpdateResourse = findElementInXLSheet(getParameterProduction, "para_successfullValidationActualUpdateResourse");
		
		/* QC */
		btn_productionQCAndSorting = findElementInXLSheet(getParameterProduction, "btn_productionQCAndSorting");
		drop_documentTypeProductionQC = findElementInXLSheet(getParameterProduction, "drop_documentTypeProductionQC");
		txt_docNumberQC = findElementInXLSheet(getParameterProduction, "txt_docNumberQC");
		div_selectedQcDocProduction = findElementInXLSheet(getParameterProduction, "div_selectedQcDocProduction");
		chk_qcProductionAccordingToProductReplaceProduct = findElementInXLSheet(getParameterProduction, "chk_qcProductionAccordingToProductReplaceProduct");
		txt_passQuantityProductionQCOneRow = findElementInXLSheet(getParameterProduction, "txt_passQuantityProductionQCOneRow");
		txt_failQuantityProductionQCOneRow = findElementInXLSheet(getParameterProduction, "txt_failQuantityProductionQCOneRow");
		btn_releseProductionQC = findElementInXLSheet(getParameterProduction, "btn_releseProductionQC");
		para_successfullValidationProductionQC = findElementInXLSheet(getParameterProduction, "para_successfullValidationProductionQC");
		column_headerTotalQtyQC = findElementInXLSheet(getParameterProduction, "column_headerTotalQtyQC");
		txt_totalQtyQC = findElementInXLSheet(getParameterProduction, "txt_totalQtyQC");
		tab_historyQC = findElementInXLSheet(getParameterProduction, "tab_historyQC");
		column_headerFailedQtyHistoryTabQC = findElementInXLSheet(getParameterProduction, "column_headerFailedQtyHistoryTabQC");
		txt_failedQtyQC = findElementInXLSheet(getParameterProduction, "txt_failedQtyQC");
		tab_historySelectedQC = findElementInXLSheet(getParameterProduction, "tab_historySelectedQC");
		btn_reverseQC = findElementInXLSheet(getParameterProduction, "btn_reverseQC");
		tab_pendingQC = findElementInXLSheet(getParameterProduction, "tab_pendingQC");
		tab_pendingSelectedQC = findElementInXLSheet(getParameterProduction, "tab_pendingSelectedQC");
		div_selectedQcDocProductionHistoryTab = findElementInXLSheet(getParameterProduction, "div_selectedQcDocProductionHistoryTab");
		div_qcDocNo = findElementInXLSheet(getParameterProduction, "div_qcDocNo");
		column_headerPassedQtyHistoryTabQC = findElementInXLSheet(getParameterProduction, "column_headerPassedQtyHistoryTabQC");
		txt_passedQtyQC = findElementInXLSheet(getParameterProduction, "txt_passedQtyQC");
		
		/* Production Costing */
		btn_productionCostingActions = findElementInXLSheet(getParameterProduction, "btn_productionCostingActions");
		header_productionCostingWindow = findElementInXLSheet(getParameterProduction, "header_productionCostingWindow");
		btn_actualUpdateProductionCosting = findElementInXLSheet(getParameterProduction, "btn_actualUpdateProductionCosting");
		tab_inputProductsProductionCosting = findElementInXLSheet(getParameterProduction, "tab_inputProductsProductionCosting");
		txt_actualUpdateProductionCosting = findElementInXLSheet(getParameterProduction, "txt_actualUpdateProductionCosting");
		btn_updateProductionCosting = findElementInXLSheet(getParameterProduction, "btn_updateProductionCosting");
		para_successfullValidationActualUpdateProductionCosting = findElementInXLSheet(getParameterProduction, "para_successfullValidationActualUpdateProductionCosting");
		btn_closeSecondWindowCostingUpdate = findElementInXLSheet(getParameterProduction, "btn_closeSecondWindowCostingUpdate");
		chk_pendingProductionCostingSelected = findElementInXLSheet(getParameterProduction, "chk_pendingProductionCostingSelected");
		btn_updateProductionCostingMainWindow = findElementInXLSheet(getParameterProduction, "btn_updateProductionCostingMainWindow");
		para_batchCostingUpdateValidation = findElementInXLSheet(getParameterProduction, "para_batchCostingUpdateValidation");
		btn_closeMainCostingUpdateWindowProductionOrder = findElementInXLSheet(getParameterProduction, "btn_closeMainCostingUpdateWindowProductionOrder");
		header_completedPO = findElementInXLSheet(getParameterProduction, "header_completedPO");
		chk_pendingProductionCosting = findElementInXLSheet(getParameterProduction, "chk_pendingProductionCosting");
		
		/* Costing Summary - Actual */
		column_headerActualCostingSummary = findElementInXLSheet(getParameterProduction, "column_headerActualCostingSummary");
		td_resourceCostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCostActualCostingSummary");
		td_resourceCost1ActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCost1ActualCostingSummary");
		td_resourceCost2ActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCost2ActualCostingSummary");
		td_materialCostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_materialCostActualCostingSummary");
		td_material1CostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_material1CostActualCostingSummary");
		td_material2CostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_material2CostActualCostingSummary");
		td_material3CostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_material3CostActualCostingSummary");
		td_overheadCostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_overheadCostActualCostingSummary");
		td_overhead1CostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead1CostActualCostingSummary");
		td_overhead2CostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead2CostActualCostingSummary");
		td_overhead3CostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead3CostActualCostingSummary");
		td_totalCostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_totalCostActualCostingSummary");
		td_unitCostActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostActualCostingSummary");
		td_unitCostResourseActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostResourseActualCostingSummary");
		td_unitCostMateraialActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialActualCostingSummary");
		td_unitCostOverheadActualCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadActualCostingSummary");
		btn_closeMainValidation = findElementInXLSheet(getParameterProduction, "btn_closeMainValidation");

		/* Cost Estimation - Base Amount */
		column_headerBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "column_headerBaseAmountCostingSummary");
		td_resourceCostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCostBaseAmountCostingSummary");
		td_resourceCost1BaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCost1BaseAmountCostingSummary");
		td_resourceCost2BaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_resourceCost2BaseAmountCostingSummary");
		td_materialCostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_materialCostBaseAmountCostingSummary");
		td_material1CostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_material1CostBaseAmountCostingSummary");
		td_material2CostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_material2CostBaseAmountCostingSummary");
		td_material3CostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_material3CostBaseAmountCostingSummary");
		td_overheadCostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_overheadCostBaseAmountCostingSummary");
		td_overhead1CostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead1CostBaseAmountCostingSummary");
		td_overhead2CostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead2CostBaseAmountCostingSummary");
		td_overhead3CostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_overhead3CostBaseAmountCostingSummary");
		td_totalCostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_totalCostBaseAmountCostingSummary");
		td_unitCostBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostBaseAmountCostingSummary");
		td_unitCostResourseBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostResourseBaseAmountCostingSummary");
		td_unitCostMateraialBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialBaseAmountCostingSummary");
		td_unitCostOverheadBaseAmountCostingSummary = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadBaseAmountCostingSummary");
	
		/* Generate Internal Receipt / Journal Entry */
		btn_convertToInternalReceipt = findElementInXLSheet(getParameterProduction, "btn_convertToInternalReceipt");
		header_genarateInternalReciptSmoke = findElementInXLSheet(getParameterProduction, "header_genarateInternalReciptSmoke");
		chk_seletcOroductForInternalRecipt = findElementInXLSheet(getParameterProduction, "chk_seletcOroductForInternalRecipt");
		btn_genarateInternalRecipt = findElementInXLSheet(getParameterProduction, "btn_genarateInternalRecipt");
		para_validationInternalREciptGenarateSuccessfull = findElementInXLSheet(getParameterProduction, "para_validationInternalREciptGenarateSuccessfull");
		lnk_genaratedInternalREciptSmoke = findElementInXLSheet(getParameterProduction, "lnk_genaratedInternalREciptSmoke");
		header_releasedInternalRecipt = findElementInXLSheet(getParameterProduction, "header_releasedInternalRecipt");
		btn_journal = findElementInXLSheet(getParameterProduction, "btn_journal");
		header_journalEntryPopup = findElementInXLSheet(getParameterProduction, "header_journalEntryPopup");
		lbl_debitValueJournelEntry = findElementInXLSheet(getParameterProduction, "lbl_debitValueJournelEntry");
		lbl_creditValueJournelEntry = findElementInXLSheet(getParameterProduction, "lbl_creditValueJournelEntry");
		lbl_debitTotalJournelEntrySingleEntry = findElementInXLSheet(getParameterProduction, "lbl_debitTotalJournelEntrySingleEntry");
		lbl_creditTotalJournelEntrySingleEntry = findElementInXLSheet(getParameterProduction, "lbl_creditTotalJournelEntrySingleEntry");
		txt_reciptDateGenarateInternalREceiptWindow = findElementInXLSheet(getParameterProduction, "txt_reciptDateGenarateInternalREceiptWindow");
		a_documentDate = findElementInXLSheet(getParameterProduction, "a_documentDate");
		a_postDate = findElementInXLSheet(getParameterProduction, "a_postDate");
		a_dueDate = findElementInXLSheet(getParameterProduction, "a_dueDate");
		div_warehouseInternalRecipt = findElementInXLSheet(getParameterProduction, "div_warehouseInternalRecipt");
		div_planedQtyInternalRecipt = findElementInXLSheet(getParameterProduction, "div_planedQtyInternalRecipt");
		div_overheadMainList = findElementInXLSheet(getParameterProduction, "div_overheadMainList");
		txt_searchResources = findElementInXLSheet(getParameterProduction, "txt_searchResources");
		td_resultResourceCodeReplace = findElementInXLSheet(getParameterProduction, "td_resultResourceCodeReplace");
		div_productionUnitReplaceCode = findElementInXLSheet(getParameterProduction, "div_productionUnitReplaceCode");
		div_pricingProfileReplaceCode = findElementInXLSheet(getParameterProduction, "div_pricingProfileReplaceCode");
		lbl_debitValueJournelEntryStaging = findElementInXLSheet(getParameterProduction, "lbl_debitValueJournelEntryStaging");
		lbl_creditValueJournelEntryStaging = findElementInXLSheet(getParameterProduction, "lbl_creditValueJournelEntryStaging");
		span_firstErrorOnGridInputProductActualUpdate = findElementInXLSheet(getParameterProduction, "span_firstErrorOnGridInputProductActualUpdate");
		div_notEnoughQuantityAvaialableValidation = findElementInXLSheet(getParameterProduction, "div_notEnoughQuantityAvaialableValidation");
		
		/* PROD_E2E_002 */
		btn_inventoryModule = findElementInXLSheet(getParameterProduction, "btn_inventoryModule");
		btn_internalOrderInventoryModule = findElementInXLSheet(getParameterProduction, "btn_internalOrderInventoryModule");
		btn_newInternalOrder = findElementInXLSheet(getParameterProduction, "btn_newInternalOrder");
		txt_tiltleInternelOrder = findElementInXLSheet(getParameterProduction, "txt_tiltleInternelOrder");
		btn_wipRequestJourneyIO = findElementInXLSheet(getParameterProduction, "btn_wipRequestJourneyIO");
		drop_toWarehouseIO = findElementInXLSheet(getParameterProduction, "drop_toWarehouseIO");
		btn_requesterIO = findElementInXLSheet(getParameterProduction, "btn_requesterIO");
		txt_requestedIO = findElementInXLSheet(getParameterProduction, "txt_requestedIO");
		td_firstResultOfRequestedLookup = findElementInXLSheet(getParameterProduction, "td_firstResultOfRequestedLookup");
		btn_productLookupFirstRowIO = findElementInXLSheet(getParameterProduction, "btn_productLookupFirstRowIO");
		btn_productLookupSecondRowIO = findElementInXLSheet(getParameterProduction, "btn_productLookupSecondRowIO");
		btn_productLookupThirdRowIO = findElementInXLSheet(getParameterProduction, "btn_productLookupThirdRowIO");
		td_resultProductReplaceInfo = findElementInXLSheet(getParameterProduction, "td_resultProductReplaceInfo");
		btn_productionOrderLookupIOFirstRow = findElementInXLSheet(getParameterProduction, "btn_productionOrderLookupIOFirstRow");
		btn_productionOrderLookupIOSecondRow = findElementInXLSheet(getParameterProduction, "btn_productionOrderLookupIOSecondRow");
		btn_productionOrderLookupIOThirdRow = findElementInXLSheet(getParameterProduction, "btn_productionOrderLookupIOThirdRow");
		txt_searchProductionOrderIO = findElementInXLSheet(getParameterProduction, "txt_searchProductionOrderIO");
		td_resultProductionOrderIOReplace = findElementInXLSheet(getParameterProduction, "td_resultProductionOrderIOReplace");
		drop_operationFirstRow = findElementInXLSheet(getParameterProduction, "drop_operationFirstRow");
		drop_operationSecondRow = findElementInXLSheet(getParameterProduction, "drop_operationSecondRow");
		drop_operationThirdRow = findElementInXLSheet(getParameterProduction, "drop_operationThirdRow");
		txt_qtyFirstRow = findElementInXLSheet(getParameterProduction, "txt_qtyFirstRow");
		txt_qtySecondRow = findElementInXLSheet(getParameterProduction, "txt_qtySecondRow");
		txt_qtyThirdRow = findElementInXLSheet(getParameterProduction, "txt_qtyThirdRow");
		btn_addNewRowToGridIO = findElementInXLSheet(getParameterProduction, "btn_addNewRowToGridIO");
		header_draftedIO = findElementInXLSheet(getParameterProduction, "header_draftedIO");
		header_releasedIO = findElementInXLSheet(getParameterProduction, "header_releasedIO");
		drop_warehouseIDOFirstRow = findElementInXLSheet(getParameterProduction, "drop_warehouseIDOFirstRow");
		drop_warehouseIDOSecondRow = findElementInXLSheet(getParameterProduction, "drop_warehouseIDOSecondRow");
		drop_warehouseIDOThirdRow = findElementInXLSheet(getParameterProduction, "drop_warehouseIDOThirdRow");
		txt_actualDuarationResourse01ActualUpdate = findElementInXLSheet(getParameterProduction, "txt_actualDuarationResourse01ActualUpdate");
		btn_oneHouserActualDurationResourse01 = findElementInXLSheet(getParameterProduction, "btn_oneHouserActualDurationResourse01");
		txt_actualDuarationResourse02ActualUpdate = findElementInXLSheet(getParameterProduction, "txt_actualDuarationResourse02ActualUpdate");
		btn_oneHouserActualDurationResourse02 = findElementInXLSheet(getParameterProduction, "btn_oneHouserActualDurationResourse02");
		btn_okActualDurationResourse02 = findElementInXLSheet(getParameterProduction, "btn_okActualDurationResourse02");
		drop_batchNoShopFloor = findElementInXLSheet(getParameterProduction, "drop_batchNoShopFloor");
		btn_addNewBatchShopFloor = findElementInXLSheet(getParameterProduction, "btn_addNewBatchShopFloor");
		btn_genarateBatchShopFloor = findElementInXLSheet(getParameterProduction, "btn_genarateBatchShopFloor");
		btn_completeSecondBatchShopFloor = findElementInXLSheet(getParameterProduction, "btn_completeSecondBatchShopFloor");
		dic_relevantHistoryDocCount = findElementInXLSheet(getParameterProduction, "dic_relevantHistoryDocCount");
		dic_relevantHistoryDocument = findElementInXLSheet(getParameterProduction, "dic_relevantHistoryDocument");
		div_batchNoReleventToHistoryDocument = findElementInXLSheet(getParameterProduction, "div_batchNoReleventToHistoryDocument");
		
		/* Costing Summary  Second Batch */
		td_resourceCostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_resourceCostActualCostingSummaryInfiniteSecondBatch");
		td_resourceCost1ActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost1ActualCostingSummaryInfiniteSecondBatch");
		td_resourceCost2ActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost2ActualCostingSummaryInfiniteSecondBatch");
		td_materialCostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_materialCostActualCostingSummaryInfiniteSecondBatch");
		td_material1CostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_material1CostActualCostingSummaryInfiniteSecondBatch");
		td_material2CostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_material2CostActualCostingSummaryInfiniteSecondBatch");
		td_material3CostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_material3CostActualCostingSummaryInfiniteSecondBatch");
		td_overheadCostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overheadCostActualCostingSummaryInfiniteSecondBatch");
		td_overhead1CostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overhead1CostActualCostingSummaryInfiniteSecondBatch");
		td_overhead2CostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overhead2CostActualCostingSummaryInfiniteSecondBatch");
		td_overhead3CostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overhead3CostActualCostingSummaryInfiniteSecondBatch");
		td_totalCostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_totalCostActualCostingSummaryInfiniteSecondBatch");
		td_unitCostActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostActualCostingSummaryInfiniteSecondBatch");
		td_unitCostResourseActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostResourseActualCostingSummaryInfiniteSecondBatch");
		td_unitCostMateraialActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialActualCostingSummaryInfiniteSecondBatch");
		td_unitCostOverheadActualCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadActualCostingSummaryInfiniteSecondBatch");
		
		/* Costing Summary - Second Batch - Base Amount */
		td_resourceCostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_resourceCostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_resourceCost1BaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost1BaseAmountCostingSummaryInfiniteSecondBatch");
		td_resourceCost2BaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost2BaseAmountCostingSummaryInfiniteSecondBatch");
		td_materialCostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_materialCostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_material1CostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_material1CostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_material2CostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_material2CostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_material3CostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_material3CostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_overheadCostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overheadCostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_overhead1CostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overhead1CostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_overhead2CostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overhead2CostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_overhead3CostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_overhead3CostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_totalCostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_totalCostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_unitCostBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostBaseAmountCostingSummaryInfiniteSecondBatch");
		td_unitCostResourseBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostResourseBaseAmountCostingSummaryInfiniteSecondBatch");
		td_unitCostMateraialBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialBaseAmountCostingSummaryInfiniteSecondBatch");
		td_unitCostOverheadBaseAmountCostingSummaryInfiniteSecondBatch = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadBaseAmountCostingSummaryInfiniteSecondBatch");
		
		/* Generate Internal Receipt */
		chk_seletcOroductForInternalReciptSecondBatch = findElementInXLSheet(getParameterProduction, "chk_seletcOroductForInternalReciptSecondBatch");
		lnk_genaratedInternalREciptSmokeSecondBatch = findElementInXLSheet(getParameterProduction, "lnk_genaratedInternalREciptSmokeSecondBatch");
		
		/* Third Batch */
		btn_completeThirdBatchShopFloor = findElementInXLSheet(getParameterProduction, "btn_completeThirdBatchShopFloor");

		/* Costing Summary - Third Batch - Actual */
		td_resourceCostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_resourceCostActualCostingSummaryInfiniteThirdBatch");
		td_resourceCost1ActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost1ActualCostingSummaryInfiniteThirdBatch");
		td_resourceCost2ActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost2ActualCostingSummaryInfiniteThirdBatch");
		td_materialCostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_materialCostActualCostingSummaryInfiniteThirdBatch");
		td_material1CostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_material1CostActualCostingSummaryInfiniteThirdBatch");
		td_material2CostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_material2CostActualCostingSummaryInfiniteThirdBatch");
		td_material3CostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_material3CostActualCostingSummaryInfiniteThirdBatch");
		td_overheadCostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overheadCostActualCostingSummaryInfiniteThirdBatch");
		td_overhead1CostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overhead1CostActualCostingSummaryInfiniteThirdBatch");
		td_overhead2CostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overhead2CostActualCostingSummaryInfiniteThirdBatch");
		td_overhead3CostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overhead3CostActualCostingSummaryInfiniteThirdBatch");
		td_totalCostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_totalCostActualCostingSummaryInfiniteThirdBatch");
		td_unitCostActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostActualCostingSummaryInfiniteThirdBatch");
		td_unitCostResourseActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostResourseActualCostingSummaryInfiniteThirdBatch");
		td_unitCostMateraialActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialActualCostingSummaryInfiniteThirdBatch");
		td_unitCostOverheadActualCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadActualCostingSummaryInfiniteThirdBatch");
		
		/* Costing Summary - Third Batch - Base Amount */
		td_resourceCostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_resourceCostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_resourceCost1BaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost1BaseAmountCostingSummaryInfiniteThirdBatch");
		td_resourceCost2BaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_resourceCost2BaseAmountCostingSummaryInfiniteThirdBatch");
		td_materialCostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_materialCostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_material1CostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_material1CostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_material2CostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_material2CostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_material3CostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_material3CostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_overheadCostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overheadCostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_overhead1CostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overhead1CostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_overhead2CostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overhead2CostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_overhead3CostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_overhead3CostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_totalCostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_totalCostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_unitCostBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostBaseAmountCostingSummaryInfiniteThirdBatch");
		td_unitCostResourseBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostResourseBaseAmountCostingSummaryInfiniteThirdBatch");
		td_unitCostMateraialBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialBaseAmountCostingSummaryInfiniteThirdBatch");
		td_unitCostOverheadBaseAmountCostingSummaryInfiniteThirdBatch = findElementInXLSheet(getParameterProduction, "td_unitCostOverheadBaseAmountCostingSummaryInfiniteThirdBatch");
		
		/*Generate Internal Receipt - Third Batch*/
		chk_seletcOroductForInternalReciptThirdBatch = findElementInXLSheet(getParameterProduction, "chk_seletcOroductForInternalReciptThirdBatch");
		lnk_genaratedInternalREciptSmokeThirdBatch = findElementInXLSheet(getParameterProduction, "lnk_genaratedInternalREciptSmokeThirdBatch");
		
		/* PROD_E2E_003 */
		chk_allowInventoryWithoutCostingProductInformation = findElementInXLSheet(getParameterProduction, "chk_allowInventoryWithoutCostingProductInformation");
		lbl_creditValueJournelEntryStagingStandardCost = findElementInXLSheet(getParameterProduction, "lbl_creditValueJournelEntryStagingStandardCost");
		td_materialCostAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_materialCostAllocatedCostingSummaryBeforePurchase");
		td_material1CostAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_material1CostAllocatedCostingSummaryBeforePurchase");
		td_material2CostAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_material2CostAllocatedCostingSummaryBeforePurchase");
		td_material3CostAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_material3CostAllocatedCostingSummaryBeforePurchase");
		td_totalCostAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_totalCostAllocatedCostingSummaryBeforePurchase");
		td_unitCostAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_unitCostAllocatedCostingSummaryBeforePurchase");
		td_unitCostMateraialAllocatedCostingSummaryBeforePurchase = findElementInXLSheet(getParameterProduction, "td_unitCostMateraialAllocatedCostingSummaryBeforePurchase");
		btn_okActualDurationResourse01 = findElementInXLSheet(getParameterProduction, "btn_okActualDurationResourse01");
		lbl_creditValueJournelEntryQAStandardCost = findElementInXLSheet(getParameterProduction, "lbl_creditValueJournelEntryQAStandardCost");
		
	}

	public static void readData() throws Exception {
		
		siteURL = findElementInXLSheet(getDataProduction, "site url");
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-C6E8CJU")) {
			siteURL = findElementInXLSheet(getDataProduction, "siteUrlExternal");
		} else {
			siteURL = findElementInXLSheet(getDataProduction, "site url");
		}
		
		UserNameData = findElementInXLSheet(getDataProduction, "user name data");
		PasswordData = findElementInXLSheet(getDataProduction, "password data");
		automationUserEmail = findElementInXLSheet(getDataProduction, "automationUserEmail");
		automationUserPassword = findElementInXLSheet(getDataProduction, "automationUserPassword");
		
		/*Multi Platform*/
		siteUrlMultiPlatformm = findElementInXLSheet(getDataProduction, "siteUrlMultiPlatformm");
		usernameMultiPlatformm = findElementInXLSheet(getDataProduction, "usernameMultiPlatformm");
		passwordMultiPlatformm = findElementInXLSheet(getDataProduction, "passwordMultiPlatformm");
		
		descData = findElementInXLSheet(getDataProduction, "desc data");
		productGrpData = findElementInXLSheet(getDataProduction, "productGrp data");
		BOMQuantityData = findElementInXLSheet(getDataProduction, "BOMQuantity data");
		rawQuantityData = findElementInXLSheet(getDataProduction, "rawQuantity data");
		rawMaterialData = findElementInXLSheet(getDataProduction, "rawMaterial data");
		outputProductData = findElementInXLSheet(getDataProduction, "outputProduct data");
		BOOCodeData = findElementInXLSheet(getDataProduction, "BOOCode data");
		BOODescrData = findElementInXLSheet(getDataProduction, "BOODescr data");
		outputProductData = findElementInXLSheet(getDataProduction, "outputProduct data");
		ElementCategoryData = findElementInXLSheet(getDataProduction, "ElementCategory data");
		ElementDescData = findElementInXLSheet(getDataProduction, "ElementDesc data");
		
		ElementCategoryDataOperation = findElementInXLSheet(getDataProduction, "ElementCategoryOperation data");
		ElementDescDataOperation = findElementInXLSheet(getDataProduction, "ElementDescOperation data");
		GroupIdData = findElementInXLSheet(getDataProduction, "GroupId data");
		GroupIdData2 = findElementInXLSheet(getDataProduction, "GroupId2 data");
		GroupIdDataQC = findElementInXLSheet(getDataProduction, "GroupIdData QC");
		GroupIdData1 = findElementInXLSheet(getDataProduction, "GroupId1 data");
		GroupIdData4 = findElementInXLSheet(getDataProduction, "GroupId4 data");
		
		modelCodeData = findElementInXLSheet(getDataProduction, "modelCode data");
		productGroupData = findElementInXLSheet(getDataProduction, "productGroup data");
		ProductionUnitData = findElementInXLSheet(getDataProduction, "productionUnit data");
		
		batchQtyData = findElementInXLSheet(getDataProduction, "batchQty data");
		standardCostData = findElementInXLSheet(getDataProduction, "standardCost data");
		PricingProfileData = findElementInXLSheet(getDataProduction, "PricingProfile data");
		
		BOOOperationData = findElementInXLSheet(getDataProduction, "BOOOperation data");
		productData = findElementInXLSheet(getDataProduction, "product data");
		plannedqtyData = findElementInXLSheet(getDataProduction, "plannedQty data");
		
		productionOrderDescData = findElementInXLSheet(getDataProduction, "productionOrderDesc data");
		productionOrderModelData = findElementInXLSheet(getDataProduction, "productionOrderModel data");
		
		shippingAddressData = findElementInXLSheet(getDataProduction, "shippingAddress data");
		
		plannedQtyData = findElementInXLSheet(getDataProduction, "plannedQty data1");
		BatchNoData = findElementInXLSheet(getDataProduction, "BatchNo data");
		
		producedData = findElementInXLSheet(getDataProduction, "produced data");
		actualQtyData = findElementInXLSheet(getDataProduction, "actualQty data");
		
		productionUnitData = findElementInXLSheet(getDataProduction, "productionUnit data");
		
		locationCodeData = findElementInXLSheet(getDataProduction, "locationCode data");
		descriptionData = findElementInXLSheet(getDataProduction, "description data");
		inputwareData = findElementInXLSheet(getDataProduction, "inputware data");
		outputwareData = findElementInXLSheet(getDataProduction, "outputware data");
		WIPwareData = findElementInXLSheet(getDataProduction, "WIPware data");
		
		overheadData = findElementInXLSheet(getDataProduction, "overhead data");
		
		pricingProfileCodeData = findElementInXLSheet(getDataProduction, "pricingProfileCode data");
		descPricingProfileData = findElementInXLSheet(getDataProduction, "descPricingProfile data");
		
		rateData = findElementInXLSheet(getDataProduction, "rate data");
		
		overheadCodeData = findElementInXLSheet(getDataProduction, "overheadCode data");
		overheadDescData = findElementInXLSheet(getDataProduction, "overheadDesc data");
		
		productionOrderGroupData = findElementInXLSheet(getDataProduction, "productionOrderGroup data");
		
		productionUnitBulkData = findElementInXLSheet(getDataProduction, "productionUnitBulk data");
		productSearchData = findElementInXLSheet(getDataProduction, "productSearch Data");
		productModel = findElementInXLSheet(getDataProduction, "productModel data");
		
		titleData = findElementInXLSheet(getDataProduction, "title data");
		requesterData = findElementInXLSheet(getDataProduction, "requester data");
		
		additionalResourcesData = findElementInXLSheet(getDataProduction, "additionalResources Data");
		
		plannedqtyBulkData = findElementInXLSheet(getDataProduction, "plannedqtyBulk Data");
		shippingData = findElementInXLSheet(getDataProduction, "shipping data");
		lotNo = findElementInXLSheet(getDataProduction, "lotNo data");
		
		bulkOrderData = findElementInXLSheet(getDataProduction, "bulkOrder data");
		standardCostData1 = findElementInXLSheet(getDataProduction, "standardCost Data1");
		searchProductData = findElementInXLSheet(getDataProduction, "searchProduct Data");
		plannedqtyData1 = findElementInXLSheet(getDataProduction, "plannedqty Data1");
		
		actualQtyData1 = findElementInXLSheet(getDataProduction, "actualQty Data1");
		modelData = findElementInXLSheet(getDataProduction, "model data");
		
		batchData = findElementInXLSheet(getDataProduction, "batch data");
		GroupIdDatafinal = findElementInXLSheet(getDataProduction, "GroupId Datafinal");
		
		productCodeData = findElementInXLSheet(getDataProduction, "productCode Data");
		basePriceData = findElementInXLSheet(getDataProduction, "basePrice Data");
		manufacturerData = findElementInXLSheet(getDataProduction, "manufacturer Data");
		lengthData = findElementInXLSheet(getDataProduction, "length Data");
		widthData = findElementInXLSheet(getDataProduction, "width Data");
		heightData = findElementInXLSheet(getDataProduction, "height Data");
		volumeData = findElementInXLSheet(getDataProduction, "volume Data");
		

		/* QA Form level action verification */
		
		previousBOMData = findElementInXLSheet(getDataProduction, "previousBOM Data");
		reasonData = findElementInXLSheet(getDataProduction, "reason Data");
		reasonData1 = findElementInXLSheet(getDataProduction, "reason Data1");
		BOOCodeData1 = findElementInXLSheet(getDataProduction, "BOO Code1");
		subjectData = findElementInXLSheet(getDataProduction, "subject Data");
		AssignToData = findElementInXLSheet(getDataProduction, "Assign ToData");
		modelCodeData1 = findElementInXLSheet(getDataProduction, "modelCode Data1");
		modelCode = findElementInXLSheet(getDataProduction, "modelCode");
		plannedqtyDataver = findElementInXLSheet(getDataProduction, "plannedqtyDataver");
		productionOrderData = findElementInXLSheet(getDataProduction, "productionOrderData");
		prodNo = findElementInXLSheet(getDataProduction, "prodNo");
		previousbulkOrderData = findElementInXLSheet(getDataProduction, "previousbulkOrderData");
		costEstDescription = findElementInXLSheet(getDataProduction, "costEstDescription");
		costProductData = findElementInXLSheet(getDataProduction, "costProductData");
		costEstQuantityData = findElementInXLSheet(getDataProduction, "costEstQuantityData");
		pricingProfileCostData = findElementInXLSheet(getDataProduction, "pricingProfileCostData");
		
		/*PROD_PM_017to024*/
		commonDescription = findElementInXLSheet(getDataProduction, "commonDescription");
		productGroupRegression = findElementInXLSheet(getDataProduction, "productGroupRegression");
		productionTypeProduct = findElementInXLSheet(getDataProduction, "productionTypeProduct");
		productionTypeProductWithDescription = findElementInXLSheet(getDataProduction, "productionTypeProductWithDescription");
		productionUnit = findElementInXLSheet(getDataProduction, "productionUnit");
		inputWarehouseAutoSelectedPM = findElementInXLSheet(getDataProduction, "inputWarehouseAutoSelectedPM");
		outputWarehouseAutoSelectedPM = findElementInXLSheet(getDataProduction, "outputWarehouseAutoSelectedPM");
		wipWarehouseAutoSelectedPM = findElementInXLSheet(getDataProduction, "wipWarehouseAutoSelectedPM");
		
		/* PROD_PM_026 */
		barcodeBookPM = findElementInXLSheet(getDataProduction, "barcodeBookPM");
		
		/* PROD_PM_027 */
		costicPriorityNormalPM = findElementInXLSheet(getDataProduction, "costicPriorityNormalPM");
		
		/* PROD_PM_028 */
		pricingProfilePM = findElementInXLSheet(getDataProduction, "pricingProfilePM");
		
		/* PROD_PM_030 */
		bomPM = findElementInXLSheet(getDataProduction, "bomPM");
		
		/* PROD_PM_035 */
		booPM = findElementInXLSheet(getDataProduction, "booPM");
		
		/* PROD_PM_036 */
		descriptionBOOPM = findElementInXLSheet(getDataProduction, "descriptionBOOPM");
		batchQuantityPM = findElementInXLSheet(getDataProduction, "batchQuantityPM");
		
		/* PROD_PM_041 */
		processTimeTypePM = findElementInXLSheet(getDataProduction, "processTimeTypePM");
		
		/* PROD_PM_052 */
		byProductPM = findElementInXLSheet(getDataProduction, "byProductPM");
		byProductsQtyPM = findElementInXLSheet(getDataProduction, "byProductsQtyPM");
		byProductsLeadDaysPM = findElementInXLSheet(getDataProduction, "byProductsLeadDaysPM");
		
		/* PROD_PM_053_054 */
		serviceProductBOOPM = findElementInXLSheet(getDataProduction, "serviceProductBOOPM");
		batchSpecificProductBOOPM = findElementInXLSheet(getDataProduction, "batchSpecificProductBOOPM");
		estimatedAmountServiceProductServiceProductTabBOOPM = findElementInXLSheet(getDataProduction, "estimatedAmountServiceProductServiceProductTabBOOPM");
		leadDaysServiceProductServiceProductTabBOOPM = findElementInXLSheet(getDataProduction, "leadDaysServiceProductServiceProductTabBOOPM");
		
		/* PROD_PM_55 */
		resourseRegressionPM = findElementInXLSheet(getDataProduction, "resourseRegressionPM");
		
		/* PROD_PM_57 */
		descriptionStepsTabPM = findElementInXLSheet(getDataProduction, "descriptionStepsTabPM");
		
		/* PROD_PM_067 */
		minimumLotSizePMRegression = findElementInXLSheet(getDataProduction, "minimumLotSizePMRegression");
		maximumLotSizePMRegression = findElementInXLSheet(getDataProduction, "maximumLotSizePMRegression");
		productionScrapPMRegression = findElementInXLSheet(getDataProduction, "productionScrapPMRegression");
		duplicatedProductOnProductionModelRegression = findElementInXLSheet(getDataProduction, "duplicatedProductOnProductionModelRegression");
		
		/* PROD_PM_069 */
		editedMinimumLotSizePMRegression = findElementInXLSheet(getDataProduction, "editedMinimumLotSizePMRegression");
		editedMaximumLotSizePMRegression = findElementInXLSheet(getDataProduction, "editedMaximumLotSizePMRegression");
		editedBatchQuantityPMRegression = findElementInXLSheet(getDataProduction, "editedBatchQuantityPMRegression");
		editedDescriptionPMRegression = findElementInXLSheet(getDataProduction, "editedDescriptionPMRegression");
		
		/* Production Order */
		/* PROD_PO_011 */
		selectedProductFrontPageRegressionPO = findElementInXLSheet(getDataProduction, "selectedProductFrontPageRegressionPO");

		/*Pre-Requisites PO Regression*/
		urlReleasedPORegression = findElementInXLSheet(getDataProduction, "urlReleasedPORegression");
		
		/* PROD_PO_014 */
		prductionUnitAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "prductionUnitAaccordigToPMRegression");
		inputWarehouseAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "inputWarehouseAaccordigToPMRegression");
		outputWarehouseAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "outputWarehouseAaccordigToPMRegression");
		wipWarehouseAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "wipWarehouseAaccordigToPMRegression");
		siteAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "siteAaccordigToPMRegression");
		costingPriorityAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "costingPriorityAaccordigToPMRegression");
		bomVarientAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "bomVarientAaccordigToPMRegression");
		varientDescriptionAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "varientDescriptionAaccordigToPMRegression");
		booNoAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "booNoAaccordigToPMRegression");
		booDescriptionAaccordigToPMRegression = findElementInXLSheet(getDataProduction, "booDescriptionAaccordigToPMRegression");
		
		/* PROD_PO_015 */
		productionOrderPriorityRegression = findElementInXLSheet(getDataProduction, "productionOrderPriorityRegression");
		
		/* PROD_PO_016 */
		planedQtyPORegression = findElementInXLSheet(getDataProduction, "planedQtyPORegression");
		
		/* PROD_PO_024 */
		productionOrderGroupRegression = findElementInXLSheet(getDataProduction, "productionOrderGroupRegression");
		productionOrderDescriptionRegression = findElementInXLSheet(getDataProduction, "productionOrderDescriptionRegression");
		
		/* PROD_PO_025 */
		barcodeBookPORegression = findElementInXLSheet(getDataProduction, "barcodeBookPORegression");
		productionUnitRegressionPODupliacted = findElementInXLSheet(getDataProduction, "productionUnitRegressionPODupliacted");
		inputWarehouseRegressionPODupliacted = findElementInXLSheet(getDataProduction, "inputWarehouseRegressionPODupliacted");
		outputWarehouseRegressionPODupliacted = findElementInXLSheet(getDataProduction, "outputWarehouseRegressionPODupliacted");
		wipWarehouseRegressionPODupliacted = findElementInXLSheet(getDataProduction, "wipWarehouseRegressionPODupliacted");
		siteRegressionPODupliacted = findElementInXLSheet(getDataProduction, "siteRegressionPODupliacted");
		costingPriorityRegressionPODupliacted = findElementInXLSheet(getDataProduction, "costingPriorityRegressionPODupliacted");
		booRegressionPODupliacted = findElementInXLSheet(getDataProduction, "booRegressionPODupliacted");
		descriptionDuplicatedPORegression = findElementInXLSheet(getDataProduction, "descriptionDuplicatedPORegression");
		priorityDuplicatedPORegression = findElementInXLSheet(getDataProduction, "priorityDuplicatedPORegression");

		/* PROD_PO_026 */
		descriptionEditedPORegression = findElementInXLSheet(getDataProduction, "descriptionEditedPORegression");
		priorityEditedPORegression = findElementInXLSheet(getDataProduction, "priorityEditedPORegression");
		planedQtyEditedPORegression = findElementInXLSheet(getDataProduction, "planedQtyEditedPORegression");

		/* PROD_PO_050 */
		productionUnitRegressionPOCopyFrom = findElementInXLSheet(getDataProduction, "productionUnitRegressionPOCopyFrom");
		inputWarehouseRegressionPOCopyFrom = findElementInXLSheet(getDataProduction, "inputWarehouseRegressionPOCopyFrom");
		outputWarehouseRegressionPOCopyFrom = findElementInXLSheet(getDataProduction, "outputWarehouseRegressionPOCopyFrom");
		wipWarehouseRegressionPOCopyFrom = findElementInXLSheet(getDataProduction, "wipWarehouseRegressionPOCopyFrom");
		siteRegressionPOCopyFrom = findElementInXLSheet(getDataProduction, "siteRegressionPOCopyFrom");
		costingPriorityRegressionPOCopyFrom = findElementInXLSheet(getDataProduction, "costingPriorityRegressionPOCopyFrom");
		descriptionCopyFromPORegression = findElementInXLSheet(getDataProduction, "descriptionCopyFromPORegression");
		priorityCopyFromPORegression = findElementInXLSheet(getDataProduction, "priorityCopyFromPORegression");
		
		/* Production Multi Platform Scenarios */
		/* PROD_E2E_001 */
		descriptionBOM = findElementInXLSheet(getDataProduction, "descriptionBOM");
		productionGroupE2E = findElementInXLSheet(getDataProduction, "productionGroupE2E");
		menfacTypeProduction = findElementInXLSheet(getDataProduction, "menfacTypeProduction");
		productDesriptionProductInfo = findElementInXLSheet(getDataProduction, "productDesriptionProductInfo");
		hundred = findElementInXLSheet(getDataProduction, "hundred");
		rowMaterialQtyOne = findElementInXLSheet(getDataProduction, "rowMaterialQtyOne");
		rowMaterialQtyTwo = findElementInXLSheet(getDataProduction, "rowMaterialQtyTwo");
		rowMaterialQtyThree = findElementInXLSheet(getDataProduction, "rowMaterialQtyThree");
		scrapRowMaterilThree = findElementInXLSheet(getDataProduction, "scrapRowMaterilThree");
		overheadDescription = findElementInXLSheet(getDataProduction, "overheadDescription");
		rateTypeOverhead = findElementInXLSheet(getDataProduction, "rateTypeOverhead");
		resourseDescription = findElementInXLSheet(getDataProduction, "resourseDescription");
		employeeResourseCreation = findElementInXLSheet(getDataProduction, "employeeResourseCreation");
		resourseOneHourlyRate = findElementInXLSheet(getDataProduction, "resourseOneHourlyRate");
		hourlyRateOfOverheadeAttachedToResourseOne = findElementInXLSheet(getDataProduction, "hourlyRateOfOverheadeAttachedToResourseOne");
		hourlyRateOfOverheadeAttachedToResourseTwo = findElementInXLSheet(getDataProduction, "hourlyRateOfOverheadeAttachedToResourseTwo");
		hourlyRateOfOverheadeTwoAttachedToResourseTwo = findElementInXLSheet(getDataProduction, "hourlyRateOfOverheadeTwoAttachedToResourseTwo");
		elementCategorySupply = findElementInXLSheet(getDataProduction, "elementCategorySupply");
		activityTypeMoveStock = findElementInXLSheet(getDataProduction, "activityTypeMoveStock");
		elementCategoryOperation = findElementInXLSheet(getDataProduction, "elementCategoryOperation");
		activityTypeProduce = findElementInXLSheet(getDataProduction, "activityTypeProduce");
		produceElementDescription1 = findElementInXLSheet(getDataProduction, "produceElementDescription1");
		groupIdBOOOne = findElementInXLSheet(getDataProduction, "groupIdBOOOne");
		batchMethodTransferBOO = findElementInXLSheet(getDataProduction, "batchMethodTransferBOO");
		produceElementDescription2 = findElementInXLSheet(getDataProduction, "produceElementDescription2");
		groupIdBOOTwo = findElementInXLSheet(getDataProduction, "groupIdBOOTwo");
		activityTypeQualityCheck = findElementInXLSheet(getDataProduction, "activityTypeQualityCheck");
		qcElementDescription = findElementInXLSheet(getDataProduction, "qcElementDescription");
		groupIdBOOThree = findElementInXLSheet(getDataProduction, "groupIdBOOThree");
		moveStockElementDescription = findElementInXLSheet(getDataProduction, "moveStockElementDescription");
		inputWarehouseLI = findElementInXLSheet(getDataProduction, "inputWarehouseLI");
		outputWarehouseLI = findElementInXLSheet(getDataProduction, "outputWarehouseLI");
		wipWarehouseLI = findElementInXLSheet(getDataProduction, "wipWarehouseLI");
		locationDescription = findElementInXLSheet(getDataProduction, "locationDescription");
		locationAddress = findElementInXLSheet(getDataProduction, "locationAddress");
		materialEstimationTabPP = findElementInXLSheet(getDataProduction, "materialEstimationTabPP");
		labourResorceEstimatioTabPP = findElementInXLSheet(getDataProduction, "labourResorceEstimatioTabPP");
		overheadEstimatioTabPP = findElementInXLSheet(getDataProduction, "overheadEstimatioTabPP");
		serviceEstimatioTabPP = findElementInXLSheet(getDataProduction, "serviceEstimatioTabPP");
		expenceEstimatioTabPP = findElementInXLSheet(getDataProduction, "expenceEstimatioTabPP");
		materialActualTabPP = findElementInXLSheet(getDataProduction, "materialActualTabPP");
		labourResourseActualTabPP = findElementInXLSheet(getDataProduction, "labourResourseActualTabPP");
		overheadActualTabPP = findElementInXLSheet(getDataProduction, "overheadActualTabPP");
		serviceActualTabPP = findElementInXLSheet(getDataProduction, "serviceActualTabPP");
		expenceActualTabPP = findElementInXLSheet(getDataProduction, "expenceActualTabPP");
		descriptionPP = findElementInXLSheet(getDataProduction, "descriptionPP");
		pricingProfileGroup = findElementInXLSheet(getDataProduction, "pricingProfileGroup");
		
		/*Production Model*/
		batchQuantityE2E = findElementInXLSheet(getDataProduction, "batchQuantityE2E");

		/*Production Order*/
		planedQtyE2E = findElementInXLSheet(getDataProduction, "planedQtyE2E");
		
		/*MRP Run*/
		shippingAddress = findElementInXLSheet(getDataProduction, "shippingAddress");

		/* Purchase Order */
		qtyProductPurchaseOrder = findElementInXLSheet(getDataProduction, "qtyProductPurchaseOrder");
		unitPriceProduct1PurchaseOrder = findElementInXLSheet(getDataProduction, "unitPriceProduct1PurchaseOrder");
		unitPriceProduct2PurchaseOrder = findElementInXLSheet(getDataProduction, "unitPriceProduct2PurchaseOrder");
		unitPriceProduct3PurchaseOrder = findElementInXLSheet(getDataProduction, "unitPriceProduct3PurchaseOrder");
		billingAddressPO = findElementInXLSheet(getDataProduction, "billingAddressPO");
		
		/* Shop Floor Update */
		actualQuantityOutputProductFirstOperation = findElementInXLSheet(getDataProduction, "actualQuantityOutputProductFirstOperation");
		actualQuantityInputProductFirstOperation = findElementInXLSheet(getDataProduction, "actualQuantityInputProductFirstOperation");
		actualQuantityOutputProductSecondOperation = findElementInXLSheet(getDataProduction, "actualQuantityOutputProductSecondOperation");
		actualQuantityInputProductSecondOperation = findElementInXLSheet(getDataProduction, "actualQuantityInputProductSecondOperation");
		actualQuantity2InputProductFirstOperation = findElementInXLSheet(getDataProduction, "actualQuantity2InputProductFirstOperation");

		/* QC */
		qcTypeProductionOrder = findElementInXLSheet(getDataProduction, "qcTypeProductionOrder");
		firstFailQty = findElementInXLSheet(getDataProduction, "firstFailQty");
		secondFailQty = findElementInXLSheet(getDataProduction, "secondFailQty");
		thirdFailQty = findElementInXLSheet(getDataProduction, "thirdFailQty");
		thirdPassQty = findElementInXLSheet(getDataProduction, "thirdPassQty");
		
		/* Production Costing */
		actualUpdateQuantityProductionQuntity = findElementInXLSheet(getDataProduction, "actualUpdateQuantityProductionQuntity");

		/* PROD_E2E_002 */
		titleInternelOrder = findElementInXLSheet(getDataProduction, "titleInternelOrder");
		stepOnePO = findElementInXLSheet(getDataProduction, "stepOnePO");
		stepTwoPO = findElementInXLSheet(getDataProduction, "stepTwoPO");
		internalOrderQtyWIPRequest = findElementInXLSheet(getDataProduction, "internalOrderQtyWIPRequest");
		
		/* PROD_E2E_003 */
		standardCostPM = findElementInXLSheet(getDataProduction, "standardCostPM");
		
	}
	
}
