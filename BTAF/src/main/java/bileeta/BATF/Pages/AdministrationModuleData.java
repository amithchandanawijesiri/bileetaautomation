package bileeta.BATF.Pages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import bileeta.BTAF.Utilities.TestBase;

public class AdministrationModuleData extends TestBase
{
	/* Reading the element locators to variables */
	
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	protected static String btn_navigationmenu;
    protected static String sidemenu;
	protected static String btn_administration;
	protected static String subsidemenu;
	protected static String btn_userinformation;
    protected static String userinformationpage;
    //create new user
	protected static String btn_newuser;
	protected static String newuserpage;
	protected static String cbox;
	protected static String fullname;
	protected static String displaynamefield;
	protected static String emailnamefield;
	protected static String userlevelfield;
	protected static String usertypefield;
	//draft
	protected static String btn_draft;
	protected static String draftpage;
	//edit
    protected static String btn_edit;
	protected static String editpage;
	protected static String UserLevelField2;
    protected static String btn_update;
	protected static String updatepage;
	//user permission
	protected static String grantedbalancinglevel;
	protected static String btn_addpermission;
	protected static String userpermissionpage1;
	protected static String balancinglevel;
	protected static String btn_form;
	protected static String formtab;
    protected static String modulename;
    protected static String btn_AllowTemplatetoall;
    protected static String btn_AllowPrinttoall;
	protected static String btn_AllowReversetoall;
    protected static String btn_AllowAllReports;
    protected static String btn_AllowReleasetoall;
	protected static String btn_update2;
	protected static String btn_update3;
	//protected static String userpermissionpage2;
	//release
	protected static String btn_release;
	protected static String releasedpage;
	//change password
	protected static String btn_configuration;
	protected static String btn_changepassword;
	protected static String changepasswordpopup;
	protected static String currentpassword;
	protected static String newpassword;
	protected static String confirmnewpassword;
	protected static String btn_change;
	protected static String newhomepage;
    //search a user
	protected static String searchuser;
	protected static String template;
	//protected static String btn_search;
	protected static String username;
	
	//forgot password
	protected static String btn_forgotpassword;
	protected static String resetpasswordpage;
	protected static String emailaddress;
	protected static String btn_next;
	protected static String mailURL;
	protected static String maillogo;
	protected static String mailid;
	protected static String btn_next1;
	protected static String mailpassword;
	protected static String mailpasswordpopup;
	protected static String btn_next2;
	protected static String mailpage;
	protected static String btn_refresh;
	
	//terminate a user,inactive user
	protected static String btn_user;
	protected static String btn_edit1;
	protected static String btn_active;
	protected static String status;
	protected static String btn_apply;
	protected static String btn_apply2;
	protected static String btn_update4;
	protected static String btn_changeperiod;
	protected static String btn_from;
	protected static String btn_fromdate;
	protected static String btn_to;
	protected static String btn_todate;
	protected static String calander;
	protected static String Assignuserpopup;
	protected static String btn_editblock;
	protected static String activeuserpage;
	protected static String btn_changeperiod1;
	protected static String btn_editactive;
	
	//Print tem
	protected static String btn_printtemplatesetup;
	protected static String printtemplatesetuppage;
	protected static String btn_print;
	protected static String selectbalancinglevel;
	protected static String btn_new;
	protected static String addnewtemplatepage;
	protected static String templatename;
	protected static String description;
	protected static String form;
	protected static String datasourse;
	protected static String emailtemplate;
	protected static String btn_update5;
	protected static String  Printtemplate;
	
	//report
	protected static String btn_report;
	protected static String datasourse1;
	protected static String btn_user1;
	protected static String btn_username2; 
	//work flow
	protected static String btn_workflowconfiguration;
	protected static String workflowconfigurationpage;
	protected static String btn_newworkflow;
	protected static String newworkflowpage;
	protected static String workflowname;
	protected static String formname;
	protected static String journeyname1;
	protected static String action;
	protected static String fromaction;
	protected static String btn_create;
	protected static String workflowconfignewpage;
	protected static String btn_sendmail;
	protected static String btn_udatetosendmail;
	protected static String  workflowupdatepage;
	protected static String  btn_activeflow;
	protected static String  confirmationpopup;
	protected static String searchuser01;
	
	
		
	
	//schedule tem
	protected static String btn_scheduletemplate;
	protected static String scheduletemplatepage;
	protected static String btn_newscheduletemplate;
	protected static String newscheduletemplatepage;
	protected static String templatecode;
	protected static String templatedescription;
	protected static String btn_Recurringseriesoftask;
	protected static String btn_daily;
	protected static String draftscheduletemplatepage;
	protected static String btn_releasest;
	protected static String releasesecheduletemplatepage;
	// scduletem task
	protected static String btn_entution;
	protected static String btn_rolecentre;
	protected static String rolecentrepage;
	protected static String btn_ScheduledJobs;
	protected static String ScheduledJobspage;
	protected static String btn_expand;
	protected static String btn_selectall;
	protected static String btn_run;
	protected static String message;
	
	//journey comfofiguration
	protected static String btn_journeyconfiguration;
	protected static String journeyconfigurationpage;
	protected static String btn_WIPrequest;
	protected static String WIPrequestpage;
	protected static String stockreservation;
	protected static String btn_update6;
	protected static String journeyupdatepage;
	
	//superuser can send resetpassword link
	protected static String btn_action;
	protected static String btn_resetpassword;
	protected static String resetpasswordpopup;
	protected static String btn_yes;
	
	//user can activate substitute rule
	protected static String btn_maintainsubstitue;
	protected static String vactionsubstituepopup;
	protected static String btn_isvaction;
	protected static String btn_search;
	protected static String assignuserpopup;
	protected static String username3;
	protected static String btn_startdate;
	protected static String btn_selectastartdate;
	protected static String btn_enddate;
	protected static String btn_selectenddate;
	protected static String btnapply;
	protected static String btn_usernow;
	protected static String btn_applynow;
	
	//search user workflow
	protected static String searchuserworkflow;
	protected static String btn_workflownameapper;
	protected static String workflowinformationpage;
	protected static String btn_editworkflow;
	protected static String btn_start;
	protected static String btn_newapprovalprocess;
	protected static String addeditapprovalprocesspage;
	protected static String displayname;
	protected static String noofapprovals;
	protected static String btn_sendmail2;  
	protected static String btn_lookup;
	protected static String btn_activeyes;
	
	
	protected static String btn_service;
	protected static String btn_case;
	protected static String  casepage;
	
	protected static String  UserNameDatatask;
	protected static String PasswordDatatask;
	protected static String btn_task1;
	protected static String taskpage;
	protected static String btn_Approval;
	protected static String approvalpage;
	protected static String  lnk_home2;
	protected static String  btn_arrow;
	protected static String approvalrequestpage;
	protected static String remark;
	protected static String btn_ok;
	protected static String btn_activeblock;
	
	protected static String tasktemplate;
	protected static String txt_taskTemplate;
	protected static String task;
	protected static String searchtask;
	protected static String updatedUserinformationPage;
	protected static String  TerminateStatusValue;
	
	//ActionCurrencyInformation
	protected static String  btn_CurrencyInformation;
	protected static String  CurrencyInformationPage;
	protected static String ticktoactivate;
	protected static String  btn_updateCurrencyInformationpage;
	protected static String btn_closevactionsubstitute;
	protected static String btn_UserPermission;
	protected static String  UserPermissionPage;
	protected static String btn_searchuser;
	protected static String user;
	protected static String btn_Report;
	protected static String  UserReportPage;
	protected static String btn_Addresstype;
	protected static String  AddressTypePage;
	protected static String  btn_IntegrationServiceSetup;
	protected static String IntegrationServiceSetupPage;
	protected static String  btn_editpencil;
	protected static String  btn_IntegrationServiceSetupupdate;
	protected static String  btn_PermissionGroup;
	protected static String  btn_NewPermissionGroup;
	protected static String PermissionGroupPage;
	protected static String GroupCode;
	protected static String GroupName;
	protected static String btn_searchuserPermissionGroup;
	protected static String  DraftedPermissionGroupPage;
	protected static String  btn_updateandnew;
	protected static String  btn_copyfrom;
	protected static String btn_draftandnew;
	protected static String Newscheduletemplatepage;
	protected static String  btn_SystemPolicyConfiguration;
	protected static String  SystemPolicyConfigurationSetuppage;
	protected static String  btn_viewpost;
	protected static String UpdatedSystemPolicyConfigurationSetuppage;
	protected static String btn_updatePolicyConfigurationSetuppage;
	protected static String ProductCodeName;
	protected static String btn_BalancingLevelSettings;
	protected static String BalancingLevelSettingPage;
	protected static String ProductCode;
	protected static String btn_productInformationupdate;
	protected static String  btn_PasswordPolicies;
	protected static String   PasswordPoliciesPage;
	protected static String btn_updatePasswordPolicies;
	protected static String btn_SharingGroup;
	protected static String SharingGroupPage;
	protected static String btn_TermsandConditions;
	protected static String TermsandConditionsPage;
	protected static String  btn_NewTermsandCondition;
	protected static String NewTermsandConditionsPage;
	protected static String BalancingLevel;
	protected static String code;
	protected static String DescriptionCode;
	protected static String  btn_UpdateandNew;


	
	//////////////////////
	/* Reading the Data to variables */
	
	protected static String UserNameData;
	protected static String PasswordData;
	protected static String txt_cbox;
	protected static String txt_FullName;
	protected static String txt_DisplayNameField;
	protected static String txt_EmailNameField;
	protected static String txt_UserLevelField;
	protected static String txt_UserTypeField;
	protected static String txt_BalancingLevel;
	protected static String txt_ModuleName;
	//edit a user
	protected static String txt_UserLevelField2;
	//change password
	protected static String txt_CurrentPassword;
	protected static String txt_NewPassword;
	protected static String txt_ConfirmNewPassword ;
	protected static String txt_SearchUser;
	protected static String txt_Template;
	//forgot password
	protected static String txt_EmailAddress;
	protected static String txt_mailpassword;
	//terminate user
	protected static String txt_status;
	//inactive user
	protected static String txt_status2;
	//block a user
	protected static String txt_status3;
	//active a blocked user
	protected static String txt_status4;
	
	//activate a inactive user
	protected static String txt_status5;
	
    //Terminate a inactive user
	protected static String txt_Templatestatus;
	protected static String txt_SearchUserstatus;
	protected static String txt_status6;
	//print template
	protected static String txt_selectbalancinglevel;
	protected static String txt_templatename;
	protected static String txt_description;
	protected static String txt_form;
	protected static String txt_datasourse;
	protected static String txt_emailtemplate;
	// report
	protected static String txt_datasourse1;
	
	//work flow
	protected static String txt_workflowname;
	protected static String txt_formname;
	protected static String txt_journeyname;
	protected static String txt_action;
	protected static String txt_fromaction;
	//schedule template
	protected static String txt_templatecode;
	protected static String txt_templatedescription;
	//journey config
	protected static String txt_stockreservation;
	//
	protected static String txt_searchuser01;
	//searchuser workflow
	protected static String txt_searchuserworkflow;
	protected static String txt_displayname;
	protected static String txt_noofapprovals;
	//search user for sustitute
	protected static String txt_SearchUser3;
	
	protected static String txt_EmailAddressvalidate;
	protected static String txt_journeyname1;
	protected static String txt_SearchUsercase;
	//aprove task
	protected static String txt_usernametask;
	protected static String txt_passwordtask;
	protected static String txt_remark;
	protected static String txt_searchtask;
	
	//
	protected static String txt_SearchUserstatusnew;
	protected static String txt_Groupcode;
	protected static String  txt_GroupName;
	protected static String  txt_GroupName2;
	protected static String  txt_code;
	protected static String  txt_DescriptionCode;
	//ActionCurrencyInformation

	
	 
	
	public static void readElementlocators() throws Exception
	{
		
		siteLogo= findElementInXLSheet(getParameterAdministration,"siteLogo"); 
		txt_username= findElementInXLSheet(getParameterAdministration,"user name"); 
		txt_password= findElementInXLSheet(getParameterAdministration,"password"); 
		btn_login= findElementInXLSheet(getParameterAdministration,"login button"); 
		lnk_home=findElementInXLSheet(getParameterAdministration,"lnk_home");
		//admin page
		btn_navigationmenu= findElementInXLSheet(getParameterAdministration, "btn_navigation");
		sidemenu= findElementInXLSheet(getParameterAdministration, "sidemenu");
	    btn_administration= findElementInXLSheet(getParameterAdministration, "btn_administration");
	    subsidemenu= findElementInXLSheet(getParameterAdministration, "subsidemenu");
	    btn_userinformation=findElementInXLSheet(getParameterAdministration, "btn_userinformation");
	    userinformationpage=findElementInXLSheet(getParameterAdministration, "userinformationpage");
	    //create new user
	    btn_newuser=findElementInXLSheet(getParameterAdministration, "btn_newuser");
	    newuserpage=findElementInXLSheet(getParameterAdministration, "newuserpage");
		cbox=findElementInXLSheet(getParameterAdministration, "cbox");
		fullname=findElementInXLSheet(getParameterAdministration, "fullname");
		displaynamefield=findElementInXLSheet(getParameterAdministration, "displaynamefield");
		emailnamefield=findElementInXLSheet(getParameterAdministration, "emailnamefield");
		userlevelfield=findElementInXLSheet(getParameterAdministration, "userlevelfield");
		usertypefield=findElementInXLSheet(getParameterAdministration, "usertypefield");
		//draft
		btn_draft=findElementInXLSheet(getParameterAdministration, "btn_draft");
		draftpage=findElementInXLSheet(getParameterAdministration, "draftpage");
	   //edit
		
		btn_edit=findElementInXLSheet(getParameterAdministration, "btn_edit");
	    editpage=findElementInXLSheet(getParameterAdministration, "editpage");
	    UserLevelField2=findElementInXLSheet(getParameterAdministration,"UserLevelField2");
	    //update
	    btn_update=findElementInXLSheet(getParameterAdministration, "btn_update");
		updatepage=findElementInXLSheet(getParameterAdministration, "updatepage");
		//permission
	    //grantedbalancinglevel=findElementInXLSheet(getParameterAdministration, "grantedbalancinglevel");
	 	btn_addpermission=findElementInXLSheet(getParameterAdministration, "btn_addpermission");
	 	userpermissionpage1=findElementInXLSheet(getParameterAdministration, "userpermissionpage1");
		//fill form
	    balancinglevel=findElementInXLSheet(getParameterAdministration, "balancinglevel");
		btn_form=findElementInXLSheet(getParameterAdministration, "btn_form");
		formtab=findElementInXLSheet(getParameterAdministration, "formtab");
		modulename=findElementInXLSheet(getParameterAdministration, "modulename");
		// click btn
		btn_AllowTemplatetoall=findElementInXLSheet(getParameterAdministration, "btn_AllowTemplatetoall");
		btn_AllowPrinttoall=findElementInXLSheet(getParameterAdministration,"btn_AllowPrinttoall");
		btn_AllowReversetoall=findElementInXLSheet(getParameterAdministration, "btn_AllowReversetoall");
		btn_AllowAllReports=findElementInXLSheet(getParameterAdministration, "btn_AllowAllReports");
		btn_AllowReleasetoall=findElementInXLSheet(getParameterAdministration, "btn_AllowReleasetoall");
		
		btn_update2=findElementInXLSheet(getParameterAdministration, "btn_update2");
		btn_update3=findElementInXLSheet(getParameterAdministration, "btn_update3");
//userpermissionpage=findElementInXLSheet(getParameterAdministration, "userpermissionpage");
		btn_release=findElementInXLSheet(getParameterAdministration, "btn_release");
		releasedpage=findElementInXLSheet(getParameterAdministration, "releasedpage");
		//change password
		btn_configuration=findElementInXLSheet(getParameterAdministration, "btn_configuration");
		btn_changepassword=findElementInXLSheet(getParameterAdministration, "btn_changepassword"); 
		changepasswordpopup=findElementInXLSheet(getParameterAdministration, "changepasswordpopup"); 
		currentpassword=findElementInXLSheet(getParameterAdministration, "currentpassword"); 
		newpassword=findElementInXLSheet(getParameterAdministration, "newpassword"); 
		confirmnewpassword=findElementInXLSheet(getParameterAdministration, "confirmnewpassword"); 
		btn_change=findElementInXLSheet(getParameterAdministration, "btn_change"); 
		newhomepage=findElementInXLSheet(getParameterAdministration, "newhomepage"); 
		//search user
		searchuser=findElementInXLSheet(getParameterAdministration, "searchuser"); 
		template=findElementInXLSheet(getParameterAdministration, "template"); 
	    //btn_search=findElementInXLSheet(getParameterAdministration, "btn_search"); 
	    username=findElementInXLSheet(getParameterAdministration, "username"); 
	    
	    //forgot password
	    btn_forgotpassword=findElementInXLSheet(getParameterAdministration, "btn_forgotpassword"); 
	    resetpasswordpage=findElementInXLSheet(getParameterAdministration, "resetpasswordpage"); 
	    emailaddress=findElementInXLSheet(getParameterAdministration, "emailaddress");
	    btn_next=findElementInXLSheet(getParameterAdministration, "btn_next");
	    mailURL=findElementInXLSheet(getParameterAdministration, "mailURL");
	    maillogo=findElementInXLSheet(getParameterAdministration, "maillogo");
	    mailid=findElementInXLSheet(getParameterAdministration, "mailid");
	    btn_next1=findElementInXLSheet(getParameterAdministration, "btn_next1");
	    mailpassword=findElementInXLSheet(getParameterAdministration,"mailpassword1");
	    mailpasswordpopup=findElementInXLSheet(getParameterAdministration,"mailpasswordpopup");
	    btn_next2=findElementInXLSheet(getParameterAdministration,"btn_next2");
	    mailpage=findElementInXLSheet(getParameterAdministration,"mailpage");
	    btn_refresh=findElementInXLSheet(getParameterAdministration,"btn_refresh");

	    
	    //terminate a user
	    
	    btn_user=findElementInXLSheet(getParameterAdministration, "btn_user");
	    btn_edit1=findElementInXLSheet(getParameterAdministration, "btn_edit1");
	    btn_active=findElementInXLSheet(getParameterAdministration, "btn_active");
	    status=findElementInXLSheet(getParameterAdministration, "status");
	    btn_apply=findElementInXLSheet(getParameterAdministration, "btn_apply");
	    btn_apply2=findElementInXLSheet(getParameterAdministration, "btn_apply2");
	    btn_update4=findElementInXLSheet(getParameterAdministration, "btn_update4");
	    calander=findElementInXLSheet(getParameterAdministration, "calander");
	    Assignuserpopup=findElementInXLSheet(getParameterAdministration, "Assignuserpopup");
	    btn_editblock=findElementInXLSheet(getParameterAdministration, "btn_editblock");
	    activeuserpage=findElementInXLSheet(getParameterAdministration, "activeuserpage");
	    btn_activeblock=findElementInXLSheet(getParameterAdministration, "btn_activeblock");
	    
	     btn_changeperiod=findElementInXLSheet(getParameterAdministration, "btn_changeperiod");
		 btn_from=findElementInXLSheet(getParameterAdministration, "btn_from");
	     btn_fromdate=findElementInXLSheet(getParameterAdministration, "btn_fromdate");
		 btn_to=findElementInXLSheet(getParameterAdministration,"btn_to");
		 btn_todate=findElementInXLSheet(getParameterAdministration,"btn_todate");
		 btn_changeperiod1=findElementInXLSheet(getParameterAdministration,"btn_changeperiod1");
	    
	    //print template
	    btn_printtemplatesetup=findElementInXLSheet(getParameterAdministration,"btn_printtemplatesetup");
	    printtemplatesetuppage=findElementInXLSheet(getParameterAdministration,"printtemplatesetuppage");
	    btn_print=findElementInXLSheet(getParameterAdministration,"btn_print");
	    selectbalancinglevel=findElementInXLSheet(getParameterAdministration,"selectbalancinglevel");
	    btn_new=findElementInXLSheet(getParameterAdministration,"btn_new");
	    addnewtemplatepage=findElementInXLSheet(getParameterAdministration,"addnewtemplatepage");
	    templatename=findElementInXLSheet(getParameterAdministration,"templatename");
	    description=findElementInXLSheet(getParameterAdministration,"description");
	    form=findElementInXLSheet(getParameterAdministration,"form");
	    datasourse=findElementInXLSheet(getParameterAdministration,"datasourse");
	    emailtemplate=findElementInXLSheet(getParameterAdministration,"emailtemplate");
	    btn_update5=findElementInXLSheet(getParameterAdministration,"btn_update5");
	    Printtemplate=findElementInXLSheet(getParameterAdministration,"Printtemplate");
	    
	    //report
	    btn_report=findElementInXLSheet(getParameterAdministration,"btn_report");
	    datasourse1=findElementInXLSheet(getParameterAdministration,"datasourse1");
	    btn_user1=findElementInXLSheet(getParameterAdministration,"btn_user1");
	    btn_username2=findElementInXLSheet(getParameterAdministration,"btn_username2");
	    
	    //workflow
	    btn_workflowconfiguration=findElementInXLSheet(getParameterAdministration,"btn_workflowconfiguration");
	    workflowconfigurationpage=findElementInXLSheet(getParameterAdministration,"workflowconfigurationpage");
	    btn_newworkflow=findElementInXLSheet(getParameterAdministration,"btn_newworkflow");
	    newworkflowpage=findElementInXLSheet(getParameterAdministration,"newworkflowpage");
	    workflowname=findElementInXLSheet(getParameterAdministration,"workflowname");
	    formname=findElementInXLSheet(getParameterAdministration,"formname");
	   

//	   journeyname1=findElementInXLSheet(getParameterAdministration,"journeyname1");
	    action=findElementInXLSheet(getParameterAdministration,"action");
	    fromaction=findElementInXLSheet(getParameterAdministration,"fromaction");
		btn_create=findElementInXLSheet(getParameterAdministration,"btn_create");
		journeyupdatepage=findElementInXLSheet(getParameterAdministration,"journeyupdatepage");
		
		//schedule template
		 btn_scheduletemplate=findElementInXLSheet(getParameterAdministration,"btn_scheduletemplate"); 
		 scheduletemplatepage=findElementInXLSheet(getParameterAdministration,"scheduletemplatepage");
         btn_newscheduletemplate=findElementInXLSheet(getParameterAdministration,"btn_newscheduletemplate");
		 newscheduletemplatepage=findElementInXLSheet(getParameterAdministration,"newscheduletemplatepage");
		 templatecode=findElementInXLSheet(getParameterAdministration,"templatecode");
		 templatedescription=findElementInXLSheet(getParameterAdministration,"templatedescription");
	     btn_Recurringseriesoftask=findElementInXLSheet(getParameterAdministration,"btn_Recurringseriesoftask");
	     btn_daily=findElementInXLSheet(getParameterAdministration,"btn_daily");
	     draftscheduletemplatepage=findElementInXLSheet(getParameterAdministration,"draftscheduletemplatepage");
	     btn_releasest=findElementInXLSheet(getParameterAdministration,"btn_releasest");
	     releasesecheduletemplatepage=findElementInXLSheet(getParameterAdministration,"releasesecheduletemplatepage");
	     
	     // schedule template task
	      btn_entution=findElementInXLSheet(getParameterAdministration,"btn_entution"); 
	 	 btn_rolecentre=findElementInXLSheet(getParameterAdministration,"btn_rolecentre1"); 
	 	 rolecentrepage=findElementInXLSheet(getParameterAdministration,"rolecentrepage"); 
	 	 btn_ScheduledJobs=findElementInXLSheet(getParameterAdministration,"btn_ScheduledJobs"); 
	 	 ScheduledJobspage=findElementInXLSheet(getParameterAdministration,"ScheduledJobspage"); 
	 	btn_expand=findElementInXLSheet(getParameterAdministration,"btn_expand"); 
	 	btn_selectall=findElementInXLSheet(getParameterAdministration,"btn_selectall"); 
	 	btn_run=findElementInXLSheet(getParameterAdministration,"btn_run"); 
	 	message=findElementInXLSheet(getParameterAdministration,"message"); 
	    
	     //journey config
	     btn_journeyconfiguration=findElementInXLSheet(getParameterAdministration,"btn_journeyconfiguration");
		 journeyconfigurationpage=findElementInXLSheet(getParameterAdministration,"journeyconfigurationpage");
		 btn_WIPrequest=findElementInXLSheet(getParameterAdministration,"btn_WIPrequest1");
		 WIPrequestpage=findElementInXLSheet(getParameterAdministration,"WIPrequestpage");
		 stockreservation=findElementInXLSheet(getParameterAdministration,"stockreservation");
		 btn_update6=findElementInXLSheet(getParameterAdministration,"btn_update6");
		 
		 //super user can send resetpassword link
		 btn_action=findElementInXLSheet(getParameterAdministration,"btn_action"); 
	     btn_resetpassword=findElementInXLSheet(getParameterAdministration,"btn_resetpassword");
	     resetpasswordpopup=findElementInXLSheet(getParameterAdministration,"resetpasswordpopup");
		 btn_yes=findElementInXLSheet(getParameterAdministration,"btn_yes");
			
		 //substitute
	    btn_maintainsubstitue=findElementInXLSheet(getParameterAdministration,"btn_maintainsubstitue"); 
        vactionsubstituepopup=findElementInXLSheet(getParameterAdministration,"vactionsubstituepopup"); 
	    btn_isvaction=findElementInXLSheet(getParameterAdministration,"btn_isvaction"); 
	    btn_search=findElementInXLSheet(getParameterAdministration,"btn_search"); 
	    assignuserpopup=findElementInXLSheet(getParameterAdministration,"assignuserpopup"); 
	    username3=findElementInXLSheet(getParameterAdministration,"username3");
	    btn_startdate=findElementInXLSheet(getParameterAdministration,"btn_startdate"); 
	    btn_selectastartdate=findElementInXLSheet(getParameterAdministration,"btn_selectastartdate"); 
	    btn_enddate=findElementInXLSheet(getParameterAdministration,"btn_enddate"); 
	    btn_selectenddate=findElementInXLSheet(getParameterAdministration,"btn_selectenddate"); 
		btnapply=findElementInXLSheet(getParameterAdministration,"btnapply");  
		 btn_usernow=findElementInXLSheet(getParameterAdministration,"btn_usernow");
		 btn_applynow=findElementInXLSheet(getParameterAdministration,"btn_applynow");
		
		 //search user workflow
		 searchuserworkflow=findElementInXLSheet(getParameterAdministration,"searchuserworkflow");
		 btn_workflownameapper=findElementInXLSheet(getParameterAdministration,"btn_workflownameapper");
		 workflowinformationpage=findElementInXLSheet(getParameterAdministration,"workflowinformationpage");
		 btn_editworkflow=findElementInXLSheet(getParameterAdministration,"btn_editworkflow");
		 btn_start=findElementInXLSheet(getParameterAdministration,"btn_start1");
		 btn_newapprovalprocess=findElementInXLSheet(getParameterAdministration,"btn_newapprovalprocess");
		 addeditapprovalprocesspage=findElementInXLSheet(getParameterAdministration,"addeditapprovalprocesspage");
		 displayname=findElementInXLSheet(getParameterAdministration,"displayname");
		 noofapprovals=findElementInXLSheet(getParameterAdministration,"noofapprovals");
		 btn_sendmail2=findElementInXLSheet(getParameterAdministration,"btn_sendmail2");
		 btn_lookup=findElementInXLSheet(getParameterAdministration,"btn_lookup");
		 btn_udatetosendmail=findElementInXLSheet(getParameterAdministration,"btn_udatetosendmail");
		 workflowupdatepage=findElementInXLSheet(getParameterAdministration,"workflowupdatepage");
		 btn_activeflow=findElementInXLSheet(getParameterAdministration,"btn_activeflow");
		 confirmationpopup=findElementInXLSheet(getParameterAdministration,"confirmationpopup");
		 btn_activeyes=findElementInXLSheet(getParameterAdministration,"btn_activeyes");
		 
		 //
		 btn_service=findElementInXLSheet(getParameterAdministration,"btn_service");
		 btn_case=findElementInXLSheet(getParameterAdministration,"btn_case");
		 casepage=findElementInXLSheet(getParameterAdministration,"casepage");
		 UserNameDatatask=findElementInXLSheet(getParameterAdministration,"UserNameDatatask");
		 PasswordDatatask=findElementInXLSheet(getParameterAdministration,"PasswordDatatask");
		 btn_task1=findElementInXLSheet(getParameterAdministration,"btn_task1");
		 taskpage=findElementInXLSheet(getParameterAdministration,"taskpage");
		 btn_Approval=findElementInXLSheet(getParameterAdministration,"btn_Approval");
		 approvalpage=findElementInXLSheet(getParameterAdministration,"approvalpage");
		 lnk_home2=findElementInXLSheet(getParameterAdministration,"lnk_home2");
		 btn_arrow=findElementInXLSheet(getParameterAdministration,"btn_arrow");
		 approvalrequestpage=findElementInXLSheet(getParameterAdministration,"approvalrequestpage");
		 remark=findElementInXLSheet(getParameterAdministration,"remark");
		 btn_ok=findElementInXLSheet(getParameterAdministration,"btn_ok");
		 searchuser01=findElementInXLSheet(getParameterAdministration,"searchuser01");
		 //
		  tasktemplate=findElementInXLSheet(getParameterAdministration,"tasktemplate");
		  task=findElementInXLSheet(getParameterAdministration,"task");
		  searchtask=findElementInXLSheet(getParameterAdministration,"searchtask");
		  btn_editactive=findElementInXLSheet(getParameterAdministration,"btn_editactive");
		  updatedUserinformationPage=findElementInXLSheet(getParameterAdministration,"updatedUserinformationPage");
		  TerminateStatusValue=findElementInXLSheet(getParameterAdministration,"TerminateStatusValue");
		//Action
		  btn_CurrencyInformation=findElementInXLSheet(getParameterAdministration,"btn_CurrencyInformation");
		  CurrencyInformationPage=findElementInXLSheet(getParameterAdministration,"CurrencyInformationPage");
		  ticktoactivate=findElementInXLSheet(getParameterAdministration,"ticktoactivate");
		  btn_updateCurrencyInformationpage=findElementInXLSheet(getParameterAdministration,"btn_updateCurrencyInformationpage");
		  btn_closevactionsubstitute=findElementInXLSheet(getParameterAdministration,"btn_closevactionsubstitute");
		  btn_UserPermission=findElementInXLSheet(getParameterAdministration,"btn_UserPermission");
		  UserPermissionPage=findElementInXLSheet(getParameterAdministration,"UserPermissionPage");
		  user=findElementInXLSheet(getParameterAdministration,"user");
		  btn_searchuser=findElementInXLSheet(getParameterAdministration,"btn_searchuser");
		  btn_Report=findElementInXLSheet(getParameterAdministration,"btn_Report");
		  UserReportPage=findElementInXLSheet(getParameterAdministration,"UserReportPage");
		  btn_Addresstype=findElementInXLSheet(getParameterAdministration,"btn_Addresstype");
		  AddressTypePage=findElementInXLSheet(getParameterAdministration,"AddressTypePage");
		  btn_IntegrationServiceSetup=findElementInXLSheet(getParameterAdministration,"btn_IntegrationServiceSetup");
		  IntegrationServiceSetupPage=findElementInXLSheet(getParameterAdministration,"IntegrationServiceSetupPage");
		  btn_editpencil=findElementInXLSheet(getParameterAdministration,"btn_editpencil");
		  btn_IntegrationServiceSetupupdate=findElementInXLSheet(getParameterAdministration,"btn_IntegrationServiceSetupupdate");
		  btn_PermissionGroup=findElementInXLSheet(getParameterAdministration,"btn_PermissionGroup");
		  btn_NewPermissionGroup=findElementInXLSheet(getParameterAdministration,"btn_NewPermissionGroup");
		  PermissionGroupPage=findElementInXLSheet(getParameterAdministration,"PermissionGroupPage");
		  GroupCode=findElementInXLSheet(getParameterAdministration,"GroupCode");
		  GroupName=findElementInXLSheet(getParameterAdministration,"GroupName");
		  btn_searchuserPermissionGroup=findElementInXLSheet(getParameterAdministration,"btn_searchuserPermissionGroup");
		  DraftedPermissionGroupPage=findElementInXLSheet(getParameterAdministration,"DraftedPermissionGroupPage");
		  btn_updateandnew=findElementInXLSheet(getParameterAdministration,"btn_updateandnew");
		  btn_copyfrom=findElementInXLSheet(getParameterAdministration,"btn_copyfrom");
		  btn_draftandnew=findElementInXLSheet(getParameterAdministration,"btn_draftandnew");
		  Newscheduletemplatepage=findElementInXLSheet(getParameterAdministration,"Newscheduletemplatepage");
		  btn_SystemPolicyConfiguration=findElementInXLSheet(getParameterAdministration,"btn_SystemPolicyConfiguration");
		  SystemPolicyConfigurationSetuppage=findElementInXLSheet(getParameterAdministration,"SystemPolicyConfigurationSetuppage");
		  btn_viewpost=findElementInXLSheet(getParameterAdministration,"btn_viewpost");
		  UpdatedSystemPolicyConfigurationSetuppage=findElementInXLSheet(getParameterAdministration,"UpdatedSystemPolicyConfigurationSetuppage");
		  btn_updatePolicyConfigurationSetuppage=findElementInXLSheet(getParameterAdministration,"btn_updatePolicyConfigurationSetuppage");
		  ProductCodeName=findElementInXLSheet(getParameterAdministration,"ProductCodeName");
		  btn_BalancingLevelSettings=findElementInXLSheet(getParameterAdministration,"btn_BalancingLevelSettings");
		  BalancingLevelSettingPage=findElementInXLSheet(getParameterAdministration,"BalancingLevelSettingPage");
		  ProductCode=findElementInXLSheet(getParameterAdministration,"ProductCode");
		  btn_productInformationupdate=findElementInXLSheet(getParameterAdministration,"btn_productInformationupdate");
		  btn_PasswordPolicies=findElementInXLSheet(getParameterAdministration,"btn_PasswordPolicies");
		  PasswordPoliciesPage=findElementInXLSheet(getParameterAdministration,"PasswordPoliciesPage");
		  btn_updatePasswordPolicies=findElementInXLSheet(getParameterAdministration,"btn_updatePasswordPolicies");
		  btn_SharingGroup=findElementInXLSheet(getParameterAdministration,"btn_SharingGroup");
		  SharingGroupPage=findElementInXLSheet(getParameterAdministration,"SharingGroupPage");
		  btn_TermsandConditions=findElementInXLSheet(getParameterAdministration,"btn_TermsandConditions");
		  TermsandConditionsPage=findElementInXLSheet(getParameterAdministration,"TermsandConditionsPage");
		  btn_NewTermsandCondition=findElementInXLSheet(getParameterAdministration,"btn_NewTermsandCondition");
		  NewTermsandConditionsPage=findElementInXLSheet(getParameterAdministration,"NewTermsandConditionsPage");
		  BalancingLevel=findElementInXLSheet(getParameterAdministration,"BalancingLevel");
		  code=findElementInXLSheet(getParameterAdministration,"code");
		  DescriptionCode=findElementInXLSheet(getParameterAdministration,"DescriptionCode");
		  btn_UpdateandNew=findElementInXLSheet(getParameterAdministration,"btn_UpdateandNew");
	}
	
	
	
	public static void readData() throws Exception
	{
		
		String computerName = System.getenv().get("COMPUTERNAME");
		if (computerName.equals("DESKTOP-0C81D38") || computerName.equals("MADHUSHAN-LAP4574")) {
			siteURL = findElementInXLSheet(getDataAdministration, "site url external");
		} else {
			siteURL = findElementInXLSheet(getDataAdministration, "site url");
		}
		UserNameData=findElementInXLSheet(getDataAdministration,"user name data");
		PasswordData=findElementInXLSheet(getDataAdministration,"password data");
		//new user
		txt_cbox=findElementInXLSheet(getDataAdministration, "txt_Cbox");
		txt_FullName=findElementInXLSheet(getDataAdministration, "txt_FullName");
		txt_DisplayNameField=findElementInXLSheet(getDataAdministration, "txt_DisplayNameField");
		getRandomNumber();
		txt_EmailNameField=findElementInXLSheet(getDataAdministration, "txt_EmailNameField");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String curDate = dtf.format(now);
		curDate = curDate.replaceAll("\\s", "");
		String curDatefinale = curDate.replaceAll("/", "").replaceAll(":", "");
		txt_EmailNameField=txt_EmailNameField.replace("ssssss", curDatefinale);
		txt_UserLevelField=findElementInXLSheet(getDataAdministration, "txt_UserLevelField");
		txt_UserTypeField=findElementInXLSheet(getDataAdministration, "txt_UserTypeField");
      	txt_BalancingLevel=findElementInXLSheet(getDataAdministration, "txt_BalancingLevel");
      	txt_ModuleName=findElementInXLSheet(getDataAdministration, "txt_ModuleName");
      	//edit
      	txt_UserLevelField2=findElementInXLSheet(getDataAdministration, "txt_UserLevelField2");
      	//change password
		txt_CurrentPassword=findElementInXLSheet(getDataAdministration, "txt_CurrentPassword");
		txt_NewPassword=findElementInXLSheet(getDataAdministration, "txt_NewPassword");
		txt_ConfirmNewPassword=findElementInXLSheet(getDataAdministration, "txt_ConfirmNewPassword");
	    //search user
		txt_SearchUser=findElementInXLSheet(getDataAdministration, "txt_SearchUser");
		txt_SearchUser=txt_EmailNameField;
	    txt_Template=findElementInXLSheet(getDataAdministration, "txt_Template");
	    //Forgot password
	    txt_EmailAddress=findElementInXLSheet(getDataAdministration,"txt_EmailAddress");
	    txt_mailpassword=findElementInXLSheet(getParameterAdministration,"txt_mailpassword");
	    txt_EmailAddressvalidate=findElementInXLSheet(getParameterAdministration," txt_EmailAddressvalidate");
	    //Terminate user
	    txt_status=findElementInXLSheet(getDataAdministration, "txt_status");
	    //Inactive user
	    txt_status2=findElementInXLSheet(getDataAdministration, "txt_status2");
	    //block a user
	    txt_status3=findElementInXLSheet(getDataAdministration, "txt_status3");
	    //active a blocked user
	    txt_status4=findElementInXLSheet(getDataAdministration, "txt_status4");
	    //activate a inactive user
	    txt_status5=findElementInXLSheet(getDataAdministration, "txt_status5");
	    //terminate a inactive user
	    txt_Templatestatus=findElementInXLSheet(getDataAdministration, "txt_Templatestatus");
	    txt_SearchUserstatus=findElementInXLSheet(getDataAdministration, "txt_SearchUserstatus");
	    txt_status6=findElementInXLSheet(getDataAdministration, "txt_status6");
	    //print template
	    txt_selectbalancinglevel=findElementInXLSheet(getDataAdministration, "txt_selectbalancinglevel");
	    txt_templatename=findElementInXLSheet(getDataAdministration,"txt_templatename");
	    txt_description=findElementInXLSheet(getDataAdministration,"txt_description");
	    txt_form=findElementInXLSheet(getDataAdministration,"txt_form");
	    txt_datasourse=findElementInXLSheet(getDataAdministration,"txt_datasourse");
	    txt_emailtemplate=findElementInXLSheet(getDataAdministration,"txt_emailtemplate");
	    // report
	    txt_datasourse1=findElementInXLSheet(getDataAdministration,"txt_datasourse1");
	    //work flow
	    txt_workflowname=findElementInXLSheet(getDataAdministration,"txt_workflowname");
	    txt_formname=findElementInXLSheet(getDataAdministration,"txt_formname");
		txt_journeyname1=findElementInXLSheet(getDataAdministration,"txt_journeyname1");
		txt_action=findElementInXLSheet(getDataAdministration,"txt_action");
		txt_fromaction=findElementInXLSheet(getDataAdministration,"txt_fromaction");
		
		//schedule template
		txt_templatecode=findElementInXLSheet(getDataAdministration,"txt_templatecode");
		txt_templatedescription=findElementInXLSheet(getDataAdministration,"txt_templatedescription");
		
		//journe config
		txt_stockreservation=findElementInXLSheet(getDataAdministration,"txt_stockreservation");
	    //
		txt_searchuser01=findElementInXLSheet(getDataAdministration,"txt_searchuser01");
		txt_SearchUserstatusnew=findElementInXLSheet(getDataAdministration,"txt_SearchUserstatusnew");
		//
		txt_searchuserworkflow=findElementInXLSheet(getDataAdministration,"txt_searchuserworkflow");
		txt_displayname=findElementInXLSheet(getDataAdministration,"txt_displayname");
	    txt_noofapprovals=findElementInXLSheet(getDataAdministration,"txt_noofapprovals");
	    txt_SearchUser3=findElementInXLSheet(getDataAdministration,"txt_SearchUser3");
	    txt_SearchUsercase=findElementInXLSheet(getDataAdministration,"txt_SearchUsercase");
	    
	    //approve task
	    txt_usernametask=findElementInXLSheet(getDataAdministration,"txt_usernametask");
	    txt_passwordtask=findElementInXLSheet(getDataAdministration,"txt_passwordtask");
	    txt_remark=findElementInXLSheet(getDataAdministration,"txt_remark");
	    //
	    txt_taskTemplate=findElementInXLSheet(getDataAdministration,"txt_taskTemplate");
	    txt_searchtask=findElementInXLSheet(getDataAdministration,"txt_searchtask");
	    txt_Groupcode=findElementInXLSheet(getDataAdministration,"txt_Groupcode");
	    txt_GroupName=findElementInXLSheet(getDataAdministration,"txt_GroupName");
	    txt_GroupName2=findElementInXLSheet(getDataAdministration,"txt_GroupName2");
	    txt_code=findElementInXLSheet(getDataAdministration,"txt_code");
	    txt_DescriptionCode=findElementInXLSheet(getDataAdministration,"txt_DescriptionCode");
	}

}
