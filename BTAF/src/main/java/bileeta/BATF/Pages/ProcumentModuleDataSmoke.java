package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class ProcumentModuleDataSmoke extends TestBase {


	/* Reading the element locators to variables */
	//Com_TC_001
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String lnk_home;
	//Com_TC_002
	protected static String btn_nav;
	protected static String sideMenu;
	protected static String subsideMenu;
	protected static String btn_probutton;
	//PR_VGC_001
	protected static String btn_vengroupbutton;
	protected static String btn_newvengroupbutton;
	protected static String txt_VendorGroupConfiguration;
	protected static String txt_newvendorgrouppage;
	//PR_VGC_002
	protected static String txt_vendorgroup;
	protected static String txt_currency;
	protected static String btn_btndetails;
	protected static String txt_PurchaseTaxGroup;
	protected static String txt_VendorClassification;
	protected static String txt_StandardDeliveryMode;
	protected static String txt_StandardDeliveryTerm;
	protected static String txt_Warehouse;
	protected static String btn_PartialDelivery;
	protected static String btn_ConsolidatedDelivery;
	protected static String btn_btnpaymentinfo;
	protected static String txt_PaymentAgreement;
	protected static String txt_PaymentTerm;
	protected static String txt_Tolerance ;
	protected static String txt_RegulationTaxGroup ;
	protected static String txt_CreditDays ;
	protected static String txt_CreditLimit ;
	protected static String btn_btndraft ;
	protected static String btn_VendorGroupConfiguration;
	protected static String txt_VendorGroupConfigurationNumber;
	protected static String txt_VendorGroupConfigurationSerch;
	protected static String sel_selVendorGroupConfiguration;

	//PR_VGC_003
	protected static String btn_vendorinfo ;
	protected static String btn_release ;
	//PR_VI_001
	protected static String btn_veninfobutton;
	protected static String btn_newvenbutton;
	protected static String txt_VendorInformationpage;
	protected static String txt_newVendorInformationpage;
	//PR_VI_002
	protected static String txt_VendorType; 
	protected static String txt_VendorNameTitle; 
	protected static String txt_VendorName;
	protected static String btn_ReceivableAccount;
	protected static String txt_ReceivableAccount;
	protected static String sel_ReceivableAccount;
	protected static String txt_ContactTitle;
	protected static String txt_ContactName;
	protected static String txt_MobileNumber;
	protected static String btn_DetailsTab;
	protected static String txt_SelectIndustry;
	protected static String txt_TaxRegistrationNumber;
	protected static String txt_ReferenceAccoutNo;
	protected static String btn_3rdPartyAccount;
	protected static String txt_3rdPartyAccount;
	protected static String sel_3rdPartyAccount;
	protected static String txt_PassportNo;
	protected static String txt_newVendorName;
	protected static String btn_VendorInformation;
	protected static String txt_VendorInformationserch;
	protected static String sel_selVendorInformation;
	protected static String btn_newReceivableAccount;
	protected static String txt_newReceivableAccountname;
	protected static String txt_AccountGroup;
	protected static String btn_btnUpdate;
	protected static String btn_btnReceivableAccountclose;
	
	//PR_VI_003
	protected static String btn_vendor ;
	
	//PM_VA_001
	protected static String VendorAgreementbutton;
	protected static String VendorAgreementpage;
	protected static String newVendorAgreementbutton;
	protected static String newVendorAgreementpage;
	protected static String txt_AgreementNo;
	protected static String txt_AgreementTitle;
	protected static String btn_btnAgreementDate;
	protected static String btn_selAgreementDate;
	protected static String txt_AgreementGroup;
	protected static String txt_ACurrency;
	protected static String txt_ValidityPeriodFor;
	protected static String txt_Description;
	protected static String btn_btnVendorAccount;
	protected static String txt_txtVendorAccount;
	protected static String sel_selVendorAccount;
	protected static String btn_btnproductdetails;
	protected static String txt_ProductRelation;
	protected static String btn_btnproduct;
	protected static String txt_txtproduct;
	protected static String sel_selproduct;
	protected static String txt_minOrderQty;
	protected static String txt_maxOrderQty;
	protected static String btn_btnupdatetask;
	protected static String btn_btnAssignTo;
	protected static String txt_txtAssignTo;
	protected static String sel_selAssignTo;
	protected static String  txt_txtsubject;
	
	//PM_PR_001
	protected static String btn_PurchaseRequisitionbutton;
	protected static String txt_PurchaseRequisitionbuttonpage;
	protected static String btn_newPurchaseRequisitionbutton;
	protected static String txt_newPurchaseRequisitionbuttonpage;
	//PM_PR_002
	protected static String btn_RequestDate;
	protected static String txt_RCurrency;
	protected static String btn_Rbtnproduct;
	protected static String txt_Rtxtproduct;
	protected static String sel_Rselproduct;
	protected static String btn_btnCheckout;
	protected static String btn_selRequestDate;
	protected static String txt_PurchaseRequisitionNumber;
	protected static String sel_selPurchaseRequisitionNumber;
	
	//PM_PR_003
	protected static String btn_btnPurchaseRequisition;

	//PM_PR_004
	protected static String btn_RbtnGridMasterInfo;
	protected static String btn_RbtnProductPurchasingHistory;
	protected static String txt_RPHUnitPrice;
	protected static String btn_RPHclose;
	protected static String txt_RUnitPrice;
	
	//PM_PR_005
	protected static String btn_btnRelPurchaseRequisition;
	protected static String btn_btnAction;
	protected static String btn_btnConverttoPurchaseOrder;
	protected static String btn_btnGeneratePO;
	protected static String btn_btnPOtoShipmentCosting;
	protected static String btn_RRDclose;
	protected static String btn_btnVendor;
	protected static String txt_txtVendor;
	protected static String sel_selVendor;
	protected static String btn_btnBillingAddress;
	protected static String txt_txtBillingAddress;
	protected static String btn_btnApply;
	protected static String btn_btnShippingAddress;
	protected static String txt_txtShippingAddress;
	
	//PM_PO_001
	protected static String btn_PurchaseOrderbutton;
	protected static String txt_PurchaseOrderpage;
	protected static String btn_newPurchaseOrderbutton;
	protected static String txt_newPurchaseOrderpage;
	protected static String txt_InboundShipmentpage;
	protected static String txt_PurchaseInvoicepage;
	protected static String txt_ShipmentCostingInformationpage;
	protected static String btn_PbtnVendor;
	protected static String btn_Pbtndetails;
	protected static String btn_btnPInvoiceAccount;
	protected static String btn_btnsummary;
	protected static String btn_pbtnproduct;
	protected static String txt_ptxtproduct;
	protected static String sel_pselproduct;
	protected static String btn_btnGotopage;
	protected static String btn_SerialRange;
	
	protected static String txt_pro7POqty1;
	protected static String txt_pro7POqty2;
	protected static String txt_pro7POqty3;
	protected static String txt_pro7POqty4;
	protected static String txt_pro7POqty5;
	protected static String txt_pro7POqty6;
	protected static String txt_pro7POqty7;
	
	protected static String btn_pro7POLook1;
	protected static String btn_pro7POLook2;
	protected static String btn_pro7POLook3;
	protected static String btn_pro7POLook4;
	protected static String btn_pro7POLook5;
	protected static String btn_pro7POLook6;
	protected static String btn_pro7POLook7;
	protected static String btn_pro7PILook1;
	protected static String btn_pro7PILook2;
	protected static String btn_pro7PILook3;
	protected static String btn_pro7PILook4;
	protected static String btn_pro7PILook5;
	protected static String btn_pro7PILook6;
	protected static String btn_pro7PILook7;
	protected static String btn_pro7ISLook1;
	protected static String btn_pro7ISLook2;
	protected static String btn_pro7ISLook3;
	protected static String btn_pro7ISLook4;
	protected static String btn_pro7ISLook5;
	protected static String btn_pro7ISLook6;
	protected static String btn_pro7ISLook7;

	
	protected static String btn_POProductAvailability;
	protected static String txt_POProductAvailabilityValue;
	protected static String btn_pro7SCILook1;



	//PM_PO_002
	protected static String btn_btnPOtoPIService;
	protected static String txt_txtVendorReferenceNo;
	
	//PM_PO_003
	protected static String btn_btnLPOtoShipmentCosting;
	protected static String btn_btndropdown;
	
	//PM_PO_005
	protected static String btn_btnConsolidatedPO;
	protected static String btn_btnDocumentList;
	protected static String btn_btnproductline1;
	protected static String btn_btnproductline2;
	protected static String btn_btnPApply;


	//PM_PO_006
	protected static String btn_btnaddProduct;
	protected static String btn_pbtnNxtproduct;
	protected static String txt_ptxtNxtproduct;
	protected static String btn_btnCostAllocation;
	protected static String btn_ImportCostEstimationShipmentbutton;
	protected static String txt_txtCEstDescription;
	protected static String txt_txtCostingType;
	protected static String txt_EstimateCost;
	protected static String btn_Apportionmentbutton;
	protected static String txt_ApportionmentBasis;
	protected static String btn_AppoCheckout;
	protected static String btn_AppoApply;
	protected static String btn_btnGenerateEstimation;
	protected static String btn_btnApplytomaingrid;
	protected static String btn_ApponxtCheckout;
	protected static String txt_ptxtBproduct;
	protected static String btn_btnSerialBatch;
	protected static String btn_btnselProductList;
	protected static String txt_BatchNo;
	protected static String txt_SerialNo;
	protected static String btn_updateSerial;
	protected static String btn_closeProductList;
	protected static String btn_ExpiryDate;
	protected static String btn_selExpiryDate;
	
	//PM_PO_007
	protected static String btn_ImportEstimationbutton;
	protected static String btn_addNewEstimationbutton;
	protected static String txt_txtQESTDescription;
	protected static String txt_EstimatedQty;
	protected static String btn_EstimationDetailsbutton;
	protected static String txt_CostingType;
	protected static String txt_EstimatenewCost;
	protected static String btn_Summary;
	protected static String btn_Checkout;
	protected static String btn_goPurchaseRequisitionpage;
	protected static String txt_serchPurchaseRequisition;
	protected static String btn_btnnxtGenerateEstimation;


	//PM_PI_001
	protected static String btn_PurchaseInvoicebutton;
	protected static String txt_PurchaseInvoicebuttonpage;
	protected static String btn_newPurchaseInvoicebutton;
	protected static String txt_newPurchaseInvoicebuttonpage;
	protected static String btn_btnPIToShipmentCosting;
	protected static String txt_PIcurrency;
	protected static String btn_btnPIVendor;
	protected static String txt_txtPIVendor;
	protected static String sel_selPIVendor;
	protected static String btn_btnPIBillingAddress;
	protected static String txt_txtPIBillingAddress;
	protected static String btn_btnPIShippingAddress;
	protected static String txt_txtPIShippingAddress;
	protected static String btn_PIbtnproduct;
	protected static String txt_PItxtproduct;
	protected static String sel_PIselproduct;
	protected static String btn_PIbtnNxtproduct;
	protected static String txt_PItxtNxtproduct;
	protected static String btn_PIbtnaddProduct;
	//PM_PI_002
	protected static String btn_btnPIToInboundShipment;
	//PM_PI_003
	protected static String btn_btnPIService;
	protected static String btn_btnPIsummary;
	protected static String txt_PItxtSproduct;
	
	//PM_PRO_001
	protected static String btn_PurchaseReturnOrderbutton;
	protected static String btn_newPurchaseReturnOrderbutton;
	protected static String btn_btnPROVendor;
	protected static String txt_txtPROVendor;
	protected static String sel_selPROVendor;
	protected static String btn_documentlistbutton;
	protected static String btn_PROsearch;
	protected static String btn_PROcheckbox;
	protected static String btn_PROapply;
	protected static String txt_PROqty;
	protected static String btn_PROdraft;
	protected static String btn_btnPOBillingAddress;
	protected static String txt_txtPOBillingAddress;
	protected static String btn_btnPOShippingAddress;
	protected static String txt_txtPOShippingAddress;
	protected static String btn_btnPOApply;
	protected static String btn_btnPOSerialNo;
	
	//PM_PRO_002
	protected static String btn_PObtnproduct;
	protected static String txt_POtxtproduct;
	protected static String sel_POselproduct;
	protected static String btn_POwarehouse;
	protected static String txt_POSerialNo;
	protected static String btn_POMasterInfo;
	protected static String btn_POProductBS;
	protected static String txt_POSNo;
	protected static String btn_POMasterInfoclose;
	protected static String btn_PROLook1;
	protected static String btn_PRILook1;
	protected static String txt_POProductAvailabilityValue1;


	protected static String txt_pageDraft;
	protected static String txt_pageRelease;
	
	//PM_RFQ_001
	protected static String btn_RequestForQuotation;
	protected static String btn_NewRequestForQuotation;
	protected static String txt_RequestForQuotationpage;
	protected static String txt_NewRequestForQuotationpage;
	protected static String btn_btncontactperson;
	protected static String txt_txtcontactperson;
	protected static String sel_selcontactperson;
	protected static String btn_btnRFQVendor;
	protected static String txt_txtRFQVendor;
	protected static String sel_selRFQVendor;
	protected static String btn_btnRFQproduct;
	protected static String txt_txtRFQproduct;
	protected static String sel_selRFQproduct;
	protected static String txt_RFQqty;
	protected static String btn_RFQdeadline;
	protected static String sel_selRFQdeadline;
	protected static String btn_QuotationTab;
	protected static String btn_RFQadd;
	protected static String txt_RFQaddVendor;
	protected static String txt_RFQQuotationNo;
	protected static String txt_RFQShippingTerm;
	protected static String txt_RFQUnitPrice;
	protected static String txt_RFQMinQty;
	protected static String txt_RFQMaxQty;
	protected static String btn_RFQcheckout;
	protected static String btn_RFQapply;
	protected static String btn_RFQsummarytab;
	protected static String btn_qualifiedvendorbutton;
	protected static String btn_Qualifiedtik;
	protected static String btn_RFQQualifiedapply;
	protected static String btn_Update;
	protected static String btn_btnbackground;
	protected static String btn_Edit;
	protected static String btn_btnRFQapply;
	
	//SMOKE_PRO_03
	protected static String btn_ImportCostEstimation;
	protected static String ImportCostEstimationpage;
	protected static String btn_newImportCostEstimation;
	protected static String newImportCostEstimationpage;
	protected static String txt_ICEDescription;
	protected static String btn_ICEProductbutoon;
	protected static String txt_ICEProducttxt;
	protected static String sel_ICEProductsel;
	protected static String btn_ICEEstimationDetails;
	protected static String txt_ICECostingType;
	protected static String txt_ICEEstimateCost;
	protected static String btn_ICESummary;
	protected static String btn_ICECheckout;
	protected static String btn_ICEDraft;
	protected static String btn_ICERelease;

	//SMOKE_PRO_05
	protected static String btn_PurchaseDesk;
	protected static String PurchaseDeskpage;
	protected static String sel_Requisitionsel;
	protected static String txt_SearchPRN;
	protected static String btn_SearchbuttonPRN;

	
	//SMOKE_PRO_10
	protected static String btn_DocFlow;
	
	//SMOKE_PRO_15to17
	protected static String btn_ProcurementParametersBtnA;
	protected static String btn_LicenseTypeParametersBtnA;
	protected static String btn_LicenseTypeParametersAddBtnA;
	protected static String txt_LicenseTypeParametersAddTxtA;
	protected static String btn_LicenseInformationBtnA;
	protected static String btn_LicenseInformationNewBtnA;
	protected static String txt_LicenceTypeTxtA;
	protected static String txt_LicencePiorityTxtA;
	protected static String txt_LicenceValidityPeriodTxtA;
	protected static String txt_ProcessTimePeriodTxtA;
	protected static String txt_PreLicenseTypeTxtA;
	protected static String txt_PostLicenseTypeTxtA;
	protected static String btn_LicenseProcessStepsTabA;
	protected static String btn_ProcessStepsAddBtnA;
	protected static String txt_ProcessStepsAddTxtA;
	protected static String txt_ProcessStepsAddTimeTxtA;
	protected static String btn_LicenseInformationDraftBtnA;
	protected static String txt_LicenseInformationSearchTxtA;
	protected static String txt_LicenseInformationStatusTxtA;
	protected static String btn_LicenseRequestBtnA;
	protected static String btn_NewLicenseRequestBtnA;
	protected static String txt_LicenseRequestTitleTxtA;
	protected static String txt_LicenseRequestLicenseTypeA;
	protected static String txt_LicenseRequestLicenseModeA;
	protected static String btn_LicenseRequestRequestbyBtnA;
	protected static String txt_LicenseRequestRequestbyTxtA;
	protected static String sel_LicenseRequestRequestbySelA;
	
	//product7
	
	protected static String PRPlus1;
	protected static String PRPlus2;
	protected static String PRPlus3;
	protected static String PRPlus4;
	protected static String PRPlus5;
	protected static String PRPlus6;
	protected static String PRPro2;
	protected static String PRPro3;
	protected static String PRPro4;
	protected static String PRPro5;
	protected static String PRPro6;
	protected static String PRPro7;
	
	protected static String POPlus1;
	protected static String POPlus2;
	protected static String POPlus3;
	protected static String POPlus4;
	protected static String POPlus5;
	protected static String POPlus6;
	protected static String POPro2;
	protected static String POPro3;
	protected static String POPro4;
	protected static String POPro5;
	protected static String POPro6;
	protected static String POPro7;
	
	protected static String Cap1;
	protected static String Cap2;
	protected static String Cap4;
	protected static String Cap5;
	protected static String Cap6;
	protected static String Cap7;
	
	protected static String BatchNu;
	protected static String BatchLotNu;
	protected static String BatchExpiryDate;
	protected static String SerialExpiryDate;
	protected static String BatchExpiryDateSel;
	protected static String SerialFromNu;
	protected static String SerialLotNu;
	protected static String SerialBatchNu;
	
	protected static String PIPlus1;
	protected static String PIPro1;

	protected static String RFQPro1;
	protected static String RFQPlus1;
	protected static String RFQPlus2;
	protected static String RFQPlus3;
	protected static String RFQPlus4;
	protected static String RFQPlus5;
	protected static String RFQPlus6;

	protected static String RFQUnitPrice2;
	protected static String RFQUnitPrice3;
	protected static String RFQUnitPrice4;
	protected static String RFQUnitPrice5;
	protected static String RFQUnitPrice6;
	protected static String RFQUnitPrice7;

	protected static String RFQMinQty2;
	protected static String RFQMinQty3;
	protected static String RFQMinQty4;
	protected static String RFQMinQty5;
	protected static String RFQMinQty6;
	protected static String RFQMinQty7;
	
	protected static String RFQMaxQty2;
	protected static String RFQMaxQty3;
	protected static String RFQMaxQty4;
	protected static String RFQMaxQty5;
	protected static String RFQMaxQty6;
	protected static String RFQMaxQty7;

	protected static String Qualifiedtik2;
	protected static String Qualifiedtik3;
	protected static String Qualifiedtik4;
	protected static String Qualifiedtik5;
	protected static String Qualifiedtik6;
	protected static String Qualifiedtik7;

	protected static String nextdate;
	protected static String btn_btnPOtoPurchaseInvoice;
	
	protected static String pro7qty1;
	protected static String pro7qty2;
	protected static String pro7qty3;
	protected static String pro7qty4;
	protected static String pro7qty5;
	protected static String pro7qty6;
	protected static String pro7qty7;
	protected static String pro7price1;
	protected static String pro7price2;
	protected static String pro7price3;
	protected static String pro7price4;
	protected static String pro7price5;
	protected static String pro7price6;
	protected static String pro7price7;
	
	protected static String Warehouse1;
	protected static String Warehouse2;
	protected static String Warehouse3;
	protected static String Warehouse4;
	protected static String Warehouse5;
	protected static String Warehouse6;
	protected static String Warehouse7;
	
	protected static String WarehousePI1;
	protected static String WarehousePI2;
	protected static String WarehousePI3;
	protected static String WarehousePI4;
	protected static String WarehousePI5;
	protected static String WarehousePI6;
	protected static String WarehousePI7;
	
	protected static String pageDraft;
	protected static String pageRelease;
	protected static String Status;
	protected static String Header;
	
	protected static String Product;
	protected static String ProductQty;


	
	//Master_1
	protected static String btn_Delete;
	protected static String txt_DeleteText;
	protected static String btn_Yes;
	protected static String btn_History;
	protected static String txt_HistoryText;
	protected static String btn_Activities;
	protected static String txt_ActivitiesText;
	protected static String btn_headerclose;
	protected static String btn_Duplicate;
	protected static String txt_lblVendorGroup;
	protected static String txt_dupVendorGroup;
	protected static String btn_btnEdit;
	protected static String txt_ReleseText;
	protected static String btn_DraftAndNew;
	protected static String btn_UpdateAndNew;
	
	//Master_2
	protected static String btn_VendorGroupbtn;
	protected static String btn_VendorGroupAdd;
	protected static String txt_newVendorGroupTxt;
	protected static String btn_VendorGroupUpdate;
	protected static String btn_Reminders;
	protected static String txt_RemindersText;
	protected static String btn_ActivitiesHeaderclose;
	
	//Master_3
	protected static String btn_ConverttoPurchaseOrder;
	protected static String btn_Reverse;
	protected static String txt_ReverseText;
	protected static String btn_CopyFrom;
	protected static String txt_CopyFromText;
	
	//Master_4
	protected static String btn_CustomerDemand;
	protected static String btn_CustomerDemandsel;
	protected static String btn_GenerateProductionOrder;
	protected static String btn_GenerateAssemblyProposal;
	protected static String txt_GenerateProductionOrdertxt;
	protected static String txt_GenerateAssemblyProposaltxt;

	//Transaction_3
	protected static String btn_Complete;
	protected static String txt_CompleteText;
	
	//Transaction_4
	protected static String btn_AddNewDocument;
	protected static String ZoomBar;
	protected static String btn_No;
	protected static String btn_ReverseButton;
	protected static String btn_Print;
	protected static String txt_PrintoutTemplates;
	protected static String btn_Back;
	protected static String btn_RackwiseAvailable;

	
	//Transaction_5
	protected static String btn_Acknowledgement;
	protected static String txt_Acknowledgementtxt;
	protected static String btn_ChangeCostAllocations;
	protected static String txt_ChangeCostAllocationstxt;
	protected static String txt_PurchaseInvoicetxt;
	
	//Transaction_6
	protected static String btn_CostEstimation;
	protected static String txt_CostEstimationtxt;
	protected static String btn_Close;
	protected static String txt_Closetxt;
	protected static String btn_Hold;
	protected static String btn_DotheOutboundAdvance;
	protected static String txt_DotheOutboundAdvancetxt;
	protected static String btn_ExtendExpiryDate;
	protected static String txt_ExtendExpiryDatetxt;
	protected static String btn_UpdateLogisticDates;
	protected static String txt_UpdateLogisticDatestxt;
	protected static String btn_UnHold;
	protected static String txt_Reason;
	protected static String btn_OK;
	protected static String txt_PurchaseOrdertxt;
	protected static String btn_ImportShipment;
	protected static String txt_ImportShipmenttxt;
	protected static String btn_CostEstimationClose;
	
	//Transaction_8
	protected static String btn_GeneratePurchaseOrder;
	
	//Master_001
	protected static String btn_SalesAndMarketing;
	protected static String btn_AccountGroupConfiguration;
	protected static String btn_NewAccountGroupConfiguration;
	protected static String btn_addAccountGroup;
	protected static String btn_addNewAccountGroup;
	protected static String txt_NewAccountGrouptxt;
	protected static String btn_AccountGroupUpdate;
	protected static String sel_AccountGroupsel;
	protected static String btn_InventoryAndWarehousing;
	protected static String btn_WarehouseInformation;
	protected static String btn_NewWarehouse;
	protected static String txt_WarehouseCode;
	protected static String txt_WarehouseName;
	protected static String btn_AutoLot;
	protected static String txt_LotBookNo;
	
	protected static String btn_AgreementGroupAdd;
	protected static String btn_AgreementGroupParameters;
	protected static String txt_AgreementGroupParameterstxt;
	protected static String btn_AgreementGroupParametersUpdate;
	protected static String txt_QuaranWarehouse;

	
	
	/* Reading the Data to variables */
	//Com_TC_001
	protected static String userNameData;
	protected static String passwordData;

	//PR_VGC_002
	protected static String vendorgroup;
	protected static String currency;
	protected static String PurchaseTaxGroup;
	protected static String VendorClassification;
	protected static String StandardDeliveryMode;
	protected static String StandardDeliveryTerm;
	protected static String Warehouse;
	protected static String PaymentTerm;
	protected static String Tolerance ;
	protected static String CreditDays;
	protected static String CreditLimit ;
	//PR_VI_002
	protected static String VendorType;
	protected static String VendorNameTitle;
	protected static String VendorName;
	protected static String txtReceivableAccount;
	protected static String ContactTitle;
	protected static String ContactName;
	protected static String MobileNumber;
	protected static String SelectIndustry;
	protected static String TaxRegistrationNumber;
	protected static String ReferenceAccoutNo;
	protected static String txt3rdPartyAccount;
	protected static String PassportNo;
	protected static String newReceivableAccountname;
	protected static String vendorgroupforven;
	
	//PM_VA_001
	protected static String AgreementNo;
	protected static String AgreementTitle;
	protected static String AgreementDate;
	protected static String AgreementGroup;
	protected static String Acurrency;
	protected static String ValidityPeriodFor;
	protected static String Description;
	protected static String txtVendorAccount;
	protected static String ProductRelation;
	protected static String txtproduct;
	protected static String minOrderQty;
	protected static String maxOrderQty;
	protected static String txtAssignTo;
	protected static String txtsubject;
	
	//PM_PR_002
	

	protected static String RequestDate;
	protected static String RCurrency;
	protected static String Rtxtproduct;
	
	//PM_PR_005

	protected static String txtVendor;
	protected static String txtBillingAddress;
	protected static String txtShippingAddress;
	
	//PM_PO_001
	protected static String ptxtproduct;
	protected static String Count;
	protected static String TestQuantity;

	//PM_PO_002
	protected static String ptxtSproduct;
	protected static String txtVendorReferenceNo;
	
	//PM_PO_006
	protected static String ptxtNxtproduct;
	protected static String txtCEstDescription;
	protected static String EstimateCost;
	protected static String ApportionmentBasis;
	protected static String ptxtBproduct;
	protected static String SerialNo;
	protected static String BatchNo;
	protected static String ExpiryDate;
	
	//PM_PO_007
	protected static String txtQESTDescription;
	protected static String EstimatedQty;
	protected static String EstimatenewCost;



	//PM_PI_001
	
	protected static String PIcurrency;
	protected static String txtPIVendor;
	protected static String txtPIBillingAddress;
	protected static String txtPIShippingAddress;
	protected static String PItxtproduct;
	protected static String PItxtNxtproduct;
	
	//PM_PI_003
	protected static String PItxtSproduct;
	
	//PM_PRO_001
	protected static String txtPROVendor;
	protected static String PROqty;
	protected static String txtPOBillingAddress;
	protected static String txtPOShippingAddress;
	
	
	//PM_PRO_002
	protected static String POtxtproduct;
	protected static String POwarehouse;
	protected static String POSerialNo;
	//PM_RFQ_001
	protected static String txtcontactperson;
	protected static String txtRFQVendor;
	protected static String txtRFQproduct;
	protected static String RFQqty;
	protected static String RFQQuotationNo;
	protected static String RFQUnitPrice;
	protected static String RFQMinQty;
	protected static String RFQMaxQty;
	
	//SMOKE_PRO_03
	protected static String ICEDescription;
	protected static String ICEProducttxt;
	protected static String ICEEstimateCost;
	
	//SMOKE_PRO_15to17
	protected static String LicencePiorityTxtA;
	protected static String ProcessStepsAddTxt1A;
	protected static String ProcessStepsAddTxt2A;
	protected static String LicenseRequestLicenseModeA;
	protected static String LicenseRequestRequestbyTxtA;

	//Master_001
	protected static String WarehouseCode;
	protected static String WarehouseName;
	protected static String AgreementGroupParameterstxt;

	//Calling the constructor

	
	public static void readElementlocators() throws Exception
	{
		//Com_TC_001
		siteLogo= findElementInXLSheet(getParameterProcurementSmoke,"site logo"); 
		txt_username= findElementInXLSheet(getParameterProcurementSmoke,"userName"); 
		txt_password= findElementInXLSheet(getParameterProcurementSmoke,"password"); 
		btn_login= findElementInXLSheet(getParameterProcurementSmoke,"login"); 
		lnk_home=findElementInXLSheet(getParameterProcurementSmoke,"headerlink");

		//Com_TC_002
		btn_nav = findElementInXLSheet(getParameterProcurementSmoke,"navbutton"); 
		sideMenu = findElementInXLSheet(getParameterProcurementSmoke,"sidemenu");
		btn_probutton = findElementInXLSheet(getParameterProcurementSmoke,"probutton");
		subsideMenu = findElementInXLSheet(getParameterProcurementSmoke,"subsidemenu");

		//PR_VGC_001
		
		btn_vengroupbutton = findElementInXLSheet(getParameterProcurementSmoke,"vendorgroupbutton");
		btn_newvengroupbutton = findElementInXLSheet(getParameterProcurementSmoke,"newvendorgroupbutton");
		txt_VendorGroupConfiguration = findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupConfiguration");
		txt_newvendorgrouppage=findElementInXLSheet(getParameterProcurementSmoke, "newvendorgrouppage");
		//PR_VGC_002
		txt_vendorgroup= findElementInXLSheet(getParameterProcurementSmoke, "vendorGroup");
		txt_currency=findElementInXLSheet(getParameterProcurementSmoke, "currency");
		btn_btndetails = findElementInXLSheet(getParameterProcurementSmoke, "btndetails");
		txt_PurchaseTaxGroup = findElementInXLSheet(getParameterProcurementSmoke, "PurchaseTaxGroup");
		txt_VendorClassification = findElementInXLSheet(getParameterProcurementSmoke, "VendorClassification");
		txt_StandardDeliveryMode= findElementInXLSheet(getParameterProcurementSmoke, "StandardDeliveryMode");
		txt_StandardDeliveryTerm= findElementInXLSheet(getParameterProcurementSmoke, "StandardDeliveryTerm");
		txt_Warehouse = findElementInXLSheet(getParameterProcurementSmoke, "Warehouse");
		btn_PartialDelivery= findElementInXLSheet(getParameterProcurementSmoke, "PartialDelivery");
		btn_ConsolidatedDelivery= findElementInXLSheet(getParameterProcurementSmoke, "ConsolidatedDelivery");
		btn_btnpaymentinfo = findElementInXLSheet(getParameterProcurementSmoke, "btnpaymentinfo");
		txt_PaymentAgreement=findElementInXLSheet(getParameterProcurementSmoke, "PaymentAgreement");
		txt_PaymentTerm=findElementInXLSheet(getParameterProcurementSmoke, "PaymentTerm");
		txt_Tolerance =findElementInXLSheet(getParameterProcurementSmoke, "Tolerance");
		txt_RegulationTaxGroup =findElementInXLSheet(getParameterProcurementSmoke, "RegulationTaxGroup");
		txt_CreditDays=findElementInXLSheet(getParameterProcurementSmoke, "CreditDays");
		txt_CreditLimit=findElementInXLSheet(getParameterProcurementSmoke, "CreditLimit");
		btn_btndraft =findElementInXLSheet(getParameterProcurementSmoke, "btndraft");
		btn_VendorGroupConfiguration=findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupConfiguration");
		txt_VendorGroupConfigurationNumber=findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupConfigurationNumber");
		txt_VendorGroupConfigurationSerch=findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupConfigurationSerch");
		sel_selVendorGroupConfiguration=findElementInXLSheet(getParameterProcurementSmoke, "selVendorGroupConfiguration");
		//PR_VGC_003
		btn_vendorinfo = findElementInXLSheet(getParameterProcurementSmoke, "btnvendorinfo");
		btn_release = findElementInXLSheet(getParameterProcurementSmoke, "btnrelease");
		//PR_VI_001
		btn_veninfobutton=findElementInXLSheet(getParameterProcurementSmoke,"vendorinfobutton");
		btn_newvenbutton=findElementInXLSheet(getParameterProcurementSmoke,"newvendorbutton");
		txt_VendorInformationpage=findElementInXLSheet(getParameterProcurementSmoke,"VendorInformationpage");
		txt_newVendorInformationpage=findElementInXLSheet(getParameterProcurementSmoke,"newVendorInformationpage");
		//PR_VI_002
		txt_VendorType = findElementInXLSheet(getParameterProcurementSmoke, "VendorType"); 
		txt_VendorNameTitle = findElementInXLSheet(getParameterProcurementSmoke, "VendorNameTitle"); 
		txt_VendorName= findElementInXLSheet(getParameterProcurementSmoke, "VendorName");
		btn_ReceivableAccount = findElementInXLSheet(getParameterProcurementSmoke, "btnReceivableAccount");
		txt_ReceivableAccount = findElementInXLSheet(getParameterProcurementSmoke, "txtReceivableAccount");
		sel_ReceivableAccount = findElementInXLSheet(getParameterProcurementSmoke, "selReceivableAccount");
		txt_ContactTitle = findElementInXLSheet(getParameterProcurementSmoke, "ContactTitle");
		txt_ContactName =findElementInXLSheet(getParameterProcurementSmoke, "ContactName");
		txt_MobileNumber= findElementInXLSheet(getParameterProcurementSmoke, "MobileNumber");
		txt_SelectIndustry= findElementInXLSheet(getParameterProcurementSmoke, "SelectIndustry");
		txt_TaxRegistrationNumber= findElementInXLSheet(getParameterProcurementSmoke, "TaxRegistrationNumber");
		txt_ReferenceAccoutNo= findElementInXLSheet(getParameterProcurementSmoke, "ReferenceAccoutNo");
		btn_3rdPartyAccount = findElementInXLSheet(getParameterProcurementSmoke, "btn3rdPartyAccount");
		txt_3rdPartyAccount = findElementInXLSheet(getParameterProcurementSmoke, "txt3rdPartyAccount");
		sel_3rdPartyAccount = findElementInXLSheet(getParameterProcurementSmoke, "sel3rdPartyAccount");
		txt_PassportNo = findElementInXLSheet(getParameterProcurementSmoke, "PassportNo");
		txt_newVendorName= findElementInXLSheet(getParameterProcurementSmoke, "newVendorName");
		btn_VendorInformation= findElementInXLSheet(getParameterProcurementSmoke, "VendorInformation");
		txt_VendorInformationserch= findElementInXLSheet(getParameterProcurementSmoke, "VendorInformationserch");
		sel_selVendorInformation= findElementInXLSheet(getParameterProcurementSmoke, "selVendorInformation");
		btn_newReceivableAccount= findElementInXLSheet(getParameterProcurementSmoke, "newReceivableAccount");
		txt_newReceivableAccountname= findElementInXLSheet(getParameterProcurementSmoke, "newReceivableAccountname");
		txt_AccountGroup= findElementInXLSheet(getParameterProcurementSmoke, "AccountGroup");
		btn_btnUpdate= findElementInXLSheet(getParameterProcurementSmoke, "btnUpdateven");
		btn_btnReceivableAccountclose= findElementInXLSheet(getParameterProcurementSmoke, "btnReceivableAccountclose");
		
		//PR_VI_003
		btn_vendor = findElementInXLSheet(getParameterProcurementSmoke, "btnvendor");
		//PM_VA_001
		VendorAgreementbutton = findElementInXLSheet(getParameterProcurementSmoke, "VendorAgreementbutton");
		VendorAgreementpage= findElementInXLSheet(getParameterProcurementSmoke, "VendorAgreementpage");
		newVendorAgreementbutton = findElementInXLSheet(getParameterProcurementSmoke, "newVendorAgreementbutton");
		newVendorAgreementpage= findElementInXLSheet(getParameterProcurementSmoke, "newVendorAgreementpage");
		txt_AgreementNo = findElementInXLSheet(getParameterProcurementSmoke, "AgreementNo");
		txt_AgreementTitle = findElementInXLSheet(getParameterProcurementSmoke, "AgreementTitle");
		btn_btnAgreementDate = findElementInXLSheet(getParameterProcurementSmoke, "btnAgreementDate");
		btn_selAgreementDate=findElementInXLSheet(getParameterProcurementSmoke, "btnselAgreementDate");
		txt_AgreementGroup = findElementInXLSheet(getParameterProcurementSmoke, "AgreementGroup");
		txt_ACurrency = findElementInXLSheet(getParameterProcurementSmoke, "ACurrency");
		txt_ValidityPeriodFor = findElementInXLSheet(getParameterProcurementSmoke, "ValidityPeriodFor");
		txt_Description = findElementInXLSheet(getParameterProcurementSmoke, "Description");
		btn_btnVendorAccount = findElementInXLSheet(getParameterProcurementSmoke, "btnVendorAccount");
		txt_txtVendorAccount = findElementInXLSheet(getParameterProcurementSmoke, "txtVendorAccount");
		sel_selVendorAccount = findElementInXLSheet(getParameterProcurementSmoke, "selVendorAccount");
		btn_btnproductdetails = findElementInXLSheet(getParameterProcurementSmoke, "btnproductdetails");
		txt_ProductRelation = findElementInXLSheet(getParameterProcurementSmoke, "ProductRelation");
		btn_btnproduct = findElementInXLSheet(getParameterProcurementSmoke, "btnproduct");
		txt_txtproduct = findElementInXLSheet(getParameterProcurementSmoke, "txtproduct");
		sel_selproduct = findElementInXLSheet(getParameterProcurementSmoke, "selproduct");
		txt_minOrderQty = findElementInXLSheet(getParameterProcurementSmoke, "minOrderQty");
		txt_maxOrderQty = findElementInXLSheet(getParameterProcurementSmoke, "maxOrderQty");
		btn_btnupdatetask = findElementInXLSheet(getParameterProcurementSmoke, "btnupdatetask");
		btn_btnAssignTo = findElementInXLSheet(getParameterProcurementSmoke, "btnAssignTo");
		txt_txtAssignTo= findElementInXLSheet(getParameterProcurementSmoke, "txtAssignTo");
		sel_selAssignTo= findElementInXLSheet(getParameterProcurementSmoke, "selAssignTo");
		txt_txtsubject= findElementInXLSheet(getParameterProcurementSmoke, "txtsubject");
		
		//PM_PR_001
		btn_PurchaseRequisitionbutton = findElementInXLSheet(getParameterProcurementSmoke, "PurchaseRequisitionbutton");
		txt_PurchaseRequisitionbuttonpage = findElementInXLSheet(getParameterProcurementSmoke, "PurchaseRequisitionbuttonpage");
		btn_newPurchaseRequisitionbutton =findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseRequisitionbutton");
		txt_newPurchaseRequisitionbuttonpage= findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseRequisitionbuttonpage");
		//PM_PR_002
		btn_RequestDate = findElementInXLSheet(getParameterProcurementSmoke, "btnRequestDate");
		btn_selRequestDate = findElementInXLSheet(getParameterProcurementSmoke, "btnselRequestDate");
		txt_RCurrency = findElementInXLSheet(getParameterProcurementSmoke, "RCurrency");
		btn_Rbtnproduct = findElementInXLSheet(getParameterProcurementSmoke, "Rbtnproduct");
		txt_Rtxtproduct = findElementInXLSheet(getParameterProcurementSmoke, "Rtxtproduct");
		sel_Rselproduct = findElementInXLSheet(getParameterProcurementSmoke, "Rselproduct");
		btn_btnCheckout = findElementInXLSheet(getParameterProcurementSmoke, "btnCheckout");
		txt_PurchaseRequisitionNumber= findElementInXLSheet(getParameterProcurementSmoke, "PurchaseRequisitionNumber");
		btn_goPurchaseRequisitionpage= findElementInXLSheet(getParameterProcurementSmoke, "goPurchaseRequisitionpage");
		txt_serchPurchaseRequisition= findElementInXLSheet(getParameterProcurementSmoke, "serchPurchaseRequisition");
		sel_selPurchaseRequisitionNumber= findElementInXLSheet(getParameterProcurementSmoke, "selPurchaseRequisitionNumber");

		
		//PM_PR_003
		btn_btnPurchaseRequisition = findElementInXLSheet(getParameterProcurementSmoke, "btnPurchaseRequisition"); 
		
		//PM_PR_004
		btn_RbtnGridMasterInfo =findElementInXLSheet(getParameterProcurementSmoke, "RbtnGridMasterInfo");
		btn_RbtnProductPurchasingHistory=findElementInXLSheet(getParameterProcurementSmoke, "RbtnProductPurchasingHistory");
		txt_RPHUnitPrice=findElementInXLSheet(getParameterProcurementSmoke, "RPHUnitPrice");
		btn_RPHclose=findElementInXLSheet(getParameterProcurementSmoke, "RPHclose");
		txt_RUnitPrice=findElementInXLSheet(getParameterProcurementSmoke, "RUnitPrice");
		
		//PM_PR_005
		btn_btnRelPurchaseRequisition= findElementInXLSheet(getParameterProcurementSmoke, "btnRelPurchaseRequisition");
		btn_btnAction = findElementInXLSheet(getParameterProcurementSmoke, "btnAction");
		btn_btnConverttoPurchaseOrder= findElementInXLSheet(getParameterProcurementSmoke, "btnConverttoPurchaseOrder");
		btn_btnGeneratePO= findElementInXLSheet(getParameterProcurementSmoke, "btnGeneratePO");
		btn_btnPOtoShipmentCosting= findElementInXLSheet(getParameterProcurementSmoke, "btnPOtoShipmentCosting");
		btn_RRDclose= findElementInXLSheet(getParameterProcurementSmoke, "RRDclose");
		btn_btnVendor= findElementInXLSheet(getParameterProcurementSmoke, "btnVendor");
		txt_txtVendor= findElementInXLSheet(getParameterProcurementSmoke, "txtVendor");
		sel_selVendor= findElementInXLSheet(getParameterProcurementSmoke, "selVendor");
		btn_btnBillingAddress= findElementInXLSheet(getParameterProcurementSmoke, "btnBillingAddress");
		txt_txtBillingAddress= findElementInXLSheet(getParameterProcurementSmoke, "txtBillingAddress");
		btn_btnApply= findElementInXLSheet(getParameterProcurementSmoke, "btnApply");
		btn_btnShippingAddress= findElementInXLSheet(getParameterProcurementSmoke, "btnShippingAddress");
		txt_txtShippingAddress= findElementInXLSheet(getParameterProcurementSmoke, "txtShippingAddress");
		
		//PM_PO_001
		btn_PurchaseOrderbutton= findElementInXLSheet(getParameterProcurementSmoke, "PurchaseOrderbutton");
		txt_PurchaseOrderpage= findElementInXLSheet(getParameterProcurementSmoke, "PurchaseOrderpage");
		btn_newPurchaseOrderbutton= findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseOrderbutton");
		txt_newPurchaseOrderpage= findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseOrderpage");
		txt_InboundShipmentpage= findElementInXLSheet(getParameterProcurementSmoke, "InboundShipmentpage");
		txt_PurchaseInvoicepage= findElementInXLSheet(getParameterProcurementSmoke, "PurchaseInvoicepage");
		txt_ShipmentCostingInformationpage= findElementInXLSheet(getParameterProcurementSmoke, "ShipmentCostingInformationpage");
		btn_PbtnVendor= findElementInXLSheet(getParameterProcurementSmoke, "PbtnVendor");
		btn_Pbtndetails= findElementInXLSheet(getParameterProcurementSmoke, "Pbtndetails");
		btn_btnPInvoiceAccount= findElementInXLSheet(getParameterProcurementSmoke, "btnPInvoiceAccount");
		btn_btnsummary= findElementInXLSheet(getParameterProcurementSmoke, "btnsummary");
		btn_pbtnproduct= findElementInXLSheet(getParameterProcurementSmoke, "pbtnproduct");
		txt_ptxtproduct= findElementInXLSheet(getParameterProcurementSmoke, "ptxtproduct");
		sel_pselproduct= findElementInXLSheet(getParameterProcurementSmoke, "pselproduct");
		btn_btnGotopage= findElementInXLSheet(getParameterProcurementSmoke, "btnGotopage");
		btn_SerialRange= findElementInXLSheet(getParameterProcurementSmoke, "SerialRange");
		
		txt_pro7POqty1= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty1");
		txt_pro7POqty2= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty2");
		txt_pro7POqty3= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty3");
		txt_pro7POqty4= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty4");
		txt_pro7POqty5= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty5");
		txt_pro7POqty6= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty6");
		txt_pro7POqty7= findElementInXLSheet(getParameterProcurementSmoke, "pro7POqty7");
		
		btn_pro7POLook1= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook1");
		btn_pro7POLook2= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook2");
		btn_pro7POLook3= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook3");
		btn_pro7POLook4= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook4");
		btn_pro7POLook5= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook5");
		btn_pro7POLook6= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook6");
		btn_pro7POLook7= findElementInXLSheet(getParameterProcurementSmoke, "pro7POLook7");
		
		btn_pro7PILook1= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook1");
		btn_pro7PILook2= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook2");
		btn_pro7PILook3= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook3");
		btn_pro7PILook4= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook4");
		btn_pro7PILook5= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook5");
		btn_pro7PILook6= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook6");
		btn_pro7PILook7= findElementInXLSheet(getParameterProcurementSmoke, "pro7PILook7");
		
		btn_pro7ISLook1= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook1");
		btn_pro7ISLook2= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook2");
		btn_pro7ISLook3= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook3");
		btn_pro7ISLook4= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook4");
		btn_pro7ISLook5= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook5");
		btn_pro7ISLook6= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook6");
		btn_pro7ISLook7= findElementInXLSheet(getParameterProcurementSmoke, "pro7ISLook7");

		
		btn_POProductAvailability= findElementInXLSheet(getParameterProcurementSmoke, "POProductAvailability");
		txt_POProductAvailabilityValue= findElementInXLSheet(getParameterProcurementSmoke, "POProductAvailabilityValue");
		btn_pro7SCILook1= findElementInXLSheet(getParameterProcurementSmoke, "pro7SCILook1");
		

		
		//PM_PO_002
		btn_btnPOtoPIService= findElementInXLSheet(getParameterProcurementSmoke, "btnPOtoPIService");
		txt_txtVendorReferenceNo= findElementInXLSheet(getParameterProcurementSmoke, "txtVendorReferenceNo");
		
		//PM_PO_003
		btn_btnLPOtoShipmentCosting= findElementInXLSheet(getParameterProcurementSmoke, "btnLPOtoShipmentCosting");
		btn_btndropdown= findElementInXLSheet(getParameterProcurementSmoke, "btndropdown");
		
		//PM_PO_005
		btn_btnConsolidatedPO= findElementInXLSheet(getParameterProcurementSmoke, "btnConsolidatedPO");
		btn_btnDocumentList= findElementInXLSheet(getParameterProcurementSmoke, "btnDocumentList");
		btn_btnproductline1= findElementInXLSheet(getParameterProcurementSmoke, "btnproductline1");
		btn_btnproductline2= findElementInXLSheet(getParameterProcurementSmoke, "btnproductline2");
		btn_btnPApply = findElementInXLSheet(getParameterProcurementSmoke, "btnPApply");
		
		//PM_PO_006
		btn_btnaddProduct= findElementInXLSheet(getParameterProcurementSmoke, "btnaddProduct");
		btn_pbtnNxtproduct= findElementInXLSheet(getParameterProcurementSmoke, "pbtnNxtproduct");
		txt_ptxtNxtproduct= findElementInXLSheet(getParameterProcurementSmoke, "ptxtNxtproduct");
		btn_btnCostAllocation= findElementInXLSheet(getParameterProcurementSmoke, "btnCostAllocation");
		btn_ImportCostEstimationShipmentbutton= findElementInXLSheet(getParameterProcurementSmoke, "ImportCostEstimationShipmentbutton");
		txt_txtCEstDescription= findElementInXLSheet(getParameterProcurementSmoke, "txtCEstDescription");
		txt_txtCostingType= findElementInXLSheet(getParameterProcurementSmoke, "txtCostingType");
		txt_EstimateCost= findElementInXLSheet(getParameterProcurementSmoke, "EstimateCost");
		btn_Apportionmentbutton= findElementInXLSheet(getParameterProcurementSmoke, "Apportionmentbutton");
		txt_ApportionmentBasis= findElementInXLSheet(getParameterProcurementSmoke, "ApportionmentBasis");
		btn_AppoCheckout= findElementInXLSheet(getParameterProcurementSmoke, "AppoCheckout");
		btn_AppoApply= findElementInXLSheet(getParameterProcurementSmoke, "AppoApply");
		btn_btnGenerateEstimation= findElementInXLSheet(getParameterProcurementSmoke, "btnGenerateEstimation");
		btn_btnApplytomaingrid= findElementInXLSheet(getParameterProcurementSmoke, "btnApplytomaingrid");
		btn_ApponxtCheckout= findElementInXLSheet(getParameterProcurementSmoke, "ApponxtCheckout");
		txt_ptxtBproduct= findElementInXLSheet(getParameterProcurementSmoke, "ptxtBproduct");
		btn_btnSerialBatch= findElementInXLSheet(getParameterProcurementSmoke, "btnSerialBatch");
		btn_btnselProductList= findElementInXLSheet(getParameterProcurementSmoke, "btnselProductList");
		txt_BatchNo= findElementInXLSheet(getParameterProcurementSmoke, "BatchNo");
		txt_SerialNo= findElementInXLSheet(getParameterProcurementSmoke, "SerialNo");
		btn_updateSerial= findElementInXLSheet(getParameterProcurementSmoke, "updateSerial");
		btn_closeProductList= findElementInXLSheet(getParameterProcurementSmoke, "closeProductList");
		btn_ExpiryDate = findElementInXLSheet(getParameterProcurementSmoke, "ExpiryDate");
		btn_selExpiryDate = findElementInXLSheet(getParameterProcurementSmoke, "selExpiryDate");
		
		//PM_PO_007
		btn_ImportEstimationbutton= findElementInXLSheet(getParameterProcurementSmoke, "ImportEstimationbutton");
		btn_addNewEstimationbutton= findElementInXLSheet(getParameterProcurementSmoke, "addNewEstimationbutton");
		txt_txtQESTDescription= findElementInXLSheet(getParameterProcurementSmoke, "txtQESTDescription");
		txt_EstimatedQty= findElementInXLSheet(getParameterProcurementSmoke, "EstimatedQty");
		btn_EstimationDetailsbutton= findElementInXLSheet(getParameterProcurementSmoke, "EstimationDetailsbutton");
		txt_CostingType= findElementInXLSheet(getParameterProcurementSmoke, "CostingType");
		txt_EstimatenewCost= findElementInXLSheet(getParameterProcurementSmoke, "EstimatenewCost");
		btn_Summary= findElementInXLSheet(getParameterProcurementSmoke, "Summary");
		btn_Checkout= findElementInXLSheet(getParameterProcurementSmoke, "Checkout");
		btn_btnnxtGenerateEstimation= findElementInXLSheet(getParameterProcurementSmoke, "btnnxtGenerateEstimation");


		
		//PM_PI_001
		btn_PurchaseInvoicebutton= findElementInXLSheet(getParameterProcurementSmoke, "PurchaseInvoicebutton");
		txt_PurchaseInvoicebuttonpage= findElementInXLSheet(getParameterProcurementSmoke, "PurchaseInvoicebuttonpage");
		btn_newPurchaseInvoicebutton= findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseInvoicebutton");
		txt_newPurchaseInvoicebuttonpage= findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseInvoicebuttonpage");
		btn_btnPIToShipmentCosting= findElementInXLSheet(getParameterProcurementSmoke, "btnPIToShipmentCosting");
		txt_PIcurrency= findElementInXLSheet(getParameterProcurementSmoke, "PIcurrency");
		btn_btnPIVendor= findElementInXLSheet(getParameterProcurementSmoke, "btnPIVendor");
		txt_txtPIVendor= findElementInXLSheet(getParameterProcurementSmoke, "txtPIVendor");
		sel_selPIVendor= findElementInXLSheet(getParameterProcurementSmoke, "selPIVendor");
		btn_btnPIBillingAddress= findElementInXLSheet(getParameterProcurementSmoke, "btnPIBillingAddress");
		txt_txtPIBillingAddress= findElementInXLSheet(getParameterProcurementSmoke, "txtPIBillingAddress");
		btn_btnPIShippingAddress= findElementInXLSheet(getParameterProcurementSmoke, "btnPIShippingAddress");
		txt_txtPIShippingAddress= findElementInXLSheet(getParameterProcurementSmoke, "txtPIShippingAddress");
		btn_PIbtnproduct= findElementInXLSheet(getParameterProcurementSmoke, "PIbtnproduct");
		txt_PItxtproduct= findElementInXLSheet(getParameterProcurementSmoke, "PItxtproduct");
		sel_PIselproduct= findElementInXLSheet(getParameterProcurementSmoke, "PIselproduct");
		btn_PIbtnNxtproduct= findElementInXLSheet(getParameterProcurementSmoke, "PIbtnNxtproduct");
		txt_PItxtNxtproduct= findElementInXLSheet(getParameterProcurementSmoke, "PItxtNxtproduct");
		btn_PIbtnaddProduct= findElementInXLSheet(getParameterProcurementSmoke, "PIbtnaddProduct");
		
		//PM_PI_002
		btn_btnPIToInboundShipment= findElementInXLSheet(getParameterProcurementSmoke, "btnPIToInboundShipment");
		
		//PM_PI_003
		btn_btnPIService= findElementInXLSheet(getParameterProcurementSmoke, "btnPIService");
		btn_btnPIsummary=findElementInXLSheet(getParameterProcurementSmoke, "btnPIsummary");
		txt_PItxtSproduct=findElementInXLSheet(getParameterProcurementSmoke, "PItxtSproduct");
		
		//PM_PRO_001
		btn_PurchaseReturnOrderbutton=findElementInXLSheet(getParameterProcurementSmoke, "PurchaseReturnOrderbutton");
		btn_newPurchaseReturnOrderbutton=findElementInXLSheet(getParameterProcurementSmoke, "newPurchaseReturnOrderbutton");
		btn_btnPROVendor=findElementInXLSheet(getParameterProcurementSmoke, "btnPROVendor");
		txt_txtPROVendor=findElementInXLSheet(getParameterProcurementSmoke, "txtPROVendor");
		sel_selPROVendor=findElementInXLSheet(getParameterProcurementSmoke, "selPROVendor");
		btn_documentlistbutton=findElementInXLSheet(getParameterProcurementSmoke, "documentlistbutton");
		btn_PROsearch=findElementInXLSheet(getParameterProcurementSmoke, "PROsearch");
		btn_PROcheckbox=findElementInXLSheet(getParameterProcurementSmoke, "PROcheckbox");
		btn_PROapply=findElementInXLSheet(getParameterProcurementSmoke, "PROapply");
		txt_PROqty=findElementInXLSheet(getParameterProcurementSmoke, "PROqty");
		btn_PROdraft=findElementInXLSheet(getParameterProcurementSmoke, "PROdraft");
		btn_btnPOBillingAddress=findElementInXLSheet(getParameterProcurementSmoke, "btnPOBillingAddress");
		txt_txtPOBillingAddress=findElementInXLSheet(getParameterProcurementSmoke, "txtPOBillingAddress");
		btn_btnPOShippingAddress=findElementInXLSheet(getParameterProcurementSmoke, "btnPOShippingAddress");
		txt_txtPOShippingAddress=findElementInXLSheet(getParameterProcurementSmoke, "txtPOShippingAddress");
		btn_btnPOApply=findElementInXLSheet(getParameterProcurementSmoke, "btnPOApply");
		btn_btnPOSerialNo=findElementInXLSheet(getParameterProcurementSmoke, "btnPOSerialNo");
		
		//PM_PRO_002
		btn_PObtnproduct=findElementInXLSheet(getParameterProcurementSmoke, "PObtnproduct");
		txt_POtxtproduct=findElementInXLSheet(getParameterProcurementSmoke, "POtxtproduct");
		sel_POselproduct=findElementInXLSheet(getParameterProcurementSmoke, "POselproduct");
		btn_POwarehouse=findElementInXLSheet(getParameterProcurementSmoke, "POwarehouse");
		txt_POSerialNo=findElementInXLSheet(getParameterProcurementSmoke, "POSerialNo");
		btn_POMasterInfo=findElementInXLSheet(getParameterProcurementSmoke, "POMasterInfo");
		btn_POProductBS=findElementInXLSheet(getParameterProcurementSmoke, "POProductB/S");
		txt_POSNo=findElementInXLSheet(getParameterProcurementSmoke, "POSNo");
		btn_POMasterInfoclose=findElementInXLSheet(getParameterProcurementSmoke, "POMasterInfoclose");
		btn_PROLook1=findElementInXLSheet(getParameterProcurementSmoke, "PROLook1");
		btn_PRILook1=findElementInXLSheet(getParameterProcurementSmoke, "PRILook1");
		txt_POProductAvailabilityValue1=findElementInXLSheet(getParameterProcurementSmoke, "POProductAvailabilityValue1");
		
		
		txt_pageDraft=findElementInXLSheet(getParameterProcurementSmoke, "pageDraft");
		txt_pageRelease=findElementInXLSheet(getParameterProcurementSmoke, "pageRelease");
		
		//PM_RFQ_001
		btn_RequestForQuotation=findElementInXLSheet(getParameterProcurementSmoke, "RequestForQuotation");
		btn_NewRequestForQuotation=findElementInXLSheet(getParameterProcurementSmoke, "NewRequestForQuotation");
		txt_RequestForQuotationpage=findElementInXLSheet(getParameterProcurementSmoke, "RequestForQuotationpage");
		txt_NewRequestForQuotationpage=findElementInXLSheet(getParameterProcurementSmoke, "NewRequestForQuotationpage");
		btn_btncontactperson=findElementInXLSheet(getParameterProcurementSmoke, "btncontactperson");
		txt_txtcontactperson=findElementInXLSheet(getParameterProcurementSmoke, "txtcontactperson");
		sel_selcontactperson=findElementInXLSheet(getParameterProcurementSmoke, "selcontactperson");
		btn_btnRFQVendor=findElementInXLSheet(getParameterProcurementSmoke, "btnRFQVendor");
		txt_txtRFQVendor=findElementInXLSheet(getParameterProcurementSmoke, "txtRFQVendor");
		sel_selRFQVendor=findElementInXLSheet(getParameterProcurementSmoke, "selRFQVendor");
		btn_btnRFQproduct=findElementInXLSheet(getParameterProcurementSmoke, "btnRFQproduct");
		txt_txtRFQproduct=findElementInXLSheet(getParameterProcurementSmoke, "txtRFQproduct");
		sel_selRFQproduct=findElementInXLSheet(getParameterProcurementSmoke, "selRFQproduct");
		txt_RFQqty=findElementInXLSheet(getParameterProcurementSmoke, "RFQqty");
		btn_RFQdeadline=findElementInXLSheet(getParameterProcurementSmoke, "RFQdeadline");
		sel_selRFQdeadline=findElementInXLSheet(getParameterProcurementSmoke, "selRFQdeadline");
		btn_QuotationTab=findElementInXLSheet(getParameterProcurementSmoke, "QuotationTab");
		btn_RFQadd=findElementInXLSheet(getParameterProcurementSmoke, "RFQadd");
		txt_RFQaddVendor=findElementInXLSheet(getParameterProcurementSmoke, "RFQaddVendor");
		txt_RFQQuotationNo=findElementInXLSheet(getParameterProcurementSmoke, "RFQQuotationNo");
		txt_RFQShippingTerm=findElementInXLSheet(getParameterProcurementSmoke, "RFQShippingTerm");
		txt_RFQUnitPrice=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice");
		txt_RFQMinQty=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty");
		txt_RFQMaxQty=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty");
		btn_RFQcheckout=findElementInXLSheet(getParameterProcurementSmoke, "RFQcheckout");
		btn_RFQapply=findElementInXLSheet(getParameterProcurementSmoke, "RFQapply");
		btn_RFQsummarytab=findElementInXLSheet(getParameterProcurementSmoke, "RFQsummarytab");
		btn_qualifiedvendorbutton=findElementInXLSheet(getParameterProcurementSmoke, "qualifiedvendorbutton");
		btn_Qualifiedtik=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik");
		btn_RFQQualifiedapply=findElementInXLSheet(getParameterProcurementSmoke, "RFQQualifiedapply");
		btn_Update=findElementInXLSheet(getParameterProcurementSmoke, "btnUpdate");
		btn_btnRFQapply=findElementInXLSheet(getParameterProcurementSmoke, "btnRFQapply");
		btn_btnbackground=findElementInXLSheet(getParameterProcurementSmoke, "bbtnbackground");
		btn_Edit=findElementInXLSheet(getParameterProcurementSmoke, "btnEdit");
		
		//SMOKE_PRO_03
		btn_ImportCostEstimation=findElementInXLSheet(getParameterProcurementSmoke, "ImportCostEstimation");
		ImportCostEstimationpage=findElementInXLSheet(getParameterProcurementSmoke, "ImportCostEstimationpage");
		btn_newImportCostEstimation=findElementInXLSheet(getParameterProcurementSmoke, "newImportCostEstimation");
		newImportCostEstimationpage=findElementInXLSheet(getParameterProcurementSmoke, "newImportCostEstimationpage");
		txt_ICEDescription=findElementInXLSheet(getParameterProcurementSmoke, "ICEDescription");
		btn_ICEProductbutoon=findElementInXLSheet(getParameterProcurementSmoke, "ICEProductbutoon");
		txt_ICEProducttxt=findElementInXLSheet(getParameterProcurementSmoke, "ICEProducttxt");
		sel_ICEProductsel=findElementInXLSheet(getParameterProcurementSmoke, "ICEProductsel");
		btn_ICEEstimationDetails=findElementInXLSheet(getParameterProcurementSmoke, "ICEEstimationDetails");
		txt_ICECostingType=findElementInXLSheet(getParameterProcurementSmoke, "ICECostingType");
		txt_ICEEstimateCost=findElementInXLSheet(getParameterProcurementSmoke, "ICEEstimateCost");
		btn_ICESummary=findElementInXLSheet(getParameterProcurementSmoke, "ICESummary");
		btn_ICECheckout=findElementInXLSheet(getParameterProcurementSmoke, "ICECheckout");
		btn_ICEDraft=findElementInXLSheet(getParameterProcurementSmoke, "ICEDraft");
		btn_ICERelease=findElementInXLSheet(getParameterProcurementSmoke, "ICERelease");
		
		//SMOKE_PRO_05
		btn_PurchaseDesk=findElementInXLSheet(getParameterProcurementSmoke, "PurchaseDesk");
		PurchaseDeskpage=findElementInXLSheet(getParameterProcurementSmoke, "PurchaseDeskpage");
		sel_Requisitionsel=findElementInXLSheet(getParameterProcurementSmoke, "Requisitionsel");
		txt_SearchPRN=findElementInXLSheet(getParameterProcurementSmoke, "SearchPRN");
		btn_SearchbuttonPRN=findElementInXLSheet(getParameterProcurementSmoke, "SearchbuttonPRN");
		
		//SMOKE_PRO_10
		btn_DocFlow=findElementInXLSheet(getParameterProcurementSmoke, "DocFlow");
		
		//SMOKE_PRO_15to17
		btn_ProcurementParametersBtnA =findElementInXLSheet(getParameterProcurementSmoke, "ProcurementParametersBtnA");
		btn_LicenseTypeParametersBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseTypeParametersBtnA");
		btn_LicenseTypeParametersAddBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseTypeParametersAddBtnA");
		txt_LicenseTypeParametersAddTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseTypeParametersAddTxtA");
		btn_LicenseInformationBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseInformationBtnA");
		btn_LicenseInformationNewBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseInformationNewBtnA");
		txt_LicenceTypeTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenceTypeTxtA");
		txt_LicencePiorityTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicencePiorityTxtA");
		txt_LicenceValidityPeriodTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenceValidityPeriodTxtA");
		txt_ProcessTimePeriodTxtA =findElementInXLSheet(getParameterProcurementSmoke, "ProcessTimePeriodTxtA");
		txt_PreLicenseTypeTxtA =findElementInXLSheet(getParameterProcurementSmoke, "PreLicenseTypeTxtA");
		txt_PostLicenseTypeTxtA =findElementInXLSheet(getParameterProcurementSmoke, "PostLicenseTypeTxtA");
		btn_LicenseProcessStepsTabA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseProcessStepsTabA");
		btn_ProcessStepsAddBtnA =findElementInXLSheet(getParameterProcurementSmoke, "ProcessStepsAddBtnA");
		txt_ProcessStepsAddTxtA =findElementInXLSheet(getParameterProcurementSmoke, "ProcessStepsAddTxtA");
		txt_ProcessStepsAddTimeTxtA =findElementInXLSheet(getParameterProcurementSmoke, "ProcessStepsAddTimeTxtA");
		btn_LicenseInformationDraftBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseInformationDraftBtnA");
		txt_LicenseInformationSearchTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseInformationSearchTxtA");
		txt_LicenseInformationStatusTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseInformationStatusTxtA");
		btn_LicenseRequestBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestBtnA");
		btn_NewLicenseRequestBtnA =findElementInXLSheet(getParameterProcurementSmoke, "NewLicenseRequestBtnA");
		txt_LicenseRequestTitleTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestTitleTxtA");
		txt_LicenseRequestLicenseTypeA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestLicenseTypeA");
		txt_LicenseRequestLicenseModeA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestLicenseModeA");
		btn_LicenseRequestRequestbyBtnA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestRequestbyBtnA");
		txt_LicenseRequestRequestbyTxtA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestRequestbyTxtA");
		sel_LicenseRequestRequestbySelA =findElementInXLSheet(getParameterProcurementSmoke, "LicenseRequestRequestbySelA");

		
		//product7
		
		PRPlus1=findElementInXLSheet(getParameterProcurementSmoke, "PRPlus1");
		PRPlus2=findElementInXLSheet(getParameterProcurementSmoke, "PRPlus2");
		PRPlus3=findElementInXLSheet(getParameterProcurementSmoke, "PRPlus3");
		PRPlus4=findElementInXLSheet(getParameterProcurementSmoke, "PRPlus4");
		PRPlus5=findElementInXLSheet(getParameterProcurementSmoke, "PRPlus5");
		PRPlus6=findElementInXLSheet(getParameterProcurementSmoke, "PRPlus6");
		PRPro2=findElementInXLSheet(getParameterProcurementSmoke, "PRPro2");
		PRPro3=findElementInXLSheet(getParameterProcurementSmoke, "PRPro3");
		PRPro4=findElementInXLSheet(getParameterProcurementSmoke, "PRPro4");
		PRPro5=findElementInXLSheet(getParameterProcurementSmoke, "PRPro5");
		PRPro6=findElementInXLSheet(getParameterProcurementSmoke, "PRPro6");
		PRPro7=findElementInXLSheet(getParameterProcurementSmoke, "PRPro7");
		
		POPlus1=findElementInXLSheet(getParameterProcurementSmoke, "POPlus1");
		POPlus2=findElementInXLSheet(getParameterProcurementSmoke, "POPlus2");
		POPlus3=findElementInXLSheet(getParameterProcurementSmoke, "POPlus3");
		POPlus4=findElementInXLSheet(getParameterProcurementSmoke, "POPlus4");
		POPlus5=findElementInXLSheet(getParameterProcurementSmoke, "POPlus5");
		POPlus6=findElementInXLSheet(getParameterProcurementSmoke, "POPlus6");
		POPro2=findElementInXLSheet(getParameterProcurementSmoke, "POPro2");
		POPro3=findElementInXLSheet(getParameterProcurementSmoke, "POPro3");
		POPro4=findElementInXLSheet(getParameterProcurementSmoke, "POPro4");
		POPro5=findElementInXLSheet(getParameterProcurementSmoke, "POPro5");
		POPro6=findElementInXLSheet(getParameterProcurementSmoke, "POPro6");
		POPro7=findElementInXLSheet(getParameterProcurementSmoke, "POPro7");
		
		Cap1=findElementInXLSheet(getParameterProcurementSmoke, "Cap1");
		Cap2=findElementInXLSheet(getParameterProcurementSmoke, "Cap2");
		Cap4=findElementInXLSheet(getParameterProcurementSmoke, "Cap4");
		Cap5=findElementInXLSheet(getParameterProcurementSmoke, "Cap5");
		Cap6=findElementInXLSheet(getParameterProcurementSmoke, "Cap6");
		Cap7=findElementInXLSheet(getParameterProcurementSmoke, "Cap7");

		BatchNu=findElementInXLSheet(getParameterProcurementSmoke, "BatchNu");
		BatchLotNu=findElementInXLSheet(getParameterProcurementSmoke, "BatchLotNu");
		BatchExpiryDate=findElementInXLSheet(getParameterProcurementSmoke, "BatchExpiryDate");
		SerialExpiryDate=findElementInXLSheet(getParameterProcurementSmoke, "SerialExpiryDate");
		BatchExpiryDateSel=findElementInXLSheet(getParameterProcurementSmoke, "BatchExpiryDateSel");
		SerialFromNu=findElementInXLSheet(getParameterProcurementSmoke, "SerialFromNu");
		SerialLotNu=findElementInXLSheet(getParameterProcurementSmoke, "SerialLotNu");
		SerialBatchNu=findElementInXLSheet(getParameterProcurementSmoke, "SerialBatchNu");
		
		PIPlus1=findElementInXLSheet(getParameterProcurementSmoke, "PIPlus1");
		PIPro1=findElementInXLSheet(getParameterProcurementSmoke, "PIPro1");
		
		RFQPro1=findElementInXLSheet(getParameterProcurementSmoke, "RFQPro1");
		RFQPlus1=findElementInXLSheet(getParameterProcurementSmoke, "RFQPlus1");
		RFQPlus2=findElementInXLSheet(getParameterProcurementSmoke, "RFQPlus2");
		RFQPlus3=findElementInXLSheet(getParameterProcurementSmoke, "RFQPlus3");
		RFQPlus4=findElementInXLSheet(getParameterProcurementSmoke, "RFQPlus4");
		RFQPlus5=findElementInXLSheet(getParameterProcurementSmoke, "RFQPlus5");
		RFQPlus6=findElementInXLSheet(getParameterProcurementSmoke, "RFQPlus6");
		
		RFQUnitPrice2=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice2");
		RFQUnitPrice3=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice3");
		RFQUnitPrice4=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice4");
		RFQUnitPrice5=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice5");
		RFQUnitPrice6=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice6");
		RFQUnitPrice7=findElementInXLSheet(getParameterProcurementSmoke, "RFQUnitPrice7");

		RFQMinQty2=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty2");
		RFQMinQty3=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty3");
		RFQMinQty4=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty4");
		RFQMinQty5=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty5");
		RFQMinQty6=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty6");
		RFQMinQty7=findElementInXLSheet(getParameterProcurementSmoke, "RFQMinQty7");
		
		RFQMaxQty2=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty2");
		RFQMaxQty3=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty3");
		RFQMaxQty4=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty4");
		RFQMaxQty5=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty5");
		RFQMaxQty6=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty6");
		RFQMaxQty7=findElementInXLSheet(getParameterProcurementSmoke, "RFQMaxQty7");

		Qualifiedtik2=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik2");
		Qualifiedtik3=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik3");
		Qualifiedtik4=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik4");
		Qualifiedtik5=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik5");
		Qualifiedtik6=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik6");
		Qualifiedtik7=findElementInXLSheet(getParameterProcurementSmoke, "Qualifiedtik7");

		nextdate=findElementInXLSheet(getParameterProcurementSmoke, "nextdate");
		btn_btnPOtoPurchaseInvoice=findElementInXLSheet(getParameterProcurementSmoke, "btnPOtoPurchaseInvoice");
		
		pro7qty1=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty1");
		pro7qty2=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty2");
		pro7qty3=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty3");
		pro7qty4=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty4");
		pro7qty5=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty5");
		pro7qty6=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty6");
		pro7qty7=findElementInXLSheet(getParameterProcurementSmoke, "pro7qty7");
		pro7price1=findElementInXLSheet(getParameterProcurementSmoke, "pro7price1");
		pro7price2=findElementInXLSheet(getParameterProcurementSmoke, "pro7price2");
		pro7price3=findElementInXLSheet(getParameterProcurementSmoke, "pro7price3");
		pro7price4=findElementInXLSheet(getParameterProcurementSmoke, "pro7price4");
		pro7price5=findElementInXLSheet(getParameterProcurementSmoke, "pro7price5");
		pro7price6=findElementInXLSheet(getParameterProcurementSmoke, "pro7price6");
		pro7price7=findElementInXLSheet(getParameterProcurementSmoke, "pro7price7");
		
		Warehouse1=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse1");
		Warehouse2=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse2");
		Warehouse3=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse3");
		Warehouse4=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse4");
		Warehouse5=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse5");
		Warehouse6=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse6");
		Warehouse7=findElementInXLSheet(getParameterProcurementSmoke, "Warehouse7");
		
		WarehousePI1=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI1");
		WarehousePI2=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI2");
		WarehousePI3=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI3");
		WarehousePI4=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI4");
		WarehousePI5=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI5");
		WarehousePI6=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI6");
		WarehousePI7=findElementInXLSheet(getParameterProcurementSmoke, "WarehousePI7");


		pageDraft=findElementInXLSheet(getParameterProcurementSmoke, "pageDraft");
		pageRelease=findElementInXLSheet(getParameterProcurementSmoke, "pageRelease");
		Status=findElementInXLSheet(getParameterProcurementSmoke, "Status");
		Header=findElementInXLSheet(getParameterProcurementSmoke, "Header");

		Product=findElementInXLSheet(getParameterProcurementSmoke, "Product");
		ProductQty=findElementInXLSheet(getParameterProcurementSmoke, "ProductQty");
		
		//Master_1
		
		btn_Delete=findElementInXLSheet(getParameterProcurementSmoke, "Delete");
		txt_DeleteText=findElementInXLSheet(getParameterProcurementSmoke, "DeleteText");
		btn_Yes=findElementInXLSheet(getParameterProcurementSmoke, "Yes");
		btn_History=findElementInXLSheet(getParameterProcurementSmoke, "History");
		txt_HistoryText=findElementInXLSheet(getParameterProcurementSmoke, "HistoryText");
		btn_Activities=findElementInXLSheet(getParameterProcurementSmoke, "Activities");
		txt_ActivitiesText=findElementInXLSheet(getParameterProcurementSmoke, "ActivitiesText");
		btn_headerclose=findElementInXLSheet(getParameterProcurementSmoke, "headerclose");
		btn_Duplicate=findElementInXLSheet(getParameterProcurementSmoke, "Duplicate");
		txt_lblVendorGroup=findElementInXLSheet(getParameterProcurementSmoke, "lblVendorGroup");
		txt_dupVendorGroup=findElementInXLSheet(getParameterProcurementSmoke, "dupVendorGroup");
		btn_btnEdit=findElementInXLSheet(getParameterProcurementSmoke, "btnEdit");
		txt_ReleseText=findElementInXLSheet(getParameterProcurementSmoke, "ReleseText");
		btn_DraftAndNew=findElementInXLSheet(getParameterProcurementSmoke, "Draft&New");
		btn_UpdateAndNew=findElementInXLSheet(getParameterProcurementSmoke, "Update&New");
		
		//Master_2
		btn_VendorGroupbtn=findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupbtn");
		btn_VendorGroupAdd=findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupAdd");
		txt_newVendorGroupTxt=findElementInXLSheet(getParameterProcurementSmoke, "newVendorGroupTxt");
		btn_VendorGroupUpdate=findElementInXLSheet(getParameterProcurementSmoke, "VendorGroupUpdate");
		btn_Reminders=findElementInXLSheet(getParameterProcurementSmoke, "Reminders");
		txt_RemindersText=findElementInXLSheet(getParameterProcurementSmoke, "RemindersText");
		btn_ActivitiesHeaderclose=findElementInXLSheet(getParameterProcurementSmoke, "ActivitiesHeaderclose");
		
		//Master_3
		btn_ConverttoPurchaseOrder=findElementInXLSheet(getParameterProcurementSmoke, "ConverttoPurchaseOrder");
		btn_Reverse=findElementInXLSheet(getParameterProcurementSmoke, "Reverse");
		txt_ReverseText=findElementInXLSheet(getParameterProcurementSmoke, "ReverseText");
		btn_CopyFrom=findElementInXLSheet(getParameterProcurementSmoke, "CopyFrom");
		txt_CopyFromText=findElementInXLSheet(getParameterProcurementSmoke, "CopyFromText");
		
		//Master_4
		btn_CustomerDemand=findElementInXLSheet(getParameterProcurementSmoke, "CustomerDemand");
		btn_CustomerDemandsel=findElementInXLSheet(getParameterProcurementSmoke, "CustomerDemandsel");
		btn_GenerateProductionOrder=findElementInXLSheet(getParameterProcurementSmoke, "GenerateProductionOrder");
		btn_GenerateAssemblyProposal=findElementInXLSheet(getParameterProcurementSmoke, "GenerateAssemblyProposal");
		txt_GenerateProductionOrdertxt=findElementInXLSheet(getParameterProcurementSmoke, "GenerateProductionOrdertxt");
		txt_GenerateAssemblyProposaltxt=findElementInXLSheet(getParameterProcurementSmoke, "GenerateAssemblyProposaltxt");

		
		//Transaction_3
		btn_Complete=findElementInXLSheet(getParameterProcurementSmoke, "Complete");
		txt_CompleteText=findElementInXLSheet(getParameterProcurementSmoke, "CompleteText");
		
		//Transaction_4
		btn_AddNewDocument=findElementInXLSheet(getParameterProcurementSmoke, "AddNewDocument");
		ZoomBar=findElementInXLSheet(getParameterProcurementSmoke, "ZoomBar");
		btn_No=findElementInXLSheet(getParameterProcurementSmoke, "No");
		btn_ReverseButton=findElementInXLSheet(getParameterProcurementSmoke, "ReverseButton");
		btn_Print=findElementInXLSheet(getParameterProcurementSmoke, "Print");
		txt_PrintoutTemplates=findElementInXLSheet(getParameterProcurementSmoke, "PrintoutTemplates");
		btn_Back=findElementInXLSheet(getParameterProcurementSmoke, "Back");
		btn_RackwiseAvailable=findElementInXLSheet(getParameterProcurementSmoke, "RackwiseAvailable");
		
		//Transaction_5
		btn_Acknowledgement=findElementInXLSheet(getParameterProcurementSmoke, "Acknowledgement");
		txt_Acknowledgementtxt=findElementInXLSheet(getParameterProcurementSmoke, "Acknowledgementtxt");
		btn_ChangeCostAllocations=findElementInXLSheet(getParameterProcurementSmoke, "ChangeCostAllocations");
		txt_ChangeCostAllocationstxt=findElementInXLSheet(getParameterProcurementSmoke, "ChangeCostAllocationstxt");
		txt_PurchaseInvoicetxt=findElementInXLSheet(getParameterProcurementSmoke, "PurchaseInvoicetxt");

		//Transaction_6
		btn_CostEstimation=findElementInXLSheet(getParameterProcurementSmoke, "CostEstimation");
		txt_CostEstimationtxt=findElementInXLSheet(getParameterProcurementSmoke, "CostEstimationtxt");
		btn_Close=findElementInXLSheet(getParameterProcurementSmoke, "Close");
		txt_Closetxt=findElementInXLSheet(getParameterProcurementSmoke, "Closetxt");
		btn_Hold=findElementInXLSheet(getParameterProcurementSmoke, "Hold");
		btn_DotheOutboundAdvance=findElementInXLSheet(getParameterProcurementSmoke, "DotheOutboundAdvance");
		txt_DotheOutboundAdvancetxt=findElementInXLSheet(getParameterProcurementSmoke, "DotheOutboundAdvancetxt");
		btn_ExtendExpiryDate=findElementInXLSheet(getParameterProcurementSmoke, "ExtendExpiryDate");
		txt_ExtendExpiryDatetxt=findElementInXLSheet(getParameterProcurementSmoke, "ExtendExpiryDatetxt");
		btn_UpdateLogisticDates=findElementInXLSheet(getParameterProcurementSmoke, "UpdateLogisticDates");
		txt_UpdateLogisticDatestxt=findElementInXLSheet(getParameterProcurementSmoke, "UpdateLogisticDatestxt");
		btn_UnHold=findElementInXLSheet(getParameterProcurementSmoke, "UnHold");
		txt_Reason=findElementInXLSheet(getParameterProcurementSmoke, "Reason");
		btn_OK=findElementInXLSheet(getParameterProcurementSmoke, "OK");
		txt_PurchaseOrdertxt=findElementInXLSheet(getParameterProcurementSmoke, "PurchaseOrdertxt");
		btn_ImportShipment=findElementInXLSheet(getParameterProcurementSmoke, "ImportShipment");
		txt_ImportShipmenttxt=findElementInXLSheet(getParameterProcurementSmoke, "ImportShipmenttxt");
		btn_CostEstimationClose=findElementInXLSheet(getParameterProcurementSmoke, "CostEstimationClose");
		
		//Transaction_8
		btn_GeneratePurchaseOrder=findElementInXLSheet(getParameterProcurementSmoke, "GeneratePurchaseOrder");

		//Master_001
		btn_SalesAndMarketing=findElementInXLSheet(getParameterProcurementSmoke, "SalesAndMarketing");
		btn_AccountGroupConfiguration=findElementInXLSheet(getParameterProcurementSmoke, "AccountGroupConfiguration");
		btn_NewAccountGroupConfiguration=findElementInXLSheet(getParameterProcurementSmoke, "NewAccountGroupConfiguration");
		btn_addAccountGroup=findElementInXLSheet(getParameterProcurementSmoke, "addAccountGroup");
		btn_addNewAccountGroup=findElementInXLSheet(getParameterProcurementSmoke, "addNewAccountGroup");
		txt_NewAccountGrouptxt=findElementInXLSheet(getParameterProcurementSmoke, "NewAccountGrouptxt");
		btn_AccountGroupUpdate=findElementInXLSheet(getParameterProcurementSmoke, "AccountGroupUpdate");
		sel_AccountGroupsel=findElementInXLSheet(getParameterProcurementSmoke, "AccountGroupsel");
		
		btn_InventoryAndWarehousing=findElementInXLSheet(getParameterProcurementSmoke, "InventoryAndWarehousing");
		btn_WarehouseInformation=findElementInXLSheet(getParameterProcurementSmoke, "WarehouseInformation");
		btn_NewWarehouse=findElementInXLSheet(getParameterProcurementSmoke, "NewWarehouse");
		txt_WarehouseCode=findElementInXLSheet(getParameterProcurementSmoke, "WarehouseCode");
		txt_WarehouseName=findElementInXLSheet(getParameterProcurementSmoke, "WarehouseName");
		txt_QuaranWarehouse=findElementInXLSheet(getParameterProcurementSmoke, "QuaranWarehouse");
		btn_AutoLot=findElementInXLSheet(getParameterProcurementSmoke, "AutoLot");
		txt_LotBookNo=findElementInXLSheet(getParameterProcurementSmoke, "LotBookNo");
		btn_AgreementGroupAdd=findElementInXLSheet(getParameterProcurementSmoke, "AgreementGroupAdd");
		btn_AgreementGroupParameters=findElementInXLSheet(getParameterProcurementSmoke, "AgreementGroupParameters");
		txt_AgreementGroupParameterstxt=findElementInXLSheet(getParameterProcurementSmoke, "AgreementGroupParameterstxt");
		btn_AgreementGroupParametersUpdate=findElementInXLSheet(getParameterProcurementSmoke, "AgreementGroupParametersUpdate");
		

	}
	
	public static void readData() throws Exception
	{
		//Com_TC_001
		siteURL = findElementInXLSheet(getDataProcurementSmoke,"site url");
		userNameData=findElementInXLSheet(getDataProcurementSmoke,"user name data");
		passwordData=findElementInXLSheet(getDataProcurementSmoke,"password data");
		

		//PR_VGC_002
		vendorgroup = findElementInXLSheet(getDataProcurementSmoke, "vendorgroup");
		currency = findElementInXLSheet(getDataProcurementSmoke, "currency");
		PurchaseTaxGroup=findElementInXLSheet(getDataProcurementSmoke, "PurchaseTaxGroup");
		VendorClassification=findElementInXLSheet(getDataProcurementSmoke, "VendorClassification");
		StandardDeliveryMode=findElementInXLSheet(getDataProcurementSmoke, "StandardDeliveryMode");
		StandardDeliveryTerm=findElementInXLSheet(getDataProcurementSmoke, "StandardDeliveryTerm");
		Warehouse=findElementInXLSheet(getDataProcurementSmoke, "Warehouse");
		PaymentTerm=findElementInXLSheet(getDataProcurementSmoke, "PaymentTerm");
		Tolerance =findElementInXLSheet(getDataProcurementSmoke, "Tolerance ");
		CreditDays=findElementInXLSheet(getDataProcurementSmoke, "CreditDays");
		CreditLimit=findElementInXLSheet(getDataProcurementSmoke, "CreditLimit") ;
		//PR_VI_002
		VendorType =findElementInXLSheet(getDataProcurementSmoke, "VendorType") ; 
		VendorNameTitle =findElementInXLSheet(getDataProcurementSmoke, "VendorNameTitle") ; 
		VendorName=findElementInXLSheet(getDataProcurementSmoke, "VendorName") ; 
		txtReceivableAccount=findElementInXLSheet(getDataProcurementSmoke, "txtReceivableAccount") ;
		ContactTitle=findElementInXLSheet(getDataProcurementSmoke, "ContactTitle") ;
		ContactName=findElementInXLSheet(getDataProcurementSmoke, "ContactName") ;
		MobileNumber=findElementInXLSheet(getDataProcurementSmoke, "MobileNumber");
		SelectIndustry=findElementInXLSheet(getDataProcurementSmoke, "SelectIndustry");
		TaxRegistrationNumber=findElementInXLSheet(getDataProcurementSmoke, "TaxRegistrationNumber");
		ReferenceAccoutNo=findElementInXLSheet(getDataProcurementSmoke, "ReferenceAccoutNo");
		txt3rdPartyAccount=findElementInXLSheet(getDataProcurementSmoke, "txt3rdPartyAccount");
		PassportNo=findElementInXLSheet(getDataProcurementSmoke, "PassportNo");
		newReceivableAccountname=findElementInXLSheet(getDataProcurementSmoke, "newReceivableAccountname");
		vendorgroupforven=findElementInXLSheet(getDataProcurementSmoke, "vendorgroupforven");
		//PM_VA_001
		AgreementNo=findElementInXLSheet(getDataProcurementSmoke, "AgreementNo");
		AgreementTitle=findElementInXLSheet(getDataProcurementSmoke, "AgreementTitle");
		AgreementDate=findElementInXLSheet(getDataProcurementSmoke, "AgreementDate");
		AgreementGroup=findElementInXLSheet(getDataProcurementSmoke, "AgreementGroup");
		Acurrency=findElementInXLSheet(getDataProcurementSmoke, "Acurrency");
		ValidityPeriodFor=findElementInXLSheet(getDataProcurementSmoke, "ValidityPeriodFor");
		Description=findElementInXLSheet(getDataProcurementSmoke, "Description");
		txtVendorAccount=findElementInXLSheet(getDataProcurementSmoke, "txtVendorAccount");
		ProductRelation=findElementInXLSheet(getDataProcurementSmoke, "ProductRelation");
		txtproduct=findElementInXLSheet(getDataProcurementSmoke, "txtproduct");
		minOrderQty=findElementInXLSheet(getDataProcurementSmoke, "minOrderQty");
		maxOrderQty=findElementInXLSheet(getDataProcurementSmoke, "maxOrderQty");
		txtAssignTo=findElementInXLSheet(getDataProcurementSmoke, "txtAssignTo");
		txtsubject=findElementInXLSheet(getDataProcurementSmoke, "txtsubject");
		
		//PM_PR_002

		RequestDate=findElementInXLSheet(getDataProcurementSmoke, "RequestDate");
		RCurrency=findElementInXLSheet(getDataProcurementSmoke, "RCurrency");
		Rtxtproduct=findElementInXLSheet(getDataProcurementSmoke, "Rtxtproduct");
		
		//PM_PR_005
		txtVendor=findElementInXLSheet(getDataProcurementSmoke, "txtVendor");
		txtBillingAddress=findElementInXLSheet(getDataProcurementSmoke, "txtBillingAddress");
		txtShippingAddress=findElementInXLSheet(getDataProcurementSmoke, "txtShippingAddress");
		
		//PM_PO_001
		ptxtproduct=findElementInXLSheet(getDataProcurementSmoke, "ptxtproduct");
		Count=findElementInXLSheet(getDataProcurementSmoke, "Count");
		TestQuantity=findElementInXLSheet(getDataProcurementSmoke, "TestQuantity");
		
		//PM_PO_002
		ptxtSproduct=findElementInXLSheet(getDataProcurementSmoke, "ptxtSproduct");
		txtVendorReferenceNo=findElementInXLSheet(getDataProcurementSmoke, "txtVendorReferenceNo");
		
		//PM_PO_006
		ptxtNxtproduct= findElementInXLSheet(getDataProcurementSmoke, "ptxtNxtproduct");
		txtCEstDescription= findElementInXLSheet(getDataProcurementSmoke, "txtCEstDescription");
		EstimateCost= findElementInXLSheet(getDataProcurementSmoke, "EstimateCost");
		ptxtBproduct= findElementInXLSheet(getDataProcurementSmoke, "ptxtBproduct");
		SerialNo= findElementInXLSheet(getDataProcurementSmoke, "SerialNo");
		BatchNo= findElementInXLSheet(getDataProcurementSmoke, "BatchNo");
		ExpiryDate = findElementInXLSheet(getDataProcurementSmoke, "ExpiryDate");
		
		//PM_PO_007
		txtQESTDescription= findElementInXLSheet(getDataProcurementSmoke, "txtQESTDescription");
		EstimatedQty= findElementInXLSheet(getDataProcurementSmoke, "EstimatedQty");

		
		//PM_PI_001
		PIcurrency= findElementInXLSheet(getDataProcurementSmoke, "PIcurrency");
		txtPIVendor= findElementInXLSheet(getDataProcurementSmoke, "txtPIVendor");
		txtPIBillingAddress= findElementInXLSheet(getDataProcurementSmoke, "txtPIBillingAddress");
		txtPIShippingAddress= findElementInXLSheet(getDataProcurementSmoke, "txtPIShippingAddress");
		PItxtproduct= findElementInXLSheet(getDataProcurementSmoke, "PItxtproduct");
		PItxtNxtproduct= findElementInXLSheet(getDataProcurementSmoke, "PItxtNxtproduct");
		
		//PM_PI_003
		PItxtSproduct= findElementInXLSheet(getDataProcurementSmoke, "PItxtSproduct");
		EstimatenewCost= findElementInXLSheet(getDataProcurementSmoke, "EstimatenewCost");
		
		//PM_PRO_001
		txtPROVendor= findElementInXLSheet(getDataProcurementSmoke, "txtPROVendor");
		PROqty= findElementInXLSheet(getDataProcurementSmoke, "PROqty");
		txtPOBillingAddress= findElementInXLSheet(getDataProcurementSmoke, "txtPOBillingAddress");
		txtPOShippingAddress= findElementInXLSheet(getDataProcurementSmoke, "txtPOShippingAddress");
		
		//PM_PRO_002
		POtxtproduct= findElementInXLSheet(getDataProcurementSmoke, "POtxtproduct");
		POwarehouse= findElementInXLSheet(getDataProcurementSmoke, "POwarehouse");
		POSerialNo= findElementInXLSheet(getDataProcurementSmoke, "POSerialNo");
		//PM_RFQ_001
		txtcontactperson= findElementInXLSheet(getDataProcurementSmoke, "txtcontactperson");
		txtRFQVendor= findElementInXLSheet(getDataProcurementSmoke, "txtRFQVendor");
		txtRFQproduct= findElementInXLSheet(getDataProcurementSmoke, "txtRFQproduct");
		RFQqty= findElementInXLSheet(getDataProcurementSmoke, "RFQqty");
		RFQQuotationNo= findElementInXLSheet(getDataProcurementSmoke, "RFQQuotationNo");
		RFQUnitPrice= findElementInXLSheet(getDataProcurementSmoke, "RFQUnitPrice");
		RFQMinQty= findElementInXLSheet(getDataProcurementSmoke, "RFQMinQty");
		RFQMaxQty= findElementInXLSheet(getDataProcurementSmoke, "RFQMaxQty");
		
		
		//SMOKE_PRO_03
		ICEDescription= findElementInXLSheet(getDataProcurementSmoke, "ICEDescription");
		ICEProducttxt= findElementInXLSheet(getDataProcurementSmoke, "ICEProducttxt");
		ICEEstimateCost= findElementInXLSheet(getDataProcurementSmoke, "ICEEstimateCost");
		
		//SMOKE_PRO_15to17
		LicencePiorityTxtA= findElementInXLSheet(getDataProcurementSmoke, "LicencePiorityTxtA");
		ProcessStepsAddTxt1A= findElementInXLSheet(getDataProcurementSmoke, "ProcessStepsAddTxt1A");
		ProcessStepsAddTxt2A= findElementInXLSheet(getDataProcurementSmoke, "ProcessStepsAddTxt2A");
		LicenseRequestLicenseModeA= findElementInXLSheet(getDataProcurementSmoke, "LicenseRequestLicenseModeA");
		LicenseRequestRequestbyTxtA= findElementInXLSheet(getDataProcurementSmoke, "LicenseRequestRequestbyTxtA");
		
		//Master_001
		WarehouseCode= findElementInXLSheet(getDataProcurementSmoke, "WarehouseCode");
		WarehouseName= findElementInXLSheet(getDataProcurementSmoke, "WarehouseName");
		AgreementGroupParameterstxt= findElementInXLSheet(getDataProcurementSmoke, "AgreementGroupParameterstxt");
		

	}


	
}
