package bileeta.BATF.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import bileeta.BTAF.Utilities.TestBase;

public class SalesAndMarketingModuleData extends TestBase {
	/* Reading the element locators to variables */
	
	protected static String siteURL;
	protected static String siteLogo;
	protected static String txt_username;
	protected static String txt_password;
	protected static String btn_login;
	protected static String setProcessDate;
	protected static String lnk_home;
	protected static String sales_Marketing;

	protected static String nav_btn;

	protected static String userNameData;
	protected static String passwordData;

	// stock Adjustment

	protected static String stockAdjustment;
	protected static String newStockAdjustment;
	protected static String descriptionTxtStockAdj;
	protected static String warehouseStockAdj;
	protected static String productLookUpBatchSpecific;
	protected static String productValTxt;
	protected static String selectedProduct;
	protected static String qtyTxtBatchSpecific;

	protected static String addNewBatchFifo;
	protected static String productLookUpBatchFifo;
	protected static String qtyTxtBatchFifo;
	protected static String addNewLot;
	protected static String productLookUpLot;
	protected static String qtyTxtLot;
	protected static String addNewSerialSpecific;
	protected static String productLookUpSerialSpecific;
	protected static String qtyTxtSerialSpecific;
	protected static String addNewSerialBatchSpec;
	protected static String productLookUpSerialBatchSpec;
	protected static String qtyTxtSerialBatchSpec;
	protected static String addNewSerialBthFifo;
	protected static String productLookUpSerialBatchFifo;
	protected static String qtyTxtSerialBatchFifo;
	protected static String addNewSeriFifo;
	protected static String productLookUpSerialFifo;
	protected static String qtyTxtSerialFifo;

	protected static String batchSpecificItem;
	protected static String serialSpecificItem;
	protected static String serialBatchSpecificItem;
	protected static String serialBatchFifoItem;
	protected static String serialFifoItem;
	protected static String lotItem;
	protected static String rangeSerialSpecific;
	protected static String serialNoSerialSpecific;
	protected static String lotNoSerialSpecific;
	protected static String expiryDateSerialSpecific;
	protected static String countText;
	protected static String batchFifoItem;
	protected static String batchNoTxtBatchSpecific;
	protected static String lotNoBatchSpecific;
	protected static String expiryDateBatchSpecific;
	protected static String expiryDateValBatchSpec;
	protected static String nextMonth;
	protected static String manufacDateSerialSpec;
	protected static String manufacDate;
	protected static String batchNoTxtSerialBatchSpecific;

	protected static String lotNoText;

	// Sales Order -> Outbound Shipment -> Sales Invoice -- 1
	protected static String searchBar;

	protected static String navMenuBtn;
	protected static String salesAndMarketing;
	protected static String salesOrder;
	protected static String salesOrderHeader;
	protected static String newSalesOrder;
	protected static String journey1;
	protected static String salesOrderToSalesInvoice;
	protected static String salesOrderToSalesInvoiceSer;
	protected static String customerAccountSearch;
	protected static String customerAccountText;
	protected static String selected_val;
	protected static String currencyDropdown;
	protected static String billingAddressSearch;
	protected static String billingAddressText;
	protected static String shippingAddressText;
	protected static String apply_btn;
	protected static String salesUnit;
	protected static String accountOwner;
	protected static String productLookup;
	protected static String productLookupSerialSpec;
	protected static String productLookupInv;
	protected static String productLookupBatchSpecific;
	protected static String productLookupBatchFIFO;
	protected static String productLookupSerialBatchSpecific;
	protected static String productLookupSerialBatchFifo;
	protected static String productLookupSerialFifo;
	protected static String productTextInv;
	protected static String productTextSer;
	protected static String productTextSerialSpec;
	protected static String searchInv;
	protected static String proSelectedVAl;
	protected static String invSelectedVal;
	protected static String InvenValueSalesOrder;
	protected static String outboundInvenValue;
	protected static String accountNameOutbound;
	protected static String searchButton;
	protected static String unitPriceTxt;
	protected static String unitPriceInve;
	protected static String unitPriceTxtService;
	protected static String unitPriceSerialSpec;
	protected static String unitPriceBatchSpecific;
	protected static String unitPriceBatchFifo;
	protected static String unitPriceSerialBatchSpec;
	protected static String unitPriceSerialBatchFifo;
	protected static String unitPriceSerialFifo;
	protected static String addNewRecord;
	protected static String addNewRecordSerialSpecific;
	protected static String addNewBatchSpecific;
	protected static String addNewSerialBatchFifo;
	protected static String addNewBatchFIFO;
	protected static String addNewSerialFifo;
	protected static String invenSearch;
	protected static String wareHouse;
	protected static String wareHouseBatchSpecific;
	protected static String wareHouseBatchFifo;
	protected static String wareHouseSerialSpecific;
	protected static String wareHouseSerialFifo;
	protected static String wareHouseSerialBatchFifo;
	protected static String wareHouseSerialBatchSpecific;
	protected static String row1;
	protected static String qtyText;
	protected static String qtyTextInve;
	protected static String qtyTextService;
	protected static String qtyTextSerialSpec;
	protected static String qtyTextBatchSpecific;
	protected static String qtyTextBatchFifo;
	protected static String qtyTxtSerialBatchSpecific;
	protected static String qtyTextSerialBatchFifo;
	protected static String qtyTextSerialFifo;
	protected static String bannerTotal1;
	protected static String bottomTotal;
	protected static String serialBatch;
	protected static String productListBtn;
	protected static String productListBtn2;
	protected static String range;
	protected static String count;
	protected static String serialText;
	protected static String updateBtn;
	protected static String serialbatchSpecificListBtn;
	protected static String batchNoTextSerialBatchSpecific;
	protected static String batchSpecificListBtn;
	protected static String backBtn;
	protected static String batchNoText;
	protected static String entutionLink;
	protected static String outboundLink;
	protected static String gotoPageWindow;
	protected static String invoiceLink;
	protected static String checkoutSalesOrder;
	protected static String draft;
	protected static String release;
	protected static String releaseInHeader;
	protected static String documentVal;
	protected static String serviceProductValOutbound;
	protected static String totalSalesInvoice;
	protected static String checkoutOutbound;
	protected static String outboundDraft;
	protected static String outboundRelease;
	protected static String checkoutInvoice1;
	protected static String taskAndEvent;
	protected static String salesInvoiceBtn;
	protected static String newSalesInvoice;
	protected static String salesInvoiceToOutboundShipment;
	protected static String customerAccountSearchInv;
	protected static String menu;
	protected static String serialWise;
	protected static String serialTextVal;
	protected static String serialTextVal2;
	protected static String batchSpecificBatchNo;
	protected static String serialBatchSpec;
	protected static String serialMenuClose;
	protected static String menuBatchSpecific;
	protected static String menuSerialBatchSpec;

	protected static String serviceOut;
	protected static String serialSpecOut;
	protected static String batchSpeccOut;
	protected static String batchFifoOut;
	protected static String serialBatchSpecOut;
	protected static String serialbatchFifoOut;
	protected static String serialFifoOut;

	// Verify that user is able to create sales order (Sales order to Sales invoice
	// [Service]) ---- 2
	protected static String journey2;
	protected static String customerAccSearchService;
	protected static String customerSearchTxt;
	protected static String customerSelectedValue;
	protected static String currencyService;
	protected static String billingAddressService;
	protected static String billingAddressTxt;
	protected static String shippingAddressTxt;
	protected static String applyBtnService;
	protected static String salesUnitService;
	protected static String productLookupService;
	protected static String productTxtService;
	protected static String selectedValueSearch;
	protected static String unitPriceServicePro;
	protected static String checkoutService;
	protected static String draftService;
	protected static String releaseService;
	protected static String linkSalesInvoiceService;
	protected static String checkoutInvoiceService;
	protected static String serSalesInvoice;
	protected static String draftInvoiceService;
	protected static String releaseInvoiceService;

	// Sales Order -> Outbound Shipment -- 3
	protected static String journey3;
	protected static String customerAccountOutbound;
	protected static String customerAccountTxtOutbound;
	protected static String selectedValAcccountOutbound;
	protected static String currencyOutbound;
	protected static String billingAddressSearchOutbound;
	protected static String billingAddrTxtOutbound;
	protected static String shippingAddressSearchOutbound;
	protected static String shippingAddrTxtOutbound;
	protected static String applyAddress;
	protected static String salesUnitOutbound;
	protected static String productSearchOutboundSer;
	protected static String productSearchTxtOutboundSer;
	protected static String selectedRowSer;
	protected static String unitPriceSer;
	protected static String unitPriceInv;
	protected static String addNewProductOutboundInv;
	protected static String productSearchOutboundInv;
	protected static String productSearchTxtOutboundInv;
	protected static String warehouse;
	protected static String selectedRowInv;
	protected static String checkOutbound;
	protected static String draftOutbound;
	protected static String releaseOutbound;
	protected static String linkSalesInvoice;
	protected static String checkoutInvoice;
	protected static String draftInvoice;
	protected static String linkOutbound;
	// protected static String checkOutbound;
	protected static String draftutbound;
	// protected static String releaseOutbound;

	// Sales Order -> Sales Invoice [Drop Shipment] -- 4
	protected static String journey4;
	protected static String selectedValDropSer;
	protected static String selectedValDropInv;
	protected static String unitPriceInven;
	protected static String actionMenu;
	protected static String delete;
	protected static String documentFlow;
	protected static String convertToPurchaseOrder;
	protected static String vendorLookup;
	protected static String vendorText;
	protected static String vendorSelectedVal;
	protected static String salesOrderDropShipment;

	// Sales Invoice -> Outbound Shipment -- 5
	protected static String salesInvoice;
	protected static String newSalesInv;
	protected static String invoiceOutboundShipment;
	protected static String journeySalesInvoice1;
	protected static String cusAccount;
	protected static String productLookupInvoice;
	protected static String productLookupSerialSpecSI;
	protected static String productLookUpInven;
	protected static String productLookupBatchSpecificSI;
	protected static String productLookupBatchFIFOSI;
	protected static String productLookupSerialBatchSpecificSI;
	protected static String productLookupSerialBatchFifoSI;
	protected static String productLookupSerialFifoSI;
	protected static String serSelectedVAl;
	protected static String unitPriceTxtSISer;
	protected static String unitPriceSerialSpecSI;
	protected static String unitPriceBatchSpecificSI;
	protected static String unitPriceBatchFifoSI;
	protected static String qtyTextSISer;
	protected static String qtyTextSerialSpecSI;
	protected static String qtyTextBatchSpecificSI;
	protected static String qtyTextBatchFifoSI;
	protected static String wareHouseSalesInvoice;
	protected static String wareHouseSerialSpecificSI;
	protected static String wareHouseBatchSpecificSI;
	protected static String wareHouseBatchFifoSI;
	protected static String wareHouseSerialBatchSpecSI;
	protected static String wareHouseSerialBtchFifoSI;
	protected static String wareHouseSerialFifoSI;
	protected static String unitPriceSerialBatchSpecSI;
	protected static String unitPriceSerialBatchFifoSI;
	protected static String unitPriceSerialFifoSI;
	protected static String qtyTextSerialBtchSpecSI;
	protected static String qtyTextSerialBatchFifoSI;
	protected static String qtyTextSerialFifoSI;
	protected static String addNewRecordSerialSpecSI;
	protected static String addNewBatchSpecificSI;
	protected static String addNewBatchFIFOSI;
	protected static String addNewSerBatchSpecificSI;
	protected static String addNewSerialBatchFifoSI;
	protected static String addNewSerialFifoSI;
	protected static String menuSerial;
	protected static String menuClose;
	protected static String checkoutInvoiceOut;
	protected static String menuBatchSpecificSI;
	protected static String menuSerialBatchSpecSI;
	protected static String productListBtnSerialSpecific;
	protected static String productList;
	protected static String warehouseInv;

	// Sales Invoice -- Service -- 6
	protected static String salesInvoiceService;
	protected static String newSalesInvoiceBtn;
	protected static String salesInvoiceSerJourney;
	protected static String cusAccountSearch;
	protected static String productLookupBtn;
	protected static String unitPriceService;
	protected static String warehouseSer;
	protected static String checkoutSalesInvoice;
	protected static String serviceSelected;
	protected static String qtyServiceInvoice;
	protected static String addNewProduct;
	protected static String totalSalesInvoiceSer;
	protected static String productSearchOutbound;
	protected static String productSearchTxt;
	protected static String proSelectedVal;

	// Sales Quatation with a Lead -- 7

	protected static String salesQuatationBtn;
	protected static String newSalesQuatation;
	protected static String journey1Quatation;
	protected static String accountHead;
	protected static String customerAccSearchQuat;
	protected static String selectedValLead;
	protected static String billingAddrSearch;
	protected static String billingAddrTxtQuat;
	protected static String shippingAddrTxtQuat;
	protected static String productSearchQuatation;
	protected static String selectedValQuatInv;
	protected static String selectedValQuatSer;
	protected static String qtyQuatationSer;
	protected static String qtyQuatationInv;
	protected static String unitpriceQuatation;
	protected static String unitpriceQuatationInv;
	protected static String unitpriceBatchSpecQout;
	protected static String unitpriceBatchFifoQout;
	protected static String unitpriceSerialBatchSpecQout;
	protected static String unitpriceSerBatchFifoQout;
	protected static String unitpriceSerialFifoQout;
	protected static String qtyBatchSpecQuot;
	protected static String qtyBatchFifoQuot;
	protected static String qtySerialBatchSpecQuot;
	protected static String qtySerBatchFifoQuot;
	protected static String qtySerialFifoQuot;
	protected static String addNew;
	protected static String addNewBatchSpecificQuot;
	protected static String addNewBatchFifoQuot;
	protected static String addNewSerialBatchSpecQuot;
	protected static String addNewSerBatchFifoQuot;
	protected static String addNewSerialFifoQuot;
	protected static String searchBtnQuata;
	protected static String searchBatchSpecQuot;
	protected static String searchBatchFifoQuot;
	protected static String searchSerialBatchSpecQuot;
	protected static String searchSerBatchFifoQuot;
	protected static String searchSerialFifoQuot;
	protected static String checkoutQuatation;
	protected static String draftQuatation;
	protected static String releaseQuatation;
	protected static String versionBtn;
	protected static String versionTab;
	protected static String tickConfirmVersion;
	protected static String confirmVersion;
	protected static String confirmBtn;
	protected static String linkQuatation;
	protected static String warehouseSerialSpecificQuot;
	protected static String warehouseBatchSpecificQuot;
	protected static String warehouseBatchFifoQuot;
	protected static String warehouseSerBatchSpecQuot;
	protected static String warehouseSerialBatchFifoQuot;
	protected static String warehouseSerialFifoQuot;
	protected static String menuQuot;
	protected static String menuBatchSpec;
	protected static String menuSerialBatchSpecQuot;
	protected static String batchNo;
	protected static String serialBatchNo;
	protected static String checkoutSalesOrderQuata;
	protected static String draftSalesOrderQuata;
	protected static String releaseSalesOrderQuata;
	protected static String linkSalesOrder;
	protected static String checkoutOutboundquata;
	protected static String draftOutboundquata;
	protected static String serialOutboundQuata;
	protected static String sideMenuQuat;
	protected static String batchSpecificQuat;
	protected static String batchNoTxtBatchSpec;
	protected static String TextSerialBatchSpec;
	protected static String serialBatchSpecItem;
	protected static String batchNoTxtQuat;
	protected static String updateBtnQuat;
	protected static String backBtnQuat;
	protected static String serialTextQuat;

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Sales Invoice - Drop Shipment) -- 8
	protected static String journey2Quatation;
	protected static String actionBtn;
	protected static String convert;
	protected static String vendorSearch;
	protected static String vendorTextt;
	protected static String selectedValQuat2;

	// Verify that user is able to create sales Quotation with a lead(Sales
	// Quotation to Outbound Shipment) ---9

	protected static String journey3Quatation;
	protected static String warehouse3quat;
	protected static String menuOne;
	protected static String menuBatchSpecic;
	protected static String menuSerialBatchSpecQuot3;

	// Verify that user is able to create Sales Return Order (Sales Return Order to
	// Sales Return Invoice) -- 10

	protected static String salesReturnOrder;
	protected static String newSalesReturnOrder;
	protected static String salesReturnJourney1;
	protected static String customerAccSearchReturn;
	protected static String customerAccTxtReturn;
	protected static String selectedValCustomerReturn;
	protected static String currencyReturn;
	protected static String contactPersonReturn;
	protected static String billingAddrReturnSearch;
	protected static String billingAddrTxtReturn;
	protected static String shippingAddrTxtReturn;
	protected static String salesUnitReturn;
	protected static String documentListReturn;
	protected static String fromDateReturn;
	protected static String fromDateSelected;
	protected static String toDateReturn;
	protected static String toDateSelected;
	protected static String searchBtnReturn;
	protected static String tickSalesOrder;
	protected static String applyReturn;
	protected static String checkDocumentListRet2;
	protected static String checkDocumentListRet3;
	protected static String checkDocumentListRet4;
	protected static String checkDocumentListRet5;
	protected static String checkDocumentListRet6;
	protected static String checkDocumentListRet7;
	protected static String returnReason;
	protected static String dispositionCategory;
	protected static String checkoutReturn;
	protected static String draftReturn;
	protected static String releaseReturn;
	protected static String linkReturn;
	protected static String checkoutInboundshipment;
	protected static String draftInbound;
	protected static String serialInbound;

	protected static String listInbound1;
	protected static String listInbound2;
	protected static String listInbound3;
	protected static String listInbound4;
	protected static String listInbound5;
	protected static String listInbound6;
	protected static String checkBox;
	protected static String updateInbound;
	protected static String backInbound;
	protected static String releaseInbound;
	protected static String linkReturnInvoice;
	protected static String checkoutReturnInvoice;
	protected static String draftReturnInvoice;
	protected static String releaseReturnInvoice;

	// Verify that user is able to create Sales Return Order (Sales Return Order to
	// Sales Return Invoice- none deliveerd) --- 11
	protected static String journey2NonDeli;
	protected static String checkoutInboundshipment2;
	protected static String loadedProduct;

	// Verify that user is able to create Sales Return Order (Direct Sales Return
	// Order to Sales Return Invoice) -- 12

	protected static String salesReturnJourney3;
	protected static String productDirect;
	protected static String productText;
	protected static String productSelectedDirect;
	protected static String warehouseDirect;
	protected static String returnReasonDirect;
	protected static String expiryDateDirect;
	protected static String expiryDateDirectVAl;
	protected static String manufacDateDirect;
	protected static String serialNoTextDirect;
	protected static String LotNoTextDirect;
	protected static String expiryDate;
	protected static String expiryDateVal;

	// Verify that user cannot create new sales return invoice directly --- 13
	protected static String salesReturnInvoice;
	protected static String newSalesReturnInvoice;
	protected static String selectedValReturnInvoice;

	// Verify that user is able to create Consolidate Invoice --- 14
	protected static String consolidatedInvoice;
	protected static String newConsolidateInvoice;
	protected static String customerSearchConsolidate;
	protected static String textConsolidate;
	protected static String selectedValConsolidate;
	protected static String docList;
	protected static String checkboxConso;
	protected static String applyConso;
	protected static String searchInvoice;
	protected static String checkboxInvoice;
	protected static String applyInvoice;

	// Create New Account -- 15

	protected static String accountInformtion;
	protected static String newAccount;
	protected static String accountTypeAccInfor;
	protected static String accountNameTypeAccInfor;
	protected static String accountNameText;
	protected static String accountGroupAccInfor;
	protected static String currencyAccInfor;
	protected static String salesUnitAccInfor;
	protected static String accountOwnerAccInfor;
	protected static String contactTitleAccInfor;
	protected static String contactNameAccInfor;
	protected static String phoneAccInfor;
	protected static String mobileAccInfor;
	protected static String emailAccInfor;
	protected static String designationAccInfor;
	protected static String draftAccInfor;
	protected static String releaseAccInfor;
	protected static String editAccInfor;
	protected static String addedRow;
	protected static String addContactIcon;
	protected static String nameTitleAccInfor;
	protected static String nameAccInfor;
	protected static String designationAccInforr;
	protected static String phoneAccInforr;
	protected static String updateAccInfor;
	protected static String UpdateBarAccInfor;
	protected static String inquiryAccInfo;
	protected static String inquiryAboutAccInfor;
	protected static String categoryAccInfor;
	protected static String currentStageAccInfor;
	protected static String updateInquiryAccInfor;

	// Sales Inquiry -- 16
	protected static String salesInquiryBtn;
	protected static String newSalesInqBtn;
	protected static String leadSelectDropdown;
	protected static String leadAccSearchBtn;
	protected static String leadAccSearchText;
	protected static String leadTxtSearchBtn;
	protected static String leadSelectedVal;
	protected static String inquiryAboutTxt;
	protected static String leadSelect;
	protected static String inquiryCategory;
	protected static String inquiryOrigin;
	protected static String salesUnitInq;
	protected static String responsibleEmpSearch;
	protected static String responsibleEmpText;
	protected static String selectedValEmp;
	protected static String priorityDropdownInq;
	protected static String draftInq;
	protected static String addNewContactInq;
	protected static String nameTitleInq;
	protected static String nameTextInq;
	protected static String designationTextInq;
	protected static String phone1TextInq;
	protected static String phone2TextInq;
	protected static String releaseInq;

	// Lead Information -- 17

	protected static String leadInformation;
	protected static String newLead;
	protected static String nameTitleLead;
	protected static String nameLead;
	protected static String mobileLeadText;
	protected static String companyLead;
	protected static String NICLead;
	protected static String designationLead;
	protected static String accountOwnerLead;
	protected static String salesUnitLead;
	protected static String draftLead;
	protected static String releaseLead;
	protected static String updateTopBtnLead;
	protected static String addNewContactLead;
	protected static String nameTagLead;
	protected static String nameNewLead;
	protected static String updateBtnLead;
	protected static String addInquiryLeadBtn;
	protected static String inquiryAboutLead;
	protected static String caegoryLead;
	protected static String currentStageLead;
	protected static String updateLead;
	protected static String updateInquiry;

	// Contact Information -- 18
	protected static String contactInformationBtn;
	protected static String newContactBtn;
	protected static String nameTitleContact;
	protected static String nameTagContact;
	// protected static String nameContact;
	protected static String accountSearchContact;
	protected static String designationContact;
	protected static String salesUnitContact;
	protected static String accountOwnerContact;
	protected static String phoneContact;
	protected static String mobileContact;
	protected static String emailContact;
	protected static String IDContact;
	protected static String birthdateContact;
	protected static String billingContact;
	protected static String countryContact;
	protected static String updateContact;
	protected static String inquiryInformationTabContact;
	protected static String newInquiryBtnContact;
	protected static String inquiryAbout;

	// Verify that User is able to create new Pricing Rule with by one and get one
	// free items -- 19
	protected static String pricingRuleBtn;
	protected static String newPricingRule;
	protected static String newPricingruleookup;
	protected static String ruleCode;
	protected static String ruleDescription;
	protected static String draftPricingRule;
	protected static String choosePricingPolicyBtn;
	protected static String policyHeader;
	protected static String policyRowVal;
	protected static String editPolicy;
	protected static String discountTypePolicy;
	protected static String fromQty;
	protected static String toQty;
	protected static String advanceOpt;
	protected static String sameProductCheckBox;
	protected static String qtyTextBox;
	protected static String updateInEditValues;
	protected static String apply;
	protected static String advanceBtnInPolicy;
	protected static String activateScheduleBtn;
	protected static String daily;
	protected static String everyDay;
	protected static String fromTime;
	protected static String toTime;
	protected static String advanceRelationTab;
	protected static String applyBtnAdvanced;
	protected static String qtyInAdvance;
	protected static String updateRule;
	protected static String activateBtn;
	protected static String confirmationYes;
	protected static String validatorAlert;

	// Verify that User is able to create new Pricing Model with attaching the
	// Pricing rule --- 20

	protected static String pricingModel;
	protected static String newPricingModel;
	protected static String modelCode;
	protected static String description;
	protected static String rulesTab;
	protected static String addPricingRule;
	protected static String searchTxtRule;
	protected static String searchBtnRule;
	protected static String selectedRule;
	protected static String editPeriod;
	protected static String startDate;
	protected static String startDateValue;
	protected static String endDateValue;
	protected static String endDate;
	protected static String applyModel;
	protected static String draftModel;
	protected static String releaseModel;

	// Verify that user is able to created a Sales Order with added Price Model with
	// a Price Rule --- 21
	protected static String salesPriceModel;
	protected static String selectedQty;
	protected static String applyFreeIssues;
	protected static String updateFreeIssues;
	protected static String proSelectedFree;
	protected static String closeAlert;
	protected static String warehouseFree;
	protected static String qtyFree;
	protected static String discountFree;
	protected static String taxGroupFree;

	// Verify that User is able to created a new Sales Exchage order --- 22
	protected static String salesExchangeOrder;
	protected static String newSalesExchangeOrder;
	protected static String startNewJourney;
	protected static String journey1ExchangeToInvoice;
	protected static String customerSearchExchange;
	protected static String documentList;
	protected static String fromDate;
	protected static String dateSelected;
	protected static String toDate;
	protected static String searchDocumentList;
	protected static String checkDocumentList;
	protected static String applyExchange;
	protected static String addNewExchange;
	protected static String searchProOutput;
	protected static String searchProText;
	protected static String selectedOutput;
	protected static String warehouseOutput;
	protected static String product1List;
	protected static String checkFirst;
	protected static String productList2;
	protected static String proDetailsOutput;
	protected static String SerialWiseCost;
	protected static String firstSerial;

	// Free text invoice -- 23
	protected static String freeTextInvoice;
	protected static String newFreeTextInvoice;
	protected static String cusAccSearchFreeText;
	protected static String glAccSearch;
	protected static String glAccText;
	protected static String selectedRowGLAcc;
	protected static String GLAmount;
	protected static String checkoutGLAcc;
	protected static String checkAmount;
	protected static String draftGLAcc;
	protected static String releaseGLAcc;

	// Verify that user cannot proceed the Sales Order with adding full qty as
	// Separate product line in same Sales Order --- 24
	protected static String onHandQty;
	protected static String menuProduct;
	protected static String menuBtn;
	protected static String menuBtnFull;
	protected static String productAvailability;
	protected static String closeAvailabilityMenu;
	protected static String warehouse2;
	protected static String warehouseFullQty;
	protected static String qtyTextFull;
	protected static String unitPriceTxtFull;
	protected static String productListBtn1;
	protected static String closeAdvance;
	protected static String advanceMenu;
	protected static String productGroupConfiguration;
	protected static String newProductGroup;
	protected static String addProductGrp;
	protected static String plusMark;
	protected static String productGrpTxt;
	// protected static String activeCheckBox;
	protected static String updateProductGrp;
	protected static String selectProductGrp;
	protected static String detailsTabProductGrp;
	protected static String activateInventory;
	protected static String activatePurchase;
	protected static String activateSales;
	protected static String confirmYes;
	// protected static String

	// Vehicle Information -- 25
	protected static String vehicleInformation;
	protected static String vehicleInformationHeader;
	protected static String newVehicleInformationBtn;
	protected static String newVehicleInformationHeader;
	protected static String vehicleNoTxt;
	protected static String capacityTxt;
	protected static String capacityMeasure;
	protected static String weightTxt;
	protected static String weightMeasure;
	protected static String draftVehicleInf;
	protected static String vehicleSearchTop;
	protected static String sideMenuValVehicleInf;
	protected static String activeBtnVehicleInf;
	protected static String statusVehicleInf;
	protected static String alertYes;
	protected static String afterYes;

	// Create a Delivery Plan -- 26
	protected static String invenWarehouse;
	protected static String deliveryPlan;
	protected static String deliveryPlanInfo;
	protected static String deliveryPlanHeader;
	protected static String bookNo;
	protected static String vehicleSearch;
	protected static String vehicleSearchText;
	protected static String vehicleSelectedVal;
	protected static String fromDateVehicle;
	protected static String fromDateVal;
	protected static String toDateVehicle;
	protected static String toDateVal;
	protected static String draftVehicle;
	protected static String planningCount;
	protected static String loadingCount;
	protected static String deliveredCount;
	protected static String planningTab;
	protected static String valueVehicle;
	protected static String valueVehicleInLoading;
	protected static String releaseBtnVehicle;
	protected static String countLoading;
	protected static String loadingTab;
	protected static String releaseBtnInLoading;
	protected static String addDeliveryPlan;
	protected static String fromdateDeliveryPlan;
	protected static String fromdateyear;
	protected static String todateDeliveryPlan;
	protected static String searchBtn;
	protected static String selectedValDeliveryPlan;
	protected static String checkboxDeliveryplan;
	protected static String applyDeliveryPlan;
	protected static String salesInvoiceSearchTxt;
	protected static String deliveredBtn;
	protected static String countDelivered;
	protected static String verifyPlan;

	//
	protected static String reports;
	protected static String reportTemplate;
	protected static String productCodeDrop;
	protected static String productCodeText;
	protected static String quickPreview;
	protected static String timePeriod;
	protected static String today;
	protected static String binCardOutboundVal;

	// 5
	protected static String binCardInboundVal;

	// 6
	protected static String administration;
	protected static String journeyConfiguration;
	protected static String salesOrderToSalesInvoiceJourney;
	protected static String autogenerateOutboundShipment;
	protected static String autogenerateSalesInvoice;
	protected static String updateJourney;
	protected static String outboundDoc;
	protected static String salesInvoiceDoc;

	// 7
	protected static String entutionHeader;
	protected static String taskEvent;
	protected static String salesInvoiceTile;
	protected static String valueSalesOrder;

	protected static String updateAndNew;

	// Collection Unit
	protected static String collectionUnit;
	protected static String newCollectionUnit;
	protected static String collectionUnitCode;
	protected static String collectionUnitName;
	protected static String commissionRate;
	protected static String employeeSearch;
	protected static String employeeSearchText;
	protected static String selectedValEmployee;
	protected static String sharingRate;
	protected static String history;
	protected static String activities;
	protected static String historyReleased;
	
	// Sales Order
	protected static String POJourneyWindow;
	protected static String journeyWindowClose;
	protected static String reminders;
	protected static String createReminder;
	protected static String convertInboundCustomerMemo;
	
	//Sales Quotation
	protected static String unitPriceTextQuat;
	protected static String unitPriceAftTextQuat;
	protected static String activityHeader;
	protected static String activityMenuClose;
	protected static String convertToAccount;
	protected static String capacityPlanning;
	protected static String capacityPlanningHeader;
	protected static String capacityPlanningClose;
	protected static String linkMenuClose;
	protected static String duplicate;
	protected static String generateNewVersion;
	protected static String qoutationVersion;
	
	//Sales Return Invoice
	protected static String valReturnInvoice;
	protected static String hold;
	
	//Vehicle Information
	protected static String vehicleInfEditHeader;
	protected static String updateVehicleInf;
	protected static String inactiveBtnVehicleInf;
	protected static String inactivateAlertYes;
	
	//Consolidate Invoice
	protected static String invoiceVal1;
	protected static String invoiceVal2;
	
	//Warranty Profile
	protected static String warrantyProfile;
	protected static String newWarrantyProfile;
	protected static String warrantyProfileCode;
	protected static String warrantyProfileName;
	protected static String statusWarrantyProfile;
	
	//Pricing Rule
	protected static String pricingRule;
	protected static String updatePricingRule;
	protected static String resetPricing;
	
	//Sales Parameter
	protected static String salesParameter;
	protected static String addNewSalesParameter;
	protected static String salesParameterText;
	protected static String updateSales;
	protected static String closeAlertSales;
	
	//Contact Information
	protected static String editContact;
	protected static String contactInformation;
	protected static String contactInformationHeader;
	protected static String contactNameValText;
	protected static String createInHistory;
	protected static String inquiryInformation;
	protected static String newInquiry;
	protected static String salesInquiryHeader;
	protected static String inquiryFirstRow;
	protected static String stage;
	protected static String status;
	
	//Lead Information
	protected static String leadInformationHeader;
	protected static String deleteRule;
	protected static String salesUnitNavigation;
	
	protected static String newSalesUnit;
	protected static String salesUnitCode;
	protected static String unitLeaderSearch;
	protected static String unitLeaderText;
	protected static String salesUnitName;
	protected static String unitLeaderSelected;
	protected static String sharingRateSales;
	
	protected static String warehouseInformation;
	protected static String newWarehouse;
	protected static String warehouseCode;
	protected static String warehouseName;
	protected static String businessUnit;
	
	protected static String txt_ProductA;
	protected static String txt_ProductQtyA;


	/* Reading the Data to variables */

	// stock adjustment
	protected static String qtyVal;
	protected static String descrptionValStockAdj;
	protected static String countTextVal;

	// 1
	protected static String customerAccVal;
	protected static String billingAddrVal;
	protected static String shippingAddrVal;
	protected static String salesUnitVal;
	protected static String productTextValSer;
	protected static String productTextValInv;
	protected static String unitPriceVal;
	protected static String unitPriceValSerialSpec;
	protected static String unitPriceValBatchSpec;
	protected static String unitPriceValInv;
	protected static String quantityVal;
	protected static String quantityValInv;
	protected static String quantityValSerialSpec;
	protected static String serialCountVal;
	protected static String serialNo;
	protected static String warehouseVal1;

	protected static String batchNoValBatchSpecific;
	protected static String serialBatchSpecificVal;

	// 2
	protected static String customerAccServiceVal;
	protected static String billingAddrSerVal;
	protected static String shippingAddrSerVal;
	protected static String productSerVal;
	protected static String unitPriceValue;

	// 3
	protected static String customerAccOutboundVal;
	protected static String billingOutboundVal;
	protected static String shippingOutboundVal;
	protected static String serviceOutboundVal;
	protected static String invOutboundVal;
	protected static String warehouseVal3;
	protected static String unitpriceSerOutboundVal;
	protected static String unitpriceInvOutboundVal;

	// 4
	protected static String dropshipmentSerVal;
	protected static String dropshipmentInvVal;
	protected static String vendorVal;

	// 5
	protected static String serialNumVal;
	protected static String warehouseVal;

	// 6
	protected static String unitPriceSerVal;
	protected static String qtyValSalesInvoiceSer;

	// 7
	protected static String customerAccValQuat;
	protected static String unitPriceQuatVal;
	protected static String qtyValQout;
	protected static String qtyQuatationSerVal;
	protected static String qtyQuatationInvVal;
	protected static String billingAddrQuatVal;
	protected static String shippingAddrQuatVal;
	protected static String serialNoVal;
	protected static String warehouseValQuot;

	// 8
	protected static String serialNo2;
	protected static String vendorVal2;

	// 9
	protected static String serialNo3;

	// 10
	protected static String customerAccReturnVal;

	// 12
	protected static String productValDirect;
	protected static String warehouseDirectVal;
	protected static String returnReasonVal;

	// Account Information -- 15
	protected static String accountNameVal;
	protected static String contactNameVal;
	protected static String mobileNoVal;
	protected static String phoneNoVal;
	protected static String emailVal;
	protected static String designationVal;
	protected static String inquiryAboutVal;

	// Sales Inquiry -- 16
	protected static String leadAccSearchVal;
	protected static String salesInquiryAboutVal;
	protected static String responsibleEmpVal;
	protected static String nameTextSalesInqVal;
	protected static String salesInqDesigVal;
	protected static String salesInqPhone1Val;
	protected static String salesInqPhone2Val;

	// Lead Information -- 17
	protected static String nameTextLeadVal;
	protected static String mobileTexLeadtVal;
	protected static String companyLeadVal;
	protected static String NICLeadVAl;
	protected static String designationLeadVal;
	protected static String phoneLeadVal;
	protected static String companyUpdateVal;
	protected static String NameUpdateVal;
	protected static String inquiryUpdateAboutVal;

	// Contatc Information --- 18
	protected static String nameContactVal;
	protected static String designationContactVal;
	protected static String phoneContatcVal;
	protected static String mobileContactVal;
	protected static String emailContatctVal;
	protected static String inquiryAboutValu;

	// Pricing Rule -- 19
	protected static String ruleCodeVal;
	protected static String ruleDescriptionVal;
	protected static String fromVal;
	protected static String toVal;
	protected static String qtyEditVal;
	protected static String fromTimeVal;
	protected static String toTimeVal;

	// Pricing Model -- 20
	protected static String modelCodeVal;
	protected static String modelDescriptionVal;
	protected static String pricingRuleVal;
	
	//21
	protected static String selectedQtyVal;

	// 22
	protected static String outputProval;
	protected static String warehouseOutputPro;

	// --- 23
	protected static String glAccVal;
	protected static String GLAmountVal;

	// --24
	protected static String productValInv;
	protected static String productTextVal2;

	// Vehicle Information -- 25
	protected static String vehicleNoTxtVal;
	protected static String capacityTxtVal;
	protected static String weightTxtVal;

	// --26
	protected static String vehicleSearchVal;

	//
	protected static String accGrpVal;
	protected static String currencyVal;
	protected static String leadDaysVal;

	// s4
	protected static String ProductCodeVal;

	//
	protected static String listInbound;
	protected static String checkDocumentListRet;

	// Account Group Config
	protected static String AccountGroupConfiguration;
	protected static String newAccountGroupConfiguration;
	protected static String addNewAccGrp;
	protected static String addNewRecordAccGrp;
	protected static String accGrpText;
	protected static String updateAccGrp;
	protected static String selectAccGroup;
	protected static String activeCheckBox;
	protected static String currencyAccGroup;
	protected static String salesUnitAccGrp;
	protected static String detailsAccGrp;
	protected static String leadDays;
	protected static String draftNew;
	protected static String edit;
	protected static String update;

	// Account Information
	protected static String mobileAccInf;
	protected static String annualRevenue;
	protected static String detailsTab;
	protected static String annualRevenueVal;
	protected static String newMobileNo;

	// Collection Unit
	protected static String collectionNameVal;
	protected static String commissionRateVal;
	protected static String employeeVal;
	protected static String sharingRateVal;
	protected static String commisionRateEditedVal;
	protected static String sharingRateNewVal;
	
	//Sales Target
	
	//
	protected static String unitPriceQuatValEdit;
	protected static String weightTxtNewVal;
	
	//Warranty Profile
	//protected static String profileCodeVal;
	protected static String profileNameVal;
	protected static String profileNameNew;
	
	//Pricing Rule
	protected static String ruleDescriptionNewVal;
	
	//Contact Name
	protected static String contactNameNewVal;
	
	//Lead Information
	protected static String nameTextLeadNewVal;
	
	protected static String descriptionVal;
	
	protected static String unitLeaderVal;
	protected static String salesUnitNameVal;
	protected static String warehouseNameVal;
	protected static String businessUnitVal;

	// Calling the constructor
	public static void readElementlocators() throws Exception {

		siteLogo = findElementInXLSheet(getParameterSalesAndMarketing, "site logo");
		txt_username = findElementInXLSheet(getParameterSalesAndMarketing, "user name");
		txt_password = findElementInXLSheet(getParameterSalesAndMarketing, "password");
		btn_login = findElementInXLSheet(getParameterSalesAndMarketing, "login button");
		setProcessDate = findElementInXLSheet(getParameterSalesAndMarketing, "set Process Date");
		lnk_home = findElementInXLSheet(getParameterSalesAndMarketing, "home link");
		nav_btn = findElementInXLSheet(getParameterSalesAndMarketing, "nav bar");
		sales_Marketing = findElementInXLSheet(getParameterSalesAndMarketing, "sales and marketing");

		searchBar = findElementInXLSheet(getParameterSalesAndMarketing, "search Bar");

		// Stock Adjustment

		stockAdjustment = findElementInXLSheet(getParameterSalesAndMarketing, "stock Adjustment");
		newStockAdjustment = findElementInXLSheet(getParameterSalesAndMarketing, "new Stock Adjustment");
		descriptionTxtStockAdj = findElementInXLSheet(getParameterSalesAndMarketing, "description Txt Stock Adj");
		warehouseStockAdj = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Stock Adj");
		productLookUpBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"product LookUp Batch Specific");
		productValTxt = findElementInXLSheet(getParameterSalesAndMarketing, "product Val Txt");
		selectedProduct = findElementInXLSheet(getParameterSalesAndMarketing, "selected Product");
		qtyTxtBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Batch Specific");

		addNewBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "add New Batch Fifo");
		productLookUpBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "product LookUp Batch Fifo");
		qtyTxtBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Batch Fifo");
		addNewLot = findElementInXLSheet(getParameterSalesAndMarketing, "add New Lot");
		productLookUpLot = findElementInXLSheet(getParameterSalesAndMarketing, "product LookUp Lot");
		qtyTxtLot = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Lot");
		addNewSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Specific");
		productLookUpSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"product LookUp Serial Specific");
		qtyTxtSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Serial Specific");
		addNewSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Batch Spec");
		productLookUpSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing,
				"product LookUp Serial Batch Spec");
		qtyTxtSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Serial Batch Spec");
		addNewSerialBthFifo = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Bth Fifo");
		productLookUpSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing,
				"product LookUp Serial Batch Fifo");
		qtyTxtSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Serial Batch Fifo");
		addNewSeriFifo = findElementInXLSheet(getParameterSalesAndMarketing, "add New Seri Fifo");
		productLookUpSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "product LookUp Serial Fifo");
		qtyTxtSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "qty Txt Serial Fifo");

		batchSpecificItem = findElementInXLSheet(getParameterSalesAndMarketing, "batch Specific Item");
		serialSpecificItem = findElementInXLSheet(getParameterSalesAndMarketing, "serial Specific Item");
		serialBatchSpecificItem = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch Specific Item");
		serialBatchFifoItem = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch Fifo Item");
		serialFifoItem = findElementInXLSheet(getParameterSalesAndMarketing, "serial Fifo Item");
		lotItem = findElementInXLSheet(getParameterSalesAndMarketing, "lot Item");
		rangeSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "range Serial Specific");
		serialNoSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "serial No Serial Specific");
		lotNoSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "lotNo Serial Specific");
		expiryDateSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date Serial Specific");
		countText = findElementInXLSheet(getParameterSalesAndMarketing, "count Text");
		batchFifoItem = findElementInXLSheet(getParameterSalesAndMarketing, "batch Fifo Item");
		batchNoTxtBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "batch NoTxt Batch Specific");
		lotNoBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "lotNo Batch Specific");
		expiryDateBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date Batch Specific");
		expiryDateValBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date Val Batch Spec");
		nextMonth = findElementInXLSheet(getParameterSalesAndMarketing, "next Month");
		manufacDateSerialSpec = findElementInXLSheet(getParameterSalesAndMarketing, "manufac Date Serial Spec");
		manufacDate = findElementInXLSheet(getParameterSalesAndMarketing, "manufac Date");
		batchNoTxtSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"batch No Txt Serial Batch Specific");

		lotNoText = findElementInXLSheet(getParameterSalesAndMarketing, "lot No Text");

		navMenuBtn = findElementInXLSheet(getParameterSalesAndMarketing, "nav Menu Btn");
		salesAndMarketing = findElementInXLSheet(getParameterSalesAndMarketing, "sales And Marketing");
		salesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "sales Order Btn");
		salesOrderHeader = findElementInXLSheet(getParameterSalesAndMarketing, "sales Order Header");
		newSalesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Order");
		journey1 = findElementInXLSheet(getParameterSalesAndMarketing, "journey 1");
		salesOrderToSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "sales Order To Sales Invoice");

		customerAccountSearch = findElementInXLSheet(getParameterSalesAndMarketing, "customer Account Search");
		customerAccountText = findElementInXLSheet(getParameterSalesAndMarketing, "customer Account Text");
		selected_val = findElementInXLSheet(getParameterSalesAndMarketing, "cus Selected Val");
		currencyDropdown = findElementInXLSheet(getParameterSalesAndMarketing, "currency Dropdown");
		billingAddressSearch = findElementInXLSheet(getParameterSalesAndMarketing, "billing Address Search");
		billingAddressText = findElementInXLSheet(getParameterSalesAndMarketing, "billing Address Text");
		shippingAddressText = findElementInXLSheet(getParameterSalesAndMarketing, "shipping Address Text");

		apply_btn = findElementInXLSheet(getParameterSalesAndMarketing, "apply Btn");
		salesUnit = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Dropdown");
		accountOwner = findElementInXLSheet(getParameterSalesAndMarketing, "account Owner Dropdown");

		productLookup = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup");
		productLookupSerialSpec = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Serial Spec");
		productLookupInv = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Inv");

		productLookupBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Batch Specific");
		productLookupBatchFIFO = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Batch FIFO");
		productLookupSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Serial Batch Specific");
		productLookupSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Serial Batch Fifo");
		productLookupSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Serial Fifo");
		productTextSer = findElementInXLSheet(getParameterSalesAndMarketing, "product Text Ser");
		productTextSerialSpec = findElementInXLSheet(getParameterSalesAndMarketing, "product Text Serial Spec");
		productTextInv = findElementInXLSheet(getParameterSalesAndMarketing, "product Text Inv");

		searchInv = findElementInXLSheet(getParameterSalesAndMarketing, "inv Search");
		searchButton = findElementInXLSheet(getParameterSalesAndMarketing, "search Button");
		proSelectedVAl = findElementInXLSheet(getParameterSalesAndMarketing, "pro Selected Val");
		invSelectedVal = findElementInXLSheet(getParameterSalesAndMarketing, "inv Selected Val");
		InvenValueSalesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "Inven Value Sales Order");
		outboundInvenValue = findElementInXLSheet(getParameterSalesAndMarketing, "outbound Inven Value");
		accountNameOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "account Name Outbound");
		addNewRecord = findElementInXLSheet(getParameterSalesAndMarketing, "add New Record");
		addNewBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "add New Batch Specific");
		addNewRecordSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"add New Record Serial Specific");
		addNewSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Batch Fifo");
		addNewBatchFIFO = findElementInXLSheet(getParameterSalesAndMarketing, "addNew Batch FIFO");
		addNewSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "addNew Serial Fifo");
		invenSearch = findElementInXLSheet(getParameterSalesAndMarketing, "inv Search");

		unitPriceTxt = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price");
		unitPriceInve = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Inve");
		unitPriceTxtService = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Txt Service");
		unitPriceBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Batch Specific");
		unitPriceSerialSpec = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Serial Spec");
		unitPriceBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Batch Fifo");
		unitPriceSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Serial Batch Spec");
		unitPriceSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Serial Batch Fifo");
		unitPriceSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Serial Fifo");
		wareHouse = findElementInXLSheet(getParameterSalesAndMarketing, "ware House");
		wareHouseBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Batch Specific");
		wareHouseBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse BatchFifo");
		wareHouseSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Serial Specific");
		wareHouseSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Serial Fifo");
		wareHouseSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Serial Batch Fifo");
		wareHouseSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"wareHouse Serial Batch Specific");
		qtyText = findElementInXLSheet(getParameterSalesAndMarketing, "quantity");
		qtyTextInve = findElementInXLSheet(getParameterSalesAndMarketing, "quantity Inve");
		qtyTextService = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Service");
		qtyTextSerialSpec = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Spec");
		qtyTextBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Batch Specific");
		qtyTextBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Batch Fifo");
		qtyTxtSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"qty Txt Serial Batch Specific");
		qtyTextSerialBatchFifo = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Batch Fifo");
		qtyTextSerialFifo = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Fifo");
		bannerTotal1 = findElementInXLSheet(getParameterSalesAndMarketing, "banner Total");
		bottomTotal = findElementInXLSheet(getParameterSalesAndMarketing, "bottom Total");
		serialBatch = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch");
		productListBtn = findElementInXLSheet(getParameterSalesAndMarketing, "product List Btn");
		productListBtn2 = findElementInXLSheet(getParameterSalesAndMarketing, "product List Btn 2");
		range = findElementInXLSheet(getParameterSalesAndMarketing, "range Check");
		count = findElementInXLSheet(getParameterSalesAndMarketing, "serial Count");
		serialText = findElementInXLSheet(getParameterSalesAndMarketing, "serial Text");
		backBtn = findElementInXLSheet(getParameterSalesAndMarketing, "back Btn");
		updateBtn = findElementInXLSheet(getParameterSalesAndMarketing, "update Btn");
		serialbatchSpecificListBtn = findElementInXLSheet(getParameterSalesAndMarketing,
				"serialbatch Specific List Btn");
		batchNoTextSerialBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"batch No Text Serial Batch Specific");
		batchSpecificListBtn = findElementInXLSheet(getParameterSalesAndMarketing, "batch Specific List Btn");

		checkoutSalesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Sales Order");
		draft = findElementInXLSheet(getParameterSalesAndMarketing, "draft");
		release = findElementInXLSheet(getParameterSalesAndMarketing, "release");
		releaseInHeader = findElementInXLSheet(getParameterSalesAndMarketing, "release In Header");
		documentVal = findElementInXLSheet(getParameterSalesAndMarketing, "document Val");
		serviceProductValOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "service Product Val Outbound");
		totalSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "total Sales Invoice");

		outboundLink = findElementInXLSheet(getParameterSalesAndMarketing, "outboundshipment Link");
		gotoPageWindow = findElementInXLSheet(getParameterSalesAndMarketing, "goto Page Window");
		checkoutOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Outbound");
		outboundDraft = findElementInXLSheet(getParameterSalesAndMarketing, "outbound Draft");
		outboundRelease = findElementInXLSheet(getParameterSalesAndMarketing, "outbound Release");
		checkoutInvoice1 = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Invoice");
		entutionLink = findElementInXLSheet(getParameterSalesAndMarketing, "entution Link");
		batchNoText = findElementInXLSheet(getParameterSalesAndMarketing, "batch No Text");

		menu = findElementInXLSheet(getParameterSalesAndMarketing, "menu Display");
		serialWise = findElementInXLSheet(getParameterSalesAndMarketing, "serial Wise");
		serialTextVal = findElementInXLSheet(getParameterSalesAndMarketing, "serial Text Val");
		serialTextVal2 = findElementInXLSheet(getParameterSalesAndMarketing, "serial Text Val 2");
		batchSpecificBatchNo = findElementInXLSheet(getParameterSalesAndMarketing, "batch Specific Batch No");
		serialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch Spec");
		serialMenuClose = findElementInXLSheet(getParameterSalesAndMarketing, "serial Menu Close");
		menuBatchSpecific = findElementInXLSheet(getParameterSalesAndMarketing, "menu Batch Specific");
		menuSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "menu Serial Batch Spec");

		serviceOut = findElementInXLSheet(getParameterSalesAndMarketing, "service Out");
		serialSpecOut = findElementInXLSheet(getParameterSalesAndMarketing, "serial Spec Out");
		batchSpeccOut = findElementInXLSheet(getParameterSalesAndMarketing, "batch Specc Out");
		batchFifoOut = findElementInXLSheet(getParameterSalesAndMarketing, "batch Fifo Out");
		serialBatchSpecOut = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch Spec Out");
		serialbatchFifoOut = findElementInXLSheet(getParameterSalesAndMarketing, "serial batch Fifo Out");
		serialFifoOut = findElementInXLSheet(getParameterSalesAndMarketing, "serial Fifo Out");

		// Verify that user is able to create sales order (Sales order to Sales invoice
		// [Service])--- 002

		journey2 = findElementInXLSheet(getParameterSalesAndMarketing, "journey 2");
		customerAccSearchService = findElementInXLSheet(getParameterSalesAndMarketing, "customer Acc Search Service");
		customerSearchTxt = findElementInXLSheet(getParameterSalesAndMarketing, "customer Search Txt");
		customerSelectedValue = findElementInXLSheet(getParameterSalesAndMarketing, "customer Selected Value");
		currencyService = findElementInXLSheet(getParameterSalesAndMarketing, "currency Service");
		billingAddressService = findElementInXLSheet(getParameterSalesAndMarketing, "billing Address Service");
		billingAddressTxt = findElementInXLSheet(getParameterSalesAndMarketing, "billing Address Txt");
		shippingAddressTxt = findElementInXLSheet(getParameterSalesAndMarketing, "shipping Address Txt");
		applyBtnService = findElementInXLSheet(getParameterSalesAndMarketing, "apply Btn Service");
		salesUnitService = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Service");
		productLookupService = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Service");
		productTxtService = findElementInXLSheet(getParameterSalesAndMarketing, "product Txt Service");
		selectedValueSearch = findElementInXLSheet(getParameterSalesAndMarketing, "selected Value Search");
		unitPriceServicePro = findElementInXLSheet(getParameterSalesAndMarketing, "unitPrice Service Pro");
		checkoutService = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Service");
		draftService = findElementInXLSheet(getParameterSalesAndMarketing, "draft Service");
		releaseService = findElementInXLSheet(getParameterSalesAndMarketing, "release Service");
		linkSalesInvoiceService = findElementInXLSheet(getParameterSalesAndMarketing, "linkSales Invoice Service");
		checkoutInvoiceService = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Invoice Service");
		serSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "ser Sales Invoice");
		draftInvoiceService = findElementInXLSheet(getParameterSalesAndMarketing, "draft Invoice Service");
		releaseInvoiceService = findElementInXLSheet(getParameterSalesAndMarketing, "release Invoice Service");

		// Verify that user is able to create sales order (Sales order to Outbound
		// shipment) --3

		journey3 = findElementInXLSheet(getParameterSalesAndMarketing, "journey 3");
		customerAccountOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "customer Account Outbound");
		customerAccountTxtOutbound = findElementInXLSheet(getParameterSalesAndMarketing,
				"customer Account TxtOutbound");
		selectedValAcccountOutbound = findElementInXLSheet(getParameterSalesAndMarketing,
				"selected Val Acccount Outbound");
		currencyOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "currency Outbound");
		billingAddressSearchOutbound = findElementInXLSheet(getParameterSalesAndMarketing,
				"billing Address Search Outbound");
		billingAddrTxtOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "billing Addr Txt Outbound");
		shippingAddressSearchOutbound = findElementInXLSheet(getParameterSalesAndMarketing,
				"shipping Address Search Outbound");
		shippingAddrTxtOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "shipping Addr Txt Outbound");
		applyAddress = findElementInXLSheet(getParameterSalesAndMarketing, "apply Address");
		salesUnitOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Outbound");
		productSearchOutboundSer = findElementInXLSheet(getParameterSalesAndMarketing, "product Search Outbound Ser");
		productSearchTxtOutboundSer = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Search Txt Outbound Ser");
		selectedRowSer = findElementInXLSheet(getParameterSalesAndMarketing, "selected Row Ser");
		unitPriceSer = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Ser");
		unitPriceInv = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Inv");
		addNewProductOutboundInv = findElementInXLSheet(getParameterSalesAndMarketing, "add New Product Outbound Inv");
		productSearchOutboundInv = findElementInXLSheet(getParameterSalesAndMarketing, "product Search Outbound Inv");
		productSearchTxtOutboundInv = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Search Txt Outbound Inv");
		warehouse = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse");
		selectedRowInv = findElementInXLSheet(getParameterSalesAndMarketing, "selected Row Inv");
		checkOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "check Outbound");
		draftOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "draft Outbound");
		releaseOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "release Outbound");
		linkSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "link Sales Invoice");
		checkoutInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Invoice");
		draftInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "draft Invoice");
		linkOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "link Outbound");
		// protected static String checkOutbound;
		// draftOutbound = findElementInXLSheet(getParameterSalesAndMarketing,
		// "draftutbound");
		// protected static String releaseOutbound;

		// Verify that user is able to create sales order (Sales order to Sales Invoice
		// - Drop Shipment) ---4

		journey4 = findElementInXLSheet(getParameterSalesAndMarketing, "journey 4");
		selectedValDropSer = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Drop Ser");
		selectedValDropInv = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Drop Inv");
		unitPriceInven = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Inven");
		vendorLookup = findElementInXLSheet(getParameterSalesAndMarketing, "vendor Lookup");
		vendorText = findElementInXLSheet(getParameterSalesAndMarketing, "vendor Text");
		vendorSelectedVal = findElementInXLSheet(getParameterSalesAndMarketing, "vendor Selected Val");
		// salesOrderDropShipment;

		actionMenu = findElementInXLSheet(getParameterSalesAndMarketing, "action Menu");
		delete = findElementInXLSheet(getParameterSalesAndMarketing, "delete");
		documentFlow = findElementInXLSheet(getParameterSalesAndMarketing, "document Flow");
		convertToPurchaseOrder = findElementInXLSheet(getParameterSalesAndMarketing, "convert To Purchase Order");

		// Verify that user is able to create sales Invoice (Sales invoice to Outbound
		// shipment) --- 5

		salesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "sales Invoice");
		newSalesInv = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Inv");
		journeySalesInvoice1 = findElementInXLSheet(getParameterSalesAndMarketing, "invoice OutboundShipment");
		cusAccount = findElementInXLSheet(getParameterSalesAndMarketing, "cus Account");
		productLookupInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Invoice");
		productLookupSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Serial Spec SI");
		productLookUpInven = findElementInXLSheet(getParameterSalesAndMarketing, "product LookUp Inven");
		productLookupBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Batch Specific SI");
		productLookupBatchFIFOSI = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Batch FIFO SI");
		productLookupSerialBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Serial Batch Specific SI");
		productLookupSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Serial Batch Fifo SI");
		productLookupSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"product Lookup Serial Fifo SI");
		serSelectedVAl = findElementInXLSheet(getParameterSalesAndMarketing, "ser Selected Val");
		unitPriceTxtSISer = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Txt SI Ser");
		unitPriceSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Serial Spec SI");
		unitPriceBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Batch Specific SI");
		unitPriceBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Batch Fifo SI");
		unitPriceSerialBatchSpecSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"unit Price Serial Batch Spec SI");
		unitPriceSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"unit Price Serial Batch Fifo SI");
		unitPriceSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Serial Fifo SI");
		qtyTextSISer = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text SI Ser");
		qtyTextSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Spec SI");
		qtyTextBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Batch Specific SI");
		qtyTextBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Batch Fifo SI");
		qtyTextSerialBtchSpecSI = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Btch Spec SI");
		qtyTextSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Batch Fifo SI");
		qtyTextSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Serial Fifo SI");
		wareHouseSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Sales Invoice");
		wareHouseSerialSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Serial Specific SI");
		wareHouseBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Batch Specific SI");
		wareHouseBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Batch Fifo SI");
		wareHouseSerialBatchSpecSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"wareHouse Serial Batch Spec SI");
		wareHouseSerialBtchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing,
				"wareHouse Serial Btch Fifo SI");
		wareHouseSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "wareHouse Serial Fifo SI");
		addNewRecordSerialSpecSI = findElementInXLSheet(getParameterSalesAndMarketing, "add New Record Serial Spec SI");
		addNewBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "add New Batch Specific SI");
		addNewBatchFIFOSI = findElementInXLSheet(getParameterSalesAndMarketing, "add New Batch FIFO SI");
		addNewSerBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "add New Ser Batch Specific SI");
		addNewSerialBatchFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Batch Fifo SI");
		addNewSerialFifoSI = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Fifo SI");
		menuSerial = findElementInXLSheet(getParameterSalesAndMarketing, "menu Serial");
		menuClose = findElementInXLSheet(getParameterSalesAndMarketing, "menu Close");
		checkoutInvoiceOut = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Invoice Out");
		menuBatchSpecificSI = findElementInXLSheet(getParameterSalesAndMarketing, "menu Batch Specific SI");
		menuSerialBatchSpecSI = findElementInXLSheet(getParameterSalesAndMarketing, "menu Serial Batch Spec SI");
		productListBtnSerialSpecific = findElementInXLSheet(getParameterSalesAndMarketing,
				"product List Btn Serial Specific");
		productList = findElementInXLSheet(getParameterSalesAndMarketing, "product List");
		warehouseInv = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Inv");

		// Verify that user is able to create sales Invoice (Service) --6
		salesInvoiceService = findElementInXLSheet(getParameterSalesAndMarketing, "sales Invoice Btn");
		newSalesInvoiceBtn = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Invoicee");
		salesInvoiceSerJourney = findElementInXLSheet(getParameterSalesAndMarketing, "sales Invoice Service Journey");
		cusAccountSearch = findElementInXLSheet(getParameterSalesAndMarketing, "customer Account Search Btn");
		productLookupBtn = findElementInXLSheet(getParameterSalesAndMarketing, "product Lookup Btn");
		unitPriceService = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Service");
		warehouseSer = findElementInXLSheet(getParameterSalesAndMarketing, "ware House Ser");
		checkoutSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Sales Invoice");
		serviceSelected = findElementInXLSheet(getParameterSalesAndMarketing, "service Selected");
		qtyServiceInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "qty Service Invoice");

		// Verify that user is able to create sales Quotation with a lead(Sales
		// Quotation to Sales Invoice) --- 7

		salesQuatationBtn = findElementInXLSheet(getParameterSalesAndMarketing, "sales Quatation Btn");
		newSalesQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Quatation");
		journey1Quatation = findElementInXLSheet(getParameterSalesAndMarketing, "journey1 Quatation");
		accountHead = findElementInXLSheet(getParameterSalesAndMarketing, "account Head");
		customerAccSearchQuat = findElementInXLSheet(getParameterSalesAndMarketing, "customer Acc Search Quat");
		selectedValLead = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Lead");
		billingAddrSearch = findElementInXLSheet(getParameterSalesAndMarketing, "billing Addr Search");
		billingAddrTxtQuat = findElementInXLSheet(getParameterSalesAndMarketing, "billing Addr Txt Quat");
		shippingAddrTxtQuat = findElementInXLSheet(getParameterSalesAndMarketing, "shipping Addr Txt Quat");
		selectedValQuatInv = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Quat Inv");
		selectedValQuatSer = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Quat Ser");
		productSearchQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "product Search Quatation");
		qtyQuatationSer = findElementInXLSheet(getParameterSalesAndMarketing, "qty Quatation Ser");
		qtyQuatationInv = findElementInXLSheet(getParameterSalesAndMarketing, "qty Quatation Inv");
		unitpriceQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "unitprice Quatation");
		unitpriceQuatationInv = findElementInXLSheet(getParameterSalesAndMarketing, "unitprice Quatation Inv");
		unitpriceBatchSpecQout = findElementInXLSheet(getParameterSalesAndMarketing, "unitprice Batch Spec Qout");
		unitpriceBatchFifoQout = findElementInXLSheet(getParameterSalesAndMarketing, "unitprice Batch Fifo Qout");
		unitpriceSerialBatchSpecQout = findElementInXLSheet(getParameterSalesAndMarketing,
				"unitprice Serial Batch Spec Qout");
		unitpriceSerBatchFifoQout = findElementInXLSheet(getParameterSalesAndMarketing,
				"unitprice Ser Batch Fifo Qout");
		unitpriceSerialFifoQout = findElementInXLSheet(getParameterSalesAndMarketing, "unitprice Serial Fifo Qout");
		qtyBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing, "qty Batch Spec Quot");
		qtyBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "qty Batch Fifo Quot");
		qtySerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing, "qty Serial Batch Spec Quot");
		qtySerBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "qty Ser Batch Fifo Quot");
		qtySerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "qty Serial Fifo Quot");
		addNew = findElementInXLSheet(getParameterSalesAndMarketing, "add New");
		addNewBatchSpecificQuot = findElementInXLSheet(getParameterSalesAndMarketing, "add New Batch Specific Quot");
		addNewBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "add NewBatch Fifo Quot");
		addNewSerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing,
				"add New Serial Batch Spec Quot");
		addNewSerBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "add New Ser Batch Fifo Quot");
		addNewSerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "add New Serial Fifo Quot");
		searchBtnQuata = findElementInXLSheet(getParameterSalesAndMarketing, "search Btn Quata");
		searchBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing, "search Batch Spec Quot");
		searchBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "search Batch Fifo Quot");
		searchSerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing,
				"search Serial Batch Spec Quot");
		searchSerBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "search Ser Batch Fifo Quot");
		searchSerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "search Serial Fifo Quot");
		checkoutQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Quatation");
		draftQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "draft Quatation");
		releaseQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "release Quatation");
		versionBtn = findElementInXLSheet(getParameterSalesAndMarketing, "version Btn");
		versionTab = findElementInXLSheet(getParameterSalesAndMarketing, "version Tab");
		tickConfirmVersion = findElementInXLSheet(getParameterSalesAndMarketing, "tick Confirm Version");
		confirmVersion = findElementInXLSheet(getParameterSalesAndMarketing, "confirm Version");
		confirmBtn = findElementInXLSheet(getParameterSalesAndMarketing, "confirm Btn");
		linkQuatation = findElementInXLSheet(getParameterSalesAndMarketing, "link Quatation");
		warehouseSerialSpecificQuot = findElementInXLSheet(getParameterSalesAndMarketing,
				"warehouse Serial Specific Quot");
		warehouseBatchSpecificQuot = findElementInXLSheet(getParameterSalesAndMarketing,
				"warehouse Batch Specific Quot");
		warehouseBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Batch Fifo Quot");
		warehouseSerBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing,
				"warehouse Ser Batch Spec Quot");
		warehouseSerialBatchFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing,
				"warehouse Serial Batch Fifo Quot");
		warehouseSerialFifoQuot = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Serial Fifo Quot");
		menuQuot = findElementInXLSheet(getParameterSalesAndMarketing, "menu Quot");
		menuBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "menu Batch Spec");
		menuSerialBatchSpecQuot = findElementInXLSheet(getParameterSalesAndMarketing, "menu Serial Batch Spec Quot");
		batchNo = findElementInXLSheet(getParameterSalesAndMarketing, "batch No");
		serialBatchNo = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch No");
		checkoutSalesOrderQuata = findElementInXLSheet(getParameterSalesAndMarketing, "checkout SalesOrder Quata");
		draftSalesOrderQuata = findElementInXLSheet(getParameterSalesAndMarketing, "draft SalesOrder Quata");
		releaseSalesOrderQuata = findElementInXLSheet(getParameterSalesAndMarketing, "release SalesOrder Quata");
		linkSalesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "link Sales Order");
		checkoutOutboundquata = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Outbound Quata");
		draftOutboundquata = findElementInXLSheet(getParameterSalesAndMarketing, "draft Outbound quata");
		serialOutboundQuata = findElementInXLSheet(getParameterSalesAndMarketing, "serial Outbound Quata");
		sideMenuQuat = findElementInXLSheet(getParameterSalesAndMarketing, "side Menu Quat");
		batchSpecificQuat = findElementInXLSheet(getParameterSalesAndMarketing, "batch Specific Quat");
		batchNoTxtBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "batch No Txt Batch Spec");
		TextSerialBatchSpec = findElementInXLSheet(getParameterSalesAndMarketing, "Text Serial Batch Spec");
		serialBatchSpecItem = findElementInXLSheet(getParameterSalesAndMarketing, "serial Batch Spec Item");
		batchNoTxtQuat = findElementInXLSheet(getParameterSalesAndMarketing, "batch No Txt Quat");
		updateBtnQuat = findElementInXLSheet(getParameterSalesAndMarketing, "update Btn Quat");
		backBtnQuat = findElementInXLSheet(getParameterSalesAndMarketing, "back Btn Quat");
		serialTextQuat = findElementInXLSheet(getParameterSalesAndMarketing, "serial Text Quat");

		// Verify that user is able to create sales Quotation with a lead
		// (Sales Quotation to Sales Invoice - Drop Shipment) --- 8

		journey2Quatation = findElementInXLSheet(getParameterSalesAndMarketing, "journey2 Quatation");
		actionBtn = findElementInXLSheet(getParameterSalesAndMarketing, "action Btn");
		convert = findElementInXLSheet(getParameterSalesAndMarketing, "convert");
		vendorSearch = findElementInXLSheet(getParameterSalesAndMarketing, "vendor Search");
		vendorTextt = findElementInXLSheet(getParameterSalesAndMarketing, "vendor Textt");
		selectedValQuat2 = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Quat2");

		// Verify that user is able to create sales Quotation with a lead(Sales
		// Quotation to Outbound Shipment) --9
		journey3Quatation = findElementInXLSheet(getParameterSalesAndMarketing, "journey3 Quatation");
		warehouse3quat = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse3 quat");
		menuOne = findElementInXLSheet(getParameterSalesAndMarketing, "menu One");
		menuBatchSpecic = findElementInXLSheet(getParameterSalesAndMarketing, "menu Batch Specic");
		menuSerialBatchSpecQuot3 = findElementInXLSheet(getParameterSalesAndMarketing, "menu Serial Batch Spec Quot3");

		// Verify that user is able to create Sales Return Order (Sales Return Order to
		// Sales Return Invoice) --- 10

		salesReturnOrder = findElementInXLSheet(getParameterSalesAndMarketing, "sales Return Order");
		newSalesReturnOrder = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Return Order");
		salesReturnJourney1 = findElementInXLSheet(getParameterSalesAndMarketing, "sales Return Journey1");
		customerAccSearchReturn = findElementInXLSheet(getParameterSalesAndMarketing, "customer Acc Search Return");
		customerAccTxtReturn = findElementInXLSheet(getParameterSalesAndMarketing, "customer Acc Txt Return");
		selectedValCustomerReturn = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Customer Return");
		currencyReturn = findElementInXLSheet(getParameterSalesAndMarketing, "currency Return");
		contactPersonReturn = findElementInXLSheet(getParameterSalesAndMarketing, "contact Person Return");
		billingAddrReturnSearch = findElementInXLSheet(getParameterSalesAndMarketing, "billing Addr Return Search");
		billingAddrTxtReturn = findElementInXLSheet(getParameterSalesAndMarketing, "billing Addr Txt Return");
		shippingAddrTxtReturn = findElementInXLSheet(getParameterSalesAndMarketing, "shipping Addr Txt Return");
		salesUnitReturn = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Return");
		documentListReturn = findElementInXLSheet(getParameterSalesAndMarketing, "document List Return");
		fromDateReturn = findElementInXLSheet(getParameterSalesAndMarketing, "from Date Return");
		fromDateSelected = findElementInXLSheet(getParameterSalesAndMarketing, "from Date Selected");
		toDateReturn = findElementInXLSheet(getParameterSalesAndMarketing, "to Date Return");
		toDateSelected = findElementInXLSheet(getParameterSalesAndMarketing, "to Date Selected");
		searchBtnReturn = findElementInXLSheet(getParameterSalesAndMarketing, "search Btn Return");
		tickSalesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "tick Sales Order");
		applyReturn = findElementInXLSheet(getParameterSalesAndMarketing, "apply Return");
		checkDocumentListRet2 = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret2");
		checkDocumentListRet3 = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret3");
		checkDocumentListRet4 = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret4");
		checkDocumentListRet5 = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret5");
		checkDocumentListRet6 = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret6");
		checkDocumentListRet7 = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret7");
		returnReason = findElementInXLSheet(getParameterSalesAndMarketing, "return Reason");
		dispositionCategory = findElementInXLSheet(getParameterSalesAndMarketing, "disposition Category");
		checkoutReturn = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Return");
		draftReturn = findElementInXLSheet(getParameterSalesAndMarketing, "draft Return");
		releaseReturn = findElementInXLSheet(getParameterSalesAndMarketing, "release Return");
		linkReturn = findElementInXLSheet(getParameterSalesAndMarketing, "link Return");
		checkoutInboundshipment = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Inboundshipment");
		draftInbound = findElementInXLSheet(getParameterSalesAndMarketing, "draft Inbound");
		serialInbound = findElementInXLSheet(getParameterSalesAndMarketing, "serial Inbound");
		listInbound1 = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound1");
		listInbound2 = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound2");
		listInbound3 = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound3");
		listInbound4 = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound4");
		listInbound5 = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound5");
		listInbound6 = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound6");
		checkBox = findElementInXLSheet(getParameterSalesAndMarketing, "check Box");
		updateInbound = findElementInXLSheet(getParameterSalesAndMarketing, "update Inbound");
		backInbound = findElementInXLSheet(getParameterSalesAndMarketing, "back Inbound");
		releaseInbound = findElementInXLSheet(getParameterSalesAndMarketing, "release Inbound");
		linkReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "link Return Invoice");
		checkoutReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Return Invoice");
		draftReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "draft Return Invoice");
		releaseReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "release Return Invoice");

		// Verify that user is able to create Sales Return Order (Sales Return Order to
		// Sales Return Invoice- none deliveerd) --- 11

		journey2NonDeli = findElementInXLSheet(getParameterSalesAndMarketing, "journey2 Non Deli");
		checkoutInboundshipment2 = findElementInXLSheet(getParameterSalesAndMarketing, "checkout Inboundshipment 2");
		loadedProduct = findElementInXLSheet(getParameterSalesAndMarketing, "loaded Product");

		// 12
		// salesReturnJourney3 = findElementInXLSheet(getParameterSalesAndMarketing,
		// "sales Return Journey3");
		salesReturnJourney3 = findElementInXLSheet(getParameterSalesAndMarketing, "sales Return Journey3");
		productDirect = findElementInXLSheet(getParameterSalesAndMarketing, "product Direct");
		productText = findElementInXLSheet(getParameterSalesAndMarketing, "product Text");
		productSelectedDirect = findElementInXLSheet(getParameterSalesAndMarketing, "product Selected Direct");
		warehouseDirect = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Direct");
		returnReasonDirect = findElementInXLSheet(getParameterSalesAndMarketing, "return Reason Direct");
		expiryDateDirect = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date Direct");
		expiryDateDirectVAl = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date Direct Val");
		manufacDateDirect = findElementInXLSheet(getParameterSalesAndMarketing, "manufac Date Direct");
		serialNoTextDirect = findElementInXLSheet(getParameterSalesAndMarketing, "serial No Text Direct");
		LotNoTextDirect = findElementInXLSheet(getParameterSalesAndMarketing, "Lot No Text Direct");
		expiryDate = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date");
		expiryDateVal = findElementInXLSheet(getParameterSalesAndMarketing, "expiry Date Val");

		// Verify that user cannot create new sales return invoice directly --- 13
		salesReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "sales Return Invoice");
		newSalesReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Return Invoice");
		selectedValReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Return Invoice");

		// Verify that user is able to create Consolidate Invoice --- 14

		consolidatedInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "consolidated Invoice");
		newConsolidateInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "new Consolidate Invoice");
		customerSearchConsolidate = findElementInXLSheet(getParameterSalesAndMarketing, "customer Search Consolidate");
		textConsolidate = findElementInXLSheet(getParameterSalesAndMarketing, "text Consolidate");
		selectedValConsolidate = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Consolidate");
		docList = findElementInXLSheet(getParameterSalesAndMarketing, "doc List");
		checkboxConso = findElementInXLSheet(getParameterSalesAndMarketing, "checkbox Conso");
		applyConso = findElementInXLSheet(getParameterSalesAndMarketing, "apply Conso");
		searchInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "search Invoice");
		checkboxInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "checkbox Invoice");
		applyInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "apply Invoice");

		// Verify that User is able to create new Account -- 15
		accountInformtion = findElementInXLSheet(getParameterSalesAndMarketing, "account Information");
		newAccount = findElementInXLSheet(getParameterSalesAndMarketing, "new Account Acc Infor");
		accountTypeAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "account Type Acc Infor");
		accountNameTypeAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "account Name Type Acc Infor");
		accountNameText = findElementInXLSheet(getParameterSalesAndMarketing, "account Name Text");
		accountGroupAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "account Group Acc Infor");
		currencyAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "currency Acc Infor");
		salesUnitAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Acc Infor");
		accountOwnerAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "account Owner Acc Infor");
		contactTitleAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "contact Title Acc Infor");
		contactNameAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "contact Name Acc Infor");
		phoneAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "phone Acc Infor");
		mobileAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "mobile Acc Infor");
		emailAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "email Acc Infor");
		designationAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "designation Acc Infor");
		draftAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "draft Acc Infor");
		releaseAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "release Acc Infor");
		editAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "edit Acc Infor");
		addedRow = findElementInXLSheet(getParameterSalesAndMarketing, "added Row");
		addContactIcon = findElementInXLSheet(getParameterSalesAndMarketing, "add Contact Icon");
		nameTitleAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "name Title Acc Infor");
		nameAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "name Acc Infor");
		designationAccInforr = findElementInXLSheet(getParameterSalesAndMarketing, "designation Acc Inforr");

		phoneAccInforr = findElementInXLSheet(getParameterSalesAndMarketing, "phone Acc Inforr");

		updateAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "update Acc Infor");
		UpdateBarAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "Update Bar Acc Infor");
		inquiryAccInfo = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry Acc Infor");
		inquiryAboutAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry About Acc Infor");
		categoryAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "category Acc Infor");
		currentStageAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "current Stage Acc Infor");
		updateInquiryAccInfor = findElementInXLSheet(getParameterSalesAndMarketing, "update Inquiry Acc Infor");

		// Verify that User is able to create new Sales Inquiry-- 16
		salesInquiryBtn = findElementInXLSheet(getParameterSalesAndMarketing, "sales Inquiry");
		newSalesInqBtn = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Inquiry");
		leadSelectDropdown = findElementInXLSheet(getParameterSalesAndMarketing, "lead Or Account");
		leadAccSearchBtn = findElementInXLSheet(getParameterSalesAndMarketing, "lead Acc Search Inq");
		leadAccSearchText = findElementInXLSheet(getParameterSalesAndMarketing, "lead Search Text");
		leadSelectedVal = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Inq");
		leadTxtSearchBtn = findElementInXLSheet(getParameterSalesAndMarketing, "search Btn Lead Text");
		inquiryAboutTxt = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry About");
		leadSelect = findElementInXLSheet(getParameterSalesAndMarketing, "update Inquiry Acc Infor");
		inquiryCategory = findElementInXLSheet(getParameterSalesAndMarketing, "category Inq");
		inquiryOrigin = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry Origin");
		salesUnitInq = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unti Inq");
		responsibleEmpSearch = findElementInXLSheet(getParameterSalesAndMarketing, "responsible Employee Inq");
		responsibleEmpText = findElementInXLSheet(getParameterSalesAndMarketing, "responsible Employee Inq Text");
		selectedValEmp = findElementInXLSheet(getParameterSalesAndMarketing, "responsible Person Selected Val");
		priorityDropdownInq = findElementInXLSheet(getParameterSalesAndMarketing, "priority Inq");
		draftInq = findElementInXLSheet(getParameterSalesAndMarketing, "draft Inq");
		addNewContactInq = findElementInXLSheet(getParameterSalesAndMarketing, "add New Contact Inq");
		nameTitleInq = findElementInXLSheet(getParameterSalesAndMarketing, "name Title inq");
		nameTextInq = findElementInXLSheet(getParameterSalesAndMarketing, "name Text Inq");
		designationTextInq = findElementInXLSheet(getParameterSalesAndMarketing, "desig Text Inq");
		phone1TextInq = findElementInXLSheet(getParameterSalesAndMarketing, "phone1 Inq");
		phone2TextInq = findElementInXLSheet(getParameterSalesAndMarketing, "phone2 Inq");
		releaseInq = findElementInXLSheet(getParameterSalesAndMarketing, "release Inq");

		// Verify that User is able to create Lead Infromation -- 17
		leadInformation = findElementInXLSheet(getParameterSalesAndMarketing, "lead Information");
		newLead = findElementInXLSheet(getParameterSalesAndMarketing, "new Lead");
		nameTitleLead = findElementInXLSheet(getParameterSalesAndMarketing, "name Title Lead");
		nameLead = findElementInXLSheet(getParameterSalesAndMarketing, "name Lead Text");
		mobileLeadText = findElementInXLSheet(getParameterSalesAndMarketing, "mobile Lead Text");
		companyLead = findElementInXLSheet(getParameterSalesAndMarketing, "company Text Lead");
		NICLead = findElementInXLSheet(getParameterSalesAndMarketing, "NIC Text Lead");
		designationLead = findElementInXLSheet(getParameterSalesAndMarketing, "designation Lead");
		salesUnitLead = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Lead");
		accountOwnerLead = findElementInXLSheet(getParameterSalesAndMarketing, "account Owner Lead");
		draftLead = findElementInXLSheet(getParameterSalesAndMarketing, "draft Lead");
		releaseLead = findElementInXLSheet(getParameterSalesAndMarketing, "release Lead");
		updateTopBtnLead = findElementInXLSheet(getParameterSalesAndMarketing, "release Inq");
		addNewContactLead = findElementInXLSheet(getParameterSalesAndMarketing, "add New Contact Lead");
		nameTagLead = findElementInXLSheet(getParameterSalesAndMarketing, "name Tag Lead");
		nameNewLead = findElementInXLSheet(getParameterSalesAndMarketing, "name New Lead");
		updateBtnLead = findElementInXLSheet(getParameterSalesAndMarketing, "update Button Lead");
		addInquiryLeadBtn = findElementInXLSheet(getParameterSalesAndMarketing, "add Inquiry Lead Btn");
		inquiryAboutLead = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry About Lead");
		caegoryLead = findElementInXLSheet(getParameterSalesAndMarketing, "category Lead");
		currentStageLead = findElementInXLSheet(getParameterSalesAndMarketing, "current Stage Lead");
		updateLead = findElementInXLSheet(getParameterSalesAndMarketing, "update Lead");
		updateInquiry = findElementInXLSheet(getParameterSalesAndMarketing, "update Inquiry");

		// Contact Information -- 18

		contactInformationBtn = findElementInXLSheet(getParameterSalesAndMarketing, "contact Information Btn");
		newContactBtn = findElementInXLSheet(getParameterSalesAndMarketing, "new Contact Btn");
		nameTitleContact = findElementInXLSheet(getParameterSalesAndMarketing, "name Title Contact");
		nameTagContact = findElementInXLSheet(getParameterSalesAndMarketing, "name Tag Contact");
		// nameContact = findElementInXLSheet(getParameterSalesAndMarketing, "name
		// Contact");
		accountSearchContact = findElementInXLSheet(getParameterSalesAndMarketing, "account Search Contact");
		designationContact = findElementInXLSheet(getParameterSalesAndMarketing, "designation Contact");
		salesUnitContact = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Contact");
		accountOwnerContact = findElementInXLSheet(getParameterSalesAndMarketing, "account Owner Contact");
		phoneContact = findElementInXLSheet(getParameterSalesAndMarketing, "phone Contact");
		mobileContact = findElementInXLSheet(getParameterSalesAndMarketing, "mobile Contact");
		emailContact = findElementInXLSheet(getParameterSalesAndMarketing, "email Contact");
		IDContact = findElementInXLSheet(getParameterSalesAndMarketing, "ID Contact");
		birthdateContact = findElementInXLSheet(getParameterSalesAndMarketing, "birthdate Contact");
		billingContact = findElementInXLSheet(getParameterSalesAndMarketing, "billing Contact");
		countryContact = findElementInXLSheet(getParameterSalesAndMarketing, "country Contact");
		updateContact = findElementInXLSheet(getParameterSalesAndMarketing, "update Contact");
		inquiryInformationTabContact = findElementInXLSheet(getParameterSalesAndMarketing,
				"inquiry Information Tab Contact");
		newInquiryBtnContact = findElementInXLSheet(getParameterSalesAndMarketing, "new Inquiry Btn Contact");
		inquiryAbout = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry About");

		// Verify that User is able to create new Pricing Rule with by one and get one
		// free items --- 19

		pricingRuleBtn = findElementInXLSheet(getParameterSalesAndMarketing, "pricing Rule Btn");
		newPricingRule = findElementInXLSheet(getParameterSalesAndMarketing, "new Pricing Rule");
		newPricingruleookup = findElementInXLSheet(getParameterSalesAndMarketing, "Pricingrule Lookup");
		ruleCode = findElementInXLSheet(getParameterSalesAndMarketing, "rule Code");
		ruleDescription = findElementInXLSheet(getParameterSalesAndMarketing, "rule Description");
		draftPricingRule = findElementInXLSheet(getParameterSalesAndMarketing, "draft Pricing Rule");
		choosePricingPolicyBtn = findElementInXLSheet(getParameterSalesAndMarketing, "choose Pricing Policy Btn");
		policyHeader = findElementInXLSheet(getParameterSalesAndMarketing, "policy Header");
		policyRowVal = findElementInXLSheet(getParameterSalesAndMarketing, "policy Row Val");
		editPolicy = findElementInXLSheet(getParameterSalesAndMarketing, "edit Policy");
		discountTypePolicy = findElementInXLSheet(getParameterSalesAndMarketing, "discount Type Policy");
		fromQty = findElementInXLSheet(getParameterSalesAndMarketing, "from Qty");
		toQty = findElementInXLSheet(getParameterSalesAndMarketing, "to Qty");
		advanceOpt = findElementInXLSheet(getParameterSalesAndMarketing, "advance Opt");
		sameProductCheckBox = findElementInXLSheet(getParameterSalesAndMarketing, "same Product Check Box");
		qtyTextBox = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Box");
		updateInEditValues = findElementInXLSheet(getParameterSalesAndMarketing, "update In Edit Values");
		apply = findElementInXLSheet(getParameterSalesAndMarketing, "apply");
		advanceBtnInPolicy = findElementInXLSheet(getParameterSalesAndMarketing, "advance Btn In Policy");
		activateScheduleBtn = findElementInXLSheet(getParameterSalesAndMarketing, "activate Schedule Btn");
		daily = findElementInXLSheet(getParameterSalesAndMarketing, "daily");
		everyDay = findElementInXLSheet(getParameterSalesAndMarketing, "every Day");
		fromTime = findElementInXLSheet(getParameterSalesAndMarketing, "from Time");
		toTime = findElementInXLSheet(getParameterSalesAndMarketing, "to Time");
		advanceRelationTab = findElementInXLSheet(getParameterSalesAndMarketing, "advance Relation Tab");
		applyBtnAdvanced = findElementInXLSheet(getParameterSalesAndMarketing, "apply Btn Advanced");
		qtyInAdvance = findElementInXLSheet(getParameterSalesAndMarketing, "qty In Advance");
		updateRule = findElementInXLSheet(getParameterSalesAndMarketing, "update Rule");
		activateBtn = findElementInXLSheet(getParameterSalesAndMarketing, "activate Btn");
		confirmationYes = findElementInXLSheet(getParameterSalesAndMarketing, "confirmation Yes");
		validatorAlert = findElementInXLSheet(getParameterSalesAndMarketing, "validator Alert");

		// Verify that User is able to create new Pricing Model with attaching the
		// Pricing rule --- 20

		pricingModel = findElementInXLSheet(getParameterSalesAndMarketing, "pricing Model");
		newPricingModel = findElementInXLSheet(getParameterSalesAndMarketing, "new Pricing Model");
		modelCode = findElementInXLSheet(getParameterSalesAndMarketing, "model Code");
		description = findElementInXLSheet(getParameterSalesAndMarketing, "description");
		rulesTab = findElementInXLSheet(getParameterSalesAndMarketing, "rules Tab");
		addPricingRule = findElementInXLSheet(getParameterSalesAndMarketing, "add Pricing Rule");
		searchTxtRule = findElementInXLSheet(getParameterSalesAndMarketing, "search Txt Rule");
		searchBtnRule = findElementInXLSheet(getParameterSalesAndMarketing, "search Btn Rule");
		selectedRule = findElementInXLSheet(getParameterSalesAndMarketing, "selected Rule");
		editPeriod = findElementInXLSheet(getParameterSalesAndMarketing, "edit Period");
		startDate = findElementInXLSheet(getParameterSalesAndMarketing, "start Date");
		startDateValue = findElementInXLSheet(getParameterSalesAndMarketing, "start Date Value");
		endDateValue = findElementInXLSheet(getParameterSalesAndMarketing, "end Date Value");
		endDate = findElementInXLSheet(getParameterSalesAndMarketing, "end Date");
		applyModel = findElementInXLSheet(getParameterSalesAndMarketing, "apply Model");
		draftModel = findElementInXLSheet(getParameterSalesAndMarketing, "draft Model");
		releaseModel = findElementInXLSheet(getParameterSalesAndMarketing, "release Model");

		// Verify that user is able to created a Sales Order with added Price Model with
		// a Price Rule --- 21
		salesPriceModel = findElementInXLSheet(getParameterSalesAndMarketing, "sales Price Model");
		selectedQty = findElementInXLSheet(getParameterSalesAndMarketing, "selected Qty");
		updateFreeIssues = findElementInXLSheet(getParameterSalesAndMarketing, "update Free Issues");
		applyFreeIssues = findElementInXLSheet(getParameterSalesAndMarketing, "apply Free Issues");
		proSelectedFree = findElementInXLSheet(getParameterSalesAndMarketing, "pro Selected Free");
		closeAlert = findElementInXLSheet(getParameterSalesAndMarketing, "close Alert");
		warehouseFree = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Free");
		qtyFree = findElementInXLSheet(getParameterSalesAndMarketing, "qty Free");
		discountFree = findElementInXLSheet(getParameterSalesAndMarketing, "discount Free");
		taxGroupFree = findElementInXLSheet(getParameterSalesAndMarketing, "tax Group Free");

		// Verify that User is able to created a new Sales Exchage order -- 22

		salesExchangeOrder = findElementInXLSheet(getParameterSalesAndMarketing, "sales Exchange Order");
		newSalesExchangeOrder = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Exchange Order");
		startNewJourney = findElementInXLSheet(getParameterSalesAndMarketing, "start New Journey");
		journey1ExchangeToInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "journey1 Exchange To Invoice");
		customerSearchExchange = findElementInXLSheet(getParameterSalesAndMarketing, "customer Search Exchange");
		documentList = findElementInXLSheet(getParameterSalesAndMarketing, "document List");
		fromDate = findElementInXLSheet(getParameterSalesAndMarketing, "from Date");
		dateSelected = findElementInXLSheet(getParameterSalesAndMarketing, "date Selected");
		toDate = findElementInXLSheet(getParameterSalesAndMarketing, "to Date");
		searchDocumentList = findElementInXLSheet(getParameterSalesAndMarketing, "search Document List");
		checkDocumentList = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List");
		applyExchange = findElementInXLSheet(getParameterSalesAndMarketing, "apply Exchange");
		addNewExchange = findElementInXLSheet(getParameterSalesAndMarketing, "add New Exchange");
		searchProOutput = findElementInXLSheet(getParameterSalesAndMarketing, "search Pro Output");
		searchProText = findElementInXLSheet(getParameterSalesAndMarketing, "search Pro Text");
		selectedOutput = findElementInXLSheet(getParameterSalesAndMarketing, "selected Output");
		warehouseOutput = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Output");
		product1List = findElementInXLSheet(getParameterSalesAndMarketing, "product1 List");
		checkFirst = findElementInXLSheet(getParameterSalesAndMarketing, "check First");
		productList2 = findElementInXLSheet(getParameterSalesAndMarketing, "product List2");
		proDetailsOutput = findElementInXLSheet(getParameterSalesAndMarketing, "pro Details Output");
		SerialWiseCost = findElementInXLSheet(getParameterSalesAndMarketing, "Serial Wise Cost");
		firstSerial = findElementInXLSheet(getParameterSalesAndMarketing, "first Serial");

		// Free text invoice --23
		freeTextInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "free Text Invoice");
		newFreeTextInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "new Free Text Invoice");
		cusAccSearchFreeText = findElementInXLSheet(getParameterSalesAndMarketing, "cus Acc Search Free Text");
		glAccSearch = findElementInXLSheet(getParameterSalesAndMarketing, "gl Account Search");
		glAccText = findElementInXLSheet(getParameterSalesAndMarketing, "gl Account Text");
		selectedRowGLAcc = findElementInXLSheet(getParameterSalesAndMarketing, "selected Row GL Acc");
		GLAmount = findElementInXLSheet(getParameterSalesAndMarketing, "GL Amount");
		checkoutGLAcc = findElementInXLSheet(getParameterSalesAndMarketing, "checkout GL Acc");
		draftGLAcc = findElementInXLSheet(getParameterSalesAndMarketing, "draft GL Acc");
		checkAmount = findElementInXLSheet(getParameterSalesAndMarketing, "check Amount");
		releaseGLAcc = findElementInXLSheet(getParameterSalesAndMarketing, "release GL Acc");

		// Verify that user cannot proceed the Sales Order with adding full qty -- 24
		onHandQty = findElementInXLSheet(getParameterSalesAndMarketing, "on Hand Qty");
		menuProduct = findElementInXLSheet(getParameterSalesAndMarketing, "menu Product");
		menuBtn = findElementInXLSheet(getParameterSalesAndMarketing, "menu Btn");
		menuBtnFull = findElementInXLSheet(getParameterSalesAndMarketing, "menu Btn Full");
		productAvailability = findElementInXLSheet(getParameterSalesAndMarketing, "product Availability");
		closeAvailabilityMenu = findElementInXLSheet(getParameterSalesAndMarketing, "close Availability Menu");
		warehouse2 = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse2");
		warehouseFullQty = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Full Qty");
		qtyTextFull = findElementInXLSheet(getParameterSalesAndMarketing, "qty Text Full");
		unitPriceTxtFull = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Txt Full");
		productListBtn1 = findElementInXLSheet(getParameterSalesAndMarketing, "product List Btn1");
		closeAdvance = findElementInXLSheet(getParameterSalesAndMarketing, "close Advance");
		advanceMenu = findElementInXLSheet(getParameterSalesAndMarketing, "advance Menu");

		// Vehicle Information -- 25
		vehicleInformation = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Information");
		vehicleInformationHeader = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Information Header");
		newVehicleInformationBtn = findElementInXLSheet(getParameterSalesAndMarketing, "new Veh Infor");
		newVehicleInformationHeader = findElementInXLSheet(getParameterSalesAndMarketing,
				"new Vehicle Information Header");
		vehicleNoTxt = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle No Text");
		capacityTxt = findElementInXLSheet(getParameterSalesAndMarketing, "capacity Text");
		capacityMeasure = findElementInXLSheet(getParameterSalesAndMarketing, "capacity Measure");
		weightTxt = findElementInXLSheet(getParameterSalesAndMarketing, "weight Text");
		weightMeasure = findElementInXLSheet(getParameterSalesAndMarketing, "weight Measure");
		draftVehicleInf = findElementInXLSheet(getParameterSalesAndMarketing, "draft Veh Infor");
		vehicleSearchTop = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Search Top");
		sideMenuValVehicleInf = findElementInXLSheet(getParameterSalesAndMarketing, "side Menu Val");
		activeBtnVehicleInf = findElementInXLSheet(getParameterSalesAndMarketing, "activate Btn Veh Infor");
		statusVehicleInf = findElementInXLSheet(getParameterSalesAndMarketing, "status Vehicle Inf");
		alertYes = findElementInXLSheet(getParameterSalesAndMarketing, "alert Yes");
		afterYes = findElementInXLSheet(getParameterSalesAndMarketing, "after Yes");

		// Verify that user is able to create a delivery plan --- 26

		invenWarehouse = findElementInXLSheet(getParameterSalesAndMarketing, "inven Warehouse");
		deliveryPlan = findElementInXLSheet(getParameterSalesAndMarketing, "delivery Plan");
		deliveryPlanInfo = findElementInXLSheet(getParameterSalesAndMarketing, "delivery Plan Info");
		deliveryPlanHeader = findElementInXLSheet(getParameterSalesAndMarketing, "delivery Plan Header");
		bookNo = findElementInXLSheet(getParameterSalesAndMarketing, "book No");
		vehicleSearch = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Search");
		vehicleSearchText = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Search Text");
		vehicleSelectedVal = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Selected Val");
		fromDateVehicle = findElementInXLSheet(getParameterSalesAndMarketing, "fromDate Vehicle");
		fromDateVal = findElementInXLSheet(getParameterSalesAndMarketing, "from Date Val");
		toDateVehicle = findElementInXLSheet(getParameterSalesAndMarketing, "toDate Vehicle");
		toDateVal = findElementInXLSheet(getParameterSalesAndMarketing, "to Date Val");
		draftVehicle = findElementInXLSheet(getParameterSalesAndMarketing, "draft Vehicle");
		planningCount = findElementInXLSheet(getParameterSalesAndMarketing, "planning Count");
		loadingCount = findElementInXLSheet(getParameterSalesAndMarketing, "loading Count");
		deliveredCount = findElementInXLSheet(getParameterSalesAndMarketing, "delivered Count");
		planningTab = findElementInXLSheet(getParameterSalesAndMarketing, "planning Tab");
		valueVehicle = findElementInXLSheet(getParameterSalesAndMarketing, "value Vehicle");
		valueVehicleInLoading = findElementInXLSheet(getParameterSalesAndMarketing, "value Vehicle In Loading");
		releaseBtnVehicle = findElementInXLSheet(getParameterSalesAndMarketing, "release Btn Vehicle");
		countLoading = findElementInXLSheet(getParameterSalesAndMarketing, "count Loading");
		loadingTab = findElementInXLSheet(getParameterSalesAndMarketing, "loading Tab");
		releaseBtnInLoading = findElementInXLSheet(getParameterSalesAndMarketing, "release Btn In Loading");
		addDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketing, "add Delivery Plan");
		fromdateDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketing, "fromdate Delivery Plan");
		fromdateyear= findElementInXLSheet(getParameterSalesAndMarketing, "fromdateyear");
		todateDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketing, "todate Delivery Plan");
		searchBtn = findElementInXLSheet(getParameterSalesAndMarketing, "search Btn");
		selectedValDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Delivery Plan");
		checkboxDeliveryplan = findElementInXLSheet(getParameterSalesAndMarketing, "checkbox Deliveryplan");
		applyDeliveryPlan = findElementInXLSheet(getParameterSalesAndMarketing, "apply Delivery Plan");
		salesInvoiceSearchTxt = findElementInXLSheet(getParameterSalesAndMarketing, "sales Invoice Search Txt");
		deliveredBtn = findElementInXLSheet(getParameterSalesAndMarketing, "delivered Btn");
		countDelivered = findElementInXLSheet(getParameterSalesAndMarketing, "count Delivered");
		verifyPlan = findElementInXLSheet(getParameterSalesAndMarketing, "verify Plan");

		// 4
		reports = findElementInXLSheet(getParameterSalesAndMarketing, "reports");
		reportTemplate = findElementInXLSheet(getParameterSalesAndMarketing, "report Template");
		productCodeDrop = findElementInXLSheet(getParameterSalesAndMarketing, "product Code Drop");
		productCodeText = findElementInXLSheet(getParameterSalesAndMarketing, "product Code Text");
		quickPreview = findElementInXLSheet(getParameterSalesAndMarketing, "quick Preview");
		timePeriod = findElementInXLSheet(getParameterSalesAndMarketing, "time Period");
		today = findElementInXLSheet(getParameterSalesAndMarketing, "today");
		binCardOutboundVal = findElementInXLSheet(getParameterSalesAndMarketing, "bin Card Outbound Val");

		// 5
		binCardInboundVal = findElementInXLSheet(getParameterSalesAndMarketing, "bin Card Inbound Val");

		// 6
		administration = findElementInXLSheet(getParameterSalesAndMarketing, "administration");
		journeyConfiguration = findElementInXLSheet(getParameterSalesAndMarketing, "journey Configuration");
		salesOrderToSalesInvoiceJourney = findElementInXLSheet(getParameterSalesAndMarketing,
				"sales Order To Sales Invoice Journey");
		autogenerateOutboundShipment = findElementInXLSheet(getParameterSalesAndMarketing,
				"autogenerate Outbound Shipment");
		autogenerateSalesInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "autogenerate Sales Invoice");
		updateJourney = findElementInXLSheet(getParameterSalesAndMarketing, "update Journey");
		outboundDoc = findElementInXLSheet(getParameterSalesAndMarketing, "outbound Doc");
		salesInvoiceDoc = findElementInXLSheet(getParameterSalesAndMarketing, "sales Invoice Doc");
		totalSalesInvoiceSer = findElementInXLSheet(getParameterSalesAndMarketing, "total Sales Invoice Ser");

		//
		entutionHeader = findElementInXLSheet(getParameterSalesAndMarketing, "entution Header");
		taskEvent = findElementInXLSheet(getParameterSalesAndMarketing, "task Event");
		salesInvoiceTile = findElementInXLSheet(getParameterSalesAndMarketing, "sales Invoice Tile");
		valueSalesOrder = findElementInXLSheet(getParameterSalesAndMarketing, "value Sales Order");

		// 8
		addNewProduct = findElementInXLSheet(getParameterSalesAndMarketing, "add New Product");
		productSearchOutbound = findElementInXLSheet(getParameterSalesAndMarketing, "product Search Outbound");
		productSearchTxt = findElementInXLSheet(getParameterSalesAndMarketing, "product Search Txt");
		proSelectedVal = findElementInXLSheet(getParameterSalesAndMarketing, "pro Selected Val");

		// smoke
		listInbound = findElementInXLSheet(getParameterSalesAndMarketing, "list Inbound");
		checkDocumentListRet = findElementInXLSheet(getParameterSalesAndMarketing, "check Document List Ret");

		// Account Group Config
		AccountGroupConfiguration = findElementInXLSheet(getParameterSalesAndMarketing, "Account Group Configuration");
		newAccountGroupConfiguration = findElementInXLSheet(getParameterSalesAndMarketing,
				"new Account Group Configuration");
		addNewAccGrp = findElementInXLSheet(getParameterSalesAndMarketing, "add New Acc Grp");
		addNewRecordAccGrp = findElementInXLSheet(getParameterSalesAndMarketing, "add New Record Acc Grp");
		accGrpText = findElementInXLSheet(getParameterSalesAndMarketing, "acc Grp Text");
		updateAccGrp = findElementInXLSheet(getParameterSalesAndMarketing, "update Acc Grp");
		selectAccGroup = findElementInXLSheet(getParameterSalesAndMarketing, "select Acc Group");
		activeCheckBox = findElementInXLSheet(getParameterSalesAndMarketing, "active Check Box");
		currencyAccGroup = findElementInXLSheet(getParameterSalesAndMarketing, "currency Acc Group");
		salesUnitAccGrp = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Acc Grp");
		detailsAccGrp = findElementInXLSheet(getParameterSalesAndMarketing, "details Acc Grp");
		leadDays = findElementInXLSheet(getParameterSalesAndMarketing, "lead Days");
		draftNew = findElementInXLSheet(getParameterSalesAndMarketing, "draft New");
		edit = findElementInXLSheet(getParameterSalesAndMarketing, "edit");
		update = findElementInXLSheet(getParameterSalesAndMarketing, "update");

		mobileAccInf = findElementInXLSheet(getParameterSalesAndMarketing, "mobile Acc Inf");
		annualRevenue = findElementInXLSheet(getParameterSalesAndMarketing, "annual Revenue");
		detailsTab = findElementInXLSheet(getParameterSalesAndMarketing, "details Tab");
		updateAndNew = findElementInXLSheet(getParameterSalesAndMarketing, "update And New");

		// Collection Unit
		collectionUnit = findElementInXLSheet(getParameterSalesAndMarketing, "collection Unit");
		newCollectionUnit = findElementInXLSheet(getParameterSalesAndMarketing, "new Collection Unit");
		collectionUnitCode = findElementInXLSheet(getParameterSalesAndMarketing, "collection Unit Code");
		collectionUnitName = findElementInXLSheet(getParameterSalesAndMarketing, "collection Unit Name");
		commissionRate = findElementInXLSheet(getParameterSalesAndMarketing, "commission Rate");
		employeeSearch = findElementInXLSheet(getParameterSalesAndMarketing, "employee Search");
		employeeSearchText = findElementInXLSheet(getParameterSalesAndMarketing, "employee Search Text");
		selectedValEmployee = findElementInXLSheet(getParameterSalesAndMarketing, "selected Val Employee");
		sharingRate = findElementInXLSheet(getParameterSalesAndMarketing, "sharing Rate");
		history = findElementInXLSheet(getParameterSalesAndMarketing, "history");
		activities = findElementInXLSheet(getParameterSalesAndMarketing, "activities");
		historyReleased = findElementInXLSheet(getParameterSalesAndMarketing, "history Released");
		
		// Sales Order
		POJourneyWindow = findElementInXLSheet(getParameterSalesAndMarketing, "PO Journey Window");
		journeyWindowClose = findElementInXLSheet(getParameterSalesAndMarketing, "journey Window Close");
		reminders = findElementInXLSheet(getParameterSalesAndMarketing, "reminders");
		createReminder = findElementInXLSheet(getParameterSalesAndMarketing, "create Reminder");
		convertInboundCustomerMemo = findElementInXLSheet(getParameterSalesAndMarketing, "convert Inbound Customer Memo");
		
		//Sales Quotation
		unitPriceTextQuat = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Text Quat");
		unitPriceAftTextQuat = findElementInXLSheet(getParameterSalesAndMarketing, "unit Price Aft Text Quat");
		activityHeader = findElementInXLSheet(getParameterSalesAndMarketing, "activity Header");
		activityMenuClose = findElementInXLSheet(getParameterSalesAndMarketing, "activity Menu Close");
		convertToAccount = findElementInXLSheet(getParameterSalesAndMarketing, "convert To Account");
		capacityPlanning = findElementInXLSheet(getParameterSalesAndMarketing, "capacity Planning");
		capacityPlanningHeader = findElementInXLSheet(getParameterSalesAndMarketing, "capacity Planning Header");
		capacityPlanningClose = findElementInXLSheet(getParameterSalesAndMarketing, "capacity Planning Close");
		linkMenuClose = findElementInXLSheet(getParameterSalesAndMarketing, "link Menu Close");
		duplicate = findElementInXLSheet(getParameterSalesAndMarketing, "duplicate");
		generateNewVersion = findElementInXLSheet(getParameterSalesAndMarketing, "generate New Version");
		qoutationVersion = findElementInXLSheet(getParameterSalesAndMarketing, "qoutation Version");
		
		//
		valReturnInvoice = findElementInXLSheet(getParameterSalesAndMarketing, "val Return Invoice");
		hold = findElementInXLSheet(getParameterSalesAndMarketing, "hold");
		
		//Vehicle Information
		vehicleInfEditHeader = findElementInXLSheet(getParameterSalesAndMarketing, "vehicle Inf Edit Header");
		updateVehicleInf = findElementInXLSheet(getParameterSalesAndMarketing, "update Vehicle Inf");
		inactiveBtnVehicleInf = findElementInXLSheet(getParameterSalesAndMarketing, "inactive Btn Vehicle Inf");
		inactivateAlertYes = findElementInXLSheet(getParameterSalesAndMarketing, "inactivate Alert Yes");

		//Consolidate Invoice
		invoiceVal1 = findElementInXLSheet(getParameterSalesAndMarketing, "invoice Val 1");
		invoiceVal2 = findElementInXLSheet(getParameterSalesAndMarketing, "invoice Val 2");
		
		//Warranty Profile
		warrantyProfile = findElementInXLSheet(getParameterSalesAndMarketing, "warranty Profile");
		newWarrantyProfile = findElementInXLSheet(getParameterSalesAndMarketing, "new Warranty Profile");
		warrantyProfileCode = findElementInXLSheet(getParameterSalesAndMarketing, "warranty Profile Code");
		warrantyProfileName = findElementInXLSheet(getParameterSalesAndMarketing, "warranty Profile Name");
		statusWarrantyProfile = findElementInXLSheet(getParameterSalesAndMarketing, "status Warranty Profile");
		
		//Pricing Rule
		pricingRule = findElementInXLSheet(getParameterSalesAndMarketing, "pricing Rule");
		updatePricingRule = findElementInXLSheet(getParameterSalesAndMarketing, "update Pricing Rule");
		resetPricing = findElementInXLSheet(getParameterSalesAndMarketing, "reset Pricing");
		
		//sales Parameter
		salesParameter = findElementInXLSheet(getParameterSalesAndMarketing, "sales Parameter");
		addNewSalesParameter = findElementInXLSheet(getParameterSalesAndMarketing, "add New Sales Parameter");
		salesParameterText = findElementInXLSheet(getParameterSalesAndMarketing, "sales Parameter Text");
		updateSales = findElementInXLSheet(getParameterSalesAndMarketing, "update Sales");
		closeAlertSales = findElementInXLSheet(getParameterSalesAndMarketing, "close Alert Sales");
		
		//Contact Information
		editContact = findElementInXLSheet(getParameterSalesAndMarketing, "edit Contact");
		contactInformation = findElementInXLSheet(getParameterSalesAndMarketing, "contact Information");
		contactInformationHeader = findElementInXLSheet(getParameterSalesAndMarketing, "contact Information Header");
		contactNameValText = findElementInXLSheet(getParameterSalesAndMarketing, "contact Name Val Text");
		createInHistory = findElementInXLSheet(getParameterSalesAndMarketing, "create In History");
		inquiryInformation = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry Information");
		newInquiry = findElementInXLSheet(getParameterSalesAndMarketing, "new Inquiry");
		salesInquiryHeader = findElementInXLSheet(getParameterSalesAndMarketing, "sales Inquiry Header");
		inquiryFirstRow = findElementInXLSheet(getParameterSalesAndMarketing, "inquiry First Row");
		stage = findElementInXLSheet(getParameterSalesAndMarketing, "stage");
		status = findElementInXLSheet(getParameterSalesAndMarketing, "status");
		
		//Lead Information
		leadInformationHeader = findElementInXLSheet(getParameterSalesAndMarketing, "lead Information Header");
		deleteRule = findElementInXLSheet(getParameterSalesAndMarketing, "delete Rule");
		salesUnitNavigation = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Navigation");
		
		newSalesUnit = findElementInXLSheet(getParameterSalesAndMarketing, "new Sales Unit");
		salesUnitCode = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Code");
		unitLeaderSearch = findElementInXLSheet(getParameterSalesAndMarketing, "unit Leader Search");
		unitLeaderText = findElementInXLSheet(getParameterSalesAndMarketing, "unit Leader Text");
		salesUnitName = findElementInXLSheet(getParameterSalesAndMarketing, "sales Unit Name");
		unitLeaderSelected = findElementInXLSheet(getParameterSalesAndMarketing, "unit Leader Selected");
		sharingRateSales = findElementInXLSheet(getParameterSalesAndMarketing, "sharing Rate Sales");
		
		warehouseInformation = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Information");
		newWarehouse = findElementInXLSheet(getParameterSalesAndMarketing, "new Warehouse");
		warehouseCode = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Code");
		warehouseName = findElementInXLSheet(getParameterSalesAndMarketing, "warehouse Name");
		
		txt_ProductA = findElementInXLSheet(getParameterSalesAndMarketing, "ProductA");
		txt_ProductQtyA = findElementInXLSheet(getParameterSalesAndMarketing, "ProductQtyA");
	}

	public static void readData() throws Exception {

		siteURL = findElementInXLSheet(getDataSalesAndMarketing, "site url");
		userNameData = findElementInXLSheet(getDataSalesAndMarketing, "user name data");
		passwordData = findElementInXLSheet(getDataSalesAndMarketing, "password data");
		sales_Marketing = findElementInXLSheet(getDataSalesAndMarketing, "sales and marketing");

		// Sales Order --> Outbound Shipment --> Sales Invoice -- 1

		qtyVal = findElementInXLSheet(getDataSalesAndMarketing, "qty Val");
		descrptionValStockAdj = findElementInXLSheet(getDataSalesAndMarketing, "descrption Val Stock Adj");
		countTextVal = findElementInXLSheet(getDataSalesAndMarketing, "count Text Val");

		customerAccVal = findElementInXLSheet(getDataSalesAndMarketing, "customer Acc Val");
		billingAddrVal = findElementInXLSheet(getDataSalesAndMarketing, "billing Addr Val");
		shippingAddrVal = findElementInXLSheet(getDataSalesAndMarketing, "shipping Addr Val");
		productTextValSer = findElementInXLSheet(getDataSalesAndMarketing, "product Text Val Ser");
		productTextValInv = findElementInXLSheet(getDataSalesAndMarketing, "product Text Val Inv");
		unitPriceVal = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Val");
		unitPriceValSerialSpec = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Val Serial Spec");
		unitPriceValBatchSpec = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Val Batch Spec");
		unitPriceValInv = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Val Inv");
		quantityVal = findElementInXLSheet(getDataSalesAndMarketing, "quantity Val");
		quantityValInv = findElementInXLSheet(getDataSalesAndMarketing, "quantity Val Inv");
		quantityValSerialSpec = findElementInXLSheet(getDataSalesAndMarketing, "quantity Val Serial Spec");
		serialCountVal = findElementInXLSheet(getDataSalesAndMarketing, "serial Count Val");
		serialNo = findElementInXLSheet(getDataSalesAndMarketing, "serial No");
		warehouseVal1 = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Val1");
		salesUnitVal = findElementInXLSheet(getDataSalesAndMarketing, "sales Unit Val");

		batchNoValBatchSpecific = findElementInXLSheet(getDataSalesAndMarketing, "batch NoVal Batch Specific");
		serialBatchSpecificVal = findElementInXLSheet(getDataSalesAndMarketing, "serial Batch Specific Val");

		// 2
		customerAccServiceVal = findElementInXLSheet(getDataSalesAndMarketing, "customer Acc Service Val");
		shippingAddrSerVal = findElementInXLSheet(getDataSalesAndMarketing, "shipping Addr Ser Val");
		billingAddrSerVal = findElementInXLSheet(getDataSalesAndMarketing, "billing Addr Ser Val");
		productSerVal = findElementInXLSheet(getDataSalesAndMarketing, "product Ser Val");
		unitPriceValue = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Value");

		// 3
		customerAccOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "customer Acc Outbound Val");
		billingOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "billing Outbound Val");
		shippingOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "shipping Outbound Val");
		serviceOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "service Outbound Val");
		invOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "inv Outbound Val");
		warehouseVal3 = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Val3");
		unitpriceSerOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "unitprice SerOutbound Val");
		unitpriceInvOutboundVal = findElementInXLSheet(getDataSalesAndMarketing, "unitprice InvOutbound Val");

		// 4
		dropshipmentSerVal = findElementInXLSheet(getDataSalesAndMarketing, "dropshipment Ser Val");
		dropshipmentInvVal = findElementInXLSheet(getDataSalesAndMarketing, "dropshipment Inv Val");
		vendorVal = findElementInXLSheet(getDataSalesAndMarketing, "vendor Val");

		// 5
		serialNumVal = findElementInXLSheet(getDataSalesAndMarketing, "serial Num Val");
		warehouseVal = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Val");

		// 6
		unitPriceSerVal = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Val Ser");
		qtyValSalesInvoiceSer = findElementInXLSheet(getDataSalesAndMarketing, "qty Val Sales Invoice Ser");

		// 7
		customerAccValQuat = findElementInXLSheet(getDataSalesAndMarketing, "customer Acc Val Quat");
		unitPriceQuatVal = findElementInXLSheet(getDataSalesAndMarketing, "unit Price Val Inv");
		qtyValQout = findElementInXLSheet(getDataSalesAndMarketing, "qty Val Qout");
		qtyQuatationSerVal = findElementInXLSheet(getDataSalesAndMarketing, "qty Quatation Ser Val");
		qtyQuatationInvVal = findElementInXLSheet(getDataSalesAndMarketing, "qty Quatation Inv Val");
		billingAddrQuatVal = findElementInXLSheet(getDataSalesAndMarketing, "billing Addr Quat Val");
		shippingAddrQuatVal = findElementInXLSheet(getDataSalesAndMarketing, "shipping Addr Quat Val");
		serialNoVal = findElementInXLSheet(getDataSalesAndMarketing, "serial No Val");
		warehouseValQuot = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Val Quot");

		// 8
		serialNo2 = findElementInXLSheet(getDataSalesAndMarketing, "serial No2");
		vendorVal2 = findElementInXLSheet(getDataSalesAndMarketing, "vendor Val2");

		// 9
		serialNo3 = findElementInXLSheet(getDataSalesAndMarketing, "serial No3");

		// 10
		customerAccReturnVal = findElementInXLSheet(getDataSalesAndMarketing, "customer Acc Return Val");

		// 12
		productValDirect = findElementInXLSheet(getDataSalesAndMarketing, "product Val Direct");
		warehouseDirectVal = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Direct Val");
		returnReasonVal = findElementInXLSheet(getDataSalesAndMarketing, "return Reason Val");

		// Account Information
		accountNameVal = findElementInXLSheet(getDataSalesAndMarketing, "account Name Val");
		contactNameVal = findElementInXLSheet(getDataSalesAndMarketing, "contact Name Val");
		phoneNoVal = findElementInXLSheet(getDataSalesAndMarketing, "phone No Val");
		mobileNoVal = findElementInXLSheet(getDataSalesAndMarketing, "mobile No Val");
		emailVal = findElementInXLSheet(getDataSalesAndMarketing, "email Val");
		designationVal = findElementInXLSheet(getDataSalesAndMarketing, "designation Val");
		inquiryAboutVal = findElementInXLSheet(getDataSalesAndMarketing, "inquiry About Val");

		// Sales Inquiry -- 16
		leadAccSearchVal = findElementInXLSheet(getDataSalesAndMarketing, "lead Account Search Val");
		salesInquiryAboutVal = findElementInXLSheet(getDataSalesAndMarketing, "sales Inquiry About Val");
		responsibleEmpVal = findElementInXLSheet(getDataSalesAndMarketing, "responsible Employee Text Val");
		nameTextSalesInqVal = findElementInXLSheet(getDataSalesAndMarketing, "name Text Sales Inq Val");
		salesInqDesigVal = findElementInXLSheet(getDataSalesAndMarketing, "sales Inq Desig Text Val");
		salesInqPhone1Val = findElementInXLSheet(getDataSalesAndMarketing, "sales Inq Phone1 Text Val");
		salesInqPhone2Val = findElementInXLSheet(getDataSalesAndMarketing, "sales Inq Phone2 Text Val");

		// Lead Information -- 17
		nameTextLeadVal = findElementInXLSheet(getDataSalesAndMarketing, "name Lead Val");
		mobileTexLeadtVal = findElementInXLSheet(getDataSalesAndMarketing, "mobile Lead Text Val");
		companyLeadVal = findElementInXLSheet(getDataSalesAndMarketing, "company Lead Val");
		NICLeadVAl = findElementInXLSheet(getDataSalesAndMarketing, "NIC Lead Val");
		designationLeadVal = findElementInXLSheet(getDataSalesAndMarketing, "designation Lead Val");
		phoneLeadVal = findElementInXLSheet(getDataSalesAndMarketing, "phone Lead Val");
		companyUpdateVal = findElementInXLSheet(getDataSalesAndMarketing, "company Update Val");
		NameUpdateVal = findElementInXLSheet(getDataSalesAndMarketing, "name Update Val");
		inquiryUpdateAboutVal = findElementInXLSheet(getDataSalesAndMarketing, "inquiry Update About Val");

		// Create Contact Information -- 18
		nameContactVal = findElementInXLSheet(getDataSalesAndMarketing, "name Contact Val");
		designationContactVal = findElementInXLSheet(getDataSalesAndMarketing, "designation Contact Val");
		phoneContatcVal = findElementInXLSheet(getDataSalesAndMarketing, "phone Contatc Val");
		mobileContactVal = findElementInXLSheet(getDataSalesAndMarketing, "mobile Contact Val");
		emailContatctVal = findElementInXLSheet(getDataSalesAndMarketing, "email Contatct Val");
		inquiryAboutValu = findElementInXLSheet(getDataSalesAndMarketing, "inquiry About Valu");

		// Pricing Rule -- 19
		ruleCodeVal = findElementInXLSheet(getDataSalesAndMarketing, "rule Code Val");
		ruleDescriptionVal = findElementInXLSheet(getDataSalesAndMarketing, "rule Description Val");
		fromVal = findElementInXLSheet(getDataSalesAndMarketing, "from Val");
		toVal = findElementInXLSheet(getDataSalesAndMarketing, "to Val");
		qtyEditVal = findElementInXLSheet(getDataSalesAndMarketing, "qty Edit Val");
		fromTimeVal = findElementInXLSheet(getDataSalesAndMarketing, "from Time Val");
		toTimeVal = findElementInXLSheet(getDataSalesAndMarketing, "to Time Val");
		

		// Verify that User is able to create new Pricing Model with attaching the
		// Pricing rule -- 20
		modelCodeVal = findElementInXLSheet(getDataSalesAndMarketing, "model Code Val");
		modelDescriptionVal = findElementInXLSheet(getDataSalesAndMarketing, "model Description Val");
		pricingRuleVal = findElementInXLSheet(getDataSalesAndMarketing, "pricing Rule Val");
		
		//
		selectedQtyVal = findElementInXLSheet(getDataSalesAndMarketing, "selected Qty Val");

		// 22
		outputProval = findElementInXLSheet(getDataSalesAndMarketing, "output Proval");
		warehouseOutputPro = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Output Pro");

		// Verify that User is able to create Free Text Invoice --- 23
		glAccVal = findElementInXLSheet(getDataSalesAndMarketing, "gl Acc Val");
		GLAmountVal = findElementInXLSheet(getDataSalesAndMarketing, "GL Amount Val");

		// 24
		productValInv = findElementInXLSheet(getDataSalesAndMarketing, "product Val Inv");
		productTextVal2 = findElementInXLSheet(getDataSalesAndMarketing, "product Text Val2");

		// glaccounttext = findElementInXLSheet(getDataSalesAndMarketing, "sales Unit
		// Val");

		// Verify that user cannot proceed the Sales Order with adding full qty as
		// Separate product ine in same Sales Order

		// Active Vehicle Information -- 25
		vehicleNoTxtVal = findElementInXLSheet(getDataSalesAndMarketing, "vehicle No Txt Val");
		capacityTxtVal = findElementInXLSheet(getDataSalesAndMarketing, "capacity Txt Val");
		weightTxtVal = findElementInXLSheet(getDataSalesAndMarketing, "weight Txt Val");

		// Verify that user is able to create a delivery plan --- 26
		vehicleSearchVal = findElementInXLSheet(getDataSalesAndMarketing, "vehicle Search Val");

		// s4
		ProductCodeVal = findElementInXLSheet(getDataSalesAndMarketing, "Product Code Val");

		// Account Group
		accGrpVal = findElementInXLSheet(getDataSalesAndMarketing, "acc Grp Val");
		currencyVal = findElementInXLSheet(getDataSalesAndMarketing, "currency Val");
		leadDaysVal = findElementInXLSheet(getDataSalesAndMarketing, "lead Days Val");

		//
		annualRevenueVal = findElementInXLSheet(getDataSalesAndMarketing, "annual Revenue Val");
		newMobileNo = findElementInXLSheet(getDataSalesAndMarketing, "new Mobile No");

		// Collection unit
		collectionNameVal = findElementInXLSheet(getDataSalesAndMarketing, "collection Name Val");
		commissionRateVal = findElementInXLSheet(getDataSalesAndMarketing, "commission Rate Val");
		employeeVal = findElementInXLSheet(getDataSalesAndMarketing, "employee Val");
		sharingRateVal = findElementInXLSheet(getDataSalesAndMarketing, "sharing Rate Val");
		commisionRateEditedVal = findElementInXLSheet(getDataSalesAndMarketing, "commision Rate Edited Val");
		sharingRateNewVal = findElementInXLSheet(getDataSalesAndMarketing, "sharing Rate New Val");
		
		//Sales Quotation
		unitPriceQuatValEdit = findElementInXLSheet(getDataSalesAndMarketing, "unitPrice Quat Val Edit");
		
		//Vehicle Information
		weightTxtNewVal = findElementInXLSheet(getDataSalesAndMarketing, "weight Txt New Val");
		
		//Warranty Profile
		profileNameVal = findElementInXLSheet(getDataSalesAndMarketing, "profile Name Val");
		profileNameNew = findElementInXLSheet(getDataSalesAndMarketing, "profile Name New");
		
		//Pricing Rule
		ruleDescriptionNewVal = findElementInXLSheet(getDataSalesAndMarketing, "rule Description New Val");
		
		//Contact Name
		contactNameNewVal = findElementInXLSheet(getDataSalesAndMarketing, "contact Name New Val");
		
		//Lead Information
		nameTextLeadNewVal = findElementInXLSheet(getDataSalesAndMarketing, "name Text Lead New Val");
		
		descriptionVal = findElementInXLSheet(getDataSalesAndMarketing, "description Val");
		
		unitLeaderVal = findElementInXLSheet(getDataSalesAndMarketing, "unit Leader Val");
		salesUnitNameVal = findElementInXLSheet(getDataSalesAndMarketing, "sales Unit Name Val");
		warehouseNameVal = findElementInXLSheet(getDataSalesAndMarketing, "warehouse Name Val");
		businessUnitVal = findElementInXLSheet(getDataSalesAndMarketing, "business Unit Val");
		
	}

}
