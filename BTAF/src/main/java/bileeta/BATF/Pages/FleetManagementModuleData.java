package bileeta.BATF.Pages;

import bileeta.BTAF.Utilities.TestBase;

public class FleetManagementModuleData extends TestBase {

	/* Reading the element locators to variables */
	
	//TC_MAT_RA_01
	protected static String txt_UserName;
	protected static String txt_Password;
	protected static String btn_LoginBtn;
	protected static String icn_SiteLogo;
	protected static String txt_HomeHeader;
	protected static String btn_NavButton;
	protected static String txt_SideMenu;
	protected static String txt_SubSideMenu;
	protected static String btn_FleetManagementBtn;
	protected static String btn_RentAgreementBtn;
	protected static String btn_NewRentAgreementBtn;
	protected static String txt_PageHeader;
	protected static String txt_PostDateFieldRA;
	protected static String txt_PostDateTextFieldRA;
	protected static String btn_PostDateApplyBtnRA;
	protected static String btn_AccountSearchBtnRA;
	protected static String txt_AccountSearchTxtRA;
	protected static String sel_AccountSearchSelRA;
	protected static String txt_ContactPersonRA;
	protected static String btn_VehicleSearchBtnRA;
	protected static String txt_VehicleSearchTxtRA;
	protected static String sel_VehicleSearchSelRA;
	protected static String txt_AgreementGroupRA;
	protected static String txt_CheckOutDateFieldRA;
	protected static String txt_CheckOutTimeFieldRA;
	protected static String sel_CheckOutTimeSelRA;
	protected static String btn_CheckOutTimeOkBtnRA;
	protected static String txt_CheckInDateFieldRA;
	protected static String txt_CheckInTimeFieldRA;
	protected static String sel_CheckInTimeSelRA;
	protected static String btn_CheckInTimeOkBtnRA;
	protected static String sel_DateSelRA;
	protected static String txt_BilingTypeRA;
	protected static String txt_RateTypeRA;
	protected static String btn_PackageSearchBtnRA;
	protected static String txt_PackageSearchTxtRA;
	protected static String sel_PackageSearchSelRA;
	protected static String txt_MileAgeAllocationTypeRA;
	protected static String txt_DriverNameRA;
	protected static String txt_LicenseNoRA;
	protected static String txt_IssuedDateFieldRA;
	protected static String txt_IssuedDateYearRA;
	protected static String txt_ExpiryDateFieldRA;
	protected static String txt_ExpiryDateYearRA;
	protected static String btn_EstimationTabRA;
	protected static String txt_RateRA;
	protected static String txt_PackgeAmountRA;
	protected static String btn_CheckoutBtn;
	protected static String btn_DraftBtn;
	protected static String btn_ReleaseBtn;
	protected static String btn_OKBtnRA;
	protected static String txt_PageDraft1;
	protected static String txt_PageRelease1;
	protected static String txt_TrackCode;
	protected static String btn_VersionBtnRA;
	protected static String sel_ColorSelectedRA;
	protected static String btn_ConfirmThisVersionBtnRA;
	protected static String btn_ConfirmBtnRA;
	protected static String txt_PageConfirmed;
	
	//TC_MAT_RA_02
	protected static String btn_WithDriverCheckBoxRA;
	protected static String btn_ActionBtn;
	protected static String btn_HireAllocationBtnRA;
	protected static String btn_HireAllocationCheckboxRA;
	protected static String btn_GenerateHireAllocationBtnRA;
	protected static String btn_ScrollBtnRightRA;
	protected static String btn_DriverSearchBtnRA;
	protected static String txt_DriverSearchTxtRA;
	protected static String sel_DriverSearchSelRA;
	protected static String btn_OKBtnHA;
	protected static String btn_DocStatusDropdownBtnRA;
	protected static String txt_DocStatusDropdownTxtRA;
	protected static String txt_HireAllocationNumberRA;
	
	//TC_MAT_RA_13
	protected static String txt_DropOffLocationRA;
	protected static String sel_SameAsCustomerAddressCboxRA;
	protected static String txt_DropOffAddressRA;
	
	//TC_MAT_RA_25
	protected static String txt__PickUpLocationRA;
	protected static String sel_PickUpSameAsCustomerAddressCboxRA;
	protected static String txt_PickUpAddressRA;
	
	//TC_CAL_YY_H_01
	protected static String btn_PackageInformationBtnRA;
	protected static String sel_PackageInformationFilterRA;
	protected static String txt_PackageInformationSearchTxtRA;
	protected static String sel_PackageInformationSearchSelRA;
	protected static String txt_PackageInvoicingProductRA;
	protected static String txt_PackageExtraMileageProductRA;
	protected static String txt_PackageExtraTimeProductRA;
	protected static String txt_AllottedMileageRA;
	protected static String txt_ExtraMileageRateRA;
	protected static String txt_AllotedHoursRA;
	protected static String txt_ExtraHourRateRA;
	protected static String btn_CheckOutVehicleBtnRA;
	protected static String txt_CheckoutMilageRA;
	protected static String txt_CheckoutDateRA;
	protected static String txt_CheckoutTimeRA;
	protected static String sel_CheckoutTimeSelRA;
	protected static String btn_CheckoutTimeOkBtnRA;
	protected static String txt_DocStatusDropdownTxt2RA;
	protected static String btn_CheckInVehicleBtnRA;
	protected static String txt_CheckinMilageRA;
	protected static String txt_CheckinDateRA;
	protected static String txt_CheckinTimeRA;
	protected static String sel_CheckinTimeSelRA;
	protected static String btn_CheckinTimeOkBtnRA;
	protected static String btn_CheckInVehicleCalculateBtnA;
	protected static String txt_ExcessMileageTxtRA;
	protected static String txt_ExcessMileageChargeTxtRA;
	protected static String txt_ExcessHoursTxtRA;
	protected static String txt_ExcessHoursChargeTxtRA;
	protected static String txt_DocStatusDropdownTxt3RA;
	
	//TC_CAL_YY_H_04
	protected static String txt_DocStatusDropdownTxt4RA;
	protected static String txt_DocStatusDropdownTxt5RA;
	protected static String btn_DriverReplacementBtnRA;
	protected static String txt_DriverReplacementCheInMilageRA;
	protected static String txt_DriverReplacementCheInDateRA;
	protected static String txt_DriverReplacementCheInTimeRA;
	protected static String sel_DriverReplacementCheInTimeSelRA;
	protected static String btn_DriverReplacementCheInTimeOkBtnRA;
	protected static String btn_ReplacementDriverBtnRA;
	protected static String txt_ReplacementDriverTxtRA;
	protected static String sel_ReplacementDriverSelRA;
	protected static String txt_DriverReplacementCheOutMilageRA;
	protected static String txt_DriverReplacementCheOutDateRA;
	protected static String txt_DriverReplacementCheOutTimeRA;
	protected static String sel_DriverReplacementCheOutTimeSelRA;
	protected static String btn_DriverReplacementCheOutTimeOkBtnRA;
	
	//TC_CAL_YY_H_05
	protected static String btn_VehicleReplacementBtnRA;
	protected static String txt_VehicleReplacementCheInMilageRA;
	protected static String txt_VehicleReplacementCheInDateRA;
	protected static String txt_VehicleReplacementCheInTimeRA;
	protected static String sel_VehicleReplacementCheInTimeSelRA;
	protected static String btn_VehicleReplacementCheInTimeOkBtnRA;
	protected static String btn_VehicleReplacementCheckOutBtnRA;
	protected static String btn_ReplacementVehicleBtnRA;
	protected static String txt_ReplacementVehicleTxtRA;
	protected static String sel_ReplacementVehicleSelRA;
	protected static String txt_ReplacementVehicleCheOutMilageRA;
	protected static String txt_VehicleReplacementCheOutDateRA;
	protected static String txt_VehicleReplacementCheOutTimeRA;
	protected static String sel_VehicleReplacementCheOutTimeSelRA;
	protected static String btn_VehicleReplacementCheOutTimeOkBtnRA;
	
	//TC_CAL_YL_H_01
	protected static String btn_CustomerCheckInBtnRA;
	protected static String txt_CustomerCheckInCurrentMileageRA;
	protected static String txt_CustomerCheckInChkInMileageRA;
	protected static String txt_CustomerCheckInCheckInDateRA;
	protected static String txt_CustomerCheckInCheckInTimeRA;
	protected static String sel_CustomerCheckInCheckInTimeSelRA;
	protected static String btn_CustomerCheckInCheckInTimeOkBtnRA;
	protected static String txt_DropOffAdditionalMileageRA;
	
	//TC_CAL_LY_H_01
	protected static String btn_CustomerCheckOutBtnRA;
	protected static String txt_CustomerCheckOutCurrentMileageRA;
	protected static String txt_CustomerCheckOutCheckOutMileageRA;
	protected static String txt_CustomerCheckOutCheckOutDateRA;
	protected static String txt_CustomerCheckOutCheckOutTimeRA;
	protected static String sel_CustomerCheckOutCheckOutTimeSelRA;
	protected static String btn_CustomerCheckOutCheckOutTimeOkBtnRA;
	protected static String txt_PickUpAdditionalMileageRA;
	
	//Sithara======================================================================================
	
	
	
	/* Reading the Data to variables */
	
	//TC_MAT_RA_01
	protected static String SiteUrl;
	protected static String UserName;
	protected static String Password;
	protected static String AccountSearchTxtRA;
	protected static String ContactPersonRA;
	protected static String VehicleSearchTxtRA;
	protected static String AgreementGroupRA;
	protected static String BilingTypeRA;
	protected static String RateTypeRA;
	protected static String PackageSearchTxt1RA;
	protected static String MileAgeAllocationType1RA;
	protected static String DriverNameRA;
	protected static String LicenseNoRA;
	
	//TC_MAT_RA_02
	protected static String PackageSearchTxt2RA;
	protected static String DriverSearchTxtRA;
	
	//TC_MAT_RA_03
	protected static String RateType2RA;
	protected static String PackageSearchTxt3RA;
	
	//TC_MAT_RA_04
	protected static String PackageSearchTxt4RA;
	
	//TC_MAT_RA_05
	protected static String RateType3RA;
	protected static String PackageSearchTxt5RA;
	
	//TC_MAT_RA_06
	protected static String PackageSearchTxt6RA;
	
	//TC_MAT_RA_13
	protected static String MileAgeAllocationType2RA;
	protected static String DropOffLocationRA;
	protected static String DropOffAddressRA;
	
	//TC_MAT_RA_25
	protected static String MileAgeAllocationType3RA;
	protected static String PickUpLocationRA;
	protected static String PickUpAddressRA;

	//TC_MAT_RA_37
	protected static String MileAgeAllocationType4RA;
	
	//TC_CAL_YY_H_04
	protected static String ReplacementDriverTxtRA;
	
	//TC_CAL_YY_H_05
	protected static String ReplacementVehicleTxtRA;

	//Sithara======================================================================================
	
	//TC_MAT_RA_15
	protected static String RateTypeRA2;
	
	//CAL_COMMOM
	protected static String Dformat1;
	
	//TC_CAL_YY_D_03
	protected static String ReplacementDriverTxtRASI;
	
	//TC_CAL_YY_D_04
	protected static String ReplacementVehicleTxtRASI;
	
	//TC_CAL_YY_D_05
	protected static String ReplacementVehicleTxtRASI2;


	// Calling the constructor
	public static void readElementlocators() throws Exception {
		
		//TC_MAT_RA_01
		txt_UserName= findElementInXLSheet(getParameterFleet,"UserName");
		txt_Password= findElementInXLSheet(getParameterFleet,"Password");
		btn_LoginBtn= findElementInXLSheet(getParameterFleet,"LoginBtn");
		icn_SiteLogo= findElementInXLSheet(getParameterFleet,"SiteLogo");
		txt_HomeHeader= findElementInXLSheet(getParameterFleet,"HomeHeader");
		btn_NavButton= findElementInXLSheet(getParameterFleet,"NavButton");
		txt_SideMenu= findElementInXLSheet(getParameterFleet,"SideMenu");
		txt_SubSideMenu= findElementInXLSheet(getParameterFleet,"SubSideMenu");
		btn_FleetManagementBtn= findElementInXLSheet(getParameterFleet,"FleetManagementBtn");
		btn_RentAgreementBtn= findElementInXLSheet(getParameterFleet,"RentAgreementBtn");
		btn_NewRentAgreementBtn= findElementInXLSheet(getParameterFleet,"NewRentAgreementBtn");
		txt_PageHeader= findElementInXLSheet(getParameterFleet,"PageHeader");
		txt_PostDateFieldRA= findElementInXLSheet(getParameterFleet,"PostDateFieldRA");
		txt_PostDateTextFieldRA= findElementInXLSheet(getParameterFleet,"PostDateTextFieldRA");
		btn_PostDateApplyBtnRA= findElementInXLSheet(getParameterFleet,"PostDateApplyBtnRA");
		btn_AccountSearchBtnRA= findElementInXLSheet(getParameterFleet,"AccountSearchBtnRA");
		txt_AccountSearchTxtRA= findElementInXLSheet(getParameterFleet,"AccountSearchTxtRA");
		sel_AccountSearchSelRA= findElementInXLSheet(getParameterFleet,"AccountSearchSelRA");
		txt_ContactPersonRA= findElementInXLSheet(getParameterFleet,"ContactPersonRA");
		btn_VehicleSearchBtnRA= findElementInXLSheet(getParameterFleet,"VehicleSearchBtnRA");
		txt_VehicleSearchTxtRA= findElementInXLSheet(getParameterFleet,"VehicleSearchTxtRA");
		sel_VehicleSearchSelRA= findElementInXLSheet(getParameterFleet,"VehicleSearchSelRA");
		txt_AgreementGroupRA= findElementInXLSheet(getParameterFleet,"AgreementGroupRA");
		txt_CheckOutDateFieldRA= findElementInXLSheet(getParameterFleet,"CheckOutDateFieldRA");
		txt_CheckOutTimeFieldRA= findElementInXLSheet(getParameterFleet,"CheckOutTimeFieldRA");
		sel_CheckOutTimeSelRA= findElementInXLSheet(getParameterFleet,"CheckOutTimeSelRA");
		btn_CheckOutTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"CheckOutTimeOkBtnRA");
		txt_CheckInDateFieldRA= findElementInXLSheet(getParameterFleet,"CheckInDateFieldRA");
		txt_CheckInTimeFieldRA= findElementInXLSheet(getParameterFleet,"CheckInTimeFieldRA");
		sel_CheckInTimeSelRA= findElementInXLSheet(getParameterFleet,"CheckInTimeSelRA");
		btn_CheckInTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"CheckInTimeOkBtnRA");
		sel_DateSelRA= findElementInXLSheet(getParameterFleet,"DateSelRA");
		txt_BilingTypeRA= findElementInXLSheet(getParameterFleet,"BilingTypeRA");
		txt_RateTypeRA= findElementInXLSheet(getParameterFleet,"RateTypeRA");
		btn_PackageSearchBtnRA= findElementInXLSheet(getParameterFleet,"PackageSearchBtnRA");
		txt_PackageSearchTxtRA= findElementInXLSheet(getParameterFleet,"PackageSearchTxtRA");
		sel_PackageSearchSelRA= findElementInXLSheet(getParameterFleet,"PackageSearchSelRA");
		txt_MileAgeAllocationTypeRA= findElementInXLSheet(getParameterFleet,"MileAgeAllocationTypeRA");
		txt_DriverNameRA= findElementInXLSheet(getParameterFleet,"DriverNameRA");
		txt_LicenseNoRA= findElementInXLSheet(getParameterFleet,"LicenseNoRA");
		txt_IssuedDateFieldRA= findElementInXLSheet(getParameterFleet,"IssuedDateFieldRA");
		txt_IssuedDateYearRA= findElementInXLSheet(getParameterFleet,"IssuedDateYearRA");
		txt_ExpiryDateFieldRA= findElementInXLSheet(getParameterFleet,"ExpiryDateFieldRA");
		txt_ExpiryDateYearRA= findElementInXLSheet(getParameterFleet,"ExpiryDateYearRA");
		btn_EstimationTabRA= findElementInXLSheet(getParameterFleet,"EstimationTabRA");
		txt_RateRA= findElementInXLSheet(getParameterFleet,"RateRA");
		txt_PackgeAmountRA= findElementInXLSheet(getParameterFleet,"PackgeAmountRA");
		btn_CheckoutBtn= findElementInXLSheet(getParameterFleet,"CheckoutBtn");
		btn_DraftBtn= findElementInXLSheet(getParameterFleet,"DraftBtn");
		btn_ReleaseBtn= findElementInXLSheet(getParameterFleet,"ReleaseBtn");
		btn_OKBtnRA= findElementInXLSheet(getParameterFleet,"OKBtnRA");
		txt_PageDraft1= findElementInXLSheet(getParameterFleet,"PageDraft1");
		txt_PageRelease1= findElementInXLSheet(getParameterFleet,"PageRelease1");
		txt_TrackCode= findElementInXLSheet(getParameterFleet,"TrackCode");
		btn_VersionBtnRA= findElementInXLSheet(getParameterFleet,"VersionBtnRA");
		sel_ColorSelectedRA= findElementInXLSheet(getParameterFleet,"ColorSelectedRA");
		btn_ConfirmThisVersionBtnRA= findElementInXLSheet(getParameterFleet,"ConfirmThisVersionBtnRA");
		btn_ConfirmBtnRA= findElementInXLSheet(getParameterFleet,"ConfirmBtnRA");
		txt_PageConfirmed= findElementInXLSheet(getParameterFleet,"PageConfirmed");
		
		//TC_MAT_RA_02
		btn_WithDriverCheckBoxRA= findElementInXLSheet(getParameterFleet,"WithDriverCheckBoxRA");
		btn_ActionBtn= findElementInXLSheet(getParameterFleet,"ActionBtn");
		btn_HireAllocationBtnRA= findElementInXLSheet(getParameterFleet,"HireAllocationBtnRA");
		btn_HireAllocationCheckboxRA= findElementInXLSheet(getParameterFleet,"HireAllocationCheckboxRA");
		btn_GenerateHireAllocationBtnRA= findElementInXLSheet(getParameterFleet,"GenerateHireAllocationBtnRA");
		btn_ScrollBtnRightRA= findElementInXLSheet(getParameterFleet,"ScrollBtnRightRA");
		btn_DriverSearchBtnRA= findElementInXLSheet(getParameterFleet,"DriverSearchBtnRA");
		txt_DriverSearchTxtRA= findElementInXLSheet(getParameterFleet,"DriverSearchTxtRA");
		sel_DriverSearchSelRA= findElementInXLSheet(getParameterFleet,"DriverSearchSelRA");
		btn_OKBtnHA= findElementInXLSheet(getParameterFleet,"OKBtnHA");
		btn_DocStatusDropdownBtnRA= findElementInXLSheet(getParameterFleet,"DocStatusDropdownBtnRA");
		txt_DocStatusDropdownTxtRA= findElementInXLSheet(getParameterFleet,"DocStatusDropdownTxtRA");
		txt_HireAllocationNumberRA= findElementInXLSheet(getParameterFleet,"HireAllocationNumberRA");
		
		//TC_MAT_RA_13
		txt_DropOffLocationRA= findElementInXLSheet(getParameterFleet,"DropOffLocationRA");
		sel_SameAsCustomerAddressCboxRA= findElementInXLSheet(getParameterFleet,"SameAsCustomerAddressCboxRA");
		txt_DropOffAddressRA= findElementInXLSheet(getParameterFleet,"DropOffAddressRA");

		//TC_MAT_RA_25
		txt__PickUpLocationRA= findElementInXLSheet(getParameterFleet,"PickUpLocationRA");
		sel_PickUpSameAsCustomerAddressCboxRA= findElementInXLSheet(getParameterFleet,"PickUpSameAsCustomerAddressCboxRA");
		txt_PickUpAddressRA=findElementInXLSheet(getParameterFleet,"PickUpAddressRA");

		
		//TC_CAL_YY_H_01
		btn_PackageInformationBtnRA= findElementInXLSheet(getParameterFleet,"PackageInformationBtnRA");
		sel_PackageInformationFilterRA= findElementInXLSheet(getParameterFleet,"PackageInformationFilterRA");
		txt_PackageInformationSearchTxtRA= findElementInXLSheet(getParameterFleet,"PackageInformationSearchTxtRA");
		sel_PackageInformationSearchSelRA= findElementInXLSheet(getParameterFleet,"PackageInformationSearchSelRA");
		txt_PackageInvoicingProductRA= findElementInXLSheet(getParameterFleet,"PackageInvoicingProductRA");
		txt_PackageExtraMileageProductRA= findElementInXLSheet(getParameterFleet,"PackageExtraMileageProductRA");
		txt_PackageExtraTimeProductRA= findElementInXLSheet(getParameterFleet,"PackageExtraTimeProductRA");
		txt_AllottedMileageRA= findElementInXLSheet(getParameterFleet,"AllottedMileageRA");
		txt_ExtraMileageRateRA= findElementInXLSheet(getParameterFleet,"ExtraMileageRateRA");
		txt_AllotedHoursRA= findElementInXLSheet(getParameterFleet,"AllotedHoursRA");
		txt_ExtraHourRateRA= findElementInXLSheet(getParameterFleet,"ExtraHourRateRA");
		btn_CheckOutVehicleBtnRA= findElementInXLSheet(getParameterFleet,"CheckOutVehicleBtnRA");
		txt_CheckoutMilageRA= findElementInXLSheet(getParameterFleet,"CheckoutMilageRA");
		txt_CheckoutDateRA= findElementInXLSheet(getParameterFleet,"CheckoutDateRA");
		txt_CheckoutTimeRA= findElementInXLSheet(getParameterFleet,"CheckoutTimeRA");
		sel_CheckoutTimeSelRA= findElementInXLSheet(getParameterFleet,"CheckoutTimeSelRA");
		btn_CheckoutTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"CheckoutTimeOkBtnRA");
		txt_DocStatusDropdownTxt2RA= findElementInXLSheet(getParameterFleet,"DocStatusDropdownTxt2RA");
		btn_CheckInVehicleBtnRA= findElementInXLSheet(getParameterFleet,"CheckInVehicleBtnRA");
		txt_CheckinMilageRA= findElementInXLSheet(getParameterFleet,"CheckinMilageRA");
		txt_CheckinDateRA= findElementInXLSheet(getParameterFleet,"CheckinDateRA");
		txt_CheckinTimeRA= findElementInXLSheet(getParameterFleet,"CheckinTimeRA");
		sel_CheckinTimeSelRA= findElementInXLSheet(getParameterFleet,"CheckinTimeSelRA");
		btn_CheckinTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"CheckinTimeOkBtnRA");
		btn_CheckInVehicleCalculateBtnA= findElementInXLSheet(getParameterFleet,"CheckInVehicleCalculateBtnA");
		txt_ExcessMileageTxtRA= findElementInXLSheet(getParameterFleet,"ExcessMileageTxtRA");
		txt_ExcessMileageChargeTxtRA= findElementInXLSheet(getParameterFleet,"ExcessMileageChargeTxtRA");
		txt_ExcessHoursTxtRA= findElementInXLSheet(getParameterFleet,"ExcessHoursTxtRA");
		txt_ExcessHoursChargeTxtRA= findElementInXLSheet(getParameterFleet,"ExcessHoursChargeTxtRA");
		txt_DocStatusDropdownTxt3RA= findElementInXLSheet(getParameterFleet,"DocStatusDropdownTxt3RA");
		
		//TC_CAL_YY_H_04
		txt_DocStatusDropdownTxt4RA= findElementInXLSheet(getParameterFleet,"DocStatusDropdownTxt4RA");
		txt_DocStatusDropdownTxt5RA= findElementInXLSheet(getParameterFleet,"DocStatusDropdownTxt5RA");
		btn_DriverReplacementBtnRA= findElementInXLSheet(getParameterFleet,"DriverReplacementBtnRA");
		txt_DriverReplacementCheInMilageRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheInMilageRA");
		txt_DriverReplacementCheInDateRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheInDateRA");
		txt_DriverReplacementCheInTimeRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheInTimeRA");
		sel_DriverReplacementCheInTimeSelRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheInTimeSelRA");
		btn_DriverReplacementCheInTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheInTimeOkBtnRA");
		btn_ReplacementDriverBtnRA= findElementInXLSheet(getParameterFleet,"ReplacementDriverBtnRA");
		txt_ReplacementDriverTxtRA= findElementInXLSheet(getParameterFleet,"ReplacementDriverTxtRA");
		sel_ReplacementDriverSelRA= findElementInXLSheet(getParameterFleet,"ReplacementDriverSelRA");
		txt_DriverReplacementCheOutMilageRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheOutMilageRA");
		txt_DriverReplacementCheOutDateRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheOutDateRA");
		txt_DriverReplacementCheOutTimeRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheOutTimeRA");
		sel_DriverReplacementCheOutTimeSelRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheOutTimeSelRA");
		btn_DriverReplacementCheOutTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"DriverReplacementCheOutTimeOkBtnRA");

		//TC_CAL_YY_H_05
		btn_VehicleReplacementBtnRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementBtnRA");
		txt_VehicleReplacementCheInMilageRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheInMilageRA");
		txt_VehicleReplacementCheInDateRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheInDateRA");
		txt_VehicleReplacementCheInTimeRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheInTimeRA");
		sel_VehicleReplacementCheInTimeSelRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheInTimeSelRA");
		btn_VehicleReplacementCheInTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheInTimeOkBtnRA");
		btn_VehicleReplacementCheckOutBtnRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheckOutBtnRA");
		btn_ReplacementVehicleBtnRA= findElementInXLSheet(getParameterFleet,"ReplacementVehicleBtnRA");
		txt_ReplacementVehicleTxtRA= findElementInXLSheet(getParameterFleet,"ReplacementVehicleTxtRA");
		sel_ReplacementVehicleSelRA= findElementInXLSheet(getParameterFleet,"ReplacementVehicleSelRA");
		txt_ReplacementVehicleCheOutMilageRA= findElementInXLSheet(getParameterFleet,"ReplacementVehicleCheOutMilageRA");
		txt_VehicleReplacementCheOutDateRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheOutDateRA");
		txt_VehicleReplacementCheOutTimeRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheOutTimeRA");
		sel_VehicleReplacementCheOutTimeSelRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheOutTimeSelRA");
		btn_VehicleReplacementCheOutTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"VehicleReplacementCheOutTimeOkBtnRA");
		
		//TC_CAL_YL_H_01
		btn_CustomerCheckInBtnRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInBtnRA");
		txt_CustomerCheckInCurrentMileageRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInCurrentMileageRA");
		txt_CustomerCheckInChkInMileageRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInChkInMileageRA");
		txt_CustomerCheckInCheckInDateRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInCheckInDateRA");
		txt_CustomerCheckInCheckInTimeRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInCheckInTimeRA");
		sel_CustomerCheckInCheckInTimeSelRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInCheckInTimeSelRA");
		btn_CustomerCheckInCheckInTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"CustomerCheckInCheckInTimeOkBtnRA");
		txt_DropOffAdditionalMileageRA= findElementInXLSheet(getParameterFleet,"DropOffAdditionalMileageRA");
		
		//TC_CAL_LY_H_01
		btn_CustomerCheckOutBtnRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutBtnRA");
		txt_CustomerCheckOutCurrentMileageRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutCurrentMileageRA");
		txt_CustomerCheckOutCheckOutMileageRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutCheckOutMileageRA");
		txt_CustomerCheckOutCheckOutDateRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutCheckOutDateRA");
		txt_CustomerCheckOutCheckOutTimeRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutCheckOutTimeRA");
		sel_CustomerCheckOutCheckOutTimeSelRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutCheckOutTimeSelRA");
		btn_CustomerCheckOutCheckOutTimeOkBtnRA= findElementInXLSheet(getParameterFleet,"CustomerCheckOutCheckOutTimeOkBtnRA");
		txt_PickUpAdditionalMileageRA= findElementInXLSheet(getParameterFleet,"PickUpAdditionalMileageRA");

		//Sithara======================================================================================
		
		
	}

	public static void readData() throws Exception {
		
		//TC_MAT_RA_01
		SiteUrl = findElementInXLSheet(getDataFleet,"SiteUrl");
		UserName = findElementInXLSheet(getDataFleet,"UserName");
		Password = findElementInXLSheet(getDataFleet,"Password");
		AccountSearchTxtRA = findElementInXLSheet(getDataFleet,"AccountSearchTxtRA");
		ContactPersonRA = findElementInXLSheet(getDataFleet,"ContactPersonRA");
		VehicleSearchTxtRA = findElementInXLSheet(getDataFleet,"VehicleSearchTxtRA");
		AgreementGroupRA = findElementInXLSheet(getDataFleet,"AgreementGroupRA");
		BilingTypeRA = findElementInXLSheet(getDataFleet,"BilingTypeRA");
		RateTypeRA = findElementInXLSheet(getDataFleet,"RateTypeRA");
		PackageSearchTxt1RA = findElementInXLSheet(getDataFleet,"PackageSearchTxt1RA");
		MileAgeAllocationType1RA = findElementInXLSheet(getDataFleet,"MileAgeAllocationType11RA");
		DriverNameRA = findElementInXLSheet(getDataFleet,"DriverNameRA");
		LicenseNoRA = findElementInXLSheet(getDataFleet,"LicenseNoRA");
		
		//TC_MAT_RA_02
		PackageSearchTxt2RA = findElementInXLSheet(getDataFleet,"PackageSearchTxt2RA");
		DriverSearchTxtRA = findElementInXLSheet(getDataFleet,"DriverSearchTxtRA");
		
		//TC_MAT_RA_03
		RateType2RA = findElementInXLSheet(getDataFleet,"RateType2RA");
		PackageSearchTxt3RA = findElementInXLSheet(getDataFleet,"PackageSearchTxt3RA");
		
		//TC_MAT_RA_04
		PackageSearchTxt4RA = findElementInXLSheet(getDataFleet,"PackageSearchTxt4RA");
		
		//TC_MAT_RA_05
		RateType3RA = findElementInXLSheet(getDataFleet,"RateType3RA");
		PackageSearchTxt5RA = findElementInXLSheet(getDataFleet,"PackageSearchTxt5RA");
		
		//TC_MAT_RA_06
		PackageSearchTxt6RA = findElementInXLSheet(getDataFleet,"PackageSearchTxt6RA");
		
		//TC_MAT_RA_13
		MileAgeAllocationType2RA = findElementInXLSheet(getDataFleet,"MileAgeAllocationType2RA");
		DropOffLocationRA = findElementInXLSheet(getDataFleet,"DropOffLocationRA");
		DropOffAddressRA = findElementInXLSheet(getDataFleet,"DropOffAddressRA");
		
		//TC_MAT_RA_25
		MileAgeAllocationType3RA = findElementInXLSheet(getDataFleet,"MileAgeAllocationType3RA");
		PickUpLocationRA = findElementInXLSheet(getDataFleet,"PickUpLocationRA");
		PickUpAddressRA = findElementInXLSheet(getDataFleet,"PickUpAddressRA");

		//TC_MAT_RA_37
		MileAgeAllocationType4RA = findElementInXLSheet(getDataFleet,"MileAgeAllocationType4RA");
		
		//TC_CAL_YY_H_04
		ReplacementDriverTxtRA = findElementInXLSheet(getDataFleet,"ReplacementDriverTxtRA");

		//TC_CAL_YY_H_05
		ReplacementVehicleTxtRA = findElementInXLSheet(getDataFleet,"ReplacementVehicleTxtRA");

		//Sithara======================================================================================
		
		//TC_MAT_RA_15
		RateTypeRA2 = findElementInXLSheet(getDataFleet,"RateTypeRA2");
		
		//CAL_COMMON
		Dformat1 = findElementInXLSheet(getDataFleet,"Dformat1");
		
		//TC_CAL_YY_D_03
		ReplacementDriverTxtRASI = findElementInXLSheet(getDataFleet,"ReplacementDriverTxtRASI");

		//TC_CAL_YY_H_04
		ReplacementVehicleTxtRASI= findElementInXLSheet(getDataFleet,"ReplacementVehicleTxtRASI");
		
		//TC_CAL_YY_D_05
		ReplacementVehicleTxtRASI2 = findElementInXLSheet(getDataFleet,"ReplacementVehicleTxtRASI2");

	}
}
